#!/usr/bin/env python
import argparse
import glob
import os
import subprocess as sp
import uuid
import numpy as np

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )
    p.add_argument("--file_dirlist", type=str, help="File containing the list of LVCNR files to run on. Alternatively if a path is provided it will run on all the LVCNR files on that path.")
    p.add_argument("--path_dir",
                    type=str,
                    help="In case the file does not contain the path to the files.  \
                         One can specify it here.",default='/path/to/the/files/')
    p.add_argument(
        "--approximant", type=str, help="Approximant to use", default="IMRPhenomPv3HM"
    )

    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--submit_time", type=str, help="Submit to queue.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--ell_max", type=int, help="Maximum ell to use", default=4)
    p.add_argument(
        "--min_type",
        type=str,
        help="Whether to minimize over reference  frequency or spin rotation. Applies only to Phenom waveforms",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument("--N",type=int,help="The number of cases in a chunk",default=8)
    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")

    p.add_argument(
        "--raw_grid",
        type=str,
        help="Make a coarse grid unfaithfulness calculations before numerical optimization.",
        default="False",
    )

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNRRForm_template",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm_template",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)
    try:
        os.chdir(run_dir)
    except OSError:
        print("Could not go into the run directory {}, quitting!".format(run_dir))
        exit(-1)

    script_dir =  os.environ['WAVEFORM_TOOLS_PATH']
    path_dir = args.path_dir

    isFile = os.path.isfile(args.file_dirlist)
    #print(isFile)
    if isFile:
        outfile = args.file_dirlist
        with open(outfile) as f:
            nr_files = f.read().splitlines()
    elif args.signal=="NR_v4":
        nr_files = [f for f in glob.glob( os.environ['SEOBNRv4_NRPATH']+"/*.dat")]
    else:
        nr_files = [f for f in glob.glob(args.file_dirlist+"/*.h5")]

    #print(nr_files)
    if os.path.dirname(nr_files[0]) == '':
        nr_files_full = []
        for nr_file in nr_files:
            if os.path.dirname(nr_file) == '':
                nr_files_full.append(path_dir+nr_file)

        nr_files = nr_files_full

    chk = list(chunks(nr_files, args.N))

    submit_time = args.submit_time
    queue = args.queue

    header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                 # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue
#SBATCH --exclusive

#source /home/aramosbuades/load_LALenv.sh

cd {}
"""
    unq_name = uuid.uuid4().hex

    for i in range(len(chk)):
        nm = unq_name + "_{}".format(i)
        fp = open("chunk_{}.sh".format(i), "w")
        fp.write(header.format(i,i,i, queue, submit_time, run_dir))

        k=1
        for NR_file in chk[i]:

            if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt" or args.approximant == "SEOBNRv4EHM_opt":

                cmd = """python {}/run_one_case_Ecc.py --NR_file {} --signal {} --approximant {} --ell_max {} --min_type {} --unfaithfulness_type {} --EccFphiPNorder_template  {}  --EccFrPNorder_template  {} \
                 --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {}  --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {}  --EccPNWfForm_template {} --EccPNRRForm_template {} --EcctAppend {} --EccAvNQCWaveform {} --raw_grid {}   --iota_s {} \n""".format(
                    script_dir,
                    NR_file,
                    args.signal,
                    args.approximant,
                    args.ell_max,
                    args.min_type,
                    args.unfaithfulness_type,
                    args.EccFphiPNorder_template,
                    args.EccFrPNorder_template,
                    args.EccWaveformPNorder_template,
                    args.EccPNFactorizedForm_template,
                    args.EccBeta_template,
                    args.Ecct0_template,
                    args.EccNQCWaveform_template,
                    args.EccPNWfForm_template,
                    args.EccPNRRForm_template,
                    args.EcctAppend,
                    args.EccAvNQCWaveform,
                    args.raw_grid,
                    args.iota_s
                )
            elif args.approximant == "IMRPhenomXEv1":
                cmd = """python {}/run_one_case_Ecc.py --NR_file {} --signal {} --approximant {} --ell_max {} --min_type {} --unfaithfulness_type {} --Nharmonic_template  {}  --kAdvance_template {} --raw_grid {}  --iota_s {} 1> out{}{} 2> err{}{}  \n""".format(
                        script_dir,
                        NR_file,
                        args.signal,
                        args.approximant,
                        args.ell_max,
                        args.min_type,
                        args.unfaithfulness_type,
                        args.Nharmonic_template,
                        args.kAdvance_template,
                        args.raw_grid,
                        args.iota_s,
                        i,k,i,k
                    )
            else:
                cmd = """python {}/run_one_case_Ecc.py --NR_file {} --signal {} --approximant {} --ell_max {} --min_type {} --unfaithfulness_type {} --raw_grid {}   --iota_s {} 1> out{}{} 2> err{}{}  \n""".format(
                    script_dir,
                    NR_file,
                    args.signal,
                    args.approximant,
                    args.ell_max,
                    args.min_type,
                    args.unfaithfulness_type,
                    args.raw_grid,
                    args.iota_s,
                    i,k,i,k
                )


            fp.write(cmd)
            k+= 1

        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)

        print(submit_cmd)

        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
