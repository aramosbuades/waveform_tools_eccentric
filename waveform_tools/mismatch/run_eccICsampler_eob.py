#!/usr/bin/env python

from scipy.signal import argrelextrema
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
import math

import argparse
import glob, os ,sys,h5py
import numpy as np

from joblib import Parallel, delayed

from scipy.interpolate import InterpolatedUnivariateSpline

import lal, lalsimulation

import matplotlib.pyplot as plt
from scipy.optimize import root_scalar

from eccdefs_PN import *
#from packages_generate_TD_waveform import *


def HztoMf(Hz,M):
    return Hz*(M*LALMTSUNSI)

def EffectiveSpins(q,chi1,chi2):
    """ Given mass ratio and dimless component z-spins,
    return two effective spin parameters chiHat and chi_a.
    chiHat is defined in Eq.(3) of 1508.07253.
    chi_a = (chi1 - chi2)/2.
    Both chiHat and chi_a always lie in range [-1, 1].
    """

    eta = q/(1.+q)**2
    chi_wtAvg = (q*chi1+chi2)/(1+q)
    chiHat = (chi_wtAvg - 38.*eta/113.*(chi1 + chi2))/(1. - 76.*eta/113.)
    chi_a = (chi1 - chi2)/2.

    return chiHat, chi_a



def compute_phase_amp22_diff(timeNRv4EHM, amp22v4EHM, timeNRv4HM , amp22v4HM, align_start=True):


    timeCommon = {}; ampdiff = {};


    ttv4EHM = timeNRv4EHM
    ampv4EHM = amp22v4EHM

    ttv4HM = timeNRv4HM
    ampv4HM = amp22v4HM

    dtv4E = ttv4EHM[-1]- ttv4EHM[-2]
    dtv4 = ttv4HM[-1]- ttv4HM[-2]
    dt = min(dtv4E,dtv4)


    t0v4 = ttv4HM[0]
    t0v4E = ttv4EHM[0]

    tmin = max(t0v4,t0v4E)
    tmax = min(ttv4HM[-1],ttv4EHM[-1])

    timeCommon = np.arange(tmin,tmax,dt)

    iampv4 = InterpolatedUnivariateSpline(ttv4HM,ampv4HM)

    iampv4E = InterpolatedUnivariateSpline(ttv4EHM,ampv4EHM)


    ampdiff = iampv4E(timeCommon)-iampv4(timeCommon)

    return  timeCommon, ampdiff


def compute_deriv_f(time, f):

    intrp = InterpolatedUnivariateSpline(time,f)
    deriv = intrp.derivative()(time)

    return deriv

LALMTSUNSI= 4.925491025543575903411922162094833998e-6
#(* Subscript[M, \[CircleDot]][s] = G/c^3Subscript[M, \[CircleDot]][kg] ~ 4.93 10^-6s is the geometrized solar mass in seconds. [LAL_MTSUN_SI] defined in  lal/src/std/LALConstants.h *)
LALCSI=299792458.0 # (* LAL_C_SI 299792458e0 /**< Speed of light in vacuo, m s^-1 *)
LALPCSI = 3.085677581491367e16 # (*  LAL_PC_SI 3.085677581491367278913937957796471611e16 /**< Parsec, m * *)

def AmpPhysicaltoNRTD(ampphysical, M, dMpc):
    return ampphysical*dMpc*1000000*LALPCSI/(LALCSI*(M*LALMTSUNSI))


def compute_freqInterp(time, hlm):

    philm=np.unwrap(np.angle(hlm))

    intrp = InterpolatedUnivariateSpline(time,philm)
    omegalm = intrp.derivative()(time)

    return omegalm


def SectotimeM(seconds, M):
    return seconds/(M*LALMTSUNSI)

# Function to compute amplitude, phase and frequency difference

def compute_phase_amp_om_diff(timeNRv4EHM, amplmv4EHM, phaselmv4EHM, omegalmv4EHM, timeNRv4HM , amplmv4HM,
                               phaselmv4HM ,omegalmv4HM , mode_list, align_start=True):


    timeCommon = {}; phidiff = {}; omdiff = {}; ampdiff = {};
    for l,m in mode_list:
        #print(l,m)
        ttv4EHM = timeNRv4EHM
        ampv4EHM = amplmv4EHM[l,m]
        phv4EHM = phaselmv4EHM[l,m]
        omv4EHM = omegalmv4EHM[l,m]

        ttv4HM = timeNRv4HM
        ampv4HM = amplmv4HM[l,m]
        phv4HM = phaselmv4HM[l,m]
        omv4HM = omegalmv4HM[l,m]

        dtv4E = ttv4EHM[-1]- ttv4EHM[-2]
        dtv4 = ttv4HM[-1]- ttv4HM[-2]
        dt = min(dtv4E,dtv4)


        t0v4 = ttv4HM[0]
        t0v4E = ttv4EHM[0]

        tmin = max(t0v4,t0v4E)
        tmax = min(ttv4HM[-1],ttv4EHM[-1])

        timeCommon = np.arange(tmin,tmax,dt)

        iphv4 = InterpolatedUnivariateSpline(ttv4HM,phv4HM)
        iomv4 = InterpolatedUnivariateSpline(ttv4HM,omv4HM)
        iampv4 = InterpolatedUnivariateSpline(ttv4HM,ampv4HM)

        iphv4E = InterpolatedUnivariateSpline(ttv4EHM,phv4EHM)
        iomv4E = InterpolatedUnivariateSpline(ttv4EHM,omv4EHM)
        iampv4E = InterpolatedUnivariateSpline(ttv4EHM,ampv4EHM)


        if align_start == True:
            delta_phi0 = - iphv4(tmin) + iphv4E(tmin)

        else:
            tAlign = 0
            delta_phi0 = -iphv4(tAlign) + iphv4E(tAlign)


        phidiff[l,m] = iphv4E(timeCommon)-(iphv4(timeCommon)+delta_phi0)
        ampdiff[l,m] = iampv4E(timeCommon)-iampv4(timeCommon)
        omdiff[l,m] = iomv4E(timeCommon)-iomv4(timeCommon)

    return  timeCommon, phidiff, omdiff, ampdiff


def SEOBNRv4EHM_modes(q: float, chi1: float,chi2: float, eccentricity: float, eccentric_anomaly: float,
                      f_min:float, M_fed:float, delta_t: float, EccIC: int, approx: str):


    HypPphi0, HypR0, HypE0 =[0.,0,0]


    EccFphiPNorder = 99
    EccFrPNorder = 99
    EccWaveformPNorder = 16
    EccBeta = 0.09
    Ecct0 = 100

    EccPNFactorizedForm = EccNQCWaveform =EccPNRRForm = EccPNWfForm =EccAvNQCWaveform=1
    EcctAppend=40
    #EccIC=0
    #eccentric_anomaly = 0.0

    m1= q/(1+q)*M_fed
    m2= 1/(1+q)*M_fed
    dMpc=500
    dist = dMpc*(1e6*lal.PC_SI)

    if approx=="SEOBNRv4E_opt" or  approx=="SEOBNRv4":
        SpinAlignedVersion=4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    else:
        SpinAlignedVersion=41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary

    if approx == "SEOBNRv4E_opt" or approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:


        #print("SEOBNRv4HM modes")
        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)





    hlm={}


    if SpinAlignedVersion==4:
        hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm[2,-2] = np.conjugate(hlm[2,2])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        #h55LAL = - h55 * np.exp^(-1.j * modeM * phi_ref)
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])
        #hlm[modeL,-modeM] =  ((-1)**modeL) * hlm[modeL,modeM]

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])
        #hlm[modeL,-modeM] =  ((-1)**modeL) * hlm[modeL,modeM]

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])
        #hlm[modeL,-modeM] =  ((-1)**modeL) * hlm[modeL,modeM]

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])
        #hlm[modeL,-modeM] =  ((-1)**modeL) * hlm[modeL,modeM]

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[modeL,modeM] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])
        #hlm[modeL,-modeM] =  ((-1)**modeL) * hlm[modeL,modeM]


    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)
    timeNR = SectotimeM(time_array,M_fed)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]

    #tdyn -=tdyn[-1]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]


    omegaOrbital = compute_deriv_f(tdyn, phidyn)

    #return time_array, hlm
    omegalm={}; amplm={}; phaselm = {};
    mode_list=[[2,2],[2,1],[3,3],[4,4],[5,5], [2,-2],[2,-1],[3,-3],[4,-4],[5,-5]]
    for l,m in mode_list:


        #hlm[l,m]= np.conjugate(hlm[l,m]) # Change convention to same as NR
        hlm[l,m]= -(-1)**m * hlm[l,m] # Change convention to same as NR

        phaselm[l,m] = -np.unwrap(np.angle(hlm[l,m]))
        omegalm[l,m]=-compute_freqInterp(timeNR,hlm[l,m])
        amplm[l,m]=np.abs(hlm[l,m])

    include_modes=[[2,2],[2,1],[3,3],[4,4],[5,5]]
    #for l,m in include_modes:
    #    hlm[l,m]= hlm[l,-m]


    # There is no need for alignment
    imax = np.argmax(amplm[2,2])
    #timeNR -= timeNR[imax]

    xx = rdyn*np.cos(phidyn)
    ixx = InterpolatedUnivariateSpline(tdyn,xx)
    dxdt = ixx.derivative()(tdyn)

    yy = rdyn*np.sin(phidyn)
    iyy = InterpolatedUnivariateSpline(tdyn,yy)
    dydt = iyy.derivative()(tdyn)
    zz = rdyn*0

    return timeNR, hlm, amplm, omegalm, phaselm, tdyn, xx, yy, zz, rdyn, phidyn, omegaOrbital, prdyn, pphidyn



def get_time_to_merger_nonLAL_22mode(t: np.array, amp22: np.array) -> float:

    # Compute tPeak from a SEOBNRv4 (e=0) waveform
    idx_max = np.argmax(amp22)
    t_max = t[idx_max]
    t_min = t[0]
    return t_max - t_min


def get_EOBduration(q:float,chi1:float, chi2:float, ecc:float, ecc_anomaly:float,
                    f_min:float, Mtot:float,delta_t:float, EccIC:int, approx:str) -> float:



    timeNRv4EHM, hlmv4EHM, amplmv4EHM,omegalmv4EHM,phaselmv4EHM, tdynv4EHM,  xx_ecc, yy_ecc, zz_ecc, rdynv4EHM, phidynv4EHM,omegaOrbitalv4EHM, \
    prdynv4EHM, pphidynv4EHM = SEOBNRv4EHM_modes(q,chi1, chi2, ecc, ecc_anomaly,
                                                 f_min, Mtot,delta_t, EccIC, approx)



    EOB_duration = get_time_to_merger_nonLAL_22mode(timeNRv4EHM, amplmv4EHM[2,2])

    return EOB_duration


def tdiff_v4E(
    f_min: float, EOB_ecc_duration: float, q:float, chi1:float, chi2:float,
    Mtot: float, delta_t:float,  EccIC:int, approx:str
) -> float:


    EOB_qc_duration = get_EOBduration(q,chi1, chi2, 0, 0, f_min, Mtot,delta_t, EccIC, approx)

    return EOB_ecc_duration - EOB_qc_duration


# Measure eccentricity from the final configuration
def measure_ecc_meanAno(timeNRv4EHM, amplmv4EHM, timeNRv4HM, amplmv4HM, hlmv4EHM, phdyn_v4EHM, tdyn_v4EHM,  phdyn_v4HM, tdyn_v4HM, tref):


    timeCommon, ampdiff  = compute_phase_amp22_diff(timeNRv4EHM, amplmv4EHM,timeNRv4HM,amplmv4HM)


    orbphase = -np.unwrap(np.angle(hlmv4EHM))/2
    dt = timeNRv4EHM[1]-timeNRv4EHM[0]
    if not np.allclose(np.diff(timeNRv4EHM), dt):
        raise Exception('Non uniform time array.')

    iorbphase = InterpolatedUnivariateSpline(timeNRv4EHM,orbphase)
    omega_orb = iorbphase.derivative()(timeNRv4EHM)


    if omega_orb[0]<0:
        omega_orb = -omega_orb

    ew22_ref, ew22_0 = find_eccentricity_anomaly(timeCommon, ampdiff, timeNRv4EHM, omega_orb, orbphase, tref=tref,
                                             eccentricity_evolution=False, anomaly_evolution=False)

    tdyn_v4EHM -=tdyn_v4EHM[-1]
    iphdyn_v4EHM = InterpolatedUnivariateSpline(tdyn_v4EHM, phdyn_v4EHM)
    omdyn_v4EHM = iphdyn_v4EHM.derivative()(tdyn_v4EHM)

    tdyn_v4HM -=tdyn_v4HM[-1]
    iphdyn_v4HM = InterpolatedUnivariateSpline(tdyn_v4HM, phdyn_v4HM)
    omdyn_v4HM = iphdyn_v4HM.derivative()(tdyn_v4HM)

    timeCommon, omdiff  = compute_phase_amp22_diff(tdyn_v4EHM, omdyn_v4EHM, tdyn_v4HM, omdyn_v4HM)

    ewOrb_ref, ewOrb_0 = find_eccentricity_anomaly(timeCommon, omdiff, tdyn_v4EHM, omdyn_v4EHM, phdyn_v4EHM, tref=tref,
                                             eccentricity_evolution=False, anomaly_evolution=False)

    return ew22_ref, ewOrb_ref


def find_eccentricity_anomaly(time, Amp22, t, omega_orb, orbphase, tref, eccentricity_evolution=False,  anomaly_evolution=False):

    """
    hdata: 22 mode waveform data
    tref: reference time at which parameter values would be computed
    eccentricity_evolution and anomaly_evolution: if True would save a plot of eccentricity and anomaly as a function of time
    """

    #get amplitudes, phase and orbital frequency
    # we want to start measuring the eccentricity and anomaly at tref. It's wise to get the chunk of data that starts at-least 1050M before tref
    timestart=tref-4050
    idx=np.where(time>=timestart)
    time=time[idx]
    amp=Amp22[idx]
    freq=omega_orb[idx]

    # for local maxima
    maximas=argrelextrema(amp, np.greater, order=10)
    # for local minima
    minimas=argrelextrema(amp, np.less, order=10)


    iomega_orb = InterpolatedUnivariateSpline(t,omega_orb)
    omega_orb_maxs = iomega_orb(time[maximas])
    omega_orb_mins = iomega_orb(time[minimas])

    #===============================================================================
    #Eccentricity
    #===============================================================================
    #Once the position of extremas known, we find the corresponding orbital frequencies and interpolate
    #interpolate frequency maximas
    #remove the last point which is the peak of the full waveform
    #max_curve_spline = InterpolatedUnivariateSpline(time[maximas][:-1], freq[maximas][:-1])
    max_curve_spline = InterpolatedUnivariateSpline(time[maximas][:-1], omega_orb_maxs[:-1])

    #interpolate frequency minimas
    #min_curve_spline = InterpolatedUnivariateSpline(time[minimas], freq[minimas])
    min_curve_spline = InterpolatedUnivariateSpline(time[minimas], omega_orb_mins)

    # Note: We find the position of maximas and minimas of the amplitude instead of the freuqency as it is easier to obtain;
    # These positions are identical for the maximas and minimas in orbital frequencies

    #get the omega_max and omega_min at tref
    omegamax=max_curve_spline(tref)
    omegamin=min_curve_spline(tref)
    #estimate eccentricity at tref
    ecc_ref = (np.sqrt(omegamax)-np.sqrt(omegamin))/(np.sqrt(omegamax)+np.sqrt(omegamin))

    omegamax=max_curve_spline(t[0])
    omegamin=min_curve_spline(t[0])
    ecc_0 = (np.sqrt(omegamax)-np.sqrt(omegamin))/(np.sqrt(omegamax)+np.sqrt(omegamin))

    print('\nEstimated eccentricity at tref=%.2f is : %.5f'%(tref,ecc_ref))

    if eccentricity_evolution:

        #get the time window including tref which is safe for interpolation
        tmin=max(min(time[maximas][:-1]),min(time[minimas]))
        tmax=min(max(time[maximas][:-1]),max(time[minimas]))
        print('Eccentricity evolution window: [%.2f,%.2f]'%(tmin,tmax))
        twindow=np.arange(tmin,tmax,0.1)

        #get the omega_max and omega_min as a funtion of time
        omegamax=max_curve_spline(twindow)
        omegamin=min_curve_spline(twindow)
        #estimate eccentricity
        ecc_dyn = (np.sqrt(omegamax)-np.sqrt(omegamin))/(np.sqrt(omegamax)+np.sqrt(omegamin))

        #plot eccentricity in the time window
        plt.figure()
        plt.plot(twindow, ecc_dyn, 'b')
        plt.xlabel('time', fontsize=14)
        plt.ylabel('Eccentricity', fontsize=14)
        plt.axvline(x=tref, ls='--', color='r')
        plt.plot(tref, ecc_ref, 'k')
        plt.ylim(0,0.4)
        plt.tight_layout()
        plt.show()
        #plt.savefig('%s/Case_%.4d_ecc.png'%(plot_dir, case), bbox_inches='tight')
        plt.close()

        print('Eccentricity evolution in the specified time-window is saved. Check "eccentricity_dynamic.png".\n')


    return ecc_ref, ecc_0



def measure_eccentricities(q:float,chi1:float,chi2:float, ecc:float,
                            ecc_anomaly:float, f_min:float,Mtot:float, delta_t:float):


    # First generate the waveforms
    EccIC = -1 # Use eccentric anomaly to control the length of the waveform

    approx="SEOBNRv4EHM_opt"

    eta = q/(1.+q)**2

    eob_duration = get_EOBduration(q,chi1, chi2, ecc, ecc_anomaly, f_min, Mtot,delta_t, EccIC, approx)

    f_min_ecc_EOB = f_min
    EOB_ecc_duration = eob_duration

    # Set duration of the eccentric waveform to a reasonable length
    target_duration =15000
    min_target_duration=10000

    omega_min = 10.005**(-3./2.)
    fmin_Maxv4E = omega_min/(np.pi*Mtot*lal.MTSUN_SI)-1e-9
    EOB_ecc_min_duration = get_EOBduration(q,chi1, chi2, ecc, ecc_anomaly, fmin_Maxv4E, Mtot,delta_t, EccIC, approx)


    if EOB_ecc_min_duration >target_duration:
        f_min_ecc_EOB = fmin_Maxv4E
        EOB_ecc_duration = EOB_ecc_min_duration

    else:

        while EOB_ecc_duration >target_duration:

            f_min_ecc_EOB +=1.
            if f_min_ecc_EOB >= fmin_Maxv4E:
                f_min_ecc_EOB = fmin_Maxv4E
                EOB_ecc_duration = get_EOBduration(q,chi1, chi2, ecc, ecc_anomaly, f_min_ecc_EOB, Mtot,delta_t, EccIC, approx)
                break

            else:
                EOB_ecc_duration = get_EOBduration(q,chi1, chi2, ecc, ecc_anomaly, f_min_ecc_EOB, Mtot,delta_t, EccIC, approx)



    while EOB_ecc_duration<min_target_duration:
        f_min_ecc_EOB -=2.
        EOB_ecc_duration = get_EOBduration(q,chi1, chi2, ecc, ecc_anomaly, f_min_ecc_EOB, Mtot,delta_t, EccIC, approx)


    f_min = f_min_ecc_EOB

    timeNRv4EHM, hlmv4EHM, amplmv4EHM,omegalmv4EHM,phaselmv4EHM, tdynv4EHM, xx_ecc, yy_ecc, zz_ecc,\
    rdynv4EHM, phidynv4EHM, omegaOrbitalv4EHM, prdynv4EHM,\
    pphidynv4EHM = SEOBNRv4EHM_modes(q,chi1, chi2, ecc, ecc_anomaly, f_min, Mtot,
                                                            delta_t, EccIC, approx)



    EOB_qc_min_duration = get_EOBduration(q,chi1, chi2, 0, ecc_anomaly, fmin_Maxv4E, Mtot,delta_t, EccIC, approx)
    f_min_qc_EOB = f_min_ecc_EOB
    #print(f"EOB_ecc_min_duration = {EOB_ecc_min_duration}, EOB_qc_min_duration ={EOB_qc_min_duration} ")
    #print(f"EOB_ecc_duration = {EOB_ecc_duration}, EOB_qc_duration ={EOB_qc_duration} ")

    if EOB_qc_min_duration > EOB_ecc_duration:

        print("Not able to get QC case with same length as eccentric waveform")
        print(f"q = {q}, chi1 = {chi1}, chi2 = {chi2}, ecc = {ecc}, ecc_anomaly ={ecc_anomaly}, fmin = {f_min}, Mtot = {Mtot}")
        x0 = HztoMf(f_min, Mtot)
        ecc_Om22_1PN = eOm22_1PN(eta, ecc, x0, 1)
        ecc_OmOrb_1PN = eOmOrb_1PN(eta, ecc,x0, 1)
        ecc_OmOrb_3PN = eOmOrb_3PN(eta, ecc,x0, 1)

        # alternative calculation
        ecc_Om22_0PN_2 = ew223pn(eta,x0,ecc,0)
        ecc_Om22_1PN_2 = ew223pn(eta,x0,ecc,1)
        ecc_Om22_2PN_2 = ew223pn(eta,x0,ecc,2)
        ecc_Om22_3PN_2 = ew223pn(eta,x0,ecc,3)

        ew22_ref, ewOrb_ref = -1,-1
        return q, chi1, chi2, ecc, ecc_anomaly, ew22_ref, ewOrb_ref, ecc_Om22_1PN, ecc_Om22_0PN_2, ecc_Om22_1PN_2, ecc_Om22_2PN_2, ecc_Om22_3PN_2, ecc_OmOrb_1PN, ecc_OmOrb_3PN
    else:

        f_min_qc_EOB = f_min_ecc_EOB
        EOB_qc_duration = get_EOBduration(q,chi1, chi2, 0, ecc_anomaly, f_min_qc_EOB, Mtot,delta_t, EccIC, approx)

        while EOB_qc_duration<EOB_ecc_duration:

            f_min_qc_EOB -=1
            if f_min_qc_EOB<0:
                fmin1 = 1.
                f_min_qc_EOB =fmin1
            if f_min_qc_EOB ==1.:
                f_min_qc_EOB -=0.1

            EOB_qc_duration = get_EOBduration(q,chi1, chi2, 0, ecc_anomaly, f_min_qc_EOB, Mtot,delta_t, EccIC, approx)


    fmin0 =f_min_qc_EOB
    #print(f"fmin0 = {fmin0}")
    res = root_scalar(tdiff_v4E, bracket=(fmin0, fmin_Maxv4E-1e-9),args=(EOB_ecc_duration, q,chi1, chi2, Mtot,
                                                                              delta_t,
                                                                           EccIC, approx),)

    f_min_qc_EOB = res.root

    #print(f"f_min_qc_EOB = {f_min_qc_EOB}")

    timeNRv4HM, hlmv4HM, amplmv4HM,omegalmv4HM, phaselmv4HM, tdynv4HM, xx_qc, yy_qc, zz_qc,  rdynv4HM, phidynv4HM,\
    omegaOrbitalv4HM, prdynv4HM, pphidynv4HM = SEOBNRv4EHM_modes(q,chi1, chi2, 0, 0, f_min_qc_EOB, Mtot,
                                                     delta_t, EccIC, approx)


    # Continue with the post-processing of the waveforms
    imax_ecc = np.argmax(amplmv4EHM[2,2])
    imax_qc = np.argmax(amplmv4HM[2,2])

    tPeak_ecc = timeNRv4EHM[imax_ecc]
    tPeak_qc = timeNRv4HM[imax_qc]

    timeNRv4EHM -=tPeak_ecc
    timeNRv4HM -=tPeak_qc


    tref = timeNRv4EHM[0]

    ew22_ref, ewOrb_ref = measure_ecc_meanAno(timeNRv4EHM, amplmv4EHM[2,2], timeNRv4HM,
                                                amplmv4HM[2,2], hlmv4EHM[2,2], phidynv4EHM, tdynv4EHM, phidynv4HM, tdynv4HM, tref)

    # Print bad case
    if math.isnan(ew22_ref):
        ew22_ref =-1
        print(f"ew22 is nan: q = {q}, chi1 = {chi1}, chi2 = {chi2}, ecc = {ecc}, ecc_anomaly ={ecc_anomaly}, fmin = {f_min}, Mtot = {Mtot}")

    if math.isnan(ewOrb_ref):
        ewOrb_ref =-1
        print(f"ewOrb is nan: q = {q}, chi1 = {chi1}, chi2 = {chi2}, ecc = {ecc}, ecc_anomaly ={ecc_anomaly}, fmin = {f_min}, Mtot = {Mtot}")



    x0 = HztoMf(f_min, Mtot)
    ecc_Om22_1PN = eOm22_1PN(eta, ecc, x0, 1)
    ecc_OmOrb_1PN = eOmOrb_1PN(eta, ecc,x0, 1)
    ecc_OmOrb_3PN = eOmOrb_3PN(eta, ecc,x0, 1)

    # alternative calculation
    ecc_Om22_0PN_2 = ew223pn(eta,x0,ecc,0)
    ecc_Om22_1PN_2 = ew223pn(eta,x0,ecc,1)
    ecc_Om22_2PN_2 = ew223pn(eta,x0,ecc,2)
    ecc_Om22_3PN_2 = ew223pn(eta,x0,ecc,3)

    #print(f"x_av_tref = {xav_tref}, e_tref = {ecc_ref}")
    #print(f"1PN ew22 = {ecc_Om22_1PN}")
    #print(f"relative_error  = {ecc_ref/ecc_Om22_1PN-1},  err22 = {ecc_ref/ew223pn(eta,x0,ecc,3)-1}")

    #print(q0, chi1, chi2, ecc)
    #print( q, chi1, chi2, ecc, ecc_anomaly, ew22_ref, ewOrb_ref, ecc_Om22_1PN, ecc_Om22_0PN_2, ecc_Om22_1PN_2, ecc_Om22_2PN_2, ecc_Om22_3PN_2, ecc_OmOrb_1PN, ecc_OmOrb_3PN)
    return q, chi1, chi2, ecc, ecc_anomaly, ew22_ref, ewOrb_ref, ecc_Om22_1PN, ecc_Om22_0PN_2, ecc_Om22_1PN_2, ecc_Om22_2PN_2, ecc_Om22_3PN_2, ecc_OmOrb_1PN, ecc_OmOrb_3PN


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument(
        "--outdir",
        type=str,
        help="Directory where to store the output",
        default=os.getcwd(),
    )

    p.add_argument("--Npoints",type=int,help="Number of points in random exploration",default=10)

    p.add_argument("--eccmin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccmax", type=float,  help="Maximum eccentricity", default=0.3, )

    p.add_argument("--qmin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qmax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chimin", type=float,  help="Minimum chi (chi1 != chi2)", default=-0.99, )
    p.add_argument("--chimax", type=float,  help="Maximum chi (chi1 != chi2)", default=0.99, )
    p.add_argument("--seed", type=int,  help="Random seed to use for the random distributions", default=111, )


    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)
    p.add_argument("--Nout",type=int,help="Number of iterations to produce output",default=500)
    p.add_argument("--debug",action="store_true",help="Debug mode")

    p.add_argument("--idx_chunk",type=int,help="Subset of cases to run",default=1)
    args = p.parse_args()


    NN=args.Npoints #Number of points over which t loop
    qmin=args.qmin
    qmax=args.qmax
    chimin=args.chimin
    chimax=args.chimax
    emin=args.eccmin
    emax=args.eccmax
    Nchunks = args.Nchunks
    Nout = args.Nout
    seed=args.seed
    debug=args.debug

    outdir=args.outdir
    if outdir[-1]!="/":
        outdir +='/ic_data/'

    os.makedirs(outdir,exist_ok=True)

    np.random.seed(seed)
    q_list = np.random.uniform(qmin,qmax,NN)
    np.random.seed(seed+999998)
    chi1_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+999997)
    chi2_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+99999)
    ecc_list = np.random.uniform(emin,emax,NN)
    np.random.seed(seed+999)
    mean_ano_min = np.pi/2-0.1
    mean_ano_max = np.pi/2+0.1
    mean_ano_list = np.random.uniform(mean_ano_min, mean_ano_max,NN)



    x, y1, y2, z, ww  = q_list, chi1_list, chi2_list, ecc_list, mean_ano_list

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()
    ww = ww.flatten()

    idx_list = np.arange(0,NN)

    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    idx_chunk=args.idx_chunk

    case0=cases_per_slurm_job[idx_chunk]
    cases_per_output = list(chunks(case0,Nout))



    # For the test we set
    M_fed=100.0
    f_min=20.0
    ecc_anomaly =6. # Set it such that you have a maximum very close to the start
    freq_1M= M_fed*f_min
    omega_min= freq_1M*(np.pi*lal.MTSUN_SI)
    #print(omega_min)
    delta_t=1./(2*2048.)

    k=0
    for case in cases_per_output:
        if args.debug:
            result_list=[]
            for i in case:
                print(i)
                print(x[i], y1[i], y2[i],  z[i], ww[i] )

                print('============================================================')
                result=measure_eccentricities(
                    x[i],
                    y1[i],
                    y2[i],
                    z[i],
                    ecc_anomaly, #mean_ano,
                    f_min,
                    M_fed,
                    delta_t)
                #print(f"i = {i},  result = {result}")
                result_list.append(result)
        else:
            result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
            delayed(measure_eccentricities)(
                x[i],
                y1[i],
                y2[i],
                z[i],
                ecc_anomaly, #mean_ano,
                f_min,
                M_fed,
                delta_t)
            for i in case
            )


        result = np.array(result)

        np.savetxt(outdir+"eccICtest_seed{}_Npoints{}_k{}_idx{}_result.dat".format(seed,NN,k,idx_chunk), result)
        k+=1
