import argparse
from functools import lru_cache
import os, glob
from typing import Dict, Union

import h5py
from joblib import Parallel, delayed
import numpy as np
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.types as pt
from pycbc.types import TimeSeries
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from scipy import optimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import brute, dual_annealing, minimize, minimize_scalar
from copy import deepcopy
import pandas as pd

import lal
import lalsimulation as lalsim
import loguru

from auxillary_funcs import *
from waveform_analysis import *
from waveform_parameters import waveform_params
from eccentric_waveforms import *

from scipy.signal import argrelextrema

EOB_approximants = ["SEOBNRv3", "SEOBNRv4P", "SEOBNRv4PHM", "SEOBNRv4PHMSur"]
eEOB_approximants = ["TEOBResumSE", "TEOBResumSEHM", "SEOBNREv4", "SEOBNRv4E", "SEOBNRv4E_opt", "SEOBNRv4EHM_opt"]


import sxs


def get_NR_data(NR_file: str, ell_max: int = 4, format="SpEC"):
    # This is a custom NR approximant, needed for NR-NR match
    # and symmetrized NR
    wf_modes = {}
    fp = h5py.File(NR_file, "r")
    if format == "AEI":
        tr = fp["tr"].value
        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell + 1):
                key = "Yl_{}_{}".format(ell, m)
                d = fp[key].value
                t = d[:, 0]
                idx = np.where(t >= tr)

                wf_modes["{},{}".format(ell, m)] = d[:, 1][idx] + 1j * d[:, 2][idx]
        chi1 = fp["chiA_tr"].value
        chi2 = fp["chiB_tr"].value
        m1_dim = fp["mA_tr"].value
        m2_dim = fp["mB_tr"].value
        omega_start = fp["omega_mag_tr"].value
    elif format == "SpEC":
        # Get metadata
        dirname = os.path.dirname(NR_file)
        metaname = f"{dirname}/metadata.txt"
        metadata = sxs.Metadata.from_txt_file(metaname)
        m1_dim = metadata["reference_mass1"]
        m2_dim = metadata["reference_mass2"]
        chi1 = metadata["reference_dimensionless_spin1"]
        chi2 = metadata["reference_dimensionless_spin2"]
        omega_start = np.linalg.norm(metadata["reference_orbital_frequency"])
        tr = metadata["reference_time"]
        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell + 1):
                key = "Extrapolated_N2.dir/Y_l{}_m{}.dat".format(ell, m)
                d = fp[key].value
                t = d[:, 0]
                idx = np.where(t >= tr)

                wf_modes["{},{}".format(ell, m)] = d[:, 1][idx] + 1j * d[:, 2][idx]
    fp.close()
    freq_1M = omega_start / (m1_dim + m2_dim) / (lal.MTSUN_SI) / np.pi
    params = dict(m1_dim=m1_dim, m2_dim=m2_dim, chi1=chi1, chi2=chi2, freq_1M=freq_1M)
    return wf_modes, t[idx] - tr, params


@lru_cache(maxsize=128)
def generate_psd(length: int, delta_f: float, flow: float) -> pt.FrequencySeries:
    """A memoized version of aLIGOZeroDetHighPowerGWINC

    Args:
        length (int): length of the PSD to generate
        delta_f (float): frequency spacing
        flow (float): low frequency cutoff

    Returns:
        pt.FrequencySeries: the PSD
    """
    return aLIGOZeroDetHighPowerGWINC(length, delta_f, flow)


def generate_waveform(
    p: waveform_params, f_max: float = 2048.0
) -> Union[pt.TimeSeries, pt.FrequencySeries]:
    """Generate a lalsuite waveform. Explicitly uses
    which domain is selected, either TD or FD.

    Args:
        p (waveform_params): The parameters of the waveform
        f_max (float, optional): The upper frequency cutoff for the waveform. Defaults to 2048.0.

    Raises:
        NotImplementedError: If you try a domain other than "TD" or "FD"

    Returns:
        Union[pt.TimeSeries, pt.FrequencySeries]: The desired waveform
    """
    #print("domain = ",p.domain)
    if p.domain == "TD":
        # if p.approx == "TEOBResumSe" or p.approx == "SEOBNREv4":

        if p.approx == "TEOBResumSE":
            modesTEOB = [[2, 2]]#,[2, -2]]
            t, hp, hc, hlm = generate_TEOBResumSE_simple_waveform(p, f_max, modesTEOB)

        elif p.approx == "TEOBResumSEHM":
            modesTEOB = [
                [2, 2],
                [2, 1],
                [3, 3],
                [4, 4],
                [5, 5],
            ]  # More reliable modes
            t, hp, hc, hlm = generate_TEOBResumSE_simple_waveform(p, f_max, modesTEOB)

        elif p.approx == "SEOBNREv4":
            t, hp, hc, amp22 = generate_SEOBNREv4_waveform(p, f_max)

        # Read output file of the SEOBNREv3 output file
        else:

            try:
                hp, hc = lalsim.SimInspiralChooseTDWaveform(
                    p.m1 * lal.MSUN_SI,
                    p.m2 * lal.MSUN_SI,
                    p.s1x,
                    p.s1y,
                    p.s1z,
                    p.s2x,
                    p.s2y,
                    p.s2z,
                    p.distance,
                    p.iota,
                    p.phi,
                    0.0,
                    p.ecc,
                    p.mean_anomaly,
                    p.delta_t,
                    p.f_min,
                    p.f_ref,
                    p.wf_param,
                    lalsim.GetApproximantFromString(p.approx),
                )
            except:
                m1 = p.m1 * (1 + 1e-8)
                m2 = p.m2 * (1 + 1e-8)
                hp, hc = lalsim.SimInspiralChooseTDWaveform(
                    m1 * lal.MSUN_SI,
                    m2 * lal.MSUN_SI,
                    p.s1x,
                    p.s1y,
                    p.s1z,
                    p.s2x,
                    p.s2y,
                    p.s2z,
                    p.distance,
                    p.iota,
                    p.phi,
                    0.0,
                    p.ecc,
                    p.mean_anomaly,
                    p.delta_t,
                    p.f_min,
                    p.f_ref,
                    p.wf_param,
                    lalsim.GetApproximantFromString(p.approx),
                )

        if (
            p.approx == "SEOBNREv4"
            or p.approx == "TEOBResumSE"
            or p.approx == "TEOBResumSEHM"
        ):
            # Taper
            hp_td = TimeSeries(hp, delta_t=p.delta_t)
            hc_td = TimeSeries(hc, delta_t=p.delta_t)
            hp_td = taper_timeseries(hp_td, tapermethod="startend")
            hc_td = taper_timeseries(hc_td, tapermethod="startend")
        else:
            hp_td = TimeSeries(hp.data.data, delta_t=p.delta_t)
            hc_td = TimeSeries(hc.data.data, delta_t=p.delta_t)
            hp_td = taper_timeseries(hp_td, tapermethod="startend")
            hc_td = taper_timeseries(hc_td, tapermethod="startend")

        return hp_td, hc_td

    elif p.domain == "FD":

        hp, hc = lalsim.SimInspiralChooseFDWaveform(
            p.m1 * lal.MSUN_SI,
            p.m2 * lal.MSUN_SI,
            p.s1x,
            p.s1y,
            p.s1z,
            p.s2x,
            p.s2y,
            p.s2z,
            p.distance,
            p.iota,
            p.phi,
            0.0,
            p.ecc,
            p.mean_anomaly,
            p.delta_f,
            p.f_min,
            f_max,
            p.f_ref,
            p.wf_param,
            lalsim.GetApproximantFromString(p.approx),
        )

        hp_tilde = pt.FrequencySeries(hp.data.data, p.delta_f)
        hc_tilde = pt.FrequencySeries(hc.data.data, p.delta_f)
        return hp_tilde, hc_tilde

    else:
        # Should never get here
        raise NotImplementedError


def generate_EccNRwaveform(
    p: waveform_params, f_max: float = 2048.0, delta_f : float = 0.015
) ->  pt.FrequencySeries:
    """Generate a NR waveform in FD using SimInspiralFD. The output is a FrequencySeries.

    Args:
        p (waveform_params): The parameters of the waveform
        f_max (float, optional): The upper frequency cutoff for the waveform. Defaults to 2048.0.

    Raises:
        NotImplementedError: If you try a domain other than "TD" or "FD"

    Returns:
        Union[pt.TimeSeries, pt.FrequencySeries]: The desired waveform
    """
    #print("domain = ",p.domain)
    # Sanity check
    assert p.domain == "TD", "The domain of the NR signal should be TD!"


    inspiralFDparams = {
            'm1': p.m1*lal.MSUN_SI, 'm2': p.m2*lal.MSUN_SI,
            'S1x': p.s1x, 'S1y': p.s1y, 'S1z':  p.s1z,
            'S2x': p.s2x, 'S2y': p.s2y,  'S2z': p.s2z,
            'distance': p.distance, 'inclination': p.iota,
            'phiRef': p.phi, 'longAscNodes': 0.0,
            'meanPerAno': p.mean_anomaly, 'eccentricity': p.ecc,
            'deltaF': delta_f, 'f_min': p.f_min, 'f_max': f_max,
            'f_ref': p.f_ref, 'LALparams': p.wf_param, 'approximant': lalsim.NR_hdf5
        };  # This functions returns a conditioned frequency domain version of a time domain waveform

    hp_NR, hc_NR = lalsim.SimInspiralFD(**inspiralFDparams)  # Frequency array

    hp_NR = pt.FrequencySeries(hp_NR.data.data, hp_NR.deltaF)
    hc_NR = pt.FrequencySeries(hc_NR.data.data, hc_NR.deltaF)

    return hp_NR, hc_NR



def minimize_Eccunfaithfulness(
    min_pars: np.array,
    s: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template: waveform_params,
    kappa_s: float,
    flow: float,
    fhigh: float = None,
    quantity: str = "unfaithfulness_RC",
    modes_dc: Dict = None,
    debug: bool = False,
    min_type: str = "phi",
) -> float:
    """Function called to minimize the unfaithfulness numerically over the phase and eccentricity and l0.
    The quantity to be minimized can currently be:
    1. 'unfaithfulness_RC' as defined in Cotesta et al (https://arxiv.org/pdf/1803.10701.pdf)
    2. 'unfaithfulness_IH' as defined in Harry et al(https://arxiv.org/pdf/1603.02444.pdf)


    Args:
        min_pars (np.array): the minimization parameters, f_ref,phi
        params_signal (waveform_params): Parameters of the signal waveform
        params_template (waveform_params): Parameters of the template waveform
        kappa_s (float): The effective polarization of the signal
        flow (float): The lower frequency bound
        fhigh (float, optional): The higher frequency bound. Defaults to None.
        quantity (str, optional): The quantity to minimize. Can either be 'unfaithfulness_RC' or 'unfaithfulness_IH'
        modes_dc (dict,optional): A dictionary of waveform modes. Must already have the right time spacing
    Returns:
        float: the unfaithfulness
    """

    ## This is for QC binaries with HoMs
    if min_type == "phi":

        phi_t = min_pars[0]

        params_template.phi = phi_t

    elif min_type == "reference_frequency":  # For precessing Phenom models

        f_ref_t, phi_t = min_pars
        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        params_template.f_ref = f_ref_t
        params_template.phi = phi_t

    elif min_type == "spin_rotation":  # For precessing Phenom models

        alpha, phi_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

    elif min_type == "eccentricity_22mode": # This is for non-precessing 22 mode waveforms
        # We are maximizing numerically only over initial eccentricity as the phase is degenerate
        # with the polarization angle over which RC_unfaithfulness is analytically maximizig
        ecc_t = min_pars[0]
        # print(" phi_t = ",phi_t, ",    ecc_t  = ",ecc_t)
        params_template.ecc = ecc_t

    elif min_type == "eccentricity_meanAnomaly_22mode": # This is for non-precessing 22 mode waveforms
        # We are maximizing numerically only over initial eccentricity as the phase is degenerate
        # with the polarization angle over which RC_unfaithfulness is analytically maximizig
        ecc_t, l0_t = min_pars
        # print(" phi_t = ",phi_t, ",    ecc_t  = ",ecc_t)
        params_template.ecc = ecc_t
        params_template.mean_anomaly = l0_t

    ## Now for eccentric models . Optimize numerically also over eccentricity or mean anomaly
    elif min_type == "eccentricity": # This is for non-precessing with higher modes waveforms
        # We are maximizing over phase and initial eccentricity
        phi_t, ecc_t = min_pars
        # print(" phi_t = ",phi_t, ",    ecc_t  = ",ecc_t)
        params_template.ecc = ecc_t
        params_template.phi = phi_t

    elif min_type == "eccentricity_meanAnomaly": # This is for non-precessing with higher modes waveforms
        # We are maximizing over phase, initial eccentricity and meanAnomaly
        ecc_t, l0_t, phi_t = min_pars

        params_template.ecc = ecc_t
        params_template.phi = phi_t
        params_template.mean_anomaly = l0_t

    elif min_type == "eccentricity_22mode_minimum_frequency":  # Numerical optimization over the initial eccentricity of the template
        ecc_t, f_ref_t = min_pars

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        if f_ref_t > 1.1 * params_template.f_min:
            f_ref_t = 1.1 * params_template.f_min


        params_template.f_ref = f_ref_t
        params_template.f_min = params_template.f_ref
        params_template.ecc = ecc_t

    elif min_type == "eccentricity_meanAnomaly_22mode_reference_frequency":  # Numerical optimization over the initial eccentricity of the template
        ecc_t, l0_t, f_ref_t = min_pars

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        if f_ref_t > 1.1 * params_template.f_min:
            f_ref_t = 1.1 * params_template.f_min


        params_template.f_ref = f_ref_t
        params_template.f_min = params_template.f_ref
        params_template.ecc = ecc_t
        params_template.mean_anomaly = l0_t

    elif min_type == "eccentricity_phi_minimum_frequency":  # Numerical optimization over the initial eccentricity of the template
        phi_t, ecc_t, f_ref_t = min_pars

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        if f_ref_t > 1.1 * params_template.f_min:
            f_ref_t = 1.1 * params_template.f_min


        params_template.f_ref = f_ref_t
        params_template.f_min = params_template.f_ref
        params_template.phi = phi_t
        params_template.ecc = ecc_t

    elif min_type == "eccentricity_reference_frequency":
        # We are maximizing over reference frequency, phase and initial eccentricity
        phi_t, ecc_t, f_ref_t = min_pars

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        params_template.f_ref = f_ref_t
        params_template.phi = phi_t
        params_template.ecc = ecc_t

    elif min_type == "eccentricity_spin_rotation":

        alpha, ecc_t, phi_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.ecc = ecc_t
        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

    elif min_type == "eccentricity_mean_anomaly_reference_frequency":

        phi_t, ecc_t, l0_t, f_ref_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.ecc = ecc_t
        params_template.mean_anomaly = l0_t

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        params_template.f_ref = f_ref_t

    elif min_type == "eccentricity_mean_anomaly_spin_rotation":

        alpha, ecc_t, phi_t, l0_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.ecc = ecc_t
        params_template.mean_anomaly = l0_t

        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

    else:
        # We are maximizing over phase only
        phi_t = min_pars[0]

    #    if ( (type(phi_t) is list) and (len(phi_t) == 1 ) ) :
    #        phi_t= phi_t[0]

        params_template.phi = phi_t

    ## Avoid simulated annealing points from outside the boundaries
    if params_template.ecc < 0:
        params_template.ecc = 0.0

    # print(" phi_t = ", params_template.phi)
    if quantity == "unfaithfulness_RC":
        return unfaithfulness_RC(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug
        )
    elif quantity == "unfaithfulness_IH":
        return unfaithfulness_IH(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc
        )
    else:
        raise NotImplementedError


def rotate_inplane_spin(s1x: float, s1y: float, alpha: float):
    """Perform a rigid rotation of the in-plane components of the spin
    by angle alpha, counterclockwise around the z-axis

    Args:
        s1x (float): x-component
        s1y (float): y-component
        alpha (float): angle in radians
    """
    sx_new = np.cos(alpha) * s1x - np.sin(alpha) * s1y
    sy_new = np.sin(alpha) * s1x + np.cos(alpha) * s1y
    return sx_new, sy_new


# With the new minimize_unfaithfulness function, this function below is no longer needed
# Left here for legacy
def minimize_unfaithfulness_spin_rotation(
    min_pars: np.array,
    s: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template_f: waveform_params,
    kappa_s: float,
    flow: float,
    fhigh: float = None,
    quantity: str = "unfaithfulness_RC",
    modes_dc: Dict = None,
    debug: bool = False,
) -> float:
    """Function called to minimize the unfaithfulness numerically over the phase and f_ref.
    The quantity to be minimized can currently be:
    1. 'unfaithfulness_RC' as defined in Cotesta et al (https://arxiv.org/pdf/1803.10701.pdf)
    2. 'unfaithfulness_IH' as defined in Harry et al(https://arxiv.org/pdf/1603.02444.pdf)


    Args:
        min_pars (np.array): the minimization parameters, f_ref,alpha
        params_template (waveform_params): Parameters of the template waveform
        kappa_s (float): The effective polarization of the signal
        flow (float): The lower frequency bound
        fhigh (float, optional): The higher frequency bound. Defaults to None.
        quantity (str, optional): The quantity to minimize. Can either be 'unfaithfulness_RC' or 'unfaithfulness_IH'
        modes_dc (dict,optional): A dictionary of waveform modes. Must already have the right time spacing
    Returns:
        float: the unfaithfulness
    """
    params_template = deepcopy(params_template_f)
    # Update the template parameters
    if len(min_pars) > 1:
        # We are maximizing over rigid rotation of the spin and phase
        alpha, phi_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

        # print(alpha, params_template.phi)
    else:
        # We are maximizing over phase only
        phi_t = min_pars[0]
        params_template.phi = phi_t

    if quantity == "unfaithfulness_RC":
        return unfaithfulness_RC(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug
        )
    elif quantity == "unfaithfulness_IH":
        return unfaithfulness_IH(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc
        )
    else:
        raise NotImplementedError


def unfaithfulness_RC(
    s_arg: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template: waveform_params,
    flow: float = 10.0,
    fhigh: float = None,
    modes_dict: Dict = None,
    debug: bool = False,
) -> Union[float, tuple]:
    """This function returns the overlap maximized over the effective polarization
    for given value of the phi.
    See https://arxiv.org/pdf/1803.10701.pdf and https://arxiv.org/pdf/1709.09181.pdf

    Args:
        s_arg (pt.TimeSeries): The signal in the detector
        params_template (waveform_params): The template parameters
        flow (float, optional): Low frequency cutoff. Defaults to 10.0.
        fhigh (float, optional): High frequency cutoff. Defaults to None.
        modes_dict (Dict, optional): Dictionary of modes. To be used for EOB. Defaults to None.
        debug (bool, optional): Outout debug information. Defaults to False.

    Returns:
        float: The unfaithfulness
    """
    s = s_arg.copy()
    if params_template.domain == "TD":
        # If we are not given a dictionary of modes, generate the template from scratch
        if not modes_dict:
            hp_td, hc_td = generate_waveform(params_template)
        else:
            # Combine modes
            hp_t, hc_t = combine_modes(
                params_template.iota, params_template.phi, modes_dict
            )

            # Taper
            hp_td = TimeSeries(hp_t, delta_t=params_template.delta_t)
            hc_td = TimeSeries(hc_t, delta_t=params_template.delta_t)
            hp_td = taper_timeseries(hp_td, tapermethod="startend")
            hc_td = taper_timeseries(hc_td, tapermethod="startend")

        N = max(len(hp_td), len(s))
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        hp_td.resize(pad)
        hc_td.resize(pad)
        s.resize(pad)
        s_tilde = make_frequency_series(s)

        hp_tilde = make_frequency_series(hp_td)
        hc_tilde = make_frequency_series(hc_td)
    else:
        # Frequency doman template
        # FFT the signal as well, if it's in the time domain
        if isinstance(s, pt.TimeSeries):

            N = len(s)
            pad = int(2 ** (np.floor(np.log2(N)) + 2))
            s.resize(pad)

        s_tilde = make_frequency_series(s)  # If in FD already, this is a no-op.
        f_max = s_tilde.get_sample_frequencies()[-1]

        params_template.delta_f = s_tilde.delta_f
        # Combine modes
        if modes_dict is not None:
            hp_tilde, hc_tilde = combine_modes(
                params_template.iota, params_template.phi, modes_dict
            )
        else:
            hp_tilde, hc_tilde = generate_waveform(params_template, f_max=f_max)

    # Sanity check
    assert len(s_tilde) == len(
        hp_tilde
    ), "The length of template and signal don't match!, {} and {} ".format(
        len(s_tilde),
        len(hp_tilde)
        # , pad, len(hp_td), len(s)
    )

    psd_a = generate_psd(len(s_tilde), s_tilde.delta_f, flow)
    # For debugging only
    # print("pycbc mismatch = {}".format(1-match(s_tilde,hp_tilde,psd=psd_a,low_frequency_cutoff=flow,high_frequency_cutoff=fhigh)[0]))
    # Norm of h_{+}
    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of h_{x}
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of s, |s|
    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    # Normalized templates
    hp_hat = hp_tilde / A_p
    hc_hat = hc_tilde / A_c

    # Cross term, i.e. (h_{+}|h_{x})
    # Notice that normalized=False, because we do our own normalization
    Ipc = _filter.overlap(
        hp_hat,
        hc_hat,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    # Compute the complex SNRs
    # Again, sigmasq=1, don't normalize!
    rho_p_hat = _filter.matched_filter(
        hp_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    rho_c_hat = _filter.matched_filter(
        hc_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )
    rho_p_hat = rho_p_hat.real()
    rho_c_hat = rho_c_hat.real()
    rho_p_hat = rho_p_hat.numpy()
    rho_c_hat = rho_c_hat.numpy()

    num = rho_p_hat ** 2 + rho_c_hat ** 2 - 2 * rho_p_hat * rho_c_hat * Ipc
    denum = 1 - Ipc ** 2
    overlap = np.sqrt(num / denum) * 1 / N_1
    overlap = np.max(overlap)
    #print("mm = {}".format(1 - overlap))

    """
    # Construct the PSD
    psd_a = generate_psd(len(s_tilde), s_tilde.delta_f, flow)

    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    Ipc = _filter.overlap(
        hp_tilde,
        hc_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    rho_p, _, norm = _filter.matched_filter_core(
        s_tilde,
        hp_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        h_norm=1.0,
    )
    rho_p = (4.0 * s_tilde.delta_f) * rho_p.real()

    rho_c, _, norm = _filter.matched_filter_core(
        s_tilde,
        hc_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        h_norm=1.0,
    )
    rho_c = (4.0 * s_tilde.delta_f) * rho_c.real()
    rho_p = rho_p.numpy()
    rho_c = rho_c.numpy()

    # This is a faster way to compute the overlap.
    # See https://arxiv.org/pdf/1709.09181.pdf, Eq(13)
    # The overlap is given by $\mathcal{O} = \sqrt(\rho^{2})/|s|$
    # This is completely equivalent to the expression below
    # in the debug part, but is faster computationally
    temp1 = rho_p ** 2 * A_c ** 2 + rho_c ** 2 * A_p ** 2 - 2 * rho_p * rho_c * Ipc
    temp2 = A_p ** 2 * A_c ** 2 - Ipc ** 2
    test = 1 / N_1 * np.sqrt(temp1 / temp2)
    overlap = np.max(test)
    """

    if debug:
        Ipc = _filter.overlap(
            hp_tilde,
            hc_tilde,
            psd=psd_a,
            low_frequency_cutoff=flow,
            high_frequency_cutoff=fhigh,
            normalized=False,
        )
        rho_p, _, norm = _filter.matched_filter_core(
            s_tilde,
            hp_tilde,
            psd=psd_a,
            low_frequency_cutoff=flow,
            high_frequency_cutoff=fhigh,
            h_norm=1.0,
        )
        rho_p = (4.0 * s_tilde.delta_f) * rho_p.real()

        rho_c, _, norm = _filter.matched_filter_core(
            s_tilde,
            hc_tilde,
            psd=psd_a,
            low_frequency_cutoff=flow,
            high_frequency_cutoff=fhigh,
            h_norm=1.0,
        )
        rho_c = (4.0 * s_tilde.delta_f) * rho_c.real()
        rho_p = rho_p.numpy()
        rho_c = rho_c.numpy()
        alpha = np.sqrt(rho_p ** 2 + rho_c ** 2)  # this is a time series
        kappa0 = np.angle(rho_p + 1.0j * rho_c)  # this is a time series
        gamma = np.sqrt(((A_p ** 2 - A_c ** 2) / 2.0) ** 2 + Ipc ** 2)
        sigma0 = np.angle((A_p ** 2 - A_c ** 2) / 2.0 + 1.0j * Ipc)
        beta = (A_p ** 2 + A_c ** 2) / 2.0

        num = alpha * np.sqrt(beta - gamma * np.cos(2 * kappa0 - sigma0))
        den = N_1 * np.sqrt(beta ** 2 - gamma ** 2)
        overlap = num / den

        idx_max = np.argmax(overlap)
        overlap = np.max(overlap)
        dt = idx_max * s.delta_t
        if idx_max > len(s) / 2:
            dt = dt - s.duration
        psi_max_num = beta * np.sin(kappa0[idx_max]) + gamma * np.sin(
            kappa0[idx_max] - sigma0
        )
        psi_max_denom = beta * np.cos(kappa0[idx_max]) - gamma * np.cos(
            kappa0[idx_max] - sigma0
        )
        psi_max = np.arctan2(psi_max_num, psi_max_denom)
        if psi_max < 0:
            psi_max += 2 * np.pi
        print(
            "Overlap: {},dt: {}, psi_max: {}, f_ref: {}, phi_ref: {}".format(
                overlap, dt, psi_max, params_template.f_ref, params_template.phi
            )
        )
        return 1 - overlap, dt, psi_max
    return 1 - overlap


def unfaithfulness_IH(
    s_arg: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template: waveform_params,
    flow: float = 10.0,
    fhigh: float = None,
    modes_dict: Dict = None,
    debug: bool = False,
):
    """This function returns the overlap maximized over alpha
    for given value of phi_t. See Harry et al, https://arxiv.org/pdf/1603.02444.pdf

    Args:
        s_arg (Union[pt.TimeSeries, pt.FrequencySeries]): The signal in the detector
        params_template (waveform_params): The template parameters
        flow (float, optional): Low freqeuncy cutoff. Defaults to 10.0.
        fhigh (float, optional): High frequency cutoff. Defaults to None.
        modes_dict (Dict, optional): Dictionary of modes. To be used for EOB. Defaults to None.

    Returns:
        float: The unfaithfulness
    """
    s = s_arg.copy()
    if params_template.domain == "TD":
        # If we are not given a dictionary of modes, generate the template from scratch
        if not modes_dict:
            hp_td, hc_td = generate_waveform(params_template)
        else:
            # Combine modes
            hp_t, hc_t = combine_modes(
                params_template.iota, params_template.phi, modes_dict
            )

            # Taper
            hp_td = TimeSeries(hp_t, delta_t=params_template.delta_t)
            hc_td = TimeSeries(hc_t, delta_t=params_template.delta_t)
            hp_td = taper_timeseries(hp_td, tapermethod="startend")
            hc_td = taper_timeseries(hc_td, tapermethod="startend")

        N = max(len(hp_td), len(s))
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        hp_td.resize(pad)
        hc_td.resize(pad)
        s.resize(pad)
        s_tilde = make_frequency_series(s)
        hp_tilde = make_frequency_series(hp_td)
        hc_tilde = make_frequency_series(hc_td)
    else:
        # Frequency doman template
        # FFT the signal as well, if it's in the time domain
        if isinstance(s, pt.TimeSeries):
            N = len(s)
            pad = int(2 ** (np.floor(np.log2(N)) + 2))
            s.resize(pad)
        s_tilde = make_frequency_series(s)
        f_max = s_tilde.get_sample_frequencies()[-1]
        params_template.delta_f = s_tilde.delta_f
        hp_tilde, hc_tilde = generate_waveform(params_template, f_max=f_max)

    # Sanity check
    assert len(s_tilde) == len(
        hp_tilde
    ), "The length of template and signal don't match!"

    # Construct the PSD
    psd_a = generate_psd(len(s_tilde), s_tilde.delta_f, flow)

    # Norm of h_{+}
    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of h_{x}
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of s, |s|
    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    # Normalized templates
    hp_hat = hp_tilde / A_p
    hc_hat = hc_tilde / A_c

    # Cross term, i.e. (h_{+}|h_{x})
    # Notice that normalized=False, because we do our own normalization
    Ipc = _filter.overlap(
        hp_hat,
        hc_hat,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    # Compute the complex SNRs
    # Again, sigmasq=1, don't normalize!
    rho_p_hat = _filter.matched_filter(
        hp_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    rho_c_hat = _filter.matched_filter(
        hc_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    gamma_hat = (rho_p_hat * rho_c_hat.conj()).real().numpy()
    rho_p2 = np.abs(rho_p_hat) ** 2
    rho_c2 = np.abs(rho_c_hat) ** 2

    a = Ipc * rho_p2 - gamma_hat
    b = rho_p2 - rho_c2
    c = gamma_hat - Ipc * rho_c2
    pos_root = (-b + np.sqrt(b ** 2 - 4 * a * c)) / (2 * a)
    psi_max = np.arctan(1 / pos_root * np.sqrt(A_p ** 2 / A_c ** 2))
    max_lambda_root = np.sqrt(
        (rho_p2 - rho_c2) ** 2
        + 4.0 * (Ipc * rho_p2 - gamma_hat) * (Ipc * rho_c2 - gamma_hat)
    )
    max_lambda_num = rho_p2 - (2.0 * gamma_hat * Ipc) + rho_c2 + max_lambda_root
    max_lambda_den = 1.0 - Ipc ** 2

    max_lambda_list = 0.25 * max_lambda_num.numpy() / max_lambda_den
    max_lambda_idx = np.argmax(max_lambda_list)
    max_lambda = max_lambda_list[max_lambda_idx]

    # Recall that $\lambda=\frac{1}{2}\frac{(s|h)^{2}}{|h|^{2}}$
    # On the other hand $\mathcal{O} = \frac{(s|h)}{|s||h|}$. Thus
    # \mathcal{O} = $\sqrt{\frac{2\lambda}{|s|}}$
    overlap = np.sqrt(2 * max_lambda) / N_1

    dt = max_lambda_idx * s.delta_t
    # print("IH: ",1-overlap)
    if max_lambda_idx > len(s) / 2:
        dt = dt - s.duration
    if debug:
        return 1 - overlap, dt, psi_max[max_lambda_idx]
    return 1 - overlap


def interpolate_mode_dict_and_rescale(
    modes_EOB: Dict, t_EOB: np.array, params_template: waveform_params
) -> Dict:
    """Interpolate a set of modes and rescale them to SI units

    Args:
        modes_EOB (Dict): The waveform modes
        t_EOB (np.array): Time in geometric units
        params_template (waveform_params): The parameters of the waveform. Specifies
            the time resolution via delta_t.

    Returns:
        Dict: The waveform modes at the desired timespacing
    """
    dc_rescaled = {}
    t_EOB_rescaled = t_EOB * (params_template.m1 + params_template.m2) * lal.MTSUN_SI
    new_times = np.arange(0, t_EOB_rescaled[-1], params_template.delta_t)
    amp_factor = (
        (params_template.m1 + params_template.m2)
        * lal.MRSUN_SI
        / params_template.distance
    )
    for key in modes_EOB.keys():
        intrp_re = InterpolatedUnivariateSpline(t_EOB_rescaled, np.real(modes_EOB[key]))
        intrp_im = InterpolatedUnivariateSpline(t_EOB_rescaled, np.imag(modes_EOB[key]))

        dc_rescaled[key] = (intrp_re(new_times) + 1j * intrp_im(new_times)) * amp_factor
    return dc_rescaled




##################################################

# Functions for manual unfaithfulness grids over eccentricity and mean anomaly to reduce the range over which to apply the numerical optimization scipy routines

##################################################



# Make a list of the eccentricity values where to compute the unfaithfulness
def make_centered_table(param_min:float,delta_param:float,NN: int, param_max_template:float):

    val=np.array(param_min)
    if param_min == 0.0:
        arr0=np.linspace(param_min,param_min+delta_param*2,int(NN/2)+1)

    else:
        arr0=np.linspace(param_min-delta_param,param_min,int(NN/2)+1)

    arr1=np.linspace(param_min,param_min+delta_param,int(NN/2))


    if param_min >= param_max_template:
        arr0=np.linspace(param_max_template-delta_param, param_max_template,int(NN/2)+1)
        arr1=np.linspace(param_max_template-delta_param*2, param_max_template,int(NN/2))

    arr=np.append(val,arr0)
    arr= np.append(arr1,arr)

    return np.unique(arr)

# function to compute new grid in eccentricity/ mean_anomaly/ phase
def new_paramgrid(grid: np.array, mm_list: list, param_max_template: float):
    imin=np.argmin(mm_list)
    NN = len(grid)
    param_min = grid[imin]
    delta_param = np.abs(grid[-1] - grid[-2])/2

    grid_new= make_centered_table(param_min,delta_param,NN, param_max_template)

    return grid_new


def mm_loop(s: Union[pt.TimeSeries, pt.FrequencySeries], params_signal: waveform_params, params_template: waveform_params, kappa_s: float, grid: np.array, param:str ,fhigh: float, modes_dc: Dict = None, debug: bool = False):

    mm_list=[]

    for param_template in grid:

        if param =="phi":
            params_template.phi = param_template
        elif param == "eccentricity":
            params_template.ecc = param_template
        elif param == "mean_anomaly":
            params_template.mean_anomaly = param_template


        final = unfaithfulness_RC(s, params_template, params_signal.f_min, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

        mm_list.append(final)


    return np.array(mm_list)


def unfaithfulness_RawGrid(s: Union[pt.TimeSeries, pt.FrequencySeries], params_signal: waveform_params, params_template: waveform_params, kappa_s: float, param: str, param_max: float, iters: int, NN: int,fhigh: float, modes_dc: Dict = None, debug: bool = False):

    if param == "fref" :
        param_min = (0.8/1.4)*param_max
    else:
        param_min = 0.0


    param_grid={}
    mm_list={}
    mmin_list=[]
    parammin_list=[]

    param_grid[0] = np.linspace( param_min, param_max,NN, param_max)

    for i in range(iters):

        if i !=0:
            param_grid[i]= new_paramgrid(param_grid[i-1], mm_list[i-1],param_max)

        #print("i = ",i, " grid =", param_grid[i])
        mm_list[i] = mm_loop(s, params_signal ,params_template,kappa_s,  param_grid[i],param,fhigh, modes_dc, debug)

        imin=np.argmin(mm_list[i])
        mmin_list.append(mm_list[i][imin])
        parammin_list.append(param_grid[i][imin])


    imin=np.argmin(mmin_list)
    param_min_found = parammin_list[imin]

    return param_min_found, mmin_list[imin]


def restricted_bounds(param: str, param_max: float, param_mm_min: float):

    if param == "eccentricity":
        delta_param = 0.04
    else:
        delta_param = 0.2

    if param == "fref" :
        param_min = (0.8/1.4)*param_max
    else:
        param_min = 0.0


    if param_mm_min - delta_param < param_min:
         bound_min = param_min
    else:
         bound_min = param_mm_min - delta_param

    if param_mm_min + delta_param > param_max:
         bound_max = param_max
    else:
        bound_max = param_mm_min + delta_param


    return bound_min,bound_max


def ecc_nonprecessing_restrictedbounds(s: Union[pt.TimeSeries, pt.FrequencySeries], params_signal: waveform_params, params_template: waveform_params, kappa_s: float, iters: int , NN: int, min_type: str, ecc_minAS_list: list, e0max_template: float ,fhigh: float, modes_dc: Dict = None, debug: bool = False):


    if min_type in ecc_minAS_list:

        if min_type == "eccentricity_22mode" or min_type == "eccentricity_22mode_periastronFrequency" or min_type == "eccentricity_22mode_averageFrequency": # No HoMs
            #Loop over eccentricity
            param="eccentricity"
            param_max = e0max_template
            ecc_min, mm_ecc_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.ecc = ecc_min

            mm_min = [mm_ecc_min]
            val_min = [ecc_min]
            e0min_bound, e0max_bound = restricted_bounds( "eccentricity" , e0max_template, ecc_min)
            bounds = ((e0min_bound,e0max_bound),)

        elif min_type == "eccentricity_meanAnomaly_22mode": # No HoMs

            # First loop over eccentricity
            param="eccentricity"
            param_max = e0max_template
            ecc_min, mm_ecc_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.ecc = ecc_min

            # Then loop mean_anomaly
            param="mean_anomaly"
            param_max = 2.*np.pi
            l0_min, mm_l0_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.mean_anomaly = l0_min

            #Computed constrained bounds
            mm_min = [mm_ecc_min, mm_l0_min ]
            val_min = [ ecc_min,  l0_min ]
            l0min_bound, l0max_bound = restricted_bounds( "mean_anomaly" , 2.*np.pi, l0_min)
            e0min_bound, e0max_bound = restricted_bounds( "eccentricity" , e0max_template, ecc_min)
            bounds = ((e0min_bound,e0max_bound),(l0min_bound,l0max_bound),)

        elif min_type == "eccentricity":  # With HoMs

            # First loop over eccentricity
            param="eccentricity"
            param_max = e0max_template
            ecc_min, mm_ecc_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.ecc = ecc_min

            # Then loop over coalescence phase
            param="phi"
            param_max = 2.*np.pi
            phi_min, mm_phi_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.phi = phi_min

            #Computed constrained bounds
            mm_min = [ mm_phi_min, mm_ecc_min ]
            val_min = [ val_phi_min, val_ecc_min ]
            phimin_bound,phimax_bound = restricted_bounds( "phi" , 2.*np.pi, phi_min)
            e0min_bound,e0max_bound = restricted_bounds( "eccentricity" , e0max_template, ecc_min)
            bounds = ((phimin_bound,phimax_bound),(e0min_bound,e0max_bound),)

        elif min_type == "eccentricity_meanAnomaly":  # With HoMs
            # First loop over eccentricity
            param="eccentricity"
            param_max = e0max_template
            ecc_min, mm_ecc_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.ecc = ecc_min

            # Then loop over coalescence phase
            param="phi"
            param_max = 2.*np.pi
            phi_min, mm_phi_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.phi = phi_min


            # Then loop mean_anomaly
            param="mean_anomaly"
            param_max = 2.*np.pi
            l0_min, mm_l0_min = unfaithfulness_RawGrid(s, params_signal, params_template, kappa_s, param, param_max, iters,NN,fhigh, modes_dc, debug)
            params_template.mean_anomaly = l0_min

            #Computed constrained bounds
            mm_min = [mm_phi_min, mm_ecc_min, mm_l0_min ]
            val_min = [val_phi_min, val_ecc_min, val_l0_min ]

            phimin_bound,phimax_bound = restricted_bounds( "phi" , 2.*np.pi, phi_min)
            l0min_bound, l0max_bound = restricted_bounds( "mean_anomaly" , 2.*np.pi, l0_min)
            e0min_bound, e0max_bound = restricted_bounds( "eccentricity" , e0max_template, ecc_min)
            bounds = ((phimin_bound,phimax_bound),(e0min_bound,e0max_bound),(l0min_bound,l0max_bound),)

    return bounds, np.array(mm_min), np.array(val_min)

#########################################


def generate_LALFDwaveform(
    p: waveform_params, f_max: float = 2048.0
) -> pt.FrequencySeries:
    """Generate a lalsuite waveform. Explicitly uses
    which domain is selected, either TD or FD.

    Args:
        p (waveform_params): The parameters of the waveform
        f_max (float, optional): The upper frequency cutoff for the waveform. Defaults to 2048.0.

    Raises:
        NotImplementedError: If you try a domain other than "TD" or "FD"

    Returns:
        Union[pt.TimeSeries, pt.FrequencySeries]: The desired waveform
    """
    #print("domain = ",p.domain)
    if p.approx == "TEOBResumSE":
        modesTEOB = [[2, 2]]
        t, hp, hc, hlm = generate_TEOBResumSE_simple_waveform(p, f_max, modesTEOB)
    elif p.approx == "TEOBResumSEHM":
        modesTEOB = [
                [2, 2],
                [2, 1],
                [3, 3],
                [3, 2],
                [4, 4],
                [5, 5],
        ]  # More reliable modes
        t, hp, hc, hlm = generate_TEOBResumSE_simple_waveform(p, f_max, modesTEOB)

    elif p.approx == "SEOBNREv4":
        t, hp, hc, amp22 = generate_SEOBNREv4_waveform(p, f_max)

    # Read output file of the SEOBNREv3 output file
    else:

        inspiralFDparams = {
        'm1': p.m1 * lal.MSUN_SI, 'm2': p.m2 * lal.MSUN_SI,
        'S1x': p.s1x, 'S1y': p.s1y, 'S1z': p.s1z,
        'S2x': p.s2x, 'S2y': p.s2y, 'S2z': p.s2z,
        'distance': p.distance, 'inclination': p.iota,
        'phiRef': p.phi, 'longAscNodes': 0.0,
        'meanPerAno': p.mean_anomaly, 'eccentricity': p.ecc,
        'deltaF': p.delta_f, 'f_min': p.f_min, 'f_max': f_max, 'f_ref': p.f_ref,
        'LALparams': p.wf_param, 'approximant': lalsim.GetApproximantFromString(p.approx)
        };  # This functions returns a conditioned frequency domain version of a time domain waveform
    # NOTE: due to the conditioning, the starting frequency needs to be increased for the conversion to work
        #print(inspiralFDparams)
        hp, hc = lalsim.SimInspiralFD(**inspiralFDparams)  # Frequency array


        hpy  = pycbc.types.frequencyseries.FrequencySeries(hp.data.data, hp.deltaF,  epoch='', dtype=complex, copy=True)
        hcy  = pycbc.types.frequencyseries.FrequencySeries(hc.data.data, hc.deltaF,  epoch='', dtype=complex, copy=True)

    if (
        p.approx == "SEOBNREv4"
            or p.approx == "TEOBResumSE"
            or p.approx == "TEOBResumSEHM"
    ):
        # Taper
        hp_td = TimeSeries(hp, delta_t=p.delta_t)
        hc_td = TimeSeries(hc, delta_t=p.delta_t)
        hp_td = taper_timeseries(hp_td, tapermethod="startend")
        hc_td = taper_timeseries(hc_td, tapermethod="startend")

        return hp_td, hc_td

    else:

        return hpy, hcy

#########################################
#### Functions to parse the old .dat NR files used for testing SEOBNRv4

def compute_freqInterp(time: np.array , hlm: np.array ):

    philm=np.unwrap(np.angle(hlm))

    intrp = InterpolatedUnivariateSpline(time,philm)
    omegalm = intrp.derivative()(time)

    return omegalm

def NRparameter_list(NRdatfiles: list, param_list: list):


    q0_list={}
    chi1z_list={}
    chi2z_list={}

    for file in NRdatfiles:

        basename = os.path.basename(file)
        fileName=os.path.splitext(basename)[0]
        fileName1=fileName.replace('h22_','')
        parfileName = fileName1.replace('_',':')

        indices = [i for i, s in enumerate(param_list) if parfileName in s]
        q0_list[file] = np.round(float(param_list[indices[0]][1]),3)
        chi1z_list[file] = float(param_list[indices[0]][2])
        chi2z_list[file] = float(param_list[indices[0]][3])
        #print(i," ",parfileName,"   q = ", q0_list[file],"   chi1z = ", chi1z_list[file],"   chi2z = ", chi2z_list[file])

    return q0_list,chi1z_list,chi2z_list


def get_NR_v4data(NR_file: str, q0_list: dict, chi1z_list: dict, chi2z_list: dict):
    # This is a custom NR approximant, needed for NR-NR match
    # and symmetrized NR
    wf_modes = {}

    data = np.genfromtxt(NR_file)
    times = data[:,0]
    reh22 = data[:,1]
    imh22 = data[:,2]
    h22 = reh22-1j*imh22
    #print(f'NR_file : {NR_file}')
    #print(q0_list.keys())
    q0 = q0_list[NR_file]
    m1_dim = q0/(1.+q0)
    m2_dim = 1./(1.+q0)
    chi1 = chi1z_list[NR_file]
    chi2 = chi2z_list[NR_file]

    tr=200. # arbitrarily chosen
    idx = np.where(times >= tr)

    omega22 = compute_freqInterp(times, h22)
    omega_start = omega22[idx][0]/2
    wf_modes["2,2"] = data[:, 1][idx] + 1j * data[:, 2][idx]
    wf_modes["2,-2"] = data[:, 1][idx] - 1j * data[:, 2][idx]

    freq_1M = omega_start / (m1_dim + m2_dim) / (lal.MTSUN_SI) / np.pi
    params = dict(m1_dim=m1_dim, m2_dim=m2_dim, chi1=chi1, chi2=chi2, freq_1M=freq_1M)

    return wf_modes, times[idx] - tr, params




def compute_OmegaOrbfromNRfile(NR_file:str):



    fp = h5py.File(NR_file, "r")

    t_omega_orb = fp['Omega-vs-time/X'][:]
    tHorizon = fp['HorizonBTimes'][:]
    omega_orb = fp['Omega-vs-time/Y'][:]
    iOmega_orb = InterpolatedUnivariateSpline(t_omega_orb, omega_orb)

    fp.close()

    om_orb = iOmega_orb(tHorizon)


    phi =  []

    for time in tHorizon:
        phi.append(iOmega_orb.integral(tHorizon[0],time))

    phase_orb = np.array(phi)

    return tHorizon, om_orb, phase_orb



def compute_MaxMin(tHorizon:np.array, omega_orb:np.array):

    # for local maxima
    maxima = argrelextrema(omega_orb, np.greater) # Use A22 mode as it is cleaner than the frequency
    omega_orb_maxima = omega_orb[maxima]
    tH_maxima = tHorizon[maxima]

    # for local minima
    minima = argrelextrema(omega_orb, np.less)  # Use A22 mode as it is cleaner than the frequency

    omega_orb_minima =omega_orb[minima]
    tH_minima = tHorizon[minima]


    return tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima



def SEOBNRv4EHM_modes(params: waveform_params, EccFphiPNorder: int, EccFrPNorder: int, EccWaveformPNorder: int,
                      EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int,
                      EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int, EcctAppend: float):


    HypPphi0, HypR0, HypE0 =[0.,0,0]
    EccIC=0
    eccentric_anomaly = 0.0

    if params.approx=="SEOBNRv4E_opt" or  params.approx=="SEOBNRv4":
        SpinAlignedVersion=4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    else:
        SpinAlignedVersion=41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary

    if params.approx == "SEOBNRv4E_opt" or params.approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt(params.delta_t,
                                                              params.m1*lal.MSUN_SI,
                                                              params.m2*lal.MSUN_SI,
                                                              params.f_min,
                                                              params.distance,
                                                              params.s1z,
                                                              params.s2z,
                                                              params.ecc,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:



        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModes(params.delta_t,
                                                              params.m1*lal.MSUN_SI,
                                                              params.m2*lal.MSUN_SI,
                                                              params.f_min,
                                                              params.distance,
                                                              params.s1z,
                                                              params.s2z,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)





    hlm={}
    M_fed= params.m1 + params.m2
    dMpc = params.distance/(1e6*lal.PC_SI)

    if SpinAlignedVersion==4:
        hlm['2,2'] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm['2,-2'] = np.conjugate(hlm['2,2'])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h55 )

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h44 )

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h21 )

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h33 )

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[f"{modeL},{modeM}"] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h22 )


    time_array = np.arange(0,len(hlm['2,2'])*params.delta_t, params.delta_t)
    timeNR = SectotimeM(time_array,M_fed)
    #return time_array, hlm
    return timeNR, hlm


def interpolate_mode_dict_and_rescale_v4EHM(
    modes_EOB: Dict, t_EOB: np.array, params_template: waveform_params
) -> Dict:
    """Interpolate a set of modes and rescale them to SI units

    Args:
        modes_EOB (Dict): The waveform modes
        t_EOB (np.array): Time in geometric units
        params_template (waveform_params): The parameters of the waveform. Specifies
            the time resolution via delta_t.

    Returns:
        Dict: The waveform modes at the desired timespacing
    """
    dc_rescaled = {}

    M_fed= params_template.m1 + params_template.m2
    dMpc = params_template.distance/(1e6*lal.PC_SI)
    #t_EOB_rescaled = t_EOB * (params_template.m1 + params_template.m2) * lal.MTSUN_SI
    t_EOB_rescaled = timeMtoSec(t_EOB,M_fed)

    new_times = np.arange(0, len(modes_EOB['2,2'])*params_template.delta_t, params_template.delta_t)

    #amp_factor = (
    #    (params_template.m1 + params_template.m2)
    #    * lal.MRSUN_SI        / params_template.distance    )
    for key in modes_EOB.keys():
        #print(key)
        ell, m = [int(x) for x in key.split(",")]
        intrp_re = InterpolatedUnivariateSpline(t_EOB_rescaled, np.real(modes_EOB[key]))
        intrp_im = InterpolatedUnivariateSpline(t_EOB_rescaled, np.imag(modes_EOB[key]))

        dc_rescaled[key] = AmpNRtoPhysicalTD((intrp_re(new_times) +1.j * intrp_im(new_times)), M_fed,dMpc)
    return dc_rescaled

#### Get Equal Length EOB-NR waveforms



def get_EOBduration(f_min: float, EccIC: int, params_template: waveform_params) -> float:

    EccAvNQCWaveform,EcctAppend,HypPphi0, HypR0, HypE0 = [1, 1, 4.22, 10000, 1.01]
    eccentric_anomaly=0.0 # UNUSED

    if params_template.approx == "SEOBNRv4E_opt":
        SpinAlignedVersion = 4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    else:
        SpinAlignedVersion = 41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary


    if params_template.approx == "SEOBNRv4E_opt" or params_template.approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(params_template.delta_t,
                                                              params_template.m1*lal.MSUN_SI,
                                                              params_template.m2*lal.MSUN_SI,
                                                              f_min,
                                                              params_template.distance,
                                                              params_template.s1z,
                                                              params_template.s2z,
                                                              params_template.ecc,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                               params_template.EccFphiPNorder,
                                                               params_template.EccFrPNorder,
                                                               params_template.EccWaveformPNorder,
                                                               params_template.EccPNFactorizedForm,
                                                               params_template.EccBeta,
                                                               params_template.Ecct0,
                                                               params_template.EccNQCWaveform,
                                                               params_template.EccPNRRForm,
                                                               params_template.EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:



        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(params_template.delta_t,
                                                              params_template.m1*lal.MSUN_SI,
                                                              params_template.m2*lal.MSUN_SI,
                                                              f_min,
                                                              params_template.distance,
                                                              params_template.s1z,
                                                              params_template.s2z,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)




    hlm={}
    M_fed= params_template.m1 + params_template.m2
    dMpc = params_template.distance/(1e6*lal.PC_SI)

    if SpinAlignedVersion==4:
        hlm['2,2'] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm['2,-2'] = np.conjugate(hlm['2,2'])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h55 )

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h44 )

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h21 )

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h33 )

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[f"{modeL},{modeM}"] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h22 )



    time_array = np.arange(0,len(hlm['2,2'])*params_template.delta_t, params_template.delta_t)
    timeNR = SectotimeM(time_array,M_fed)

    EOB_duration = get_time_to_merger(timeNR, hlm)

    return EOB_duration



def get_NR_duration(params_signal: waveform_params,params_template: waveform_params,NR_file: str, NR_ma: lal.Value,
                    ell_max: int = 4) -> float:

    # merger time difference manually

    _, hlm_NR = lalsim.SimInspiralNRWaveformGetHlms(
        params_signal.delta_t,
        params_signal.m1 * lal.MSUN_SI,
        params_signal.m2 * lal.MSUN_SI,
        params_signal.distance,
        params_signal.f_min,
        0.0,
        params_signal.s1x,
        params_signal.s1y,
        params_signal.s1z,
        params_signal.s2x,
        params_signal.s2y,
        params_signal.s2z,
        NR_file,
        NR_ma,
    )

    # non LAL waveform (only 22 waveform)
    modes_NR = {}

    if params_template.approx=="SEOBNRv4E_opt":
        modes_NR[2, 2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2).data.data
        modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    else:

        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell+1 ):
                #print(ell,m)
                if m!=0:
                    modes_NR[ell, m] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, ell,m).data.data

                #modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
    time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
    NR_duration = get_time_to_merger(time_NR, modes_NR)
    NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    return NR_duration

def tdiff_v4E(
    f_min: float, NR_duration: float, params: waveform_params, ell_max: int = 4
) -> float:
    """Compute the difference to the merger time between NR and EOB. Merger time
    is defined as the peak of the frame invariant amplitude.

    Args:
        f_min (float): Starting frequency [Hz]
        NR_duration (float): Duration of the waveform [M]
        params (waveform_params): The parameters of the EOB waveform
        ell_max (int, optional): Max ell to use. Defaults to 4.

    Returns:
        float: The difference in the time to merger
    """
    EccIC = 0
    EOB_duration = get_EOBduration(f_min, EccIC, params)

    #print(f_min, NR_duration, EOB_duration, NR_duration - EOB_duration)
    return NR_duration - EOB_duration



def get_fminEOB_NRlength_fast(f_min_signal: float, params_template: waveform_params,
                         NR_duration: float,  f_max: float = 2048.0) -> float:

    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(params_template.m1 + params_template.m2)*lal.MTSUN_SI)
    EccIC =0
    # Compute length of the shortest duration possible v4E waveform
    EOB_duration_fmin_Maxv4E = get_EOBduration(fmin_Maxv4E, EccIC, params_template)
    EOB_duration_fmin_NR = get_EOBduration(f_min_signal, EccIC, params_template)

    EOB_duration_fmin0 = EOB_duration_fmin_NR

    #print(EOB_duration_fmin0, EOB_duration_fmin_Maxv4E, NR_duration)

    f_min0 = f_min_signal
    while NR_duration > EOB_duration_fmin0:
        f_min0 -= 10
        EOB_duration_fmin0 = get_EOBduration(f_min0, EccIC, params_template)

    #print(EOB_duration, NR_duration)
    if EOB_duration_fmin_Maxv4E > NR_duration:
        f_min_EOB = fmin_Maxv4E
    else:
        #res = root_scalar(tdiff, bracket=(f_min0, fmin_Maxv4E),args=(NR_duration, params_template, 2),)
        res = root_scalar(tdiff_v4E, bracket=(f_min0, fmin_Maxv4E-1e-8),args=(NR_duration, params_template, 2),)
        f_min_EOB = res.root

    return f_min_EOB




######################################

def run_Eccunfaithfulness(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    index: int,
    template_approx: str = "IMRPhenomPv3HM",
    signal_approx: str = "NR_hdf5",
    NR_file: str = None,
    NR_file_2: str = None,
    parameters: Dict = None,
    fhigh: float = None,
    ellMax: int = 4,
    debug: bool = False,
    save_file: bool = True,
    unfaithfulness_type: str = "unfaithfulness_RC",
    minimization_type: str = "reference_frequency",
    simple: bool = False,
    raw_grid: str = "False",
    mode_array_signal: list = None,
    mode_array_template: list = None,
):
    """Compute the unfaithfulness maximizing over template parameters
    Currently the following combinations are supported:
    1. Signal in time domain and template in time domain
    2. Signal in time domain and template in frequency domain
    3. Signal and template in frequency domain.

    Args:
        phi_s (float): Azimuthal angle of the signal (rad). Ultimately averaged over.
        kappa_s (float): Effective polarization of the signal (rad). Ultimately averaged over.
        iota_s (float): Inclination of the signal (rad)
        index (int): Index to use to uniqualy identify the particular (phi_s,kappa_s) case
        template_approx (str, optional): Template approximant. Defaults to "IMRPhenomPv3HM".
        signal_approx (str, optional): Signal approximant. Defaults to "NR_hdf5".
        NR_file (str, optional): File containing NR data in LVC format. Defaults to None.
        NR_file_2 (str, optional): Second file containing NR data. Defaults to None.
        parameters (dict, optional): If the signal is not NR, the intrinsic parameters of the signal
        f_high (float, optional): Upper frequency cutoff. Defaults to None.
        ellMax (int, optional): Maximum ell to use. Defaults to 4.
        debug (bool, optional): Print debug information. Defaults to False.
        save_file (bool, optional): Save the output to a file. Defaults to True.
        unfaithfulness_type (str,optional): Which quantity to minimize. Defaults to "unfaithfulness_RC"
        mimization_type (str, optional): What type of mimization to use. Defaults to "reference_frequency"
    Returns:
        np.array The unfaithfulness
    """


    # Generic bounds on the initial eccentricity  of each model, below we use also the Newtonian estimate at the starting
    # frequency to set a maximum allowed eccentricity
    if template_approx == "TEOBResumSE":
        e0max_template = 0.3
    elif template_approx == "TEOBResumSEHM":
        e0max_template = 0.3
    elif template_approx == "SEOBNREv4":
        e0max_template = 0.7
    elif template_approx == "SEOBNRv4E":
        e0max_template = 0.5
    elif template_approx == "SEOBNRv4E_opt":
        e0max_template = 0.5
    elif template_approx == "SEOBNRv4EHM_opt":
        e0max_template = 0.5
    elif template_approx == "IMRPhenomXEv1":
        e0max_template = 0.2
    else:
        e0max_template = 0.0

    # Set the function we are going to minimize
    # Note that this choice does not affect what happens
    # for EOB approximants
    # Use only one minimization function

    min_func = minimize_Eccunfaithfulness

    # Figure out the domains of the signal and the template
    td_approxs = td_approximants()
    fd_approxs = fd_approximants()
    if signal_approx not in fd_approxs  or signal_approx == "SEOBNRv4":
        signal_domain = "TD"
        delta_t_signal = 1.0 / 8192
        delta_f_signal = None
    #elif signal_approx == "SEOBNREv4" or signal_approx == "TEOBResumSE" or signal_approx == "TEOBResumSEHM":
    #    signal_domain = "TD"
    #    delta_t_signal = 1.0 / 8192
    #    delta_f_signal = None
    else:
        signal_domain = "FD"
        delta_f_signal = 0.125
        delta_t_signal = None

    #if template_approx == "SEOBNREv4" or template_approx == "TEOBResumSE" or signal_approx == "TEOBResumSEHM":
    #    signal_domain = "TD"
    #    delta_t_signal = 1.0 / 8192
    #    delta_f_signal = None
    #print(template_approx, fd_approxs)

    if template_approx not in fd_approxs or template_approx == "SEOBNRv4":
        template_domain = "TD"
        delta_t_template = 1.0 / 8192
        delta_f_template = None

    else:
        template_domain = "FD"
        delta_f_template = 0.125
        delta_t_template = None

    #print(f"signal domain {signal_domain}, template domain = {template_domain}")

    #print("template_approx = ", template_approx,"     template_domain = ",template_domain)
    approx_type = get_approximant_type(template_approx)
    LAL_params_signal = None
    LAL_params_template = None


    if template_approx == "SEOBNRv4E" or template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt"  or template_approx == "SEOBNRv4E_opt1" or template_approx == "SEOBNRv4EHM_opt1" :
        LAL_params_template = lal.CreateDict()

        EccFphiPNorder = parameters["EccFphiPNorder"]
        EccFrPNorder = parameters["EccFrPNorder"]
        EccWaveformPNorder = parameters["EccWaveformPNorder"]
        EccPNFactorizedForm = parameters["EccPNFactorizedForm"]


        EccBeta = parameters["EccBeta"]
        Ecct0 = parameters["Ecct0"]
        EccNQCWaveform = parameters["EccNQCWaveform"]

        EccPNRRForm = parameters["EccPNRRForm"]
        EccPNWfForm = parameters["EccPNWfForm"]

        EcctAppend = parameters["EcctAppend"]
        EccAvNQCWaveform = parameters["EccAvNQCWaveform"]

        #print(EccFphiPNorder, EccFrPNorder, EccWaveformPNorder, EccPNFactorizedForm)
        #print(EccBeta,Ecct0,EccNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

        lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
        lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
        lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)

        lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
        lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)
        lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
        lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)


        if signal_approx =="SEOBNRv4E" or signal_approx =="SEOBNRv4E_opt" or signal_approx =="SEOBNRv4EHM_opt" or signal_approx =="SEOBNRv4E_opt1" or signal_approx =="SEOBNRv4EHM_opt1":
            LAL_params_signal = lal.CreateDict()

            lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_signal, EccFphiPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_signal, EccFrPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_signal, EccWaveformPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_signal, EccPNFactorizedForm)

            lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_signal, EccBeta)
            lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_signal, Ecct0)
            lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_signal, EccNQCWaveform)

            lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_signal, EccPNRRForm)
            lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_signal, EccPNWfForm)

            lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_signal, EcctAppend)
            lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_signal, EccAvNQCWaveform)



    elif template_approx == "IMRPhenomXEv1":
        LAL_params_template = lal.CreateDict()

        Nharmonic = parameters["Nharmonic"]
        kAdvance = parameters["kAdvance"]
        lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template,Nharmonic)
        lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)

    # Check how we are going to get the signal
    if signal_approx == "NR_hdf5":
        if NR_file:
            fp = h5py.File(NR_file, "r")
            freq_1M = fp.attrs["f_lower_at_1MSUN"]
            # Add rounding to prevent some errors in the calculation of the symmetric mass ratio (for TEOBResumSE)
            m1_dim = round(fp.attrs["mass1"], 5)
            m2_dim = round(fp.attrs["mass2"], 5)
            e0_signal = round(fp.attrs["eccentricity"], 4)
            if e0_signal > 1. or e0_signal<0 : # In case the EOB NR waveform
                e0_signal = 0.0
            mean_anomaly_signal = round(fp.attrs["mean_anomaly"], 4)
            q=m1_dim/m2_dim

            fp.close()
            prefix = os.path.basename(NR_file).split(".")[0]
            if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
                return
            LAL_params_signal = lal.CreateDict()
            ma = lalsim.SimInspiralCreateModeArray()

            # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---


            if mode_array_signal:
                for l,m in mode_array_signal:
                    lalsim.SimInspiralModeArrayActivateMode(ma, l, m)
            elif (
                template_approx == "SEOBNRv4"
                or template_approx == "TEOBResumSE"
                or template_approx == "SEOBNREv4"
                or template_approx == "SEOBNRv4E"
                or template_approx == "SEOBNRv4E_opt"
                or template_approx == "IMRPhenomXEv1"
            ):
                lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
                lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
            else:
                for ell in range(2, ellMax + 1):
                    lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
            lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
            lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)
            (
                s1x,
                s1y,
                s1z,
                s2x,
                s2y,
                s2z,
            ) = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(
                0.0, 100, NR_file
            )  # Since we are using f_ref=0, total mass does not matter

            # Compute orbital frequency and phase from the NR simulation
            tHorizon, omega_orb, phase_orb = compute_OmegaOrbfromNRfile(NR_file)
            iphase_orb = InterpolatedUnivariateSpline(tHorizon, phase_orb)

            # Compute maxima and minima from the orbital frequency and the orbital eccentricity
            tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima = compute_MaxMin(tHorizon, omega_orb)


            if len(tH_maxima)>3:

                iomega_orb_maxima = InterpolatedUnivariateSpline(tH_maxima, omega_orb_maxima)
                iomega_orb_minima = InterpolatedUnivariateSpline(tH_minima, omega_orb_minima)
                omegaOrb_maxima = iomega_orb_maxima(tHorizon)
                omegaOrb_minima = iomega_orb_minima(tHorizon)

                ecc_omegaorb = (np.sqrt(omegaOrb_maxima)-np.sqrt(omegaOrb_minima))/(np.sqrt(omegaOrb_maxima)+np.sqrt(omegaOrb_minima))
                iecc_omegaorb = InterpolatedUnivariateSpline(tHorizon, ecc_omegaorb)

                # Compute the orbit averaged orbital frequency
                omegaOrb_av =1./(tH_maxima[1:]-tH_maxima[:-1])*(iphase_orb(tH_maxima[1:])-iphase_orb(tH_maxima[:-1]))
                iOmegaOrb_av = InterpolatedUnivariateSpline(tH_maxima[1:], omegaOrb_av)
                omegaOrbAv = iOmegaOrb_av(tHorizon)

                if minimization_type == "eccentricity_22mode_periastronFrequency":

                    #omega0_min = iOmegaOrb_av(tH_maxima[0])
                    ecc0_min = iecc_omegaorb(tH_maxima[0])

                    # This would be the correct calculation but it does not work, see below.
                    #omega0_min = omega_orb_maxima[0]/np.pi
                    #freq_1M = omega0_min / lal.MTSUN_SI

                    # This calculation does not seem to be consistent, but otherwise the NR simulation is too short
                    # to take the orbital frequency at first perihelion passage as starting frequency
                    omega0_min=iomega_orb_maxima(tH_maxima[0])/np.pi*2
                    freq_1M_max = omega0_min/ np.pi / lal.MTSUN_SI

                    # Cross check whether the NR waveform will be generated at 20 solar masses
                    #fStart0 = freq_1M/20.
                    #fStart = freq_1M_max/20.
                    #if (fStart*(1.+1e-5) < freq1)


                if minimization_type == "eccentricity_22mode_averageFrequency":
                    # Use orbital averaged orbital frequency as the starting frequency
                    omega0_min = iOmegaOrb_av(tH_maxima[0])
                    ecc0_min = iecc_omegaorb(tH_maxima[0])
                    # This calculation does not seem to be consistent, but otherwise the NR simulation is too short
                    # to take the orbital frequency at first perihelion passage as starting frequency
                    freq_1M_av = omega0_min/ np.pi / lal.MTSUN_SI

                    #print(freq_1M,freq_1M_av)
                # print(m1_dim, m2_dim, s1x, s1y, s1z, s2x, s2y, s2z)
        else:
            print(
                "When using the 'NR_hdf5' approximant, you must provide the NR_file option!"
            )
            exit(-1)

    elif signal_approx == "NR_custom" and template_approx == "NR_custom":
        # If we are here, the assumption is that
        # we are compairing 2 NR wfs.
        prefix = os.path.basename(NR_file).split(".")[0]
        if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
            return
        (
            modes_template,
            t_template,
            _,
        ) = get_NR_data(NR_file_2, ell_max=ellMax)
        (
            modes_signal,
            t_signal,
            physical_params,
        ) = get_NR_data(NR_file, ell_max=ellMax)
        m1_dim = physical_params["m1_dim"]
        m2_dim = physical_params["m2_dim"]
        s1x, s1y, s1z = physical_params["chi1"]
        s2x, s2y, s2z = physical_params["chi2"]
        freq_1M = physical_params["freq_1M"]

    elif signal_approx == "NR_v4":
        # If we are here, the assumption is that
        # we are using one of the old SEOBNRv4 h22 NR files as a signal.
        prefix = os.path.basename(NR_file).split(".")[0]
        if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
            return
        # Parse list of NRfiles in the directory
        path_data = os.environ['SEOBNRv4_NRPATH']   #path_data="/home/antoniramosbuades/git/seobnrv4e_expressions/h22NRs/"
        NRdatfiles=glob.glob(path_data+"/*.dat")
        param_file=path_data+"/NR_parameters.txt"

        # Read the file containing all the parameters of the simulation
        f=open(param_file,"r")
        lines=f.readlines()
        param_list=[]
        for x in lines:
            #print(x)
            param_list.append(x.split())
        f.close()

        #print(NR_file)
        q0_list,chi1z_list,chi2z_list = NRparameter_list(NRdatfiles, param_list )
        #print(param_list)


        modes_signal, t_signal, physical_params = get_NR_v4data(NR_file, q0_list,chi1z_list, chi2z_list)

        m1_dim = physical_params["m1_dim"]
        m2_dim = physical_params["m2_dim"]
        q=m1_dim/m2_dim
        s1z = physical_params["chi1"]
        s1x, s1y, s2x, s2y = 0.,0.,0.,0,
        s2z = physical_params["chi2"]
        freq_1M = physical_params["freq_1M"]
        e0_signal = 0.0
        mean_anomaly_signal = 0.0



    else:
        # We are not using NR as the signal.
        q = parameters["q"]
        s1x, s1y, s1z = parameters["chi1"]
        s2x, s2y, s2z = parameters["chi2"]
        freq_1M = parameters["freq_1M"]
        prefix = parameters["case"]

        if "eccentricity" in parameters:
            e0_signal = parameters["eccentricity"]
        else:
            e0_signal = 0.0
        if "mean_anomaly" in parameters:
            mean_anomaly_signal = parameters["mean_anomaly"]
        else:
            mean_anomaly_signal = 0.0

        m1_dim = q / (1.0 + q)
        m2_dim = 1 - m1_dim
        if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
            return
        if signal_approx == "NRSur7dq4":
            ma = lalsim.SimInspiralCreateModeArray()
            for ell in range(2, ellMax + 1):
                lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
    Mtotals = np.arange(20, 220, 20)

    # Mtotals = [64.0]
    dist = 1e6 * lal.PC_SI * 500  # Doesn't matter what this is
    res = []
    # First set the parameters using a feducial total mass
    M_fed = 60.0
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    f_ref = freq_1M / (m1 + m2)
    f_min = 1 * f_ref

    params_signal = waveform_params(
        m1,
        m2,
        s1x,
        s1y,
        s1z,
        s2x,
        s2y,
        s2z,
        iota_s,
        phi_s,
        f_ref,
        f_min,
        dist,
        delta_t_signal,
        delta_f_signal,
        LAL_params_signal,
        signal_approx,
        signal_domain,
        e0_signal,
        mean_anomaly_signal,
    )

    if approx_type == "aligned":
        if np.abs(s1x) < 1e-3:
            s1x = 0
        if np.abs(s1y) < 1e-3:
            s1y = 0
        if np.abs(s2x) < 1e-3:
            s2x = 0
        if np.abs(s2y) < 1e-3:
            s2y = 0




    if np.abs(s1x) < 1e-3 and  np.abs(s1y) < 1e-3 and np.abs(s2x) < 1e-3 and np.abs(s2y) < 1e-3 :
        s1x = 0
        s1y = 0
        s2x = 0
        s2y = 0

        approx_type = "aligned"



    if template_approx == "SEOBNRv4E" or template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt"\
     or template_approx == "SEOBNRv4E_opt1" or template_approx == "SEOBNRv4EHM_opt1":

        params_template = waveform_params(
                            m1,
                            m2,
                            0.,
                            0.,
                            s1z,
                            0.,
                            0.,
                            s2z,
                            iota_s,
                            phi_s,
                            f_ref,
                            f_min,
                            dist,
                            delta_t_template,
                            delta_f_template,
                            LAL_params_template,
                            template_approx,
                            template_domain,
                            e0_signal,
                            mean_anomaly_signal,
                            EccFphiPNorder,
                            EccFrPNorder,
                            EccWaveformPNorder,
                            EccPNFactorizedForm,
                        )

    elif template_approx =="IMRPhenomXEv1":
        if f_ref > 10.0:
            f_ref = 10.0 # Define eccentricity at 10Hz
        params_template = waveform_params(
                            m1,
                            m2,
                            s1x,
                            s1y,
                            s1z,
                            s2x,
                            s2y,
                            s2z,
                            iota_s,
                            phi_s,
                            f_ref,
                            f_min,
                            dist,
                            delta_t_template,
                            delta_f_template,
                            LAL_params_template,
                            template_approx,
                            template_domain,
                            e0_signal,
                            mean_anomaly_signal,
                            Nharmonic ,
                            kAdvance ,
                            )
    else:
        params_template = waveform_params(
                            m1,
                            m2,
                            s1x,
                            s1y,
                            s1z,
                            s2x,
                            s2y,
                            s2z,
                            iota_s,
                            phi_s,
                            f_ref,
                            f_min,
                            dist,
                            delta_t_template,
                            delta_f_template,
                            LAL_params_template,
                            template_approx,
                            template_domain,
                            e0_signal,
                            mean_anomaly_signal,
                        )

    #NR_duration = get_NR_duration(params_signal,params_template, NR_file,ma,ellMax)


    if template_approx in EOB_approximants:
        # For EOB which supports modes we iterate f_min until we get the same time to merger
        if signal_approx == "NR_hdf5":
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, NR_file, ma
            )
        elif signal_approx == "NRSur7dq4":
            f_min_EOB = iterate_time_to_merger(params_signal, params_template, None, ma)
        elif signal_approx == "SEOBNRv4PHM":
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, None, None
            )

        """print(
            "The EOB f_min={}, NR f_min = {}".format(
                f_min_EOB * M_fed, params_signal.f_min * M_fed
            )
        )"""
        time_EOB, modes_EOB = get_final_modes(f_min_EOB, params_template, ellMax)

    """if   template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt":

        time_EOB, modes_EOB=SEOBNRv4EHM_modes(params_template, EccFphiPNorder, EccFrPNorder, EccWaveformPNorder,
                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm,
                                              EccPNWfForm, EccAvNQCWaveform, EcctAppend)
    """
        #modes_dc = interpolate_mode_dict_and_rescale_v4EHM( modes_EOB, time_EOB, params_template)

    # Is this condition necessary for TD eccentric approxs ?
    # Eccentricity varies the length of the waveforms. Should we impose that both the signal and the template have the same time from t0 to t_peak  ?
    # Currently we are doing nothing with f_min_EOB

    if template_approx in eEOB_approximants:
        # As these are non-precessing approximants one should not modify f_min of the template such that the length of the template matches the length of the signal
        f_min_EOB=params_signal.f_min



    # Check how we are going to get the signal
    if template_approx == "NR_hdf5":
        if NR_file_2:
            fp = h5py.File(NR_file_2, "r")
            freq_1M = fp.attrs["f_lower_at_1MSUN"]
            # Add rounding to prevent some errors in the calculation of the symmetric mass ratio (for TEOBResumSE)
            m1_dim = round(fp.attrs["mass1"], 5)
            m2_dim = round(fp.attrs["mass2"], 5)
            e0_template = round(fp.attrs["eccentricity"], 4)
            mean_anomaly_template = round(fp.attrs["mean_anomaly"], 4)
            q=m1_dim/m2_dim

            fp.close()
            prefix = os.path.basename(NR_file_2).split(".")[0]

            LAL_params_template = lal.CreateDict()
            ma_t = lalsim.SimInspiralCreateModeArray()

            # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
            if mode_array_template:
                for l,m in mode_array_template:
                    lalsim.SimInspiralModeArrayActivateMode(ma_t, l, m)
            elif (
                template_approx == "SEOBNRv4"
                or template_approx == "TEOBResumSE"
                or template_approx == "SEOBNREv4"
                or template_approx == "SEOBNRv4E"
                or template_approx == "SEOBNRv4E_opt"
                or template_approx == "IMRPhenomXEv1"
            ):
                lalsim.SimInspiralModeArrayActivateMode(ma_t, 2, 2)
                lalsim.SimInspiralModeArrayActivateMode(ma_t, 2, -2)
            else:
                for ell in range(2, ellMax + 1):
                    lalsim.SimInspiralModeArrayActivateAllModesAtL(ma_t, ell)
            lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_template, ma_t)
            lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_template, NR_file_2)
            (
                s1x,
                s1y,
                s1z,
                s2x,
                s2y,
                s2z,
            ) = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(
                0.0, 100, NR_file_2
            )  # Since we are using f_ref=0, total mass does not matter

            params_template = waveform_params(
                m1,
                m2,
                s1x,
                s1y,
                s1z,
                s2x,
                s2y,
                s2z,
                iota_s,
                phi_s,
                f_ref,
                f_min,
                dist,
                delta_t_template,
                delta_f_template,
                LAL_params_template,
                template_approx,
                template_domain,
                e0_template,
                mean_anomaly_template,
            )

        else:
            print(
                "When using the 'NR_hdf5' approximant, you must provide the NR_file option!"
            )
            exit(-1)



    """if template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt" or template_approx=="SEOBNRv4HM":
        fmin_NRlength =get_fminEOB_NRlength_fast(f_min, params_template, NR_duration)
        params_template.f_min = fmin_NRlength
        params_template.f_ref = params_template.f_min

        time_EOB, modes_EOB=SEOBNRv4EHM_modes(params_template, EccFphiPNorder, EccFrPNorder, EccWaveformPNorder,
                                      EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm,
                                      EccPNWfForm, EccAvNQCWaveform, EcctAppend)
    """


    for M_tot in Mtotals:

        m1 = m1_dim * M_tot
        m2 = m2_dim * M_tot


        #f_min = freq_1M / M_tot

        f_min =  freq_1M /(m1 + m2)
        f_ref = f_min
        flow = 1.05* f_min
        if flow < 10:
            flow = 10

        # Check consistency of eccentricity in the templates

        if e0_signal > 0.0:  # signal is eccentric!
            e0max_signal = e0Newt(f_min, e0_signal, f_ref)
        else:
            e0max_signal = 0.0

        if e0max_template > 0.0:  # template is eccentric!
            eNewt_template = e0Newt(f_min, e0max_template, f_ref)
            if e0max_template > eNewt_template:
                e0max_template = eNewt_template

        if e0_signal == 0.0:
            e0max_template = 0.1  # Set it to smaller value, such that it does not take too long to compute

        # Update the params
        params_template.m1 = m1
        params_template.m2 = m2
        params_signal.m1 = m1
        params_signal.m2 = m2

        params_signal.f_min = f_min
        params_signal.f_ref = f_ref

        if minimization_type == "eccentricity_22mode_periastronFrequency":
            f_min =  freq_1M_max /(m1 + m2)
            f_ref = f_min
        #    flow = 1.0 * f_min
        #    if flow < 10:
        #          flow = 10
        elif minimization_type == "eccentricity_22mode_averageFrequency":
            f_min =  freq_1M_av /(m1 + m2)
            f_ref = f_min


        params_template.f_min = f_min
        params_template.f_ref = f_ref

        if signal_approx == "NR_custom" or signal_approx == "NR_v4" :
            modes_resc = interpolate_mode_dict_and_rescale(
                modes_signal, t_signal, params_signal
            )
            sp, sc = combine_modes(iota_s, phi_s, modes_resc)
            # Taper
            # print(modes_signal['2,2'],t_signal)
            # print(sp,params_signal.delta_t)
            sp_td = TimeSeries(sp, delta_t=params_signal.delta_t)
            sc_td = TimeSeries(sc, delta_t=params_signal.delta_t)
            sp_td = taper_timeseries(sp_td, tapermethod="startend")
            sc_td = taper_timeseries(sc_td, tapermethod="startend")
            s = sp_td * np.cos(kappa_s) + sc_td * np.sin(kappa_s)

        else:
            #print("Signal generation")
            sp, sc = generate_waveform(params_signal)
            # Use SimInspiralFD for a better conditioning of the eccentric waveform in FD domain
            #sp, sc = generate_EccNRwaveform(params_signal)
            s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)
        #print("Signal generated")

        if isinstance(s, pt.TimeSeries):
            N = len(s)
            pad = int(2 ** (np.floor(np.log2(N)) + 2))
            s.resize(pad)
        s_tilde = make_frequency_series(s)

        amp_signal_FD=np.abs(s_tilde.data)
        imax= np.argmax(amp_signal_FD)
        freq_peak=s_tilde.get_sample_frequencies()[imax]

        # Set flow for mismatch integral calculation as twice the peak of the strain to avoid FT artifacts
        #if 2.*freq_peak > fhigh:
        #    flow=2.*f_min
        #else:
        #    flow=2.*freq_peak

        if flow < 10:
            flow = 10

        psd = generate_psd(len(s_tilde), s_tilde.delta_f, flow)
        SNR = _filter.sigma(
            s_tilde, psd=psd, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh
        )





        if template_approx in EOB_approximants:
            # We have to interpolate the modes to the correct time spacing and
            # rescale them
            modes_dc = interpolate_mode_dict_and_rescale(
                modes_EOB, time_EOB, params_template
            )

            final = dual_annealing(
                min_func,
                ((0, 2 * np.pi),),
                args=(
                    s,
                    params_template,
                    kappa_s,
                    flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                    "phi",
                ),
                local_search_options={"method": "Nelder-Mead"},
                maxfun=500,
            )
        elif signal_approx == "NR_custom" and template_approx == "NR_custom":
            modes_dc = interpolate_mode_dict_and_rescale(
                modes_template, t_template, params_template
            )
            final = dual_annealing(
                min_func,
                ((0, 2 * np.pi),),
                args=(
                    s,
                    params_template,
                    kappa_s,
                    flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                    "phi",
                ),
                local_search_options={"method": "Nelder-Mead"},
                maxfun=500,
            )

        elif signal_approx == "NR_hdf5" and template_approx == "NR_hdf5":
            modes_dc = None
            final = dual_annealing(
                min_func,
                ((0, 2 * np.pi),),
                args=(
                    s,
                    params_template,
                    kappa_s,
                    1.5*flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                    "phi",
                ),
                local_search_options={"method": "Nelder-Mead"},
                maxfun=500,
            )
        else:
            # We need to avoid local minima
            # Can use dual annealing.
            # However, in 2D it's easier to just brute force
            # Use a 30x30 grid
            if minimization_type == "phi": # Numerical optimization over the coalescence phase of the template
                params_template.ecc=0.0
                params_template.mean_anomaly=0.0
                bounds = ((0, 2 * np.pi),)
            elif minimization_type == "reference_frequency": # Numerical optimization over reference_frequency and coalescence phase of the template
                params_template.ecc=0.0
                params_template.mean_anomaly=0.0
                bounds = ((0.8 * f_ref, 1.2 * f_ref), (0, 2 * np.pi))
            elif minimization_type == "spin_rotation":  # Numerical optimization over spin_rotation and coalescence phase of the template
                params_template.ecc=0.0
                params_template.mean_anomaly=0.0
                bounds = ((0, 2 * np.pi), (0, 2 * np.pi))

            elif minimization_type == "eccentricity": # Numerical optimization over the initial eccentricity and coalescence phase of the template
                bounds = ((0, 2 * np.pi), (0, e0max_template))

            elif minimization_type == "eccentricity_22mode":  # Numerical optimization over the initial eccentricity of the template
                bounds = ((0, e0max_template),)

            elif minimization_type == "eccentricity_22mode_periastronFrequency" or "eccentricity_22mode_averageFrequency":
                bounds = ((0, e0max_template),)

            elif minimization_type == "eccentricity_22mode_reference_frequency":  # Numerical optimization over the initial eccentricity of the template
                bounds = ((0, e0max_template),
                          (0.8 * f_ref, 1.2 * f_ref),
                         )
            elif minimization_type == "eccentricity_22mode_minimum_frequency":
                bounds = ((0, e0max_template),
                          (0.8 * f_ref, 1.2 * f_ref),
                          )
            elif minimization_type == "eccentricity_meanAnomaly_22mode_reference_frequency":
                bounds = (
                    (0, e0max_template),
                    (0, 2 * np.pi),
                    (0.8 * f_ref, 1.2 * f_ref),
                )

            elif minimization_type == "eccentricity_phi_minimum_frequency":
                bounds = (
                    (0, 2 * np.pi),
                    (0, e0max_template),
                    (0.8 * f_ref, 1.2 * f_ref),
                )

            elif minimization_type == "eccentricity_meanAnomaly_22mode":   # Numerical optimization over the initial eccentricity and mean Anomaly of the template
                bounds = ((0, e0max_template), (0, 2 * np.pi))

            elif minimization_type == "eccentricity_meanAnomaly":   # Numerical optimization over the  coalescence phase, initial eccentricity and mean Anomaly of the template
                bounds = ((0, e0max_template), (0, 2 * np.pi), (0, 2 * np.pi))

            elif minimization_type == "eccentricity_reference_frequency":   # Numerical optimization over the  coalescence phase, initial eccentricity and reference_frequency of the template
                bounds = (
                    (0, 2 * np.pi),
                    (0, e0max_template),
                    (0.8 * f_ref, 1.2 * f_ref),
                )

            elif minimization_type == "eccentricity_spin_rotation": # Numerical optimization over the  coalescence phase, initial eccentricity and spin rotation of the template
                bounds = ((0, 2 * np.pi), (0, e0max_template), (0, 2.0 * np.pi))

            elif minimization_type == "eccentricity_mean_anomaly_reference_frequency":
                bounds = (
                    (0, 2 * np.pi),
                    (0, e0max_template),
                    (0, 2 * np.pi),
                    (0.8 * f_ref, 1.2 * f_ref),
                )

            elif minimization_type == "eccentricity_mean_anomaly_spin_rotation":
                bounds = (
                    (0.0, 2.0 * np.pi),
                    (0, e0max_template),
                    (0, 2 * np.pi),
                    (0, 2 * np.pi),
                )

            elif minimization_type == "QC22mode": # No numerical optimization as the coalescence phase and the polarization are degenerate
                params_template.ecc=0.0
                params_template.mean_anomaly=0.0
                bounds = None
                simple = False
            elif minimization_type == "QC22mode_v4E": # No numerical optimization as the coalescence phase and the polarization are degenerate
                params_template.ecc= params_signal.ecc
                params_template.mean_anomaly= params_signal.mean_anomaly
                bounds = None
                simple = False

            else:
                raise NotImplementedError


            if template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt" or template_approx=="SEOBNRv4HM":


                #fmin_NRlength =get_fminEOB_NRlength_fast(f_min, params_template, NR_duration)
                #params_template.f_min = fmin_NRlength
                #params_template.f_ref = params_template.f_min
                #print(f'fmin_NRlength = {fmin_NRlength},  f_min =  {f_min}')
                if template_approx == "SEOBNRv4HM":
                    EccFphiPNorder, EccFrPNorder, EccWaveformPNorder,   EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform, EcctAppend = 99, 99, 99, 99,99,  99, 99, 99, 99,99, 99

                time_EOB, modes_EOB=SEOBNRv4EHM_modes(params_template, EccFphiPNorder, EccFrPNorder, EccWaveformPNorder,
                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm,
                                              EccPNWfForm, EccAvNQCWaveform, EcctAppend)

                modes_dc = interpolate_mode_dict_and_rescale_v4EHM( modes_EOB, time_EOB, params_template)
            else:
                modes_dc = None


            #if template_approx == "SEOBNRv4" or template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt" or template_approx=="SEOBNRv4HM":
            #    if flow != 10:
            #        flow=2*flow


            # List the different minimization types. Only non-precessing tested!
            ecc_minAS_list = ["eccentricity",  "eccentricity_22mode", "eccentricity_meanAnomaly_22mode","eccentricity_meanAnomaly" ,"eccentricity_22mode_periastronFrequency","eccentricity_22mode_averageFrequency"]
            ecc_minPrec_list = ["eccentricity_reference_frequency", "eccentricity_spin_rotation", "eccentricity_mean_anomaly_reference_frequency", "eccentricity_mean_anomaly_spin_rotation"]

            if approx_type == "precessing":
                final = dual_annealing(
                    min_func,
                    bounds,
                    args=(
                        s,
                        params_template,
                        kappa_s,
                        flow,
                        fhigh,
                        unfaithfulness_type,
                        modes_dc,
                        debug,
                        minimization_type,
                    ),
                    local_search_options={"method": "Nelder-Mead"},
                    maxfun=2000,
                )
            elif approx_type == "aligned":
                # We are dealing with algined spin
                # Thus we optimize *only* over the phase numerically
                # This means that the type of min_func is irrelevant
                if simple:

                    final = minimize_scalar(
                        min_func,
                        bounds=(0, 2 * np.pi),
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            "phi",
                        ),
                    )
                elif minimization_type =="QC22mode":
                    # Factor 2 in flow to avoid artifacts due to the short length of some NR waveforms
                    final = unfaithfulness_RC(s, params_template, 2*flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

                elif minimization_type =="QC22mode_v4E":
                    # Factor 2 in flow to avoid artifacts due to the short length of some NR waveforms
                    #print(f"params_template = {params_template}")
                    #print(f"params_signal = {params_signal}")
                    final = unfaithfulness_RC(s, params_template, 1.15*flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)
                    #print(f"Mtot = {M_tot}, mismatch = {final}")
                elif raw_grid == "True":
                    iters=5
                    NN=6

                    #print("Bounds before constraining : ", bounds)
                    bounds, mm_raw, val_min = ecc_nonprecessing_restrictedbounds(s, params_signal,params_template,kappa_s,iters,NN,minimization_type, ecc_minAS_list,e0max_template,fhigh, modes_dc, debug)

                    #print("Constrained bounds before numerical optimization : ",bounds)
                    final = dual_annealing(
                        min_func,
                        bounds,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            minimization_type,
                        ),
                        local_search_options={"method": "Nelder-Mead"},
                        maxfun=500,
                    )

                else:
                    #print('Dual annealing')
                    final = dual_annealing(
                        min_func,
                        bounds,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            2*flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            minimization_type,
                        ),
                        local_search_options={"method": "Nelder-Mead"},
                        maxfun=500,
                    )
                    #print(f"Mtotal = {M_tot},  mismatch = {final.fun}")
            else:
                raise TypeError
        # Return the polished result, as well as the minimum on the grid
        #res.append([q, chi1z, chi2z, M_tot, final.fun, final.x, SNR])

        if minimization_type =="QC22mode":
            res.append([q, s1z, s2z, M_tot, final, SNR, iota_s])
        elif minimization_type =="QC22mode_v4E":
            res.append([q, s1z, s2z, e0_signal, mean_anomaly_signal, M_tot, final, SNR, iota_s])
        elif minimization_type == "eccentricity_22mode_periastronFrequency" or  minimization_type == "eccentricity_22mode_averageFrequency":
            res.append([q, s1z, s2z, M_tot, final.fun, SNR, *final.x, omega0_min, ecc0_min,mean_anomaly_signal, iota_s])
        else:
            res.append([q, s1z, s2z, M_tot, final.fun, SNR, *final.x, e0_signal,mean_anomaly_signal, iota_s])

    res = np.array(res)
    #print(res)
    if save_file:
        if template_approx=="NR_hdf5":
            prefix = os.path.basename(NR_file).split(".h5")[0]
            prefix1 = os.path.basename(NR_file_2).split(".h5")[0]

            np.savetxt("{}__{}__iota{:.3f}_result_{:05d}.dat".format(prefix,prefix1, iota_s, index), res)
        else:
            np.savetxt("{}_q{:.2f}_s1z{:.2f}_s2z{:.2f}_e0{:.5f}_l0{:.3f}_iota{:.3f}_result_{:05d}.dat".format(prefix,q,s1z,s2z,e0_signal,mean_anomaly_signal, iota_s, index), res)
    # return res
