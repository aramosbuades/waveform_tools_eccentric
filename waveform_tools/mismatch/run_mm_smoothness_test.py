#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp


import sys, h5py, glob, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *
from eccentric_waveforms import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSE module
try:
    import EOBRun_module
except:
    pass

import numpy as np, pandas as pd, pycbc, json

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *
from run_one_eccMatch22_nestedOpts import *
#%matplotlib inline


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]

def compute_mismatch(
                    s: Union[pt.TimeSeries, pt.FrequencySeries],
                    q: float,
                    chi1: float,
                    chi2: float,
                    params_template: waveform_params,
                    flow: float,
                    fhigh: float = None,
                    modes_dict: Dict = None,
                    debug: bool = False,
                ) -> Union[float, tuple]:


    # Peri IC. Joblib cannot pick lalDict
    LAL_params_template = lal.CreateDict()
    EccIC=0
    lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)

    params_template.wf_param=LAL_params_template
    M_fed=params_template.m1 + params_template.m2

    m1_dim = q/(1.+q)
    m2_dim = 1./(1.+q)

    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed

    params_template.m1 = m1
    params_template.m2 = m2

    params_template.s1z = chi1
    params_template.s2z = chi2


    final = unfaithfulness_RC(s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

    return q, params_template.s1z, params_template.s2z, final


# Generate signal waveforms

def generate_signals_for_q(q: float,
                    params_signal: waveform_params
                    ) -> Union[float, tuple]:


    LAL_params_signal = lal.CreateDict()
    EccIC=0
    lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_signal, EccIC)

    params_signal.wf_param=LAL_params_signal
    M_fed=params_signal.m1 + params_signal.m2
    m1_dim = q/(1.+q)
    m2_dim = 1./(1.+q)

    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed

    params_signal.m1 = m1
    params_signal.m2 = m2

    # generate signal for  fixed total mass
    sp, sc = generate_waveform(params_signal)
    s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)
    return s

## Output NQC coefficients

def SEOBNRv4EHM_modes(params: waveform_params, EccFphiPNorder: int, EccFrPNorder: int, EccWaveformPNorder: int,
                      EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int,
                      EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int, EcctAppend: float):



    HypPphi0, HypR0, HypE0 =[0.,0,0]
    EccIC=0
    eccentric_anomaly = 0.0

    if params.approx=="SEOBNRv4E_opt" or  params.approx=="SEOBNRv4":
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
        SpinAlignedVersion=4
    else:
        SpinAlignedVersion=41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary

    if params.approx == "SEOBNRv4E_opt" or params.approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(params.delta_t,
                                                              params.m1*lal.MSUN_SI,
                                                              params.m2*lal.MSUN_SI,
                                                              params.f_min,
                                                              params.distance,
                                                              params.s1z,
                                                              params.s2z,
                                                              params.ecc,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:



        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(params.delta_t,
                                                              params.m1*lal.MSUN_SI,
                                                              params.m2*lal.MSUN_SI,
                                                              params.f_min,
                                                              params.distance,
                                                              params.s1z,
                                                              params.s2z,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)





    hlm={}
    M_fed= params.m1 + params.m2
    dMpc = params.distance/(1e6*lal.PC_SI)

    if SpinAlignedVersion==4:
        hlm['2,2'] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm['2,-2'] = np.conjugate(hlm['2,2'])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h55 )

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h44 )

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h21 )

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h33 )

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[f"{modeL},{modeM}"] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h22 )


    time_array = np.arange(0,len(hlm['2,2'])*params.delta_t, params.delta_t)
    timeNR = SectotimeM(time_array,M_fed)
    #return time_array, hlm
    return timeNR, hlm, nqcCoeffsInput.data

def interpolate_mode_dict_and_rescale_v4EHM(
    modes_EOB: Dict, t_EOB: np.array, params_template: waveform_params
) -> Dict:
    """Interpolate a set of modes and rescale them to SI units

    Args:
        modes_EOB (Dict): The waveform modes
        t_EOB (np.array): Time in geometric units
        params_template (waveform_params): The parameters of the waveform. Specifies
            the time resolution via delta_t.

    Returns:
        Dict: The waveform modes at the desired timespacing
    """
    dc_rescaled = {}

    M_fed= params_template.m1 + params_template.m2
    dMpc = params_template.distance/(1e6*lal.PC_SI)
    #t_EOB_rescaled = t_EOB * (params_template.m1 + params_template.m2) * lal.MTSUN_SI
    t_EOB_rescaled = timeMtoSec(t_EOB,M_fed)

    new_times = np.arange(0, len(modes_EOB['2,2'])*params_template.delta_t, params_template.delta_t)

    #amp_factor = (
    #    (params_template.m1 + params_template.m2)
    #    * lal.MRSUN_SI        / params_template.distance    )
    for key in modes_EOB.keys():
        #print(key)
        ell, m = [int(x) for x in key.split(",")]
        intrp_re = InterpolatedUnivariateSpline(t_EOB_rescaled, np.real(modes_EOB[key]))
        intrp_im = InterpolatedUnivariateSpline(t_EOB_rescaled, np.imag(modes_EOB[key]))

        dc_rescaled[key] = AmpNRtoPhysicalTD((intrp_re(new_times) +1.j * intrp_im(new_times)), M_fed,dMpc)
    return dc_rescaled



def generate_signals_for_q_nqcCoeffs(q: float,
                    params_signal: waveform_params
                    ) -> Union[float, tuple]:


    M_fed=params_signal.m1 + params_signal.m2
    m1_dim = q/(1.+q)
    m2_dim = 1./(1.+q)

    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed

    params_signal.m1 = m1
    params_signal.m2 = m2

    EccFphiPNorder = EccFrPNorder=99
    EccWaveformPNorder=16
    EccBeta=0.09
    Ecct0=100
    EccPNFactorizedForm = EccNQCWaveform = EccPNRRForm = EccPNWfForm = EccAvNQCWaveform =1
    EcctAppend = 1


    # generate signal from the modes
    time_EOB, modes_EOB, nqcCoeffs_s =SEOBNRv4EHM_modes(params_signal, EccFphiPNorder, EccFrPNorder,
                                                        EccWaveformPNorder,
                                          EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm,
                                          EccPNWfForm, EccAvNQCWaveform, EcctAppend)

    modes_dc = interpolate_mode_dict_and_rescale_v4EHM( modes_EOB, time_EOB, params_signal)


    # Combine modes
    sp_t, sc_t = combine_modes(params_signal.iota,np.pi/2-params_signal.phi, modes_dc)
    # pi/2 to agree with the LAL convention

    # Taper
    sp_td = TimeSeries(sp_t, delta_t=params_signal.delta_t)
    sc_td = TimeSeries(sc_t, delta_t=params_signal.delta_t)
    sp_td = taper_timeseries(sp_td, tapermethod="startend")
    sc_td = taper_timeseries(sc_td, tapermethod="startend")

    sp, sc = sp_td, sc_td
    s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)

    return s, nqcCoeffs_s


    # Compute mismatch varying template spins
def compute_mismatch_nqcCoeffs(
                    s: Union[pt.TimeSeries, pt.FrequencySeries],
                    nqcCoeffs_s: np.array,
                    q: float,
                    chi1: float,
                    chi2: float,
                    params_template: waveform_params,
                    flow: float,
                    fhigh: float = None,
                    debug: bool = False,
                ) -> Union[float, tuple]:


    # Peri IC. Joblib cannot pick lalDict
    #LAL_params_template = lal.CreateDict()
    #EccIC=0
    #lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)

    params_template.wf_param=LAL_params_template
    M_fed=params_template.m1 + params_template.m2

    m1_dim = q/(1.+q)
    m2_dim = 1./(1.+q)

    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed

    params_template.m1 = m1
    params_template.m2 = m2

    params_template.s1z = chi1
    params_template.s2z = chi2


    EccFphiPNorder = EccFrPNorder=99
    EccWaveformPNorder=16
    EccBeta=0.09
    Ecct0=100
    EccPNFactorizedForm = EccNQCWaveform = EccPNRRForm = EccPNWfForm = EccAvNQCWaveform =1
    EcctAppend = 1


    # generate signal from the modes
    time_EOB, modes_EOB, nqcCoeffs_t =SEOBNRv4EHM_modes(params_template, EccFphiPNorder, EccFrPNorder,
                                                        EccWaveformPNorder, EccPNFactorizedForm, EccBeta, Ecct0,
                                                        EccNQCWaveform, EccPNRRForm, EccPNWfForm,
                                                        EccAvNQCWaveform,      EcctAppend)

    modes_dc = interpolate_mode_dict_and_rescale_v4EHM( modes_EOB, time_EOB, params_template)





    final = unfaithfulness_RC(s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

    nqcCoeffs_s_list = [item for item in nqcCoeffs_s]
    nqcCoeffs_t_list = [item for item in nqcCoeffs_t]
    out0 = [q, params_template.s1z, params_template.s2z, final]
    out0.append(nqcCoeffs_t_list)
    out0.append(nqcCoeffs_s_list)

    # Flatten output list
    out = []
    for item in out0:
        #print(item)
        if isinstance(item,list):
            for item1 in item:
                out.append(item1)
        else:
            out.append(item)


    return out


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    # Arguments of the test
    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    #p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )
    p.add_argument("--chi1_s", type=float,  help="chi_1 signal", default=0.8, )
    p.add_argument("--chi2_s", type=float,  help="chi_2 signal", default=0.8, )

    p.add_argument("--approximant", type=str,  help="Waveform approximant (default SEOBNRv4E_opt)", default="SEOBNRv4E_opt", )
    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)

    p.add_argument("--idx_chunk",type=int,help="Subset of cases to run",default=1)

    args = p.parse_args()


    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)

    if run_dir[-1]!="/":
        run_dir +='/'



    # We want to compute the unfaithfulness between SEOBNRv4E and SEOBNRv4E varying the spins between [0.8-0.99]
    # and check the smoothness of the unfaithfulness function

    # Extrinsic parameters
    phi_s = 0.
    kappa_s =0.
    iota_s = np.pi/3.


    dMpc= 500
    dist = 1e6 * lal.PC_SI * dMpc  # Doesn't matter what this is
    fhigh=2048.0

    # Choose approximants

    #template_approx="TEOBResumSE"
    template_approx=args.approximant
    signal_approx=template_approx

    signal_domain = "TD"
    delta_t_signal = 1./(4*2048.)
    delta_f_signal = None


    template_domain = "TD"
    delta_t_template = delta_t_signal
    delta_f_template = delta_f_signal

    LAL_params_template = None
    LAL_params_template = lal.CreateDict()

    # Peri IC
    EccIC=0
    lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)
    LAL_params_signal = LAL_params_template

    # Total Mass =100 and f_min =20Hz, e0 = 0.3
    M_fed = 100.0
    f_min = 20.0
    e0 = 0.3
    f_ref = 1. * f_min
    flow = f_min

    # Unfaithfulness parameters
    debug=False
    modes_dc=None

    q_min= args.qMin
    q_max= args.qMax
    Nq=args.Nq

    q_grid = np.linspace(q_min, q_max, Nq)

    Nchi = args.Nchi
    chi_min = args.chiMin
    chi_max = args.chiMax

    chi1_grid = np.linspace(chi_min, chi_max, Nchi)
    chi2_grid = np.linspace(chi_min, chi_max, Nchi)
    x, y,z = np.meshgrid(q_grid, chi1_grid,chi2_grid)

    x = x.flatten()
    y = y.flatten()
    z = z.flatten()

    NN = len(x)

    idx_list = np.arange(0,NN)
    Nchunks =args.Nchunks
    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    idx_chunk=args.idx_chunk

    case0=cases_per_slurm_job[idx_chunk]
    cases_per_output = list(chunks(case0,Nchunks))[0]


    # As a signal we a configuration in the middle interval in [0.8-0.99]
    chi1_s = args.chi1_s
    chi2_s = args.chi2_s
    m1, m2 = 50, 50
    # Initialize signal parameters
    params_signal = waveform_params(
        m1, m2,
        0., 0., chi1_s,
        0.,0., chi2_s,
        iota_s, phi_s,
        f_ref, f_min,
        dist, delta_t_signal,  delta_f_signal,
        LAL_params_signal,
        signal_approx, signal_domain,
        ecc=e0
    )

    # Initialize template parameters
    params_template = waveform_params(
        m1, m2,
        0., 0., chi1_s,
        0.,0., chi2_s,
        iota_s, phi_s,
        f_ref, f_min,
        dist, delta_t_template, delta_f_template,
        LAL_params_template,
        template_approx, template_domain,
       ecc= e0
    )
    # Compute the signals
    params_signal.wf_param=None

    """grid_s_signals = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
                    delayed(generate_signals_for_q)(
                        q_grid[i],
                        params_signal
                    )
                    for i in range(len(q_grid))
                    )
    grid_s_signals = np.array(grid_s_signals,dtype= tuple)
    s_q_list, y,z = np.meshgrid(grid_s_signals, chi1_grid,chi2_grid)
    s_q_list = s_q_list.flatten()
    y = y.flatten()
    z = z.flatten()
    """

    grid_s_signals = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
                    delayed(generate_signals_for_q_nqcCoeffs)(
                        q_grid[i],
                        params_signal
                    )
                    for i in range(len(q_grid))
                    )

    grid_s_signals = np.array(grid_s_signals,dtype= tuple)

    signals_grid=grid_s_signals[:,0]
    nqcCoeffs_s_grid=grid_s_signals[:,1]
    x, y,z = np.meshgrid(q_grid, chi1_grid,chi2_grid)
    #s_q_list,nqcCoeffs_s_list, x = np.meshgrid(signals_grid, nqcCoeffs_s_grid, q_grid)

    #s_q_list = s_q_list.flatten()
    #nqc_s_list = nqcCoeffs_s_list.flatten()
    x = x.flatten()
    y = y.flatten()
    z = z.flatten()

    nqcCoeffs_s_dict={}; signals_dict={};
    for i in range(len(q_grid)):
        nqcCoeffs_s_dict[q_grid[i]] = nqcCoeffs_s_grid[i]
        signals_dict[q_grid[i]] = signals_grid[i]
    #print("====================================================")
    # Now compute the actual unfaithfulness
    params_template.wf_param=None
    #multiprocessing
    #print(cases_per_output)
    """result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(delayed(compute_mismatch)(
                        s_q_list[i],
                        x[i],
                        y[i],
                        z[i],
                        params_template,
                        flow,
                        fhigh,
                        modes_dc,
                        debug)
                        for i in cases_per_output
                        )
    """
    result=Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(delayed(compute_mismatch_nqcCoeffs)(
                    signals_dict[x[i]],
                    nqcCoeffs_s_dict[x[i]],
                    x[i],
                    y[i],
                    z[i],
                    params_template,
                    flow,
                    fhigh,
                    debug)
                    for i in cases_per_output
                    )

    result=np.array(result)

    np.savetxt(run_dir+"mismatch_q{}_{}_chi_{}_{}_Nq{}_Nchi{}_idx{}.dat".format(q_min,q_max, chi_min, chi_max,Nq,Nchi,idx_chunk), result)
