#!/usr/bin/env python
import argparse
import glob
import numpy as np
import os

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--case_file", type=str, help="File with the cases")
    args = p.parse_args()
    if args.case_file:
        with open(args.case_file, "r") as fp:
            cases = fp.readlines()
    else:
        cases = set([x[:-17] for x in glob.glob("*result*")])
        print(len(cases))
    for case in cases:
        if "." in case:
            name = case.split(".")[0]
        else:
            name = case
        if os.path.isfile("mismatches_{}.dat".format(name)):
            print("Skipping {}".format(case))
            continue
        print(case, name)
        lst = glob.glob("{}*".format(name))
        if not lst:
            continue
        weighted_mismatches = 0.0
        SNRs = 0.0
        mismatches = 0.0
        weighted_IH_q = 0.0
        IH_q = 0.0
        for it in lst:
            d = np.genfromtxt(it)
            print(d.shape)
            if len(d.shape) > 1:
                weighted_mismatches += (1 - d[:, 1]) ** 3 * d[:, -1] ** 3
                mismatches += d[:, 1]
                SNRs += d[:, -1] ** 3
                IH_q += d[:, 2]
                weighted_IH_q += (1 - d[:, 2]) ** 3 * d[:, -1] ** 3
                masses = d[:, 0]
            else:
                weighted_mismatches += (1 - d[1]) ** 3 * d[-1] ** 3
                mismatches += d[1]
                SNRs += d[-1] ** 3
                IH_q += d[2]
                weighted_IH_q += (1 - d[2]) ** 3 * d[3] ** 3
                masses = d[0]
        mismatches /= len(lst)
        IH_q /= len(lst)
        lst = glob.glob("res*.dat")
        averaged_mismatches = 1 - (weighted_mismatches / SNRs) ** (1.0 / 3)
        averaged_IH_q = 1 - (weighted_IH_q / SNRs) ** (1.0 / 3)

        res = np.c_[masses, mismatches, averaged_mismatches]
        np.savetxt("mismatches_{}.dat".format(name), res)
