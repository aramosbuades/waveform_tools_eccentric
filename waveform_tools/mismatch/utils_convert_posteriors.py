
import h5py
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline

from scipy.signal import argrelextrema
from scipy.interpolate import CubicSpline
import lal, lalsimulation as lalsim
import matplotlib.pyplot as plt


from scipy.integrate import solve_ivp

def compute_h22OmegaOrbfromNRfile(NR_file, ModeList):

    hlm = {}
    amplm = {}
    phaselm = {}
    omegalm = {}
    tNRomegalm = {}

    fp = h5py.File(NR_file, "r")

    t_omega_orb = fp['Omega-vs-time/X'][:]
    tHorizon = fp['HorizonBTimes'][:]
    omega_orb = fp['Omega-vs-time/Y'][:]
    iOmega_orb = InterpolatedUnivariateSpline(t_omega_orb, omega_orb)


    for l,m in ModeList:

        Alm = fp['amp_l'+str(l)+'_m'+str(m)+'/Y'][:]
        tAlm =fp['amp_l'+str(l)+'_m'+str(m)+'/X'][:]

        philm = fp['phase_l'+str(l)+'_m'+str(m)+'/Y'][:]
        tphilm =fp['phase_l'+str(l)+'_m'+str(m)+'/X'][:]

        tNR = fp['NRtimes'][:]
        iAlm = InterpolatedUnivariateSpline(tAlm,Alm)
        iphilm = InterpolatedUnivariateSpline(tphilm,-philm)

        amplm[l,m] = iAlm(tNR)
        phaselm[l,m] = iphilm(tNR)
        omegalm[l,m] = iphilm.derivative()(tNR)
        hlm[l,m] = amplm[l,m] * np.exp(1.j*phaselm[l,m])

    fp.close()

    om_orb = iOmega_orb(tHorizon)


    phi =  []

    for time in tHorizon:
        phi.append(iOmega_orb.integral(tHorizon[0],time))

    phase_orb = np.array(phi)

    return tNR, tHorizon, hlm, amplm, phaselm, omegalm, om_orb, phase_orb



# Compute local maxima and minima from a certain quantity

def compute_MaxMin(tHorizon, omega_orb):

    # for local maxima
    maxima = argrelextrema(omega_orb, np.greater) # Use A22 mode as it is cleaner than the frequency
    omega_orb_maxima = omega_orb[maxima]
    tH_maxima = tHorizon[maxima]

    # for local minima
    minima = argrelextrema(omega_orb, np.less)  # Use A22 mode as it is cleaner than the frequency

    omega_orb_minima =omega_orb[minima]
    tH_minima = tHorizon[minima]


    return tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima



def compute_MaxMin22(timeNR, omega22, amp22):

    # for local maxima
    amp22 = amp22[timeNR<-50] # Avoid picking the peak of the amplitude

    maxima = argrelextrema(amp22, np.greater,order =10) # Use A22 mode as it is cleaner than the frequency
    omega_maxima = omega22[maxima]
    tNR_maxima = timeNR[maxima]

    # for local minima
    minima = argrelextrema(amp22, np.less,order =10)  # Use A22 mode as it is cleaner than the frequency

    omega_minima =omega22[minima]
    tNR_minima = timeNR[minima]

    return tNR_maxima, omega_maxima, tNR_minima, omega_minima


def compute_mean_anomaly(timeNR, tmaxs1):

    # Compute also the mean anomaly
    periods = np.diff(tmaxs1)
    periods
    meanAno_vals = np.zeros(len(tmaxs1))
    meanAno_vals = []
    tt_meanAno  =[]
    for i in range(len(tmaxs1[:-1])):
        n = 2*np.pi/periods[i]

        idx_peaks  = np.where((timeNR>tmaxs1[i]) & (timeNR<tmaxs1[i+1]))[0]
        t_between_peaks = timeNR[idx_peaks]
        t_between_peaks -= tmaxs1[i]

        meanAno_vals = np.concatenate((meanAno_vals,n*t_between_peaks))
        tt_meanAno = np.concatenate((tt_meanAno, timeNR[idx_peaks]))

    return tt_meanAno, meanAno_vals



def ew_def(om_max,om_min):
    return (np.sqrt(om_max)-np.sqrt(om_min))/(np.sqrt(om_max)+ np.sqrt(om_min))

def add_inspiral_ghost_points(tt_av,om22_av,npoints):

    om22_diff = abs(om22_av[0]-om22_av[1])
    dtom22_diff = abs(tt_av[0]-tt_av[1])

    om22_av0 = np.array([om22_av[0]-(npoints-i)*om22_diff for i in range(npoints)])
    tt_av0 = np.array([tt_av[0]-(npoints-i)*dtom22_diff for i in range(npoints)])

    om22_av = np.concatenate((om22_av0,om22_av))
    tt_av = np.concatenate((tt_av0,tt_av))


    return tt_av, om22_av


def compute_orbit_average(timeNR, omegalm,tmaxs,npoints):

    iom22 = InterpolatedUnivariateSpline(timeNR,omegalm)

    phase22 = np.array([iom22.integral(timeNR[0],t) for t in timeNR])
    iphase22 = InterpolatedUnivariateSpline(timeNR,phase22)

    tt_av = 0.5*np.array([tmaxs[i+1]+tmaxs[i] for i in range(len(tmaxs)-1)])

    om22_av =  np.array([1/(tmaxs[i+1]-tmaxs[i])*(iphase22(tmaxs[i+1])-iphase22(tmaxs[i])) for i in range(len(tmaxs)-1)])

    if len(om22_av) == 1: # Do not do anything
        om22_av_int = -1

    else:

        tt_av, om22_av = add_inspiral_ghost_points(tt_av,om22_av,npoints)

        # Option to add the plunge part. Currently not implemented
        idx_plunge = np.where((timeNR>-50) & (timeNR<0))[0]

        tt_merger = timeNR[idx_plunge]
        om_merger = omegalm[idx_plunge]


        lenOm22 = len(om22_av)
        if lenOm22 <= 3:
            lenInt = 1
        else:
            lenInt = 3

        #om22_av = np.concatenate((om22_av,om_merger))
        #tt_av = np.concatenate((tt_av,tt_merger))
        iom22_av = InterpolatedUnivariateSpline(tt_av,om22_av,k=lenInt)

        #t_insp = timeNR[timeNR<0]
        t_insp =timeNR
        om22_av_int = iom22_av(t_insp)

    return tt_av, om22_av, om22_av_int



def SectotimeM(seconds, M):
    return seconds/(M*lal.MTSUN_SI)


def AmpPhysicaltoNRTD(ampphysical, M, dMpc):
    return ampphysical*dMpc*1e6*lal.PC_SI/(lal.C_SI*(M*lal.MTSUN_SI))


def SEOBNRv4EHM_modes(q: float, chi1: float,chi2: float, eccentricity: float, eccentric_anomaly: float,
                      f_min:float, dMpc: float, M_fed:float, delta_t: float, EccIC: int, approx: str):


    HypPphi0, HypR0, HypE0 =[0.,0,0]


    EccFphiPNorder = 99
    EccFrPNorder = 99
    EccWaveformPNorder = 16
    EccBeta = 0.09
    Ecct0 = 100

    EccPNFactorizedForm = EccNQCWaveform =EccPNRRForm = EccPNWfForm =EccAvNQCWaveform=1
    EcctAppend=40
    #EccIC=0
    #eccentric_anomaly = 0.0

    m1= q/(1+q)*M_fed
    m2= 1/(1+q)*M_fed
    dist = dMpc*(1e6*lal.PC_SI)

    if approx=="SEOBNRv4E_opt"  or approx=="SEOBNRv4E_opt1" or  approx=="SEOBNRv4":
        mode_list=[[2,2]]

        SpinAlignedVersion=4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    else:
        mode_list=[[2,2],[2,1],[3,3],[4,4],[5,5]]

        SpinAlignedVersion=41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary

    if approx == "SEOBNRv4E_opt" or approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    elif approx == "SEOBNRv4E_opt1" or approx == "SEOBNRv4EHM_opt1":

        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt1(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:


        #print("SEOBNRv4HM modes")
        sphtseries, dyn, dynHi = lalsimul.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)





    hlm={}


    if SpinAlignedVersion==4:
        hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm[2,-2] = np.conjugate(hlm[2,2])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        #h55LAL = - h55 * np.exp^(-1.j * modeM * phi_ref)
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[modeL,modeM] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(hlm[modeL,modeM])


    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)
    timeNR = SectotimeM(time_array,M_fed)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]

    #tdyn -=tdyn[-1]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    iphi = InterpolatedUnivariateSpline(tdyn,phidyn)
    omegaOrbital = iphi.derivative()(tdyn)


    #return time_array, hlm
    omegalm={}; amplm={}; phaselm = {};
    for l,m in mode_list:
        hlm[l,m]=np.conjugate(hlm[l,m]) # Change convention to same as NR

        phaselm[l,m] = -np.unwrap(np.angle(hlm[l,m]))
        iphaselm = InterpolatedUnivariateSpline(timeNR,phaselm[l,m])
        omegalm[l,m]=-iphaselm.derivative()(timeNR)

        amplm[l,m]=np.abs(hlm[l,m])



    imax = np.argmax(amplm[2,2])
    timeNR -= timeNR[imax]

    return timeNR, hlm, amplm, omegalm, phaselm, tdyn, rdyn, phidyn, omegaOrbital, prdyn, pphidyn


def sanitize_dT(tNR_maxima,omega_maxima):
    dT_mean = np.mean(abs(np.diff(tNR_maxima)))

    tmaxs = []
    om_maxs = []

    for i in range(len(tNR_maxima)):
        if i!=0 or i == len(tNR_maxima)-1:
            tmaxs.append(tNR_maxima[i])
            om_maxs.append(omega_maxima[i])
        else:
            dT0 = abs(tNR_maxima[i]-tNR_maxima[i+1])
            if dT0 > 0.75*dT_mean:

                tmaxs.append(tNR_maxima[i])
                om_maxs.append(omega_maxima[i])


    tmaxs = np.array(tmaxs)
    om_maxs = np.array(om_maxs)
    return tmaxs, om_maxs



    # Make the above operations a function

def convert_eccentric_mean_anomaly(timeNR, omegalm,amplm,f_min,Mtotal):

    tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(timeNR,
                                                                           omegalm,
                                                                           amplm)



    npoints = 1

    tmaxs, om_maxs = sanitize_dT(tNR_maxima, omega_maxima)
    tmaxs, om_maxs = add_inspiral_ghost_points(tmaxs, om_maxs, npoints)

    tmins, om_mins = sanitize_dT(tNR_minima,omega_minima)
    tmins, om_mins = add_inspiral_ghost_points(tmins, om_mins, npoints)

    npoints = 3
    tt_amp22_av, amp22_av, amp22_av_int = compute_orbit_average(timeNR, amplm,tmaxs,npoints)
    res_amp22 = amplm-amp22_av_int


    tmaxs1, om_maxs1, tmins1, om_mins1 =  compute_MaxMin22(timeNR, omegalm, res_amp22)


    tmin_ew22 = max(tmaxs1[0],tmins[0])
    #print(tmin_ew22,timeNR[0])
    dtNR  = timeNR[1]-timeNR[0]
    #print(dtNR)
    tew22_ext = np.arange(tmin_ew22,0,dtNR)

    npoints = 1
    tmaxs1, om_maxs1 = add_inspiral_ghost_points(tmaxs1,om_maxs1,npoints)
    tmins1, om_mins1 = add_inspiral_ghost_points(tmins1,om_mins1,npoints)

    tt_om22_av, om22_av, om22_av_int =  compute_orbit_average(timeNR, omegalm, tmaxs1, npoints)

    iom_max = InterpolatedUnivariateSpline(tmaxs1,om_maxs1)
    iom_min = InterpolatedUnivariateSpline(tmins1, om_mins1)

    om_maxs_int = iom_max(timeNR)
    om_mins_int = iom_min(timeNR)
    ew22_nr  = ew_def(om_maxs_int,om_mins_int)
    iew22_nr = InterpolatedUnivariateSpline(timeNR,ew22_nr)
    iew22_om22av = InterpolatedUnivariateSpline(om22_av_int,ew22_nr)


    #plt.plot(om22_av_int,ew22_nr)


    Mf_min = f_min*Mtotal*lal.MTSUN_SI
    om22_min = Mf_min*2*np.pi
    ew22_ref = iew22_om22av(om22_min)

    # Find root time corresponding to reference frequency

    iom22_tt = InterpolatedUnivariateSpline(timeNR, om22_av_int-om22_min)

    om22_roots = iom22_tt.roots()

    if len(om22_roots)>1:
        print("Problem! several roots are found for the reference time. Check your calculation")

    tref = om22_roots[0]

    tt_meanAno, meanAno_vals = compute_mean_anomaly(timeNR, tmaxs1)
    imeanAno = CubicSpline(tt_meanAno, meanAno_vals)

    meanAno_ref = imeanAno(tref)
    #print(f" Injection (tref =  {tref}), eccentricity = {ew22_ref}, mean_anomaly = {meanAno_ref}")


    """
    plt.figure(figsize=(8, 6), dpi=300)

    plt.plot(timeNR,omegalm)
    #plt.scatter(tt_av,om22_av,color='k',linestyle ='dashed')
    plt.plot(timeNR,om22_av_int,color='g',linestyle ='dashed')
    #plt.scatter(tmaxs,om_maxs,color='k')
    plt.scatter(tmaxs1,om_maxs1,color='r')
    plt.plot(timeNR,om_maxs_int,color='r',linestyle='dashed')
    #plt.scatter(tmins,om_mins,color='g')
    plt.scatter(tmins1,om_mins1,color='darkorange')
    plt.plot(timeNR,om_mins_int,color='darkorange',linestyle='dashed')

    plt.plot(timeNR,ew22_nr,color='purple')
    #ew22_nr  = ew_def(om_maxs_int,om_mins_int)
    plt.axhline(y=Mf_min*2*np.pi)
    #plt.plot(tt_meanAno,meanAno_vals)
    plt.ylim(0,0.5)
    #plt.xlim(-500,200)
    plt.axvline(x=tref,color='k',linestyle='dashed')
    plt.show()

    plt.figure(figsize=(8, 6), dpi=300)

    plt.plot(timeNR,omegalm)

    plt.plot(tt_meanAno,meanAno_vals)
    plt.axvline(x=tref,color='k',linestyle='dashed')

    #plt.ylim(0,2*np.pi)
    #plt.xlim(-500,200)
    plt.axvline(x=-50)
    plt.show()
    """;
    return ew22_ref, meanAno_ref

"""
def compute_orbit_average(timeNR, omegalm,tmaxs,npoints):

    iom22 = InterpolatedUnivariateSpline(timeNR,omegalm)

    phase22 = np.array([iom22.integral(timeNR[0],t) for t in timeNR])
    iphase22 = InterpolatedUnivariateSpline(timeNR,phase22)

    tt_av = 0.5*np.array([tmaxs[i+1]+tmaxs[i] for i in range(len(tmaxs)-1)])

    om22_av =  np.array([1/(tmaxs[i+1]-tmaxs[i])*(iphase22(tmaxs[i+1])-iphase22(tmaxs[i])) for i in range(len(tmaxs)-1)])


    tt_av, om22_av = add_inspiral_ghost_points(tt_av,om22_av,npoints)

    # Option to add the plunge part. Currently not implemented
    idx_plunge = np.where((timeNR>-50.0) & (timeNR<0.0))[0]

    tt_merger = timeNR[idx_plunge]
    om_merger = omegalm[idx_plunge]

    #om22_av = np.concatenate((om22_av,om_merger))
    #tt_av = np.concatenate((tt_av,tt_merger))
    iom22_av = InterpolatedUnivariateSpline(tt_av,om22_av)

    #t_insp = timeNR[timeNR<0]
    t_insp =timeNR
    om22_av_int = iom22_av(t_insp)

    return om22_av_int
""";




def interpolate_omegaMaxMin(tNR_maxima,omega_maxima):


    tmaxs, om_maxs = sanitize_dT(tNR_maxima,omega_maxima)


    om_diff = abs(om_maxs[0]-om_maxs[1])
    dT_diff = abs(tmaxs[0] -tmaxs[1])
    tmaxs = np.concatenate(([tmaxs[0]-dT_diff],tmaxs))
    om_maxs = np.concatenate(([om_maxs[0]-om_diff],om_maxs))

    iom_max = InterpolatedUnivariateSpline(tmaxs,om_maxs)

    return tmaxs, om_maxs, iom_max


def compute_amp22Res(timeNR,omega22,amp22):

    tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(timeNR, omega22, amp22)

    tmaxs, om_maxs, iom_max = interpolate_omegaMaxMin(tNR_maxima,omega_maxima)
    tmins, om_mins, iom_min = interpolate_omegaMaxMin(tNR_minima,omega_minima)

    npoints =3
    tt_av_amp22, amp22_av, amp22_av_int = compute_orbit_average(timeNR, amp22, tmaxs, npoints)

    res_amp22 = amp22 - amp22_av_int

    return res_amp22


def newtonian_eccentricity(f,f0,e0):

    return e0*(f/f0)**(-19/18)


def get_eccentricity_meanAno_fref(timeNR, omega22, res_amp22, om22_ref, eta, npoints=3):


    # Compute max/min from the residual
    tmaxs1, om_maxs1, tmins1, om_mins1 =  compute_MaxMin22(timeNR, omega22, res_amp22)

    if len(tmaxs1)> len(tmins1):

        tt_av_om22, om22_av,om22_av_int =  compute_orbit_average(timeNR, omega22, tmaxs1,npoints)
    else:
        tt_av_om22, om22_av,om22_av_int =  compute_orbit_average(timeNR, omega22, tmins1,npoints)

    if len(om22_av) == 1 :

        tt_av  = tt_av_om22[0]

        omMax_e = om_maxs1[0]
        for i in range(len(tmaxs1)):
            if tmaxs1[i]<tt_av:
                omMax_e = om_maxs1[i]

        omMin_e = om_mins1[0]
        for i in range(len(tmins1)):
            if (tmins1[i]<= (tt_av+20)):
                omMin_e = om_mins1[i]
                #print(tmin_e)

        ew22_0 =  ew_def(omMax_e , omMin_e )
        et0 = et_from_ew22_0pn(ew22_0)


        tt_meanAno, meanAno_vals = compute_mean_anomaly(timeNR, tmaxs1)
        imeanAno = InterpolatedUnivariateSpline(tt_meanAno,meanAno_vals)

        l0 = imeanAno(tt_av)
        x0 = (om22_av[0]/2.)**(2./3.)

        # Initial vector of the evolution
        z0 = np.array([x0,et0,l0])

        # Set termination condition
        x_ref = (om22_ref/2.)**(2/3.)
        def term_backwards(t, y, *args):
                return y[0] - x_ref
        term_backwards.terminal = True

        res_back = solve_ivp(
                    QK_eqns,
                    (0, -1e7),
                    z0,
                    events=term_backwards,
                    args=([eta]),
                    rtol=1e-10,
                    #method="RK45",
                    method= 'DOP853',
                )

        x_f, et_f, l_f =  res_back.y.T[-1]

        #print(f"Final at the reference frequency (x,e,l) = {x_f}, {et_f}, {l_f}")

        ew22_inj_Mf_ref =  ew22_from_et_0pn(et_f)
        meanAno_ref =  l_f%(2*np.pi)
        tref = timeNR[0] + res_back.t[-1]
        ew22_nr, meanAno_nr, om22_av_int  =-1,-1,-1

    else:

        # Add inspiral ghost points
        #npoints = 3
        tmaxs1, om_maxs1 = add_inspiral_ghost_points(tmaxs1,om_maxs1,npoints)
        tmins1, om_mins1 = add_inspiral_ghost_points(tmins1,om_mins1,npoints)

        iom_max = InterpolatedUnivariateSpline(tmaxs1,om_maxs1)
        iom_min = InterpolatedUnivariateSpline(tmins1, om_mins1)

        om_maxs_int = iom_max(timeNR)
        om_mins_int = iom_min(timeNR)
        ew22_nr  = ew_def(om_maxs_int,om_mins_int)
        #iew22_nr = InterpolatedUnivariateSpline(timeNR,ew22_nr)
        iew22_om22av = InterpolatedUnivariateSpline(om22_av_int,ew22_nr)

        tt_meanAno, meanAno_vals = compute_mean_anomaly(timeNR, tmaxs1)
        imeanAno = CubicSpline(tt_meanAno, meanAno_vals)
        meanAno_nr = imeanAno(timeNR)%(2*np.pi)

        # Find root time corresponding to reference frequency
        if om22_ref > om22_av_int[0] and om22_ref < om22_av_int[-1]:

            ew22_inj_Mf_ref = iew22_om22av(om22_ref)

            iom22_tt = InterpolatedUnivariateSpline(timeNR, om22_av_int-om22_ref)

            om22_roots = iom22_tt.roots()

            if len(om22_roots)>1:
                print("Problem! several roots are found for the reference time. Check your calculation")

            tref = om22_roots[0]
            meanAno_ref = imeanAno(tref)

        else:
            #print('Warning waveform not long enough!')
            # Make a quick backwards PN evolution

            et0 = et_from_ew22_0pn(ew22_nr[0])
            l0 = meanAno_nr[0]
            x0 = (om22_av_int[0]/2.)**(2./3.)

            # Initial vector of the evolution
            z0 = np.array([x0,et0,l0])

            # Set termination condition
            x_ref = (om22_ref/2.)**(2/3.)
            def term_backwards(t, y, *args):
                    return y[0] - x_ref
            term_backwards.terminal = True

            res_back = solve_ivp(
                        QK_eqns,
                        (0, -1e7),
                        z0,
                        events=term_backwards,
                        args=([eta]),
                        rtol=1e-10,
                        #method="RK45",
                        method= 'DOP853',
                    )

            x_f, et_f, l_f =  res_back.y.T[-1]

            #print(f"Final at the reference frequency (x,e,l) = {x_f}, {et_f}, {l_f}")

            ew22_inj_Mf_ref =  ew22_from_et_0pn(et_f)
            meanAno_ref =  l_f%(2*np.pi)
            tref = timeNR[0] + res_back.t[-1]


    return  ew22_nr, meanAno_nr, om22_av_int, ew22_inj_Mf_ref,meanAno_ref,tref



def compute_amp22Res_e0(time,time_e0,amp22,amp22_e0):

    iamp22_e0 = CubicSpline(time_e0,amp22_e0)

    amp22_e0_int = iamp22_e0(time)

    resAmp22 = amp22-amp22_e0_int

    return resAmp22


def measure_eccMeanAno_v4EHM_samples(q:float, chi1:float, chi2:float, ecc:float, meanAno:float,
                                     f_min:float, dMpc:float, Mtot:float, delta_t:float, EccIC:int,
                                     approx:str,verbose=False):


    if verbose:

        print(f"q = {q}, chi1 = {chi1}, chi2 = {chi2}, ecc = {ecc}, meanAno = {meanAno}, fmin = {f_min},\
              dMpc = {dMpc}, Mtotal ={Mtot}")

    # Generate the waveform
    timev4E, hlmv4E, amplmv4E, omegalmv4E, phaselmv4E,tdynv4E,\
    rdynv4E, phidynv4E, omegaOrbv4E, prdynv4E, pphidynv4E = SEOBNRv4EHM_modes(q, chi1, chi2, ecc,
                                                                              meanAno, f_min, dMpc, Mtot,
                                                                              delta_t, EccIC, approx)

    # Generate e=0 waveform to compute the residual (start at 10Hz to have a longer QC than eccentric one)
    timev4E_e0, hlmv4E_e0, amplmv4E_e0, omegalmv4E_e0, phaselmv4E_e0,tdynv4E_e0,\
    rdynv4E_e0, phidynv4E_e0, omegaOrbv4E_e0, prdynv4E_e0, pphidynv4E_e0 = SEOBNRv4EHM_modes(q, chi1, chi2, 0.,
                                                                              meanAno, 10., dMpc, Mtot,
                                                                              delta_t, EccIC, approx)


    eta = q/(1.+q)**2.
    # Measure eccentricity at the reference frequency

    #res_amp22_v4E = compute_amp22Res(timev4E,omegalmv4E[2,2],amplmv4E[2,2])
    res_amp22_v4E = compute_amp22Res_e0(timev4E, timev4E_e0, amplmv4E[2,2], amplmv4E_e0[2,2])



    # We measure eccentricity/mean anomaly at a physical reference frequency
    Mf_min = f_min*Mtot*lal.MTSUN_SI
    om22_ref_sample = Mf_min*2*np.pi
    #print()

    if verbose:
        print(f"om22_ref_sample = {om22_ref_sample}")
    npoints = 3
    ew22_v4E, meanAno22_v4E, om22_av_v4E, ew22_v4E_ref, meanAno_v4E_ref, tref = get_eccentricity_meanAno_fref(\
                                                                                                 timev4E,
                                                                                                 omegalmv4E[2,2],
                                                                                                 res_amp22_v4E,
                                                                                                 om22_ref_sample,
                                                                                                 eta,
                                                                                                 npoints=npoints)
    if verbose:
        ew22Tag = np.round(ew22_v4E_ref,4)
        meanAnoTag = np.round(meanAno_v4E_ref,4)
        plt.title(f"ew22_v4E_Mfref = {ew22Tag}, ew22_v4E_Mfref = {meanAnoTag}",fontsize=12 )
        plt.plot(timev4E, ew22_v4E)
        plt.axvline(x=tref,color='k',linestyle='dashed')
        plt.show()

        plt.title(f"ew22_v4E_Mfref = {ew22Tag}, ew22_v4E_Mfref = {meanAnoTag}",fontsize=12 )
        plt.plot(timev4E, meanAno22_v4E,color='g')
        plt.axvline(x=tref,color='k',linestyle='dashed')
        plt.show()
    return ew22_v4E_ref,meanAno_v4E_ref



########################## FUNCTION TO MAKE A BACKWARDS PN EVOLUTION TO COMPUTE THE ECCENTRICITY IN CASE THE WAVEFORM IS TOO SHORT




# Newtonian order relation

def et_from_ew22_0pn(ew22):
    """
    Temporal eccentricity at Newtonian order.

     ew22 : eccentricity measured from the 22-mode frequency.
     Returns: et

    """

    psi = np.arctan2(1.-ew22*ew22,2.*ew22)

    et = np.cos(psi/3.) -  np.sqrt(3)*np.sin(psi/3.)

    return et


def ew22_from_et_0pn(et):
    """
    Eccentricity from the 22-mode frequency at Newtonian order.

     et : temporal eccentricity.
     Returns: ew22

    """
    sqrt_minus = np.sqrt(2-et)
    sqrt_plus = np.sqrt(2+et)


    num = (1+et)*sqrt_minus - sqrt_plus*(1-et)
    den = (1+et)*sqrt_minus + sqrt_plus*(1-et)

    return num/den


# Code RR equations of motion
def dxdt_2pn(x,et, eta):

    eta2 = eta*eta
    et2 = et*et
    et4 = et2*et2
    et6 = et4*et2
    et8 = et6*et2

    den0 = (1.-et2)**(7/2.)
    den1 =(1-et2)*den0
    den2 =(1-et2)*den1

    xdot0pn = (192+584*et2+74*et4)/(15*den0)

    xdot1pn = (-11888-14784*eta+et2*(87720-159600*eta)+et4*(171038-141708*eta) + et6*(11717-8288*eta))/(420*den1)

    xdot2pn = (-360224 + 4514976*eta + 1903104*eta2 + et2*(-92846560+15464736*eta+61282032*eta2) +\
               et4*(783768-207204264*eta+166506060*eta2) + et6*(83424402-123108426*eta+64828848*eta2)+\
               et8*(3523113 - 3259980*eta+1964256*eta2) -\
               3024*(96+4268*et2+4386*et4+175*et6)*(-5+2*eta)*np.sqrt(1-et2))/(45360*den2)

    dxdt  = eta*x**(5.)*(xdot0pn+xdot1pn*x +xdot2pn*x*x)

    return dxdt


def dedt_2pn(x,et, eta):

    eta2 = eta*eta

    et2 = et*et
    et4 = et2*et2
    et6 = et4*et2

    den0 = (1.-et2)**(5/2.)
    den1 =(1-et2)*den0
    den2 =(1-et2)*den1

    edot0pn = (304+121*et2)/(15*den0)

    edot1pn = -(67608 +228704*eta+et2*(-718008 + 651252*eta)+et4*(-125361 + 93184*eta))/(2520*den1)

    edot2pn = (-15198032+13509360*eta+4548096*eta2+et2*(-36993396-35583228*eta+48711348*eta2)+\
              et4*(46579718-78112266*eta+42810096*eta2)+et6*(3786543-4344852*eta+2758560*eta2)-\
               1008*(2672+6963*et2+565*et4)*(-5+2*eta)*(np.sqrt(1-et2)))/(30240*den2)

    #xdot2pn = (192+584*et*et+74*pow(et,4))/(15*pow(1-et*et,7/2.)

    dedt  = -eta*et*x**(4.)*(edot0pn+edot1pn*x+edot2pn*x*x)

    return dedt


def dldt_2pn(x,et, eta):

    et2 = et*et

    den1 = (1.-et2)
    den2 =(1-et2)*den1

    ldot0pn = 1.

    ldot1pn = -3/den1

    ldot2pn =  (-18+28*eta+et2*(-51+26*eta))/(4*den2)

    #xdot2pn = (192+584*et*et+74*pow(et,4))/(15*pow(1-et*et,7/2.)

    dldt  =  x**(3./2.)*(ldot0pn+ldot1pn*x+ldot2pn*x*x)

    return dldt


def QK_eqns(t, z, eta):
    """2PN quasi-Keplerian post-Newtonian equations

    Args:
        t (float): Time
        z (np.array): Vector of unknowns
        eta (float): Symmetric mass ratio

    Returns:
        np.array: RHS of equations
    """

    x, et, l = z


    derivs = np.array([
                      dxdt_2pn(x,et, eta),
                      dedt_2pn(x,et, eta),
                      dldt_2pn(x,et, eta)
                    ])

    return derivs
