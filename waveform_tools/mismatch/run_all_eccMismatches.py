#!/usr/bin/env python
import argparse
import glob
import os
import subprocess as sp
import uuid
import numpy as np

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )
    p.add_argument("--file_dirlist", type=str, help="File containing the list of LVCNR files to run on. Alternatively if a path is provided it will run on all the LVCNR files on that path.")
    p.add_argument("--path_dir",
                    type=str,
                    help="In case the file does not contain the path to the files.  \
                         One can specify it here.",default='/path/to/the/files/')
    p.add_argument(
        "--approximant", type=str, help="Approximant to use", default="IMRPhenomPv3HM"
    )

    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="6:00:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobs.", default="nr")
    p.add_argument("--machine", type=str, help="Machine where we are submitting the jobs.", default="minerva01")


    p.add_argument("--ell_max", type=int, help="Maximum ell to use", default=4)


    p.add_argument("--N",type=int,help="The number of cases in a chunk",default=16)
    p.add_argument("--Ne",type=int,help="Number of points for 1D eccentricity maximization.",default=50)
    p.add_argument("--Nf",type=int,help="Number of points for 1D starting frequency maximization.",default=200)
    p.add_argument("--verbose",type=bool,
    help="If True it produces a bunch of plots",default=False)

    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")


### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=99,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=99,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.09,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=1,
    )
    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )

    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )


    p.add_argument(
        "--TEOB_IC",
        type=int,
        help="TEOB initial conditions: #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation",
        default=0,
    )

    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument("--eccLength",action="store_true",help="Run ecc. length,")

    p.add_argument("--nested",action="store_true",help="Run 1d nested optimizations.")
    p.add_argument("--read22ModeEccfmin_dir",type=str,default="None",help="Directory with mismatch files from which to take optimal values of eccentricity and starting frequency from the 22-mode mismatches.")
    p.add_argument("--deltaEcc",type=float,help="Buffer to eccentricity range.",default=0.1)
    p.add_argument("--deltaF",type=float,help="Buffer to starting frequency range.",default=10.)

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)
    try:
        os.chdir(run_dir)
    except OSError:
        print("Could not go into the run directory {}, quitting!".format(run_dir))
        exit(-1)

    script_dir =  os.environ['WAVEFORM_TOOLS_PATH']
    path_dir = args.path_dir

    isFile = os.path.isfile(args.file_dirlist)

    if isFile:
        outfile = args.file_dirlist
        with open(outfile) as f:
            nr_files = f.read().splitlines()
    else:
        nr_files = [f for f in glob.glob(args.file_dirlist+"/*.h5")]


    if os.path.dirname(nr_files[0]) == '':
        nr_files_full = []
        for nr_file in nr_files:
            if os.path.dirname(nr_file) == '':
                nr_files_full.append(path_dir+nr_file)

        nr_files = nr_files_full

    chk = list(chunks(nr_files, args.N))

    submit_time = args.submit_time
    queue = args.queue
    machine = args.machine

    if machine == "minerva01":
        header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue


#source /home/aramosbuades/load_LALenv.sh

cd {}
"""
    elif machine== "picasso":
        header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -t {}           # Run time
#SBATCH --constraint=cal
#SBATCH --mem=27gb


export MKL_NUM_THREADS="1"
export MKL_DYNAMIC="FALSE"
export OMP_NUM_THREADS=1
export MPI_PER_NODE=16

cd {}
"""

    else:
        print("Unknown machine {}, quitting!".format(machine))
        exit(-1)



    unq_name = uuid.uuid4().hex




    for i in range(len(chk)):
        nm = unq_name + "_{}".format(i)
        fp = open("chunk_{}.sh".format(i), "w")
        if machine == "minerva01":
            fp.write(header.format(i,i,i, queue, submit_time, run_dir))
        if machine == "picasso":
            fp.write(header.format(i,i,i, submit_time, run_dir))

        k=1
        for NR_file in chk[i]:

            if args.eccLength:
                cmd = """python {}/run_one_eccMatch22_eccLength.py --NR_file {} --signal {} --approximant {} --ell_max {} --EccFphiPNorder_template  {}  --EccFrPNorder_template  {} \
                 --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {}  --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {} --EccPNRRForm {} --EccPNWfForm {} --EcctAppend {}  --EccAvNQCWaveform {} --Ne {} --Nf {} --verbose {} --run_dir {} & \n""".format(
                    script_dir,
                    NR_file,
                    args.signal,
                    args.approximant,
                    args.ell_max,
                    args.EccFphiPNorder_template,
                    args.EccFrPNorder_template,
                    args.EccWaveformPNorder_template,
                    args.EccPNFactorizedForm_template,
                    args.EccBeta_template,
                    args.Ecct0_template,
                    args.EccNQCWaveform_template,
                    args.EccPNRRForm,
                    args.EccPNWfForm,
                    args.EcctAppend,
                    args.EccAvNQCWaveform,
                    args.Ne,
                    args.Nf,
                    args.verbose,
                    run_dir
                )
                fp.write(cmd)
                k+= 1

            elif args.nested and args.read22ModeEccfmin_dir=='None' :
                #" python $WAVEFORM_TOOLS_PATH/run_one_eccMatch22_nestedOpts.py --run_dir $RUNDIR  --NR_file $NRDIR/SXS_BBH_1355_Res5.h5  --signal NR_hdf5 --approximant SEOBNRv4E_opt --ell_max 2 --EccFphiPNorder_template  99
                #--EccFrPNorder_template  99 --EccWaveformPNorder_template  16 --EccPNFactorizedForm_template 1 --EccBeta_template 0.09 --Ecct0_template 100.0  --EccNQCWaveform_template 1   --EccPNRRForm 1 --EccPNWfForm 1
                #--EccNQCWaveform_template 1  --EccAvNQCWaveform 1 --EcctAppend 50.0 --Ne 10 --Nf 10 --deltaEcc 0.1 --deltaF 10.          "
                if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt"  or args.approximant == "SEOBNRv4E_opt1" or args.approximant == "TEOBResumSE" :
                    cmd = """python {}/run_one_eccMatch22_nestedOpts.py --NR_file {} --signal {} --approximant {} --ell_max {} --EccFphiPNorder_template  {}  --EccFrPNorder_template  {} \
                            --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {}  --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {} --EccPNRRForm {} --EccPNWfForm {} --EcctAppend {}  --EccAvNQCWaveform {} --Ne {} \
                            --Nf {} --deltaEcc {} --deltaF {} --iota_s {}  --run_dir {} --TEOB_IC {} & \n""".format(
                    script_dir,
                    NR_file,
                    args.signal,
                    args.approximant,
                    args.ell_max,
                    args.EccFphiPNorder_template,
                    args.EccFrPNorder_template,
                    args.EccWaveformPNorder_template,
                    args.EccPNFactorizedForm_template,
                    args.EccBeta_template,
                    args.Ecct0_template,
                    args.EccNQCWaveform_template,
                    args.EccPNRRForm,
                    args.EccPNWfForm,
                    args.EcctAppend,
                    args.EccAvNQCWaveform,
                    args.Ne,
                    args.Nf,
                    args.deltaEcc,
                    args.deltaF,
                    args.iota_s,
                    run_dir,
                    args.TEOB_IC
                )

                    fp.write(cmd)
                elif args.approximant =="SEOBNRv4EHM_opt"  or args.approximant =="SEOBNRv4EHM_opt1"  or args.approximant == "TEOBResumSEHM" :
                    cmd = """python {}/run_one_eccMatchHoMs_nestedOpts.py --NR_file {} --signal {} --approximant {} --ell_max {} --EccFphiPNorder_template  {}  --EccFrPNorder_template  {} \
                            --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {}  --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {} --EccPNRRForm {} --EccPNWfForm {} --EcctAppend {}  --EccAvNQCWaveform {} --Ne {} \
                            --Nf {} --deltaEcc {} --deltaF {} --iota_s {}  --run_dir {}   --TEOB_IC {} & \n""".format(
                    script_dir,
                    NR_file,
                    args.signal,
                    args.approximant,
                    args.ell_max,
                    args.EccFphiPNorder_template,
                    args.EccFrPNorder_template,
                    args.EccWaveformPNorder_template,
                    args.EccPNFactorizedForm_template,
                    args.EccBeta_template,
                    args.Ecct0_template,
                    args.EccNQCWaveform_template,
                    args.EccPNRRForm,
                    args.EccPNWfForm,
                    args.EcctAppend,
                    args.EccAvNQCWaveform,
                    args.Ne,
                    args.Nf,
                    args.deltaEcc,
                    args.deltaF,
                    args.iota_s,
                    run_dir,
                    args.TEOB_IC
                    )

                    fp.write(cmd)

                k+= 1

            elif  args.nested and args.read22ModeEccfmin_dir != 'None' :

                cmd="""python {}/run_one_case_readEcc22_HoMs.py  --NR_file {}  --signal {} --approximant {} --ell_max {} --EccFphiPNorder_template {} --EccFrPNorder_template {} \
                         --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {} --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {}  --EccPNRRForm {} --EccPNWfForm {}  \
                         --EccAvNQCWaveform {} --EcctAppend {} --Ne {} --Nf {} --deltaEcc {} --deltaF {} --iota_s {}  --run_dir {} --mismatch_22mode_dir {}  --TEOB_IC {}
                 \n""".format(
                        script_dir,
                        NR_file,
                        args.signal,
                        args.approximant,
                        args.ell_max,
                        args.EccFphiPNorder_template,
                        args.EccFrPNorder_template,
                        args.EccWaveformPNorder_template,
                        args.EccPNFactorizedForm_template,
                        args.EccBeta_template,
                        args.Ecct0_template,
                        args.EccNQCWaveform_template,
                        args.EccPNRRForm,
                        args.EccPNWfForm,
                        args.EccAvNQCWaveform,
                        args.EcctAppend,
                        args.Ne,
                        args.Nf,
                        args.deltaEcc,
                        args.deltaF,
                        args.iota_s,
                        run_dir,
                        args.read22ModeEccfmin_dir,
                        args.TEOB_IC
                    )

                fp.write(cmd)

                k+= 1




            else:


                if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt" or args.approximant == "SEOBNRv4E_opt1":

                    cmd = """python {}/run_one_eccMatch22.py --NR_file {} --signal {} --approximant {} --ell_max {} --EccFphiPNorder_template  {}  --EccFrPNorder_template  {} \
                     --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {}  --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {} --EccPNRRForm {} --EccPNWfForm {} --EcctAppend {}  --EccAvNQCWaveform {} --Ne {} --Nf {} --verbose {} --run_dir {} & \n""".format(
                        script_dir,
                        NR_file,
                        args.signal,
                        args.approximant,
                        args.ell_max,
                        args.EccFphiPNorder_template,
                        args.EccFrPNorder_template,
                        args.EccWaveformPNorder_template,
                        args.EccPNFactorizedForm_template,
                        args.EccBeta_template,
                        args.Ecct0_template,
                        args.EccNQCWaveform_template,
                        args.EccPNRRForm,
                        args.EccPNWfForm,
                        args.EcctAppend,
                        args.EccAvNQCWaveform,
                        args.Ne,
                        args.Nf,
                        args.verbose,
                        run_dir
                    )
                elif args.approximant == "IMRPhenomXEv1":
                        cmd = """python {}/run_one_eccMatch22.py --NR_file {} --signal {} --approximant {} --ell_max {} --min_type {} --unfaithfulness_type {} --Nharmonic_template  {}  --kAdvance_template {} --Ne {} --Nf {} --verbose {} --run_dir {} & \n""".format(
                            script_dir,
                            NR_file,
                            args.signal,
                            args.approximant,
                            args.ell_max,
                            args.min_type,
                            args.unfaithfulness_type,
                            args.Nharmonic_template,
                            args.kAdvance_template,
                            args.Ne,
                            args.Nf,
                            args.verbose,
                            run_dir
                        )
                else:
                    cmd = """python {}/run_one_eccMatch22.py --NR_file {} --signal {} --approximant {} --ell_max {}  --Ne {} --Nf {} --verbose {} --run_dir {} & \n""".format(
                        script_dir,
                        NR_file,
                        args.signal,
                        args.approximant,
                        args.ell_max,
                        args.Ne,
                        args.Nf,
                        args.verbose,
                        run_dir
                    )


                fp.write(cmd)
                k+= 1
        fp.write("wait")
        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)

        print(submit_cmd)

        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
