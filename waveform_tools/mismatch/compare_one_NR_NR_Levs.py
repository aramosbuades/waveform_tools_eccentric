#!/usr/bin/env python3
import argparse
import numpy as np
import pandas as pd
from waveform_tools.mismatch.unfaithfulness_ecc import run_Eccunfaithfulness
from joblib import Parallel, delayed
import json

def compare_waveforms(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    NR_file: str,
    NR_file_2: str,
    index: int,
    ell_max: int = 5,
    mode_array_signal: str = None,
    mode_array_template: str = None,

):
    signal_approx = "NR_hdf5"
    template_approx = "NR_hdf5"
    run_Eccunfaithfulness(
        phi_s,
        kappa_s,
        iota_s,
        index,
        template_approx=template_approx,
        signal_approx=signal_approx,
        NR_file=NR_file,
        NR_file_2=NR_file_2,
        ellMax=ell_max,
        minimization_type="phi",
        mode_array_signal  = mode_array_signal,
        mode_array_template  = mode_array_template,
    )


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The first NR file")
    p.add_argument("--NR_file_2", type=str, help="The second NR file")
    p.add_argument("--ell_max", type=int, help="The max ell to use", default=5)
    p.add_argument("--iota_s", type=float, help="Inclination angle.", default=np.pi/3.)
    p.add_argument("--mode_array_signal",  type=str, help="List of modes for the signal,i.e,  [[2, 2], [2, -2]]", default=None)
    p.add_argument("--mode_array_template", type=str, help="List of modes for the signal,i.e,  [[2, 2], [2, -2]]", default=None)

    args = p.parse_args()
    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()


    if args.mode_array_signal == "None":
        mode_array_signal = None
    else:
        mode_array_signal = json.loads(args.mode_array_signal)


    if args.mode_array_template == "None" :
        mode_array_template = None
    else:
        mode_array_template = json.loads(args.mode_array_template)


    #iota_list = np.linspace(0, np.pi, 6, endpoint=False)

    #for iota_s in iota_list:
    iota_s = args.iota_s

    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(compare_waveforms)(
            y[i],
            x[i],
            iota_s,
            args.NR_file,
            args.NR_file_2,
            i,
            ell_max=args.ell_max,
            mode_array_signal=mode_array_signal,
            mode_array_template=mode_array_template,
    )
    for i in range(len(x))
    )
