#!/usr/bin/env python

import argparse
import os, sys, glob
import subprocess as sp
from joblib import Parallel, delayed


sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

import numpy as np, h5py
import lal, lalsimulation as lalsim
import matplotlib.pyplot as plt
import bilby, json
import shutil
import math

from utils_convert_posteriors import *

# Read a posterior file
def result_bilby_file(file):
    return(bilby.result.Result.from_json(file))


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument(
        "--outdir",
        type=str,
        help="Directory where to output the new json file with the converted samples.",
        default=os.getcwd(),
    )
    p.add_argument(
        "--post_file",
        type=str,
        help="Bilby result file.",
        default=os.getcwd(),
    )

    #p.add_argument("--fref", type=float,  help="Reference frequency at which to measure the eccentricity", default=20., )
    p.add_argument("--EccIC", type=int,  help=" -1 generic point in the orbit using instantaneous frequency, -2 using orbit-average frequency, 0 fixed at periastron, 2 hyperbolic IC", default=-2, )

    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=5000)
    #p.add_argument("--debug",action="store_true",help="Debug mode")

    p.add_argument("--idx_chunk",type=int,help="Subset of cases to run",default=0)
    args = p.parse_args()

    # posterior file
    post_file = args.post_file


    # Read PE file
    PB_dict = {}

    basename = os.path.basename(post_file).split('_merged_result.json')[0]
    PB_dict[basename] = result_bilby_file(post_file)
    posteriors = PB_dict[basename].posterior

    # Read parameters necessary to generate a v4EHM waveform
    chi1_posterior = np.array(posteriors["chi_1"])
    chi2_posterior = np.array(posteriors["chi_2"])
    ecc_posterior = np.array(posteriors["eccentricity"])
    meanAno_posterior = np.array(posteriors["mean_anomaly"])

    mtotal_posterior = np.array(posteriors["total_mass"])
    q_posterior = np.array(posteriors["mass_ratio"])

    q_posterior = 1./q_posterior
    dMpc_posterior = np.array(posteriors['luminosity_distance'])

    delta_t = 1./8192.
    EccIC = args.EccIC


    config_file = PB_dict[basename].meta_data['config_file']

    approx = config_file['waveform_approximant']


    f_min = float(config_file['reference_frequency'])
    approx = config_file['waveform_approximant']

    if 'SEOBNR' in approx:
        f_min = float(config_file['minimum_frequency'])

    print(f"Processing file : {os.path.basename(post_file)}")

    NN = len(q_posterior)
    idx_list = np.arange(0,NN)

    Nchunks = args.Nchunks
    Nout = Nchunks # number of cases per slurm job

    #n_slurm_jobs = int(NN/Nchunks)
    n_slurm_jobs = math.ceil(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    idx_chunk=args.idx_chunk

    case0=cases_per_slurm_job[idx_chunk]
    cases_per_output = list(chunks(case0,Nout))


    for case in cases_per_output:
        result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
            delayed(measure_eccMeanAno_v4EHM_samples)(
                 q_posterior[i],
                 chi1_posterior[i],
                 chi2_posterior[i],
                 ecc_posterior[i],
                 meanAno_posterior[i],
                 f_min,
                 dMpc_posterior[i],
                 mtotal_posterior[i],
                 delta_t,
                 EccIC,
                 approx,
                 verbose=False)
            for i in case
            )


    # Output the result to a json file
    outdir=args.outdir
    if outdir[-1]!="/":
        outdir +='/'

    os.makedirs(outdir,exist_ok=True)

    # First copy the Bilby json file
    outname_file = os.path.basename(post_file).split('.json')[0]
    outname_file +='_ew22meanAno22ref_'+str(idx_chunk)+'.dat'

    out_file = outdir + outname_file
    #shutil.copyfile(post_file, out_file)

    #ew22_posterior = [ii[0] for ii in result]
    #meanAno22_posterior = [ii[1] for ii in result]

    # Output only this posteriors to a new file
    #post_dict = {'ew22_ref': ew22_posterior, 'mean_anomaly22_ref':meanAno22_posterior}

    result = np.array(result)

    np.savetxt(out_file, result)
    #k+=1

    #with open(out_file, 'w') as f:
    #    json.dump(post_dict, f)
