#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Ne",type=int,help="Number of eccentricity points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)

## SEOBNRv4E arguments

    p.add_argument(
        "--EccFphiPNorder",
        type=int,
        help="EccFphiPNorder only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder",
        type=int,
        help="EccFrPNorder only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder",
        type=int,
        help="EccWaveformPNorder for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm",
        type=int,
        help="EccPNFactorizedForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta",
        type=float,
        help="EccBeta only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0",
        type=float,
        help="This currently has no effect",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform",
        type=int,
        help="EccNQCWaveform only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )


    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )


    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--eccMin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccMax", type=float,  help="Maximum eccentricity", default=0.7, )

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )


    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")
    p.add_argument("--teob",action="store_true",help="Use TEOBReumSE instead of SEOBNRv4E")
    p.add_argument("--output_waveform", type=str,  help="Whether to output the waveformd", default="False", )

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)


    # Plot dir
    plotdir = run_dir+"/plots"
    os.makedirs(plotdir, exist_ok=True)

    if plotdir[-1] != "/":
        plotdir=plotdir+"/"

    ## Set some Parameters

    # PN orders of the eccentric corrections
    EccWaveformPNorder = args.EccWaveformPNorder  # Full 2PN order in the WF
    EccPNFactorizedForm = args.EccPNFactorizedForm

    EccFphiPNorder = args.EccFphiPNorder # Full 2PN order
    EccFrPNorder = args.EccFrPNorder  # 2PN order without the tail term

    # Use older expressions
    EccPNRRForm = args.EccPNRRForm
    EccPNWfForm = args.EccPNWfForm

    # Parameters of the sigmoid
    EccBeta = args.EccBeta
    Ecct0 = args.Ecct0
    EccNQCWaveform = args.EccNQCWaveform


    # Set boundary for making the test
    Ne = args.Ne
    ecc_max = args.eccMax
    ecc_min = args.eccMin
    ecc_list = np.linspace(ecc_min,ecc_max,Ne)

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    q_list = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    chi_list = np.linspace(chi_min, chi_max, Nchi)
    print(chi_min, chi_max)

    submit_time = args.submit_time
    queue = args.queue

    script_dir =  args.script_dir

    header = """#!/bin/bash -
#SBATCH -J tApptest_0                # Job Name
#SBATCH -o tApptest_0.stdout          # Output file name
#SBATCH -e tApptest_0.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue


#source /home/aramosbuades/load_LALenv.sh

cd {}
"""
    fp = open("submit_tAppend_test.sh", "w")
    fp.write(header.format(queue, submit_time, run_dir))

    cmd = """python {}/tAppend_test.py --run_dir {} --EccFphiPNorder  {}  --EccFrPNorder  {} \
     --EccWaveformPNorder  {} --EccPNFactorizedForm {}  --EccBeta {} --Ecct0 {} --EccNQCWaveform {} --EccPNRRForm {} --EccPNWfForm {} --EccAvNQCWaveform {} --Ne {} --Nq {} --Nchi {} \
     --qMax {} --qMin {} --chiMax {} --chiMin {} --eccMax {} --eccMin {} --output_waveform {} \n""".format(
        script_dir,
        run_dir,
        args.EccFphiPNorder,
        args.EccFrPNorder,
        args.EccWaveformPNorder,
        args.EccPNFactorizedForm,
        args.EccBeta,
        args.Ecct0,
        args.EccNQCWaveform,
        args.EccPNRRForm,
        args.EccPNWfForm,
        args.EccAvNQCWaveform,
        args.Ne,
        args.Nq,
        args.Nchi,
        args.qMax,
        args.qMin,
        args.chiMax,
        args.chiMin,
        args.eccMax,
        args.eccMin,
        args.output_waveform
    )


    fp.write(cmd)


    fp.close()
    submit_cmd = "sbatch submit_tAppend_test.sh"

    print(submit_cmd)
    if args.submit:
        sp.call(submit_cmd, shell=True)
    else:
        print(".sh files created. To also automatically submit, rerun with --submit")
