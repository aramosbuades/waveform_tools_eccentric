#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp
import bilby
import math

# Read a posterior file
def result_bilby_file(file):
    return(bilby.result.Result.from_json(file))


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]

if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test

    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument(
        "--post_file",
        type=str,
        help="Bilby result file.",
        default=os.getcwd(),
    )

    #p.add_argument("--fref", type=float,  help="Reference frequency at which to measure the eccentricity", default=20., )
    p.add_argument("--EccIC", type=int,  help=" -1 generic point in the orbit using instantaneous frequency, -2 using orbit-average frequency, 0 fixed at periastron, 2 hyperbolic IC", default=-2, )



    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")


    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)


    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    if run_dir[-1] != "/":
        run_dir+="/"

    os.makedirs(run_dir, exist_ok=True)
    os.chdir(run_dir)

    # posterior file
    post_file = args.post_file

    ## We need to know the length of the posteriors befor submission
    # Read PE file
    PB_dict = {}

    basename = os.path.basename(post_file).split('_merged_result.json')[0]
    PB_dict[basename] = result_bilby_file(post_file)
    posteriors = PB_dict[basename].posterior

    # Read a parameter and get the length
    q_posterior = np.array(posteriors["mass_ratio"])

    NN=len(q_posterior)

    Nchunks =args.Nchunks
    Nout = Nchunks
    idx_list = np.arange(0,NN)

    #n_slurm_jobs = int(NN/Nchunks)
    n_slurm_jobs = math.ceil(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    # SLURM settings
    submit_time = args.submit_time
    queue = args.queue

    script_dir =  args.script_dir
    #f_ref = float(PB_dict[basename].meta_data['config_file']['reference_frequency'])

    EccIC = args.EccIC

    header = """#!/bin/bash -
#SBATCH -J post_converter_{}                # Job Name
#SBATCH -o post_converter_{}.stdout          # Output file name
#SBATCH -e post_converter_{}.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue

cd {}
    """
    for i in range(n_slurm_jobs):

        case = cases_per_slurm_job[i]

        fp = open("post_converter_{}.sh".format(i), "w")
        fp.write(header.format(i,i,i,queue, submit_time, run_dir))

        cmd = """python {}/run_posterior_converter.py --outdir {}  --post_file {} --EccIC {} --Nchunks {}  --idx_chunk {} \n""".format(
            script_dir,
            run_dir,
            post_file,
            EccIC,
            Nchunks,
            i
            )


        fp.write(cmd)


        fp.close()

        #print(submit_cmd)
        """"
        submit_cmd = "sbatch post_converter_{}.sh".format(i)

        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
        """
    # Generate also the submission file which joins the previous files

    header = """#!/bin/bash -
#SBATCH -J join_converted_post                # Job Name
#SBATCH -o join_converted_post.stdout          # Output file name
#SBATCH -e join_converted_post.stderr          # Error file name
#SBATCH -n 1                # Number of cores
#SBATCH --ntasks-per-node 1  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t 0:15:00           # Run time
#SBATCH --no-requeue

cd {}
        """

    cmd = """python {}/join_converted_posterior.py --outdir {} \n""".format(
            script_dir,
            run_dir
            )

    fp = open("join_converted_post.sh", "w")
    fp.write(header.format(queue, run_dir))

    fp.write(cmd)

    fp.close()
    #submit_cmd = "sbatch join_converted_post.sh"

    # Write bash file with the dependency to join the files encoded
    fp = open("bash_convert_post.sh", "w")
    cmd_f ="""sbatch --dependency=afterok"""
    for i in range(n_slurm_jobs):

        cmd = """jid{}=$(sbatch post_converter_{}.sh)\n""".format(i,i)
        cmd_f +=""":${jid"""+str(i)+"""##* }"""
        #print(cmd,cmd_f)
        fp.write(cmd)

    cmd_f +=""" join_converted_post.sh \n\n"""

    #cmd_f += """squeue -u $USER -o "%u %.10j %.8A %.4C %.40E %R""""

    fp.write(cmd_f)
    fp.close()


    submit_cmd = """bash bash_convert_post.sh"""

    if args.submit:
        sp.call(submit_cmd, shell=True)
    else:
        print(".sh files created. To also automatically submit, rerun with --submit")
