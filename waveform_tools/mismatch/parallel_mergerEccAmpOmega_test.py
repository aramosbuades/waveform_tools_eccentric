#!/usr/python

import argparse
import glob, os ,sys
#import subprocess as sp

import lal, lalsimulation
import pandas as pd
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from joblib import delayed, Parallel


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from waveform_analysis  import *

plt.rcParams["figure.figsize"] = (14,10)
plt.rcParams['font.size'] = '18'

#sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

########   Functions required by the test

def SEOBNRv4E_tAmpfreq(q: float, chi1: float,chi2: float, eccentricity: float, dMpc:float, f_min:float, Mtot:float, delta_t: float,
                    EccFphiPNorder: int, EccFrPNorder: int,  EccWaveformPNorder: int, EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int
                    ,EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int, EcctAppend: float):

    distance = dMpc*1e6*lal.PC_SI

    M = Mtot*lal.MSUN_SI # Mtot in units of solar masses
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary


    EccIC=0
    HypPphi0 = 0.0
    HypR0 = 0.0
    HypE0 = 0.0
    eccentric_anomaly =0.0
    #print("q = ",q, " chi1 = ",chi1, " chi2 = ",chi2, "ecc = ", eccentricity)
    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC, HypPphi0,  HypR0, HypE0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    amplm[2,2] = np.abs(hlm[2,2])

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))

    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax

    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])


    return timeNR, amplm[2,2], omegalm[2,2]




def SEOBNRv4_tAmpfreq(q: float, chi1: float,chi2: float,  dMpc:float, f_min:float, Mtot:float, delta_t: float):

    distance = dMpc*1e6*lal.PC_SI

    M = Mtot*lal.MSUN_SI # Mtot in units of solar masses
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    amplm[2,2] = np.abs(hlm[2,2])

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))

    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax

    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])


    return timeNR, amplm[2,2], omegalm[2,2]


def mergerTest(q:float, chi1 : float, chi2:float, eccentricity:float, M_fed:float, dMpc:float, f_min:float, delta_t:float,
                EccFphiPNorder : int, EccFrPNorder : int, EccWaveformPNorder : int, EccPNFactorizedForm : int,
                  EccBeta : float, Ecct0 : float, EccNQCWaveform : int, EccPNRRForm : int, EccPNWfForm : int, EccAvNQCWaveform: int, EcctAppend: float,
                  run_dir: str
                ):


    m1_dim = q / (1.0 + q)
    m2_dim = 1 - m1_dim
    eta= q/(1+q)**2

    timeNRv4, amp22v4, omega22v4 = SEOBNRv4_tAmpfreq(q, chi1, chi2, dMpc, f_min, M_fed, delta_t)
    iamp22v4 = InterpolatedUnivariateSpline(timeNRv4, amp22v4)
    iomega22v4 = InterpolatedUnivariateSpline(timeNRv4, omega22v4)



    timeNRv4E, amp22v4E, omega22v4E = SEOBNRv4E_tAmpfreq(q, chi1, chi2, eccentricity, dMpc, f_min, M_fed, delta_t,
                                   EccFphiPNorder, EccFrPNorder, EccWaveformPNorder, EccPNFactorizedForm,
                                                     EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform, EcctAppend)


    tPeakFile = glob.glob(run_dir+'/tpeakOmegaHiSR*.dat')
    #print(run_dir,tPeakFile)
    #sys.stdout.write(run_dir+'tISCO*dat '+ISCO_file[0]+' \n')
    #print(ISCO_file)
    if tPeakFile  != []:
        try:
            #data = np.loadtxt( tPeakFile[0] )
            data = np.genfromtxt(tPeakFile[0])
            tpeakFound =data[-1]
        except:
            tpeakFound = -1
            pass
    else:
        tpeakFound =-1.

    iamp22v4E = InterpolatedUnivariateSpline(timeNRv4E, amp22v4E)
    iomega22v4E = InterpolatedUnivariateSpline(timeNRv4E, omega22v4E)

    tt = timeNRv4E[(timeNRv4E > tt0) & (ttf >= timeNRv4E )]
    ff_amp22v4E = iamp22v4E(tt)
    ff_omega22v4E = iomega22v4E(tt)

    ff_amp22v4 = iamp22v4(tt)
    ff_omega22v4 = iomega22v4(tt)


    # Compute the absolute difference
    absdiffOmega = np.abs(ff_omega22v4E - ff_omega22v4)
    absdiffAmp = np.abs(ff_amp22v4E - ff_amp22v4)

    maxabsdiffAmp = max(absdiffAmp)
    maxabsdiffOmega = max(absdiffOmega)

    absreldiffOmega = np.abs(ff_omega22v4E/ff_omega22v4-1)
    absreldiffAmp = np.abs(ff_amp22v4E/ff_amp22v4-1)

    maxabsreldiffOmega = max(absreldiffOmega)
    maxabsreldiffAmp = max(absreldiffAmp)

    l2normdiffOmega = np.sqrt(np.abs(ff_omega22v4E*ff_omega22v4E-ff_omega22v4*ff_omega22v4))
    l2normdiffAmp = np.sqrt(np.abs(ff_amp22v4E*ff_amp22v4E-ff_amp22v4*ff_amp22v4))

    maxl2normdiffOmega = max(l2normdiffOmega)
    maxl2normdiffAmp = max(l2normdiffAmp)

    imaxAmp =np.argmax(absdiffAmp)
    imaxOmega =np.argmax(absdiffOmega)

    tmaxAmp = tt[imaxAmp]
    tmaxOmega = tt[imaxOmega]

    #diffOmega_list.append(maxdiffOmega)
    #diffAmp_list.append(maxdiffAmp)
    #if maxabsdiffAmp > diffthreshold or maxabsdiffOmega > diffthreshold:
    if maxabsreldiffOmega > diffthreshold:

        fminTag=np.round(f_ref,3)
        qTag = np.round(q,3)
        eTag = np.round(eccentricity,3)
        chi1Tag =  np.round(chi1,3)
        chi2Tag =  np.round(chi2,3)

        title=r'SEOBNRv4E $ \quad q='+str(qTag)+'$, $\quad f_{min}=$'+str(fminTag)+'Hz, $\quad M_{tot}=$'+str(M_fed)+\
                        '$M_\odot, \quad \chi1 =$'+str(chi1Tag)+'$, \quad \chi2 =$'+str(chi2Tag)+'$  \quad  e =$'+str(eTag)


        ###################################################################
        # Plot the amplitude22 and omega22

        # Plot omega22
        plt.figure()

        plt.plot(timeNRv4, omega22v4, label=r'v4 $\omega_{22}$',linestyle='solid')
        plt.plot(timeNRv4E, omega22v4E, label=r'v4E $\omega_{22}$',linestyle='dashed')#, color= 'k')

        # Plot amplitude22
        plt.plot(timeNRv4, amp22v4,label=r'v4 $|h_{22}| $',linestyle='solid')
        plt.plot(timeNRv4E, amp22v4E,label=r'v4E $|h_{22}| $',linestyle='dashed')#, color= 'k')


        plt.legend(loc='best', prop={'size': 20}, ncol=2)
        plt.xlabel('t/M',fontsize=20)
        plt.ylabel(r'',fontsize=20)
        plt.title(title,fontsize=20)
        plt.grid(b=None)
        plt.xticks(fontsize=24)
        plt.yticks(fontsize=24)

        plt.xlim(-700, ttf)
        plt.ylim(0.001,1.0)
        plt.savefig(plotdir+'OmegaAmpWaveform_q'+str(q)+'_'+str(chi1)+'_'+str(chi2)+'_e'+str(eccentricity)+'_zoom.png')
        plt.close()


        # Plot the amplitude and omega difference
        plt.figure()

        plt.plot(tt, absdiffOmega, label=r"Omega diff.",linestyle='solid',color='blue')
        plt.plot(tt, absdiffAmp, label=r"Amplitude diff.",linestyle='dashed',color='red')

        plt.legend(loc='best', prop={'size': 20}, ncol=1)
        plt.xlabel('t/M',fontsize=20)

        plt.title(title,fontsize=18)
        plt.grid(b=None)
        plt.xticks(fontsize=24)
        plt.yticks(fontsize=24)

        plt.xlim(tt0, ttf)
        #plt.savefig(plotdir+'diffOmegaAmp_q'+str(q)+'_chi'+str(chi1)+'_e'+str(eccentricity)+'_zoom.png')
        #plt.show()
        plt.savefig(plotdir+'diffOmegaAmp_q'+str(q)+'_'+str(chi1)+'_'+str(chi2)+'_e'+str(eccentricity)+'_zoom.png')

        #plt.show()
        plt.close()

    return  q, chi1, chi2, eccentricity, M_fed, f_min, tmaxOmega, tmaxAmp, maxabsdiffOmega, maxabsdiffAmp, maxabsreldiffOmega, maxabsreldiffAmp, maxl2normdiffOmega, maxl2normdiffAmp,tpeakFound

########################################################################################

# Actual test

########################################################################################


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Ne",type=int,help="Number of eccentricity points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)

## SEOBNRv4E arguments

    p.add_argument(
        "--EccFphiPNorder",
        type=int,
        help="EccFphiPNorder only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder",
        type=int,
        help="EccFrPNorder only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder",
        type=int,
        help="EccWaveformPNorder for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm",
        type=int,
        help="EccPNFactorizedForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta",
        type=float,
        help="EccBeta only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0",
        type=float,
        help="This currently has no effect",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform",
        type=int,
        help="EccNQCWaveform only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )


    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )


    p.add_argument("--eccMin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccMax", type=float,  help="Maximum eccentricity", default=0.7, )

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )

    p.add_argument("--Mtotal", type=float, help="Total mass for the test.", default=60.0,)
    p.add_argument("--fStart", type=float, help="Starting frequency for the test.", default=20.0,)

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)


    # Plot dir
    plotdir = run_dir+"/plots"
    os.makedirs(plotdir, exist_ok=True)

    if plotdir[-1] != "/":
        plotdir=plotdir+"/"


    ## Set some Parameters

    # PN orders of the eccentric corrections
    EccWaveformPNorder = args.EccWaveformPNorder  # Full 2PN order in the WF
    EccPNFactorizedForm = args.EccPNFactorizedForm

    EccFphiPNorder = args.EccFphiPNorder # Full 2PN order
    EccFrPNorder = args.EccFrPNorder  # 2PN order without the tail term

    # Use older expressions
    EccPNRRForm = args.EccPNRRForm
    EccPNWfForm = args.EccPNWfForm

    EccAvNQCWaveform = args.EccAvNQCWaveform
    EcctAppend = args.EcctAppend

    # Set some extrinsic parameters
    phi_s = 0.
    kappa_s =0.
    iota_s = 0.

    # Choose a total mass
    M_fed = args.Mtotal

    # Choose a reference frequency at which to define eccentricity
    f_ref = args.fStart
    f_min = 1 * f_ref
    print(" fmin  = ", f_min, ",   Momega_min = ",f_ref *M_fed *np.pi*lal.MTSUN_SI)

    dMpc=100.0
    distance = dMpc*1e6*lal.PC_SI
    delta_t=1/(2048.0)

    # Parameters of the sigmoid
    EccBeta = args.EccBeta
    Ecct0 = args.Ecct0
    EccNQCWaveform = args.EccNQCWaveform

    # Set boundary for making the test
    Ne = args.Ne
    ecc_max = args.eccMax
    ecc_min = args.eccMin
    ecc_list = np.linspace(ecc_min,ecc_max,Ne)

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    q_list = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    chi_list = np.linspace(chi_min, chi_max, Nchi)


    # Thresholds for the test
    tt0 = - 100
    ttf = 50.0
    diffthreshold = 0.2

    x, y1, y2, z = np.meshgrid(q_list, chi_list,chi_list, ecc_list)

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()


    result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(mergerTest)(
            x[i],
            y1[i],
            y2[i],
            z[i],
            M_fed,
            dMpc,
            f_min,
            delta_t,
            EccFphiPNorder,
            EccFrPNorder,
            EccWaveformPNorder,
            EccPNFactorizedForm,
            EccBeta,
            Ecct0,
            EccNQCWaveform,
            EccPNRRForm,
            EccPNWfForm,
            EccAvNQCWaveform,
            EcctAppend,
            run_dir
            )
        for i in range(len(x))
        )


    result = np.array(result)

    np.savetxt("mergerTest_Nq{}_Ne{}_Nchi{}_result.dat".format(Nq,Ne,Nchi), result)
