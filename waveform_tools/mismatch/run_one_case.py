#!/usr/bin/env/python3
import argparse
import numpy as np
import sys

sys.path.append("/home/sossokine/Sources/waveform_tools")
from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from joblib import delayed, Parallel


def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="For Phenom waveforms whether to minimize over reference frequency or rigid rotation of the spins",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )

    args = p.parse_args()

    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()

    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(run_unfaithfulness)(
            y[i],
            x[i],
            args.iota_s,
            i,
            NR_file=args.NR_file,
            ellMax=args.ell_max,
            template_approx=args.approximant,
            fhigh=2048.0,
            minimization_type=args.min_type,
            unfaithfulness_type=args.unfaithfulness_type,
        )
        for i in range(len(x))
    )


if __name__ == "__main__":
    main()
