#!/usr/bin/env python3

import numpy as np
import sys
import lal


from dataclasses import dataclass


@dataclass
class waveform_params:
    """Container for all the parameters needed to generate a waveform
    with LALSimulation
    """

    m1: float
    m2: float
    s1x: float
    s1y: float
    s1z: float
    s2x: float
    s2y: float
    s2z: float
    iota: float
    phi: float
    f_ref: float
    f_min: float
    distance: float
    delta_t: float
    delta_f: float
    wf_param: lal.Dict
    approx: str
    domain: str = "TD"
    ecc: float = 0.0  # eccentricity
    mean_anomaly: float = 0.0  # mean anomaly
    EccFphiPNorder: int = 99  # PN orders of FphiNC for v4E (for testing)
    EccFrPNorder: int = 99    # PN orders of FrNC for v4E (for testing)
    EccWaveformPNorder: int = 16  # PN orders of hEcc of v4E (for testing, not implemented yet)
    EccPNFactorizedForm: int = 1   # Flag to switch on and off factorized form for NC the fluxes and the eccentric corrections to the waveform
    EccBeta: float = 0.06 # Coefficient of the sigmoid
    Ecct0: float = -100 # time shift in the sigmoid. Currently, not used in favor the first zero in pr
    EccNQCWaveform: int = 1 # Activate windowing of the NQC waveform
    EccPNRRForm: int = 1 # time shift in the sigmoid. Currently, not used in favor the first zero in pr
    EccPNWfForm: int = 1 # Activate windowing of the NQC waveform
    Nharmonic: int = 7  # PN orders of hEcc of v4E (for testing, not implemented yet)
    kAdvance: int = 1   # Flag to switch on and off factorized form for NC the fluxes and the eccentric corrections to the waveform
    TEOB_IC: int = 0   # 0 periastron, 1 average frequency,  2 apastron
    
    def __post_init__(self):
        # Sanity checks
        assert self.m1 > 0 and self.m2 > 0, "Masses must be  positive!"
        assert (
            np.sqrt(self.s1x ** 2 + self.s1y ** 2 + self.s1z ** 2) <= 1
        ), "Spin on body 1 must have magnitude <=1!"
        assert (
            np.sqrt(self.s2x ** 2 + self.s2y ** 2 + self.s2z ** 2) <= 1
        ), "Spin on body 2 must have magnitude <=1!"
        assert (self.delta_t and not self.delta_f) or (
            not self.delta_t and self.delta_f
        ), "You must either set delta_t or delta_f, but not both!"
        assert (
            self.domain == "TD" or self.domain == "FD"
        ), "The domain must either be 'TD' or 'FD'!"
        assert self.ecc >= 0, "Eccentricity must be positive!"
