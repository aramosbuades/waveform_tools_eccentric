#!/usr/bin/env python
import argparse
import glob
import os
import subprocess as sp
import uuid #, json
import itertools
from itertools import  permutations
import numpy as np

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


def permutationNoRepetition(case_files):

    combination_list = []

    for item in permutations(case_files,2):

        combination_list.append(sorted(item))
    combination_list.sort()
    new_list = list(combination_list for combination_list,_ in itertools.groupby(combination_list))

    return new_list


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )

    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--file_dirlist", type=str, help="File containing the list of LVCNR files to run on. Alternatively if a path is provided it will run on all the LVCNR files on that path.")

    p.add_argument("--ell_max", type=int, help="Maximum ell to use", default=5)

    p.add_argument("--N",type=int,help="The number of cases in a chunk",default=8)
    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")

    p.add_argument("--mode_array_signal",  type=str, help="List of modes for the signal,i.e,  [[2, 2], [2, -2]]", default=None)
    p.add_argument("--mode_array_template", type=str, help="List of modes for the signal,i.e,  [[2, 2], [2, -2]]", default=None)
    p.add_argument("--iota_s", type=float, help="Inclination angle.", default=np.pi/3.)
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)
    try:
        os.chdir(run_dir)
    except OSError:
        print("Could not go into the run directory {}, quitting!".format(run_dir))
        exit(-1)

    script_dir =  os.environ['WAVEFORM_TOOLS_PATH']


    NRfile_list = sorted(glob.glob(args.file_dirlist+"*Res3.h5"))

    NR_pair_cases = []

    for NRfile in NRfile_list:

        basename  = os.path.splitext(os.path.basename(NRfile))[0]

        corename=basename.split("_Res")[0]
        case_files =  sorted(glob.glob(args.file_dirlist+corename+"*.h5"))

        caselist= permutationNoRepetition(case_files)
        NR_pair_cases.append(caselist)

    NRcases = [item for sublist in NR_pair_cases for item in sublist]

    chk = list(chunks(NRcases, args.N))

    submit_time = args.submit_time

    header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                 # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue
#SBATCH --exclusive

cd {}
"""

    if args.mode_array_signal == "None":
        #mode_array_signal = json.loads(args.mode_array_signal)
        mode_array_signal = None
    else:
        mode_array_signal = args.mode_array_signal

    if args.mode_array_template == "None":
        #mode_array_template = json.loads(args.mode_array_template)
        mode_array_template = None
    else:
        mode_array_template = args.mode_array_template

    queue = args.queue
    unq_name = uuid.uuid4().hex

    for i in range(len(chk)):

        nm = unq_name + "_{}".format(i)
        fp = open("chunk_{}.sh".format(i), "w")
        fp.write(header.format(i,i,i, queue, submit_time, run_dir))

        for k in range(len(chk[i])):
            NR_file, NR_file_2 = chk[i][k]

            cmd = """python $WAVEFORM_TOOLS_PATH/compare_one_NR_NR_Levs.py --NR_file {} --NR_file_2 {} --iota_s {} --mode_array_signal {} --mode_array_template {}  \n""".format(
                        NR_file,
                        NR_file_2,
                        args.iota_s,
                        mode_array_signal,
                        mode_array_template,
                    )
            fp.write(cmd)

        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)

        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
