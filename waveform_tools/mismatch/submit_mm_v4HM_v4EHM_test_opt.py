#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp




def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]

if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)
    p.add_argument("--Ne",type=int,help="Number of chi points",default=10)

    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )

    p.add_argument("--approximant", type=str,  help="Waveform approximant (default SEOBNRv4E_opt)", default="SEOBNRv4E_opt", )
    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=64000)


    p.add_argument("--eccMin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccMax", type=float,  help="Maximum eccentricity", default=0.3, )

    p.add_argument("--iota_s", type=float, help="The inclination of the source", default=np.pi / 3, )
    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")

##################################################

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)

    if run_dir[-1]!="/":
        run_dir +='/'

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    #q_grid = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    #chi_list = np.linspace(chi_min, chi_max, Nchi)

    ecc_min= args.eccMin
    ecc_max= args.eccMax
    Ne=args.Ne

    submit_time = args.submit_time
    queue = args.queue

    script_dir =  args.script_dir

    iota_s = args.iota_s

    """
    ecc_grid = np.linspace(ecc_min, ecc_max, Ne)
    chi1_grid = np.linspace(chi_min, chi_max, Nchi)
    chi2_grid = np.linspace(chi_min, chi_max, Nchi)
    """
    seed=777
    np.random.seed(seed)
    q_grid = np.random.uniform(q_min, q_max, Nq)
    np.random.seed(seed+999998)
    chi1_grid = np.random.uniform(chi_min, chi_max, Nchi)
    np.random.seed(seed+999997)
    chi2_grid = np.random.uniform(chi_min, chi_max, Nchi)
    np.random.seed(seed+99999)
    ecc_grid = np.random.uniform(ecc_min, ecc_max, Ne)



    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)

    # Set boundary for making the test

    x_wf, y_wf,z_wf, w_wf = np.meshgrid(q_grid, chi1_grid,chi2_grid,ecc_grid) # grid for the wf generation

    k_wf, p_wf = np.meshgrid( kappas, phis) # grid of kappa_s and phase_s on which to evaluate each generated waveform
    k_wf = k_wf.flatten()
    p_wf = p_wf.flatten()

    x_wf = x_wf.flatten()
    y_wf = y_wf.flatten()
    z_wf = z_wf.flatten()
    w_wf = w_wf.flatten()



    NN_wf = len(x_wf)
    NN_kp = len(k_wf)

    idx_list = np.arange(0,NN_wf)
    Nchunks =args.Nchunks
    n_slurm_jobs = int(NN_wf/Nchunks)
    if n_slurm_jobs ==0:
        n_slurm_jobs=1
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))


    header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue


#source /home/aramosbuades/load_LALenv.sh

cd {}
"""

    for i in range(n_slurm_jobs):

        case = cases_per_slurm_job[i]

        fp = open("chunk_{}.sh".format(i), "w")
        fp.write(header.format(i,i,i,queue, submit_time, run_dir))



        cmd = """python {}/run_mm_smoothness_test_v4HM_vs_vEHM_opt.py --run_dir {} --Nq {} --Nchi {} --Ne {} --qMin {} --qMax {} --chiMin {} --chiMax {} --eMin {} --eMax {} --iota_s {} --approximant {} --Nchunks {} --idx_chunk {} \n""".format(
                script_dir,
                run_dir,
                Nq,
                Nchi,
                Ne,
                q_min,
                q_max,
                chi_min,
                chi_max,
                ecc_min,
                ecc_max,
                iota_s,
                args.approximant,
                Nchunks,
                i
                )
        fp.write(cmd)


        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)

        print(submit_cmd)
        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
