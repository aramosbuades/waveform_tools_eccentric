#!/usr/python
# Load module
import sys, h5py, glob,os

#binary_path="/home/antoniramosbuades/git/seobnrv4e/seobnrv4e_code/"
#output_dir="."
#outfile=output_dir+"/simulation.dat"

#sys.path.append("/home/antoniramosbuades/git/waveform_tools_eccentric/waveform_tools/mismatch/")
sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])
#sys.path.append("/home/antoniramosbuades/git/teobresumse/Python/")

import matplotlib
from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSe module
#import EOBRun_module
import matplotlib.pyplot as plt, numpy as np
import pandas as pd
import pycbc, json

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *


def compute_ecc(tHorizonv4E, tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima):

    if len(tH_maxima)>3:

        iomega_orb_maxima = InterpolatedUnivariateSpline(tH_maxima, omega_orb_maxima)
        iomega_orb_minima = InterpolatedUnivariateSpline(tH_minima, omega_orb_minima)
        omegaOrb_maxima = iomega_orb_maxima(tHorizonv4E)
        omegaOrb_minima = iomega_orb_minima(tHorizonv4E)
        ecc_omegaorb = (np.sqrt(omegaOrb_maxima)-np.sqrt(omegaOrb_minima))/(np.sqrt(omegaOrb_maxima)+np.sqrt(omegaOrb_minima))

        # Compute the averaged orbital frequency
        #omegaOrb_av =1./(tH_maxima[1:]-tH_maxima[:-1])*(iphase_orb(tH_maxima[1:])-iphase_orb(tH_maxima[:-1]))

        # Just in case the waveform is noisy
        ecc_omega_orb1=ecc_omegaorb[0]
    else:
        ecc_omega_orb1=-1

    return ecc_omega_orb1



def maxima_minima_fit(ff, tt, tt0, deltaT):

    idx_data=(np.where((tt <= tt0+deltaT) & (tt >= tt0 - deltaT)))[0]
    tt_idx=tt[idx_data]
    ff_idx=ff[idx_data]


    fit = np.polyfit(tt_idx, ff_idx, 2)
    polynomial = np.poly1d(fit)
    ff_fit=polynomial(tt_idx)
    p2 = np.polyder(polynomial)
    tt_parabola_max=np.roots(p2)

    return tt_parabola_max[0], polynomial(tt_parabola_max)[0]

def loop_maxima_minima_fit(ff, tt, ttmaxmin_list, deltaT):

    tt_maxs=[]
    ff_maxs=[]
    for tt0 in ttmaxmin_list[:-1]:

        tt_max_fit, ff_max_fit = maxima_minima_fit(ff, tt, tt0, deltaT)

        tt_maxs.append(tt_max_fit)
        ff_maxs.append(ff_max_fit)

    ttmaxs=np.array(tt_maxs)
    ffmaxs=np.array(ff_maxs)

    return ttmaxs,ffmaxs

def duplicate_position(tH_minima,ii):
    tti=tH_minima[ii]
    ttdiff= np.array([np.abs(ttw-tti) for ttw in tH_minima])
    ttdiff=np.delete(ttdiff, [ii]) # Remove position of the element
    pos=np.where(ttdiff<1e-10)[0]
    #tH_minima=np.delete(tH_minima, [pos])
    #return tH_minima
    return pos

def duplicate_position_list(tH_minima):

    pos_list=[]
    for ii in range(len(tH_minima)):
        pos= duplicate_position(tH_minima,ii)

        if pos.size!=0:
            pos_list.append(pos)

    if pos_list:
        out = np.concatenate(pos_list).ravel().tolist()
        pos_to_remove = list(set(out))
    else:
        pos_to_remove=[]


    #tH_minima=np.delete(tH_minima, [pos])
    #return tH_minima
    return pos_to_remove

def remove_duplicate_elements(tH_minima,omega_orb_minima):

    pos_repeat_min=duplicate_position_list(omega_orb_minima)

    if pos_repeat_min:
        omega_orb_minima = np.delete(omega_orb_minima, pos_repeat_min)
        tH_minima = np.delete(tH_minima, pos_repeat_min)

    return tH_minima,omega_orb_minima

def measure_eccentricities(q0:float,chi1:float,chi2:float, ecc:float, omega_min:float,
                          delta_t:float):


    EccPNFactorizedForm = 1
    EccFphiPNorder = 99
    EccFrPNorder = 99
    EccWaveformPNorder = 16
    EccAvNQCWaveform=1
    EcctAppend=20
    EccPNRRForm =  EccPNWfForm = 1
    EccBeta = 0.09
    Ecct0 = 100
    EccNQCWaveform = 1
    EccIC=0

    timeNRv4E, tHorizonv4E, hlmv4E, amplmv4E, phaselmv4E, omegalmv4E, \
    omega_orbv4E, phase_orbv4E =  SEOBNRv4E_modes(q0,chi1,chi2, ecc, omega_min, delta_t,
                                                  EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                  EccPNFactorizedForm,
                                                  EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm,
                                                  EccAvNQCWaveform,EcctAppend, EccIC)



    tt = tHorizonv4E
    ff =  omega_orbv4E

    tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima = compute_MaxMin(tHorizonv4E, omega_orbv4E)
    tH_minima,omega_orb_minima = remove_duplicate_elements(tH_minima,omega_orb_minima)
    tH_maxima,omega_orb_maxima = remove_duplicate_elements(tH_maxima,omega_orb_maxima)

    if len(omega_orb_maxima)>3 and len(omega_orb_minima)>3:

        ecc_omegaorb=compute_ecc(tt, tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima)

        # polynomial fit
        deltaT=10
        tt_max_fit,ff_max_fit =loop_maxima_minima_fit(ff, tt, tH_maxima, deltaT)
        tt_min_fit,ff_min_fit =loop_maxima_minima_fit(ff, tt, tH_minima, deltaT)

        tt_max_fit,ff_max_fit = remove_duplicate_elements(tt_max_fit,ff_max_fit)
        tt_min_fit,ff_min_fit = remove_duplicate_elements(tt_min_fit,ff_min_fit)

        ecc_omegaorb_fit=compute_ecc(tt, tt_max_fit, ff_max_fit, tt_min_fit, ff_min_fit)
    else:
         ecc_omegaorb, ecc_omegaorb_fit = -1,-1




    # polynomial fit 22-mode
    if timeNRv4E[0]<-3000:
        t0=-1000
    elif timeNRv4E[0]>-3000 and  timeNRv4E[0]<-1500:
        t0 = -500

    elif timeNRv4E[0]>-1500 and  timeNRv4E[0]<-1000:
        t0 = -250
    else:
        t0=-100

    tt = timeNRv4E[timeNRv4E < t0 ]
    ff = omegalmv4E[2,2][timeNRv4E < t0]
    ampff =  amplmv4E[2,2][timeNRv4E < t0]

    tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(tt,ff,ampff)
    tNR_minima,omega_minima = remove_duplicate_elements(tNR_minima,omega_minima)
    tNR_maxima,omega_maxima = remove_duplicate_elements(tNR_maxima,omega_maxima)


    if len(omega_maxima)>3 and len(omega_minima):

        ecc_omega22=compute_ecc(tt, tNR_maxima, omega_maxima, tNR_minima, omega_minima)

        tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(tt,ff, ampff)
        tNR_minima,omega_minima = remove_duplicate_elements(tNR_minima,omega_minima)
        tNR_maxima,omega_maxima = remove_duplicate_elements(tNR_maxima,omega_maxima)

        # polynomial fit 22-mode

        deltaT=10
        tt_max_fit,ff_max_fit =loop_maxima_minima_fit(ff,tt, tNR_maxima, deltaT)
        tt_min_fit,ff_min_fit =loop_maxima_minima_fit(ff,tt, tNR_minima, deltaT)

        tt_max_fit,ff_max_fit = remove_duplicate_elements(tt_max_fit,ff_max_fit)
        tt_min_fit,ff_min_fit = remove_duplicate_elements(tt_min_fit,ff_min_fit)
        ecc_omega22_fit=compute_ecc(tt, tt_max_fit, ff_max_fit, tt_min_fit, ff_min_fit)

    else:
         ecc_omega22, ecc_omega22_fit = -1,-1

    #print(q0, chi1, chi2, ecc)
    return q0, chi1, chi2, ecc, ecc_omegaorb, ecc_omegaorb_fit, ecc_omega22, ecc_omega22_fit


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument(
        "--outdir",
        type=str,
        help="Directory where to store the output",
        default=os.getcwd(),
    )

    p.add_argument("--Npoints",type=int,help="Number of points in random exploration",default=10)

    p.add_argument("--eccmin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccmax", type=float,  help="Maximum eccentricity", default=0.3, )

    p.add_argument("--qmin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qmax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chimin", type=float,  help="Minimum chi (chi1 != chi2)", default=-0.99, )
    p.add_argument("--chimax", type=float,  help="Maximum chi (chi1 != chi2)", default=0.99, )
    p.add_argument("--seed", type=int,  help="Random seed to use for the random distributions", default=111, )


    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)
    p.add_argument("--Nout",type=int,help="Number of iterations to produce output",default=500)
    p.add_argument("--debug",action="store_true",help="Debug mode")

    p.add_argument("--idx_chunk",type=int,help="Subset of cases to run",default=1)
    args = p.parse_args()


    NN=args.Npoints #Number of points over which t loop
    qmin=args.qmin
    qmax=args.qmax
    chimin=args.chimin
    chimax=args.chimax
    emin=args.eccmin
    emax=args.eccmax
    Nchunks = args.Nchunks
    Nout = args.Nout
    seed=args.seed
    debug=args.debug

    outdir=args.outdir
    if outdir[-1]!="/":
        outdir +='/ic_data/'

    os.makedirs(outdir,exist_ok=True)

    np.random.seed(seed)
    q_list = np.random.uniform(qmin,qmax,NN)
    np.random.seed(seed+999998)
    chi1_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+999997)
    chi2_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+99999)
    ecc_list = np.random.uniform(emin,emax,NN)


    x, y1, y2, z = q_list, chi1_list,chi2_list, ecc_list

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()


    idx_list = np.arange(0,NN)

    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    idx_chunk=args.idx_chunk

    case0=cases_per_slurm_job[idx_chunk]
    cases_per_output = list(chunks(case0,Nout))



    # For the test we set
    M_fed=60.0
    f_min=20.0

    freq_1M= M_fed*f_min
    omega_min= freq_1M*(np.pi*lal.MTSUN_SI)
    #print(omega_min)
    delta_t=1./(2*2048.)

    k=0
    for case in cases_per_output:
        if args.debug:
            result_list=[]
            for i in case:
                #print(i)
                #print(x[i],
                #    y1[i],
                #    y2[i],
                #    z[i],
                #    )
                print('============================================================')
                result=measure_eccentricities(
                    x[i],
                    y1[i],
                    y2[i],
                    z[i],
                    omega_min,
                    delta_t)
                print(f"i = {i},  result = {result}")
                result_list.append(result)
        else:
            result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
            delayed(measure_eccentricities)(
                x[i],
                y1[i],
                y2[i],
                z[i],
                omega_min,
                delta_t)
            for i in case
            )


        result = np.array(result)

        np.savetxt(outdir+"eccICtest_seed{}_Npoints{}_k{}_idx{}_result.dat".format(seed,NN,k,idx_chunk), result)
        k+=1
