#!/usr/python

import argparse
import glob, os ,sys
#import subprocess as sp

import lal, lalsimulation
import pandas as pd
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from joblib import delayed, Parallel

import EOBRun_module

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from waveform_analysis  import *

plt.rcParams["figure.figsize"] = (14,10)
plt.rcParams['font.size'] = '18'

#sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

########   Functions required by the test


# Function to parse the modes of TEOBResumSe to the linear index they use
def modes_to_k(modes):
    """
    Map multipolar (l,m) -> linear index k
    """
    return [int(x[0] * (x[0] - 1) / 2 + x[1] - 2) for x in modes]

def TEOBResumSE_tAmpfreq(q: float, chi1: float,chi2: float, eccentricity: float, dMpc:float, f_min:float, Mtot:float, srate: float, phi:float, iota:float):

    print("q = ",q, " chi1 = ",chi1, " chi2 = ",chi2, "ecc = ", eccentricity)
    pars = {
        "M": Mtot,
        "q": q,
        "chi1": chi1,
        "chi2": chi2,
        "Lambda1": 0.0,
        "Lambda2": 0.0,
        "domain": 0,  # Set 1 for FD. Default = 0
        "arg_out": 1,  # Output hlm/hflm. Default = 0
        "use_mode_lm": [1],  # List of modes to use/output through EOBRunPy
        "output_lm": [1],  # List of modes to print on file
        #'srate'       : np.round(1.0/p.delta_t)*0.5, #srate at which to interpolate. Default = 4096.
        "srate_interp": srate,
        #'srate_interp'       : 1650.0, #srate at which to interpolate. Default = 4096.
        "use_geometric_units": 0,  # output quantities in geometric units. Default = 1
        "df": 0.01,  # df for FD interpolation
        "initial_frequency": f_min,  # in Hz if use_geometric_units = 0, else in geometric units
        "interp_uniform_grid": 1,  # interpolate mode by mode on a uniform grid. Default = 0 (no interpolation)
        "distance": dMpc,  # Distance in Mpc
        "inclination": iota,
        "coalescence_angle": phi,
        "ecc": eccentricity,  # Eccentricity. Default = 0.
        'ecc_freq' : 1,      #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation. Default = 1

    }

    #Run the WF generator
    t, hp, hcm, hlm, dyn = EOBRun_module.EOBRunPy(pars)

    hlm1={}
    amplm = {}
    phaselm = {}
    omegalm = {}
    amplm[2,2]=hlm['1'][0]
    hlm1[2,2]=hlm['1'][0]*np.exp(1.j*hlm['1'][1])
    timeNR = SectotimeM(t,Mtot) # In geometric units
    phaselm[2,2] = - np.unwrap(np.angle(hlm1[2,2]))
    omegalm[2,2] = compute_freqInterp(timeNR, hlm1[2,2])

    imax=np.argmax(amplm[2,2])
    tPeak=timeNR[imax]
    timeNR -=tPeak

    return timeNR, amplm[2,2], omegalm[2,2]



def mergerTest(q:float, chi1 : float, chi2:float, eccentricity:float, M_fed:float, dMpc:float, f_min:float, delta_t:float):


    m1_dim = q / (1.0 + q)
    m2_dim = 1 - m1_dim
    eta= q/(1+q)**2

    timeNRv4, amp22v4, omega22v4 = TEOBResumSE_tAmpfreq(q, chi1, chi2, 0.0, dMpc, f_min, M_fed, 1./delta_t, phi_s, iota_s)
    iamp22v4 = InterpolatedUnivariateSpline(timeNRv4, amp22v4)
    iomega22v4 = InterpolatedUnivariateSpline(timeNRv4, omega22v4)



    timeNRv4E, amp22v4E, omega22v4E = TEOBResumSE_tAmpfreq(q, chi1, chi2, eccentricity, dMpc, f_min, M_fed, 1./delta_t, phi_s, iota_s)

    iamp22v4E = InterpolatedUnivariateSpline(timeNRv4E, amp22v4E)
    iomega22v4E = InterpolatedUnivariateSpline(timeNRv4E, omega22v4E)

    tt = timeNRv4E[(timeNRv4E > tt0) & (ttf >= timeNRv4E )]
    ff_amp22v4E = iamp22v4E(tt)
    ff_omega22v4E = iomega22v4E(tt)

    ff_amp22v4 = iamp22v4(tt)
    ff_omega22v4 = iomega22v4(tt)


    # Compute the absolute difference
    absdiffOmega = np.abs(ff_omega22v4E - ff_omega22v4)
    absdiffAmp = np.abs(ff_amp22v4E - ff_amp22v4)

    maxabsdiffAmp = max(absdiffAmp)
    maxabsdiffOmega = max(absdiffOmega)

    absreldiffOmega = np.abs(ff_omega22v4E/ff_omega22v4-1)
    absreldiffAmp = np.abs(ff_amp22v4E/ff_amp22v4-1)

    maxabsreldiffOmega = max(absreldiffOmega)
    maxabsreldiffAmp = max(absreldiffAmp)

    l2normdiffOmega = np.sqrt(np.abs(ff_omega22v4E*ff_omega22v4E-ff_omega22v4*ff_omega22v4))
    l2normdiffAmp = np.sqrt(np.abs(ff_amp22v4E*ff_amp22v4E-ff_amp22v4*ff_amp22v4))

    maxl2normdiffOmega = max(l2normdiffOmega)
    maxl2normdiffAmp = max(l2normdiffAmp)

    imaxAmp =np.argmax(absdiffAmp)
    imaxOmega =np.argmax(absdiffOmega)

    tmaxAmp = tt[imaxAmp]
    tmaxOmega = tt[imaxOmega]

    #diffOmega_list.append(maxdiffOmega)
    #diffAmp_list.append(maxdiffAmp)
    #if maxabsdiffAmp > diffthreshold or maxabsdiffOmega > diffthreshold:
    if maxabsreldiffOmega > diffthreshold:

        fminTag=np.round(f_ref,3)
        qTag = np.round(q,3)
        eTag = np.round(eccentricity,3)
        chi1Tag =  np.round(chi1,3)
        chi2Tag =  np.round(chi2,3)

        title=r'TEOBResumSE $ \quad q='+str(q)+'$, $\quad f_{min}=$'+str(fminTag)+'Hz, $\quad M_{tot}=$'+str(M_fed)+\
                        '$M_\odot, \quad \chi1 =$'+str(chi1Tag)+'$, \quad \chi2 =$'+str(chi2Tag)+'$  \quad  e =$'+str(eTag)


        ###################################################################
        # Plot the amplitude22 and omega22

        # Plot omega22
        plt.figure()

        plt.plot(timeNRv4, omega22v4, label=r'TEOB $\omega_{22}$',linestyle='solid')
        plt.plot(timeNRv4E, omega22v4E, label=r'TEOBE $\omega_{22}$',linestyle='dashed')#, color= 'k')

        # Plot amplitude22
        plt.plot(timeNRv4, amp22v4/4.,label=r'TEOB $|h_{22}| $',linestyle='solid')
        plt.plot(timeNRv4E, amp22v4E/4.,label=r'TEOBE $|h_{22}| $',linestyle='dashed')#, color= 'k')


        plt.legend(loc='best', prop={'size': 20}, ncol=2)
        plt.xlabel('t/M',fontsize=20)
        plt.ylabel(r'',fontsize=20)
        plt.title(title,fontsize=20)
        plt.grid(b=None)
        plt.xticks(fontsize=24)
        plt.yticks(fontsize=24)

        plt.xlim(-700, ttf)
        plt.ylim(0.001,1.0)
        plt.savefig(plotdir+'OmegaAmpWaveform_q'+str(q)+'_chi'+str(chi1)+'_e'+str(eccentricity)+'_zoom.png')
        plt.close()


        # Plot the amplitude and omega difference
        plt.figure()

        plt.plot(tt, absdiffOmega, label=r"Omega diff.",linestyle='solid',color='blue')
        plt.plot(tt, absdiffAmp, label=r"Amplitude diff.",linestyle='dashed',color='red')

        plt.legend(loc='best', prop={'size': 20}, ncol=1)
        plt.xlabel('t/M',fontsize=20)

        plt.title(title,fontsize=18)
        plt.grid(b=None)
        plt.xticks(fontsize=24)
        plt.yticks(fontsize=24)

        plt.xlim(tt0, ttf)
        #plt.savefig(plotdir+'diffOmegaAmp_q'+str(q)+'_chi'+str(chi1)+'_e'+str(eccentricity)+'_zoom.png')
        #plt.show()
        plt.savefig(plotdir+'diffOmegaAmp_q'+str(q)+'_chi1'+str(chi1)+'_chi2'+str(chi2)+'_e'+str(eccentricity)+'_zoom.png')

        #plt.show()
        plt.close()

    return  q, chi1, chi2, eccentricity, M_fed, f_min, tmaxOmega, tmaxAmp, maxabsdiffOmega, maxabsdiffAmp, maxabsreldiffOmega, maxabsreldiffAmp, maxl2normdiffOmega, maxl2normdiffAmp

########################################################################################

# Actual test

########################################################################################


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Ne",type=int,help="Number of eccentricity points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)

    p.add_argument("--eccMin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccMax", type=float,  help="Maximum eccentricity", default=0.7, )

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)


    # Plot dir
    plotdir = run_dir+"/plots"
    os.makedirs(plotdir, exist_ok=True)

    if plotdir[-1] != "/":
        plotdir=plotdir+"/"


    ## Set some Parameters

    # Set some extrinsic parameters
    phi_s = 0.
    kappa_s =0.
    iota_s = 0.

    # Choose a total mass
    M_fed = 60.0

    # Choose a reference frequency at which to define eccentricity
    f_ref = 20.0
    f_min = 1 * f_ref
    print(" fmin  = ", f_min, ",   Momega_min = ",f_ref *M_fed *np.pi*lal.MTSUN_SI)

    dMpc=100.0
    distance = dMpc*1e6*lal.PC_SI
    delta_t=1/(4096.0)

    # Set boundary for making the test
    Ne = args.Ne
    ecc_max = args.eccMax
    ecc_min = args.eccMin
    ecc_list = np.linspace(ecc_min,ecc_max,Ne)

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    q_list = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    chi_list = np.linspace(chi_min, chi_max, Nchi)


    # Thresholds for the test
    tt0 = - 100
    ttf = 50.0
    diffthreshold = 0.1

    x, y1, y2, z = np.meshgrid(q_list, chi_list,chi_list, ecc_list)

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()


    result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(mergerTest)(
            x[i],
            y1[i],
            y2[i],
            z[i],
            M_fed,
            dMpc,
            f_min,
            delta_t
            )
        for i in range(len(x))
        )


    result = np.array(result)

    np.savetxt("mergerTest_Nq{}_Ne{}_Nchi{}_result.dat".format(Nq,Ne,Nchi), result)
