#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp




def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]

if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)

    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )

    p.add_argument("--approximant", type=str,  help="Waveform approximant (default SEOBNRv4E_opt)", default="SEOBNRv4E_opt", )
    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)


    p.add_argument("--chi1_s", type=float,  help="chi_1 signal", default=0.8, )
    p.add_argument("--chi2_s", type=float,  help="chi_2 signal", default=0.8, )

    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")

##################################################

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)

    if run_dir[-1]!="/":
        run_dir +='/'

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    q_grid = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    chi_list = np.linspace(chi_min, chi_max, Nchi)

    submit_time = args.submit_time
    queue = args.queue

    script_dir =  args.script_dir

    chi1_grid = np.linspace(chi_min, chi_max, Nchi)
    chi2_grid = np.linspace(chi_min, chi_max, Nchi)
    x, y,z = np.meshgrid(q_grid, chi1_grid,chi2_grid)

    x = x.flatten()
    y = y.flatten()
    z = z.flatten()
    NN = len(x)

    idx_list = np.arange(0,NN)
    Nchunks =args.Nchunks

    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))
    # Set boundary for making the test

    header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue


#source /home/aramosbuades/load_LALenv.sh

cd {}
"""

    for i in range(n_slurm_jobs):

        case = cases_per_slurm_job[i]

        fp = open("chunk_{}.sh".format(i), "w")
        fp.write(header.format(i,i,i,queue, submit_time, run_dir))



        cmd = """python {}/run_mm_smoothness_test.py --run_dir {} --Nq {} --Nchi {} --qMin {} --qMax {} --chiMin {} --chiMax {} --approximant {} --Nchunks {} --idx_chunk {} --chi1_s {} --chi2_s {} \n""".format(
                script_dir,
                run_dir,
                Nq,
                Nchi,
                q_min,
                q_max,
                chi_min,
                chi_max,
                args.approximant,
                Nchunks,
                i,
                args.chi1_s,
                args.chi2_s
                )
        fp.write(cmd)


        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)

        print(submit_cmd)
        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
