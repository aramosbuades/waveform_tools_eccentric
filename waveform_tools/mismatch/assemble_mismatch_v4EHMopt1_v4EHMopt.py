#!/usr/bin/env python
import argparse
import glob
import numpy as np
import os

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--result_dir", type=str, help="Directory containing the output from waveform_tools.")
    p.add_argument("--output_dir", type=str, help="Directory for the output files.")
    args = p.parse_args()

    output_dir = args.output_dir
    if output_dir[-1]!="/":
        output_dir +="/"

    result_dir = args.result_dir
    if result_dir[-1]!="/":
        result_dir +="/"

    cases = []
    for file in glob.glob(result_dir+"*_00000.dat"):
        basename = os.path.basename(file)
        name = basename.split("_result_00000.dat")[0]
        cases.append(name)

    print(len(cases))

    for case in cases:

        name=case
        lst = glob.glob(result_dir+"{}*".format(cases[0]))

        if os.path.isfile(output_dir+"mismatches_{}.dat".format(name)):
            print("Skipping {}".format(name))
            continue
        print(case)
        lst = glob.glob(result_dir+"{}*".format(name))
        if not lst:
            continue
        if len(lst) != 16:
            print("You are missing some mismatch file!")
        weighted_mismatches = 0.0
        SNRs = 0.0
        mismatches = 0.0
        weighted_IH_q = 0.0
        IH_q = 0.0
        for it in lst:
            d = np.genfromtxt(it)
            #print(d.shape)
            if len(d.shape) > 1:
                weighted_mismatches += (1 - d[:, 6]) ** 3 * d[:, 7] ** 3
                mismatches += d[:, 6]
                SNRs += d[:, 7] ** 3
                IH_q += d[:, 6]
                weighted_IH_q += (1 - d[:, 6]) ** 3 * d[:, 7] ** 3
                masses = d[:, 5]
            else:
                weighted_mismatches += (1 - d[6]) ** 3 * d[7] ** 3
                mismatches += d[6]
                SNRs += d[7] ** 3
                IH_q += d[6]
                weighted_IH_q += (1 - d[6]) ** 3 * d[7] ** 3
                masses = d[5]
            mismatches /= len(lst)
            IH_q /= len(lst)
            #lst = glob.glob("res*.dat")
            averaged_mismatches = 1 - (weighted_mismatches / SNRs) ** (1.0 / 3)
            averaged_IH_q = 1 - (weighted_IH_q / SNRs) ** (1.0 / 3)

            q_array, chi1_array, chi2_array = d[:,0], d[:,1], d[:,2]

            ecc_array  = d[:,3]
            meanAno_array = d[:,4]
            iota_array = d[:,-1]
            res = np.c_[q_array, chi1_array, chi2_array,ecc_array, meanAno_array, iota_array, masses, mismatches, averaged_mismatches, IH_q, averaged_IH_q]
            np.savetxt(output_dir+"/mismatches_{}.dat".format(name), res)
