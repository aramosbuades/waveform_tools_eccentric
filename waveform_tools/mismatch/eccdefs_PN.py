import numpy as np
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema

def eOmOrb_1PN(eta, et,x, gamma):
    return et - (1/2.)*et*x*(-4 + eta)*gamma**2

def eOm22_1PN(eta,et,x,gamma):
    return (np.sqrt(2 - et)*(1 + et) + (-1 + et)*np.sqrt(2 + et))/(np.sqrt(2 - \
et)*(1 + et) + np.sqrt(2 - 3*et + pow(et,3))) - (et*(-1380 + 192*eta \
+ pow(et,2)*(101 + 54*eta))*pow(gamma,2)*x)/(84.*(4 - 5*pow(et,2) + \
pow(et,4) + 2*np.sqrt(4 - pow(et,2))))

def eOmOrb_3PN(eta,et,xp,gamma):
    return et - ((-3 + et)*np.sqrt(1 + et)*(9*pow(et,2) - 18*(-1 + np.sqrt(1 - \
pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - pow(et,2)) + (41 \
- 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + pow(et,3)*(48 - \
35*eta + 5*pow(eta,2)))*pow(gamma,4)*pow(xp,2))/(96.*pow(-1 + et,2)) \
- (pow(et,3)*(3 + et)*np.sqrt(1 - pow(et,2))*pow(-4 + \
eta,3)*pow(gamma,6)*pow(xp,3))/(64.*pow(1 - et,2.5)*pow(1 + et,3.5)) \
+ (pow(et,2)*np.sqrt(1 - pow(et,2))*(-4 + eta)*(9*pow(et,2) - 18*(-1 \
+ np.sqrt(1 - pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - \
pow(et,2)) + (41 - 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + \
pow(et,3)*(48 - 35*eta + \
5*pow(eta,2)))*pow(gamma,6)*pow(xp,3))/(96.*pow(1 - et,2.5)*(1 + et)) \
+ (np.sqrt(1 - pow(et,2))*pow(gamma,6)*(560*pow(et,8)*(960 - 12*(71 + \
3*np.sqrt(1 - pow(et,2)))*eta + (288 - 59*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 3*np.sqrt(1 - pow(et,2))*pow(eta,3)) + \
1680*pow(et,7)*(12*(30 + np.sqrt(1 - pow(et,2))) - (424 + \
65*np.sqrt(1 - pow(et,2)))*eta + (196 - 86*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 5*np.sqrt(1 - pow(et,2))*pow(eta,3)) + \
pow(et,5)*(13440*(225 + 2*np.sqrt(1 - pow(et,2))) + 16*(-280000 + \
75657*np.sqrt(1 - pow(et,2)))*eta + 420000*np.sqrt(1 - \
pow(et,2))*pow(eta,2) - 8400*np.sqrt(1 - pow(et,2))*pow(eta,3) - \
21525*(-2 + np.sqrt(1 - pow(et,2)))*eta*pow(np.pi,2)) + \
2*et*(-20160*(-105 + 41*np.sqrt(1 - pow(et,2))) + 8*(-369040 + \
50497*np.sqrt(1 - pow(et,2)))*eta - 840*(-392 + 381*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 840*np.sqrt(1 - pow(et,2))*pow(eta,3) + \
4305*(5 + np.sqrt(1 - pow(et,2)))*eta*pow(np.pi,2)) - \
7*pow(et,3)*(-960*(-1170 + 48*np.sqrt(1 - et) + 239*np.sqrt(1 - \
pow(et,2))) + 16*(-99080 + 2160*np.sqrt(1 - et) + 17047*np.sqrt(1 - \
pow(et,2)))*eta - 240*(-588 + 36*np.sqrt(1 - et) + 217*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 240*(3*np.sqrt(1 - et) + np.sqrt(1 - \
pow(et,2)))*pow(eta,3) - 615*(-20 + 3*np.sqrt(1 - \
pow(et,2)))*eta*pow(np.pi,2)) + pow(et,2)*(6720*(-50 + 499*np.sqrt(1 \
- pow(et,2))) - 64*(72415 + 38383*np.sqrt(1 - pow(et,2)))*eta + \
560*(324 + 749*np.sqrt(1 - pow(et,2)))*pow(eta,2) - 9520*np.sqrt(1 - \
pow(et,2))*pow(eta,3) + 4305*(16 + 9*np.sqrt(1 - \
pow(et,2)))*eta*pow(np.pi,2)) + pow(et,4)*(-13440*(-225 + 8*np.sqrt(1 \
- et) + 338*np.sqrt(1 - pow(et,2))) + 32*(60550 + 2520*np.sqrt(1 - \
et) + 159541*np.sqrt(1 - pow(et,2)))*eta - 6720*(-60 + 3*np.sqrt(1 - \
et) + 100*np.sqrt(1 - pow(et,2)))*pow(eta,2) + 560*(3*np.sqrt(1 - et) \
+ np.sqrt(1 - pow(et,2)))*pow(eta,3) - 4305*(14 + 15*np.sqrt(1 - \
pow(et,2)))*eta*pow(np.pi,2)) + 210*(-1 + np.sqrt(1 - \
pow(et,2)))*(2880 + 960*pow(eta,2) + eta*(-10880 + 123*pow(np.pi,2))) \
+ 140*pow(et,6)*(144*(-130 + 29*np.sqrt(1 - pow(et,2))) + 24*(-162 + \
25*np.sqrt(1 - pow(et,2)))*pow(eta,2) + 52*np.sqrt(1 - \
pow(et,2))*pow(eta,3) + eta*(6352 - 2456*np.sqrt(1 - pow(et,2)) + \
123*pow(np.pi,2))))*pow(xp,3))/(107520.*pow(-1 + et,4)*pow(1 + \
et,2.5)) + ((-3 + et)*np.sqrt(1 + \
et)*pow(gamma,6)*(-560*pow(et,5)*eta*(-36 - 59*eta + 3*pow(eta,2)) + \
1680*pow(et,4)*(12 - 29*eta - 27*pow(eta,2) + 2*pow(eta,3)) + \
560*pow(et,3)*(-936 + 960*np.sqrt(1 - pow(et,2)) + (389 - \
852*np.sqrt(1 - pow(et,2)))*eta + (-334 + 288*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 2*pow(eta,3)) + 3*pow(et,2)*(2240*(-227 + \
150*np.sqrt(1 - pow(et,2))) + 1120*(-85 + 46*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 1120*pow(eta,3) + eta*(584944 - \
239680*np.sqrt(1 - pow(et,2)) - 7175*pow(np.pi,2))) + 70*(-1 + \
np.sqrt(1 - pow(et,2)))*(2880 + 960*pow(eta,2) + eta*(-10880 + \
123*pow(np.pi,2))) + 4*et*(6720*(-23 + 55*np.sqrt(1 - pow(et,2))) + \
140*(-421 + 432*np.sqrt(1 - pow(et,2)))*pow(eta,2) + 140*pow(eta,3) + \
eta*(130796 - 555520*np.sqrt(1 - pow(et,2)) + 4305*np.sqrt(1 - \
pow(et,2))*pow(np.pi,2))))*pow(xp,3))/(107520.*pow(-1 + et,3)) + \
(pow(1 + et,1.5)*pow(gamma,6)*(-560*pow(et,5)*eta*(-36 - 59*eta + \
3*pow(eta,2)) + 1680*pow(et,4)*(12 - 29*eta - 27*pow(eta,2) + \
2*pow(eta,3)) + 560*pow(et,3)*(-936 + 960*np.sqrt(1 - pow(et,2)) + \
(389 - 852*np.sqrt(1 - pow(et,2)))*eta + (-334 + 288*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 2*pow(eta,3)) + 3*pow(et,2)*(2240*(-227 + \
150*np.sqrt(1 - pow(et,2))) + 1120*(-85 + 46*np.sqrt(1 - \
pow(et,2)))*pow(eta,2) + 1120*pow(eta,3) + eta*(584944 - \
239680*np.sqrt(1 - pow(et,2)) - 7175*pow(np.pi,2))) + 70*(-1 + \
np.sqrt(1 - pow(et,2)))*(2880 + 960*pow(eta,2) + eta*(-10880 + \
123*pow(np.pi,2))) + 4*et*(6720*(-23 + 55*np.sqrt(1 - pow(et,2))) + \
140*(-421 + 432*np.sqrt(1 - pow(et,2)))*pow(eta,2) + 140*pow(eta,3) + \
eta*(130796 - 555520*np.sqrt(1 - pow(et,2)) + 4305*np.sqrt(1 - \
pow(et,2))*pow(np.pi,2))))*pow(xp,3))/(107520.*pow(-1 + et,3)) + \
((pow(et,2)*(-4 + eta)*(-9*pow(et,2) + 18*(-1 + np.sqrt(1 - \
pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - pow(et,2)) + (41 \
- 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + pow(et,3)*(48 - \
35*eta + 5*pow(eta,2)))*pow(gamma,6))/(96.*pow(-1 + et,2)*np.sqrt(1 + \
et)) - (pow(1 + et,1.5)*(9*pow(et,2) - 18*(-1 + np.sqrt(1 - \
pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - pow(et,2)) + (41 \
- 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + pow(et,3)*(48 - \
35*eta + 5*pow(eta,2)))*pow(gamma,4))/(96.*pow(-1 + \
et,2)*xp))*pow(xp,3) + (et*(2 + et)*(-4 + eta)*(9*pow(et,2) - 18*(-1 \
+ np.sqrt(1 - pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - \
pow(et,2)) + (41 - 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + \
pow(et,3)*(48 - 35*eta + \
5*pow(eta,2)))*pow(gamma,6)*pow(xp,2.5)*np.sqrt(xp/(1 + \
et)))/(96.*pow(-1 + et,2)) - ((-9*pow(et,2) + 18*(-1 + np.sqrt(1 - \
pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - pow(et,2)) + (41 \
- 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + pow(et,3)*(48 - \
35*eta + 5*pow(eta,2)))*pow(gamma,4)*pow(xp,3.5))/(96.*np.sqrt((1 + \
et)*pow(xp,3))) + pow(xp,2)*((pow(et,2)*pow(-4 + \
eta,2)*pow(gamma,4))/(16.*(-1 + et)) - (pow(et,4)*(4 + 5*et + \
3*pow(et,2))*pow(-4 + eta,3)*pow(gamma,6)*xp)/(64.*pow(-1 + \
et,2)*pow(1 + et,3))) + np.sqrt(xp)*(-0.010416666666666666*(np.sqrt(1 \
+ et)*(3 + et)*(-9*pow(et,2) + 18*(-1 + np.sqrt(1 - pow(et,2)))*(-5 + \
2*eta) + et*(-24 + 180*np.sqrt(1 - pow(et,2)) + (41 - 72*np.sqrt(1 - \
pow(et,2)))*eta + pow(eta,2)) + pow(et,3)*(48 - 35*eta + \
5*pow(eta,2)))*pow(gamma,4)*pow(xp,1.5))/(-1 + pow(et,2)) + ((-2 + \
et)*et*(-4 + eta)*(-9*pow(et,2) + 18*(-1 + np.sqrt(1 - \
pow(et,2)))*(-5 + 2*eta) + et*(-24 + 180*np.sqrt(1 - pow(et,2)) + (41 \
- 72*np.sqrt(1 - pow(et,2)))*eta + pow(eta,2)) + pow(et,3)*(48 - \
35*eta + 5*pow(eta,2)))*pow(gamma,6)*pow(xp,2.5))/(96.*pow(-1 + \
et,2)*np.sqrt(1 + et))) + (-0.25*(et*(-4 + \
eta)*pow(gamma,2)*pow(xp,1.5)) + (pow(et,3)*pow(-4 + \
eta,2)*pow(gamma,4)*pow(xp,2.5))/(8.*(-1 + pow(et,2))) - \
(pow(et,3)*(1 + 3*et)*pow(-4 + \
eta,3)*pow(gamma,6)*pow(xp,3.5))/(64.*(-1 + et)*pow(1 + \
et,2)))/np.sqrt(xp) + (-0.25*(et*(-4 + eta)*pow(gamma,2)*pow(xp,4)) + \
(pow(et,2)*pow(-4 + eta,2)*pow(gamma,4)*pow(xp,5))/(16.*(1 + et)) - \
(pow(et,3)*pow(-4 + eta,3)*pow(gamma,6)*pow(xp,6))/(64.*pow(1 + \
et,2)))/pow(xp,3) + \
(pow(gamma,6)*pow(xp,1.5)*(-560*pow(et,10)*eta*(-36 - 59*eta + \
3*pow(eta,2))*np.sqrt(-((-1 + et)*pow(xp,3))) - 70*(2880 + \
960*pow(eta,2) + eta*(-10880 + 123*pow(np.pi,2)))*(np.sqrt(-((-1 + \
et)*pow(xp,3))) - np.sqrt((1 + et)*pow(xp,3))) - \
560*pow(et,9)*(3*pow(eta,3)*np.sqrt((1 - et)*pow(xp,3)) + \
36*np.sqrt(-((-1 + et)*pow(xp,3))) - 51*eta*np.sqrt(-((-1 + \
et)*pow(xp,3))) + 960*np.sqrt((1 + et)*pow(xp,3)) - \
852*eta*np.sqrt((1 + et)*pow(xp,3)) + 2*pow(eta,2)*(-11*np.sqrt(-((-1 \
+ et)*pow(xp,3))) + 144*np.sqrt((1 + et)*pow(xp,3)))) + \
560*pow(et,8)*(14*pow(eta,3)*np.sqrt((1 - et)*pow(xp,3)) - \
900*np.sqrt(-((-1 + et)*pow(xp,3))) + 230*eta*np.sqrt(-((-1 + \
et)*pow(xp,3))) + 3720*np.sqrt((1 + et)*pow(xp,3)) - \
2988*eta*np.sqrt((1 + et)*pow(xp,3)) + pow(eta,2)*(-533*np.sqrt(-((-1 \
+ et)*pow(xp,3))) + 852*np.sqrt((1 + et)*pow(xp,3)))) - \
pow(et,7)*(-4305*eta*pow(np.pi,2)*(5*np.sqrt(-((-1 + et)*pow(xp,3))) \
- 4*np.sqrt((1 + et)*pow(xp,3))) + 560*pow(eta,3)*(2*np.sqrt(-((-1 + \
et)*pow(xp,3))) + 3*np.sqrt(-((-1 + pow(et,2))*pow(xp,3)))) - \
6720*(311*np.sqrt(-((-1 + et)*pow(xp,3))) - 440*np.sqrt((1 + \
et)*pow(xp,3)) + 24*np.sqrt(-((-1 + pow(et,2))*pow(xp,3)))) - \
1120*pow(eta,2)*(400*np.sqrt(-((-1 + et)*pow(xp,3))) - 348*np.sqrt((1 \
+ et)*pow(xp,3)) + 27*np.sqrt(-((-1 + pow(et,2))*pow(xp,3)))) + \
32*eta*(63431*np.sqrt(-((-1 + et)*pow(xp,3))) - 99470*np.sqrt((1 + \
et)*pow(xp,3)) + 2520*np.sqrt(-((-1 + pow(et,2))*pow(xp,3))))) - \
pow(et,6)*(1135680*np.sqrt((1 - et)*pow(xp,3)) + \
21525*eta*pow(np.pi,2)*(np.sqrt(-((-1 + et)*pow(xp,3))) - \
2*np.sqrt((1 + et)*pow(xp,3))) + 3360*pow(eta,2)*(7*np.sqrt(-((-1 + \
et)*pow(xp,3))) + 74*np.sqrt((1 + et)*pow(xp,3))) + \
3360*pow(eta,3)*(2*np.sqrt(-((-1 + et)*pow(xp,3))) - np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3)))) - 64*eta*(30624*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 40285*np.sqrt((1 + et)*pow(xp,3)) + \
2520*np.sqrt(-((-1 + pow(et,2))*pow(xp,3))))) + \
280*pow(et,5)*(6*pow(eta,3)*(4*np.sqrt(-((-1 + et)*pow(xp,3))) - \
np.sqrt(-((-1 + pow(et,2))*pow(xp,3)))) - eta*(-9604*np.sqrt(-((-1 + \
et)*pow(xp,3))) + 123*pow(np.pi,2)*np.sqrt(-((-1 + et)*pow(xp,3))) + \
14472*np.sqrt((1 + et)*pow(xp,3)) + 288*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3)))) - 24*(491*np.sqrt(-((-1 + et)*pow(xp,3))) - \
840*np.sqrt((1 + et)*pow(xp,3)) - 16*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3))) - 8*np.sqrt(-(pow(-1 + \
pow(et,2),3)*pow(xp,3)))) - 18*pow(eta,2)*(125*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 176*np.sqrt((1 + et)*pow(xp,3)) - 4*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3))) - 2*np.sqrt(-(pow(-1 + \
pow(et,2),3)*pow(xp,3))))) - \
pow(et,3)*(-4305*eta*pow(np.pi,2)*(np.sqrt(-((-1 + et)*pow(xp,3))) + \
12*np.sqrt((1 + et)*pow(xp,3))) - 32*eta*(18653*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 104790*np.sqrt((1 + et)*pow(xp,3)) - \
5040*np.sqrt(-((-1 + pow(et,2))*pow(xp,3)))) + \
3360*pow(eta,3)*(np.sqrt(-((-1 + et)*pow(xp,3))) + np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3)))) + 1120*pow(eta,2)*(119*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 36*np.sqrt((1 + et)*pow(xp,3)) - 27*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3))) - 9*np.sqrt(-(pow(-1 + \
pow(et,2),3)*pow(xp,3)))) - 6720*(61*np.sqrt(-((-1 + et)*pow(xp,3))) \
- 40*np.sqrt((1 + et)*pow(xp,3)) + 24*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3))) + 8*np.sqrt(-(pow(-1 + \
pow(et,2),3)*pow(xp,3))))) - \
2*pow(et,4)*(-4305*eta*pow(np.pi,2)*(4*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 9*np.sqrt((1 + et)*pow(xp,3))) - \
280*pow(eta,2)*(1327*np.sqrt(-((-1 + et)*pow(xp,3))) - \
1548*np.sqrt((1 + et)*pow(xp,3)) - 144*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3)))) - 3360*(533*np.sqrt(-((-1 + et)*pow(xp,3))) - \
900*np.sqrt((1 + et)*pow(xp,3)) - 64*np.sqrt(-((-1 + \
pow(et,2))*pow(xp,3)))) - 1680*pow(eta,3)*(-np.sqrt((1 - \
et)*pow(xp,3)) + np.sqrt(-((-1 + pow(et,2))*pow(xp,3))) + \
np.sqrt(-(pow(-1 + pow(et,2),3)*pow(xp,3)))) - \
16*eta*(-113291*np.sqrt(-((-1 + et)*pow(xp,3))) + 294210*np.sqrt((1 + \
et)*pow(xp,3)) + 5040*np.sqrt(-((-1 + pow(et,2))*pow(xp,3))) + \
5040*np.sqrt(-(pow(-1 + pow(et,2),3)*pow(xp,3))))) + \
pow(et,2)*(3920*pow(eta,3)*np.sqrt((1 - et)*pow(xp,3)) - \
560*pow(eta,2)*(691*np.sqrt(-((-1 + et)*pow(xp,3))) - 1020*np.sqrt((1 \
+ et)*pow(xp,3))) - 47040*(37*np.sqrt(-((-1 + et)*pow(xp,3))) - \
80*np.sqrt((1 + et)*pow(xp,3))) + eta*(754816*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 4401600*np.sqrt((1 + et)*pow(xp,3)) - \
4305*pow(np.pi,2)*(np.sqrt(-((-1 + et)*pow(xp,3))) - 6*np.sqrt((1 + \
et)*pow(xp,3))))) - 2*et*(280*pow(eta,3)*np.sqrt((1 - et)*pow(xp,3)) \
- 280*pow(eta,2)*(541*np.sqrt(-((-1 + et)*pow(xp,3))) - \
672*np.sqrt((1 + et)*pow(xp,3))) - 6720*(61*np.sqrt(-((-1 + \
et)*pow(xp,3))) - 140*np.sqrt((1 + et)*pow(xp,3))) + \
eta*(642392*np.sqrt(-((-1 + et)*pow(xp,3))) - 1872640*np.sqrt((1 + \
et)*pow(xp,3)) - 4305*pow(np.pi,2)*(np.sqrt(-((-1 + et)*pow(xp,3))) - \
4*np.sqrt((1 + et)*pow(xp,3)))))))/(107520.*pow(1 - et,4.5)*pow(1 + \
et,2.5))


# Add also some auxiliary functions




def compute_ecc(tHorizonv4E, tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima):

    if len(tH_maxima)>3 and len(tH_minima):

        iomega_orb_maxima = InterpolatedUnivariateSpline(tH_maxima, omega_orb_maxima)
        iomega_orb_minima = InterpolatedUnivariateSpline(tH_minima, omega_orb_minima)
        omegaOrb_maxima = iomega_orb_maxima(tHorizonv4E)
        omegaOrb_minima = iomega_orb_minima(tHorizonv4E)
        ecc_omegaorb = (np.sqrt(omegaOrb_maxima)-np.sqrt(omegaOrb_minima))/(np.sqrt(omegaOrb_maxima)+np.sqrt(omegaOrb_minima))

        # Compute the averaged orbital frequency
        #omegaOrb_av =1./(tH_maxima[1:]-tH_maxima[:-1])*(iphase_orb(tH_maxima[1:])-iphase_orb(tH_maxima[:-1]))

        # Just in case the waveform is noisy
        ecc_omega_orb1=ecc_omegaorb[0]
    else:
        ecc_omega_orb1=-1

    return ecc_omega_orb1



def maxima_minima_fit(ff, tt, tt0, deltaT):

    tmax = tt0+deltaT
    idx_data=(np.where((tt <= tmax) & (tt >= tt0 - deltaT)))[0]
    tt_idx=tt[idx_data]
    ff_idx=ff[idx_data]


    fit = np.polyfit(tt_idx, ff_idx, 2)
    polynomial = np.poly1d(fit)
    ff_fit=polynomial(tt_idx)
    p2 = np.polyder(polynomial)
    tt_parabola_max=np.roots(p2)

    return tt_parabola_max[0], polynomial(tt_parabola_max)[0]

def loop_maxima_minima_fit(ff, tt, ttmaxmin_list, deltaT):

    tt_maxs=[]
    ff_maxs=[]
    for tt0 in ttmaxmin_list[:-1]:

        tt_max_fit, ff_max_fit = maxima_minima_fit(ff, tt, tt0, deltaT)

        tt_maxs.append(tt_max_fit)
        ff_maxs.append(ff_max_fit)

    ttmaxs=np.array(tt_maxs)
    ffmaxs=np.array(ff_maxs)

    return ttmaxs,ffmaxs

def duplicate_position(tH_minima,ii):
    tti=tH_minima[ii]
    ttdiff= np.array([np.abs(ttw-tti) for ttw in tH_minima])
    ttdiff=np.delete(ttdiff, [ii]) # Remove position of the element
    pos=np.where(ttdiff<1e-10)[0]
    #tH_minima=np.delete(tH_minima, [pos])
    #return tH_minima
    return pos

def duplicate_position_list(tH_minima):

    pos_list=[]
    for ii in range(len(tH_minima)):
        pos= duplicate_position(tH_minima,ii)

        if pos.size!=0:
            pos_list.append(pos)

    if pos_list:
        out = np.concatenate(pos_list).ravel().tolist()
        pos_to_remove = list(set(out))
    else:
        pos_to_remove=[]


    #tH_minima=np.delete(tH_minima, [pos])
    #return tH_minima
    return pos_to_remove

def remove_duplicate_elements(tH_minima,omega_orb_minima):

    pos_repeat_min=duplicate_position_list(omega_orb_minima)

    if pos_repeat_min:
        omega_orb_minima = np.delete(omega_orb_minima, pos_repeat_min)
        tH_minima = np.delete(tH_minima, pos_repeat_min)

    return tH_minima,omega_orb_minima
#os.makedirs(plotdir, exist_ok=True)
#os.makedirs(outdir, exist_ok=True)

# Read NR waveforms
#nr_dir='/home/antoniramosbuades/Desktop/EOB_eccentric/NR_comparison/eccentric_NR/SXSfiles/'
#nrfiles_list= sorted(glob.glob(nr_dir+"*h5"))
#len(nrfiles_list)


## PYTHON EXPRESSIONS FOR OMEGA22 CONVERTED FROM MATHEMATICA

def omega22Ap_0pn(et,x):
    return -(4*np.sqrt(-((-1 + pow(et,2))*pow(x,3))))/((-2 + et)*pow(1 + et,2))


def omega22Ap_1pn(eta, et,x):
    return -0.047619047619047616*(et*(-690 + et*(607 - 78*eta) + 96*eta + 11*pow(et,2)*(-23 + 6*eta))*pow(x,2.5))/(pow(-2 + et,2)*pow(1 + et,2)*np.sqrt(1 - pow(et,2)))

def omega22Ap_2pn(eta, et,x):
    return ((127008*(-1 + np.sqrt(1 - pow(et,2)))*(-5 + 2*eta) + 882*pow(et,7)*(-180 + 48*np.sqrt(1 - pow(et,2)) + (72 - 35*np.sqrt(1 - pow(et,2)))*eta + 5*np.sqrt(1 - pow(et,2))*pow(eta,2)) + \
           2*pow(et,3)*(1468530 - 151195*np.sqrt(1 - et) + 238140*np.sqrt(1 + et) - 447174*np.sqrt(1 - pow(et,2)) + (-587412 + 910208*np.sqrt(1 - et) - 95256*np.sqrt(1 + et) + 311346*np.sqrt(1 - pow(et,2)))*eta +\
              2*(51266*np.sqrt(1 - et) - 7497*np.sqrt(1 - pow(et,2)))*pow(eta,2)) + 4*et*(-396900 - 155851*np.sqrt(1 - et) + 79380*np.sqrt(1 + et) + 121716*np.sqrt(1 - pow(et,2)) + 4*(39690 + 39782*np.sqrt(1 - et) - 7938*np.sqrt(1 + et) - 26019*np.sqrt(1 - pow(et,2)))*eta + \
              4*(1507*np.sqrt(1 - et) - 441*np.sqrt(1 - pow(et,2)))*pow(eta,2)) - 14*pow(et,2)* (34020 - 46787*np.sqrt(1 - et) + 56700*np.sqrt(1 + et) - 32508*np.sqrt(1 - pow(et,2)) + 2*(-6804 + 50339*np.sqrt(1 - et) - 11340*np.sqrt(1 + et) + 1638*np.sqrt(1 - pow(et,2)))*eta + \
              (8858*np.sqrt(1 - et) - 252*np.sqrt(1 - pow(et,2)))*pow(eta,2)) - 3*pow(et,6)* (-291060 - 10208*np.sqrt(1 - et) + 73206*np.sqrt(1 - pow(et,2)) + 3*(38808 + 4071*np.sqrt(1 - et) - 17150*np.sqrt(1 - pow(et,2)))*eta + 25*(125*np.sqrt(1 - et) + 294*np.sqrt(1 - pow(et,2)))*pow(eta,2)) + \
           6*pow(et,5)*(-3*(66150 + 1448*np.sqrt(1 - et) + 4410*np.sqrt(1 + et) - 15141*np.sqrt(1 - pow(et,2))) + (79380 + 57361*np.sqrt(1 - et) + 5292*np.sqrt(1 + et) - 24843*np.sqrt(1 - pow(et,2)))*eta + (15875*np.sqrt(1 - et) + 4557*np.sqrt(1 - pow(et,2)))*pow(eta,2)) + pow(et,4)*  (2*(-515970 + 86803*np.sqrt(1 - et) + 39690*np.sqrt(1 + et) + 153468*np.sqrt(1 - pow(et,2))) - (-412776 + 1328503*np.sqrt(1 - et) + 31752*np.sqrt(1 + et) + 336042*np.sqrt(1 - pow(et,2)))*eta + \
              (-196331*np.sqrt(1 - et) + 13230*np.sqrt(1 - pow(et,2)))*pow(eta,2)))*pow(x/(1 + et),3.5))/(5292.*pow(-2 + et,3)*pow(-1 + et,2))


def omega22Ap_3pn(eta, et,x):
    return (pow(x,4.5)*(234710784000 - 485068953600*et + 1288394553600*np.sqrt(1 - et)*et + 704132352000*pow(et,2) - 453960460800*np.sqrt(1 - et)*pow(et,2) + 2010689049600*pow(et,3) - 72383571691200*np.sqrt(1 - et)*pow(et,3) - 2328526569600*pow(et,4) + 36556321032000*np.sqrt(1 - et)*pow(et,4) - 1604834985600*pow(et,5) + 238915506614400*np.sqrt(1 - et)*pow(et,5) +  2324125742400*pow(et,6) - 137596251592800*np.sqrt(1 - et)*pow(et,6) - 52320945600*pow(et,7) - 273613281480000*np.sqrt(1 -et)*pow(et,7) - 739338969600*pow(et,8) + 187694360330400*np.sqrt(1 - et)*pow(et,8) + 297300326400*pow(et,9) + 92663817523200*np.sqrt(1 - et)*pow(et,9) - 30805790400*pow(et,10) - 93790429286400*np.sqrt(1 - et)*pow(et,10) - 1466942400*pow(et,11) + 31263476428800*np.sqrt(1 - et)*pow(et,11) - 4600331366400*np.sqrt(1 - et)*pow(et,12) - 15678680371200*np.sqrt(1 - et)*pow(et,13) + 12204960768000*np.sqrt(1 - et)*pow(et,14) - 2440992153600*np.sqrt(1 - et)*pow(et,15) - 13491713739968*et*np.sqrt(1 + et) + 16340521113344*pow(et,2)*np.sqrt(1 + et) +  29886146464112*pow(et,3)*np.sqrt(1 + et) - 54235148361552*pow(et,4)*np.sqrt(1 + et) + 58536180883864*pow(et,5)*np.sqrt(1 + et) - 50347280257232*pow(et,6)*np.sqrt(1 + et) -  97543895437168*pow(et,7)*np.sqrt(1 + et) + 176533312748400*pow(et,8)*np.sqrt(1 + et) + 15020578423800*pow(et,9)*np.sqrt(1 + et) - 142704156672000*pow(et,10)*np.sqrt(1 + et) +   48819843072000*pow(et,11)*np.sqrt(1 + et) + 31920666624000*pow(et,12)*np.sqrt(1 + et) -22532235264000*pow(et,13)*np.sqrt(1 + et) + 3755372544000*pow(et,14)*np.sqrt(1 + et) - 234710784000*np.sqrt(1 - pow(et,2)) + 1486501632000*et*np.sqrt(1 - pow(et,2)) + 899724672000*pow(et,2)*np.sqrt(1 - pow(et,2)) - 2894766336000*pow(et,3)*np.sqrt(1 - pow(et,2)) +  415633680000*pow(et,4)*np.sqrt(1 - pow(et,2)) + 2019490704000*pow(et,5)*np.sqrt(1 - pow(et,2)) - 1476722016000*pow(et,6)*np.sqrt(1 - pow(et,2)) - 107575776000*pow(et,7)*np.sqrt(1 - pow(et,2)) + 591666768000*pow(et,8)*np.sqrt(1 - pow(et,2)) - 268939440000*pow(et,9)*np.sqrt(1 - pow(et,2)) + 39118464000*pow(et,10)*np.sqrt(1 - pow(et,2)) -   886685184000*eta - 277573443840*et*eta + 114979411008000*np.sqrt(1 - et)*et*eta - 103906091520*pow(et,2)*eta - 57889738368000*np.sqrt(1 - et)*pow(et,2)*eta - 1816409992320*pow(et,3)*eta -           289900929225600*np.sqrt(1 - et)*pow(et,3)*eta + 1823693477760*pow(et,4)*eta + 173751922713600*np.sqrt(1 - et)*pow(et,4)*eta + 2223739148400*pow(et,5)*eta +    76222862654400*np.sqrt(1 - et)*pow(et,5)*eta - 2094668009280*pow(et,6)*eta - 96012218332800*np.sqrt(1 - et)*pow(et,6)*eta - 451295515440*pow(et,7)*eta +            335341239710400*np.sqrt(1 - et)*pow(et,7)*eta + 760870424160*pow(et,8)*eta - 170511470236800*np.sqrt(1 - et)*pow(et,8)*eta - 171372635280*pow(et,9)*eta -            288162253209600*np.sqrt(1 - et)*pow(et,9)*eta + 2526400800*pow(et,10)*eta + 213805876838400*np.sqrt(1 - et)*pow(et,10)*eta - 3789601200*pow(et,11)*eta +            27101271859200*np.sqrt(1 -et)*pow(et,11)*eta + 1466942400*pow(et,12)*eta - 54609375744000*np.sqrt(1 - et)*pow(et,12)*eta + 22626119577600*np.sqrt(1 - et)*pow(et,13)*eta -            8606062080000*np.sqrt(1 - et)*pow(et,14)*eta + 1721212416000*np.sqrt(1 - et)*pow(et,15)*eta + 5634155384960*et*np.sqrt(1 + et)*eta - 8815272943360*pow(et,2)*np.sqrt(1 + et)*eta -            6339501896800*pow(et,3)*np.sqrt(1 + et)*eta + 16114344688800*pow(et,4)*np.sqrt(1 + et)*eta - 20586171144080*pow(et,5)*np.sqrt(1 + et)*eta + 19402788298600*pow(et,6)*np.sqrt(1 + et)*eta +           39038288817160*pow(et,7)*np.sqrt(1 + et)*eta - 70601935765800*pow(et,8)*np.sqrt(1 + et)*eta - 6005470099800*pow(et,9)*np.sqrt(1 + et)*eta + 57081662668800*pow(et,10)*np.sqrt(1 + et)*eta -    19527937228800*pow(et,11)*np.sqrt(1 + et)*eta - 12768266649600*pow(et,12)*np.sqrt(1 + et)*eta + 9012894105600*pow(et,13)*np.sqrt(1 + et)*eta - 1502149017600*pow(et,14)*np.sqrt(1 + et)*eta +   886685184000*np.sqrt(1 - pow(et,2))*eta - 1700349235200*et*np.sqrt(1 - pow(et,2))*eta - 3079927065600*pow(et,2)*np.sqrt(1 - pow(et,2))*eta + 3275519385600*pow(et,3)*np.sqrt(1 - pow(et,2))*eta +     1717952544000*pow(et,4)*np.sqrt(1 - pow(et,2))*eta - 2586382444800*pow(et,5)*np.sqrt(1 - pow(et,2))*eta + 682780190400*pow(et,6)*np.sqrt(1 - pow(et,2))*eta +           430629091200*pow(et,7)*np.sqrt(1 - pow(et,2))*eta - 527447289600*pow(et,8)*np.sqrt(1 - pow(et,2))*eta + 225909129600*pow(et,9)*np.sqrt(1 - pow(et,2))*eta -            34717636800*pow(et,10)*np.sqrt(1 - pow(et,2))*eta + 78236928000*pow(eta,2) - 196244294400*et*pow(eta,2) - 7082677324800*np.sqrt(1 - et)*et*pow(eta,2) - 59329670400*pow(et,2)*pow(eta,2) +            3611193062400*np.sqrt(1 - et)*pow(et,2)*pow(eta,2) + 448232400000*pow(et,3)*pow(eta,2) - 13730860281600*np.sqrt(1 - et)*pow(et,3)*pow(eta,2) - 306101980800*pow(et,4)*pow(eta,2) +            5088473913600*np.sqrt(1 - et)*pow(et,4)*pow(eta,2) - 191558228400*pow(et,5)*pow(eta,2) + 106192519171200*np.sqrt(1 - et)*pow(et,5)*pow(eta,2) + 454385408400*pow(et,6)*pow(eta,2) -            57449655648000*np.sqrt(1 - et)*pow(et,6)*pow(eta,2) - 188868834000*pow(et,7)*pow(eta,2) - 154621107748800*np.sqrt(1 - et)*pow(et,7)*pow(eta,2) - 144249336000*pow(et,8)*pow(eta,2) +           101260169841600*np.sqrt(1 - et)*pow(et,8)*pow(eta,2) + 139604018400*pow(et,9)*pow(eta,2) + 70835714611200*np.sqrt(1 - et)*pow(et,9)*pow(eta,2) - 22900600800*pow(et,10)*pow(eta,2) -           63293674752000*np.sqrt(1 - et)*pow(et,10)*pow(eta,2) - 8720157600*pow(et,11)*pow(eta,2) + 7495097702400*np.sqrt(1 - et)*pow(et,11)*pow(eta,2) + 2404155600*pow(et,12)*pow(eta,2) +       5711295744000*np.sqrt(1 - et)*pow(et,12)*pow(eta,2) - 8058403584000*np.sqrt(1 - et)*pow(et,13)*pow(eta,2) + 5085400320000*np.sqrt(1 - et)*pow(et,14)*pow(eta,2) -           1017080064000*np.sqrt(1 - et)*pow(et,15)*pow(eta,2) + 148305926400*et*np.sqrt(1 + et)*pow(eta,2) - 647105192000*pow(et,2)*np.sqrt(1 +  et)*pow(eta,2) +      1416338797600*pow(et,3)*np.sqrt(1 + et)*pow(eta,2) - 2088682227600*pow(et,4)*np.sqrt(1 + et)*pow(eta,2) + 1753824077000*pow(et,5)*np.sqrt(1 + et)*pow(eta,2) -          894053855800*pow(et,6)*np.sqrt(1 + et)*pow(eta,2) + 311942392200*pow(et,7)*np.sqrt(1 + et)*pow(eta,2) - 57569314200*pow(et,8)*np.sqrt(1 + et)*pow(eta,2) +            3700489200*pow(et,9)*np.sqrt(1 + et)*pow(eta,2) - 78236928000*np.sqrt(1 - pow(et,2))*pow(eta,2) + 203416012800*et*np.sqrt(1 - pow(et,2))*pow(eta,2) +            219063398400*pow(et,2)*np.sqrt(1 - pow(et,2))*pow(eta,2) - 336418790400*pow(et,3)*np.sqrt(1 - pow(et,2))*pow(eta,2) + 92906352000*pow(et,4)*np.sqrt(1 - pow(et,2))*pow(eta,2) +           182878819200*pow(et,5)*np.sqrt(1 - pow(et,2))*pow(eta,2) - 301701153600*pow(et,6)*np.sqrt(1 - pow(et,2))*pow(eta,2) + 72858139200*pow(et,7)*np.sqrt(1 - pow(et,2))*pow(eta,2) +            109042718400*pow(et,8)*np.sqrt(1 - pow(et,2))*pow(eta,2) - 69924254400*pow(et,9)*np.sqrt(1 - pow(et,2))*pow(eta,2) + 11735539200*pow(et,10)*np.sqrt(1 - pow(et,2))*pow(eta,2) +            651974400*et*pow(eta,3) - 3259872000*pow(et,2)*pow(eta,3) - 3585859200*pow(et,3)*pow(eta,3) + 2607897600*pow(et,4)*pow(eta,3) - 3219123600*pow(et,5)*pow(eta,3) -           1426194000*pow(et,6)*pow(eta,3) + 7293963600*pow(et,7)*pow(eta,3) - 692722800*pow(et,8)*pow(eta,3) - 3463614000*pow(et,9)*pow(eta,3) + 937213200*pow(et,10)*pow(eta,3) +           366735600*pow(et,11)*pow(eta,3) - 122245200*pow(et,12)*pow(eta,3) - 2580486400*et*np.sqrt(1 + et)*pow(eta,3) + 9524640000*pow(et,2)*np.sqrt(1 + et)*pow(eta,3) -           17627788800*pow(et,3)*np.sqrt(1 + et)*pow(eta,3) + 16340260800*pow(et,4)*np.sqrt(1 + et)*pow(eta,3) - 16784833200*pow(et,5)*np.sqrt(1 + et)*pow(eta,3) +            18943864800*pow(et,6)*np.sqrt(1 + et)*pow(eta,3) - 8702150000*pow(et,7)*np.sqrt(1 + et)*pow(eta,3) + 556368000*pow(et,8)*np.sqrt(1 + et)*pow(eta,3) -
           158856000*pow(et,9)*np.sqrt(1 + et)*pow(eta,3) + 10024106400*eta*pow(np.pi,2) + 10024106400*et*eta*pow(np.pi,2) - 3854268910800*np.sqrt(1 - et)*et*eta*pow(np.pi,2) +           10024106400*pow(et,2)*eta*pow(np.pi,2) + 1929640482000*np.sqrt(1 - et)*pow(et,2)*eta*pow(np.pi,2) + 15036159600*pow(et,3)*eta*pow(np.pi,2) +           13478664068100*np.sqrt(1 - et)*pow(et,3)*eta*pow(np.pi,2) - 26939785950*pow(et,4)*eta*pow(np.pi,2) - 7703525768400*np.sqrt(1 - et)*pow(et,4)*eta*pow(np.pi,2) -           23180746050*pow(et,5)*eta*pow(np.pi,2) - 16119389597850*np.sqrt(1 - et)*pow(et,5)*eta*pow(np.pi,2) + 23493999375*pow(et,6)*eta*pow(np.pi,2) +            10946950695450*np.sqrt(1 - et)*pow(et,6)*eta*pow(np.pi,2) + 5325306525*pow(et,7)*eta*pow(np.pi,2) + 6134753116800*np.sqrt(1 - et)*pow(et,7)*eta*pow(np.pi,2) -           7831333125*pow(et,8)*eta*pow(np.pi,2) - 5894174563200*np.sqrt(1 - et)*pow(et,8)*eta*pow(np.pi,2) + 1566266625*pow(et,9)*eta*pow(np.pi,2) + 1323182044800*np.sqrt(1 - et)*pow(et,9)*eta*pow(np.pi,2) +            120289276800*np.sqrt(1 - et)*pow(et,10)*eta*pow(np.pi,2) - 842024937600*np.sqrt(1 - et)*pow(et,11)*eta*pow(np.pi,2) + 601446384000*np.sqrt(1 - et)*pow(et,12)*eta*pow(np.pi,2) -            120289276800*np.sqrt(1 - et)*pow(et,13)*eta*pow(np.pi,2) + 32578345800*pow(et,2)*np.sqrt(1 + et)*eta*pow(np.pi,2) - 122795303400*pow(et,3)*np.sqrt(1 + et)*eta*pow(np.pi,2) +            166024262250*pow(et,4)*np.sqrt(1 + et)*eta*pow(np.pi,2) - 110265170400*pow(et,5)*np.sqrt(1 + et)*eta*pow(np.pi,2) + 36963892350*pow(et,6)*np.sqrt(1 + et)*eta*pow(np.pi,2) -            5012053200*pow(et,7)*np.sqrt(1 + et)*eta*pow(np.pi,2) - 10024106400*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) + 10024106400*et*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) +            35084372400*pow(et,2)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) - 20048212800*pow(et,3)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) - 30698825850*pow(et,4)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) +         19421706150*pow(et,5)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) + 6891573150*pow(et,6)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) - 6891573150*pow(et,7)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) +            1253013300*pow(et,8)*np.sqrt(1 - pow(et,2))*eta*pow(np.pi,2) - 11959073280*pow(-2 + et,2)*pow(-1 + et,3)*et*np.sqrt(1 + et)*(-3 + 2*et)*np.log(1 + et) +            11959073280*pow(-2 + et,2)*pow(-1 + et,3)*et*np.sqrt(1 + et)*(-3 + 2*et)*np.log(x)))/(4.889808e8*pow(1 - et,2.5)*pow(-2 + et,4)*pow(1 + et,5))




def omega22P_0pn(et,x):
    return -(-4*np.sqrt(1 - et**2)*x**1.5)/(2 - 3*et + et**3)

def omega22P_1pn(eta, et,x):
    return -(et*(-690 + 96*eta + 11*et**2*(-23 + 6*eta) + et*(-607 + 78*eta))*x**2.5)/(21.*np.sqrt(1 - et**2)*(-2 + et + et**2)**2)

def omega22P_2pn(eta, et,x):
    return  -((-127008*(-1 + np.sqrt(1 - et**2))*(-5 + 2*eta) - 882*et**7*(-180 + 48*np.sqrt(1 - et**2) + (72 - 35*np.sqrt(1 - et**2))*eta + 5*np.sqrt(1 - et**2)*eta**2) +        et**4*(-2*(-1706670 + 86803*np.sqrt(1 - et) + 39690*np.sqrt(1 + et) + 381024*np.sqrt(1 - et**2)) + (-1365336 + 1328503*np.sqrt(1 - et) + 31752*np.sqrt(1 + et) + 332514*np.sqrt(1 - et**2))*eta + (196331*np.sqrt(1 - et) - 94374*np.sqrt(1 - et**2))*eta**2) +           6*et**5*(542430 - 4344*np.sqrt(1 - et) + 13230*np.sqrt(1 + et) - 132741*np.sqrt(1 - et**2) + (-216972 + 57361*np.sqrt(1 - et) - 5292*np.sqrt(1 + et) + 86583*np.sqrt(1 - et**2))*eta + (15875*np.sqrt(1 - et) - 13377*np.sqrt(1 - et**2))*eta**2) +           2*et**3*(-277830 - 151195*np.sqrt(1 - et) - 238140*np.sqrt(1 + et) + 219618*np.sqrt(1 - et**2) + 2*(55566 + 455104*np.sqrt(1 - et) + 47628*np.sqrt(1 + et) - 156555*np.sqrt(1 - et**2))*eta + 2*(51266*np.sqrt(1 - et) - 12789*np.sqrt(1 - et**2))*eta**2) +          3*et**6*(-2*(-198450 + 5104*np.sqrt(1 - et) + 50715*np.sqrt(1 - et**2)) + 3*(-52920 + 4071*np.sqrt(1 - et) + 24010*np.sqrt(1 - et**2))*eta + 5*(625*np.sqrt(1 - et) - 2058*np.sqrt(1 - et**2))*eta**2) +           14*et**2*(-283500 - 46787*np.sqrt(1 - et) + 11340*np.sqrt(1 + et) + 127764*np.sqrt(1 - et**2) + 2*(56700 + 50339*np.sqrt(1 - et) - 2268*np.sqrt(1 + et) - 46242*np.sqrt(1 - et**2))*eta + 2*(4429*np.sqrt(1 - et) - 630*np.sqrt(1 - et**2))*eta**2) + 4*et*(-714420 - 155851*np.sqrt(1 - et) + 79380*np.sqrt(1 + et) + 439236*np.sqrt(1 - et**2) + 4*(71442 + 39782*np.sqrt(1 - et) - 7938*np.sqrt(1 + et) - 57771*np.sqrt(1 - et**2))*eta + 4*(1507*np.sqrt(1 - et) - 441*np.sqrt(1 - et**2))*eta**2))*x**3.5)/(5292.*(-1 + et)**4*(1 + et)**1.5*(2 + et)**3)


def omega22P_3pn(eta, et,x):

    return 2.0450700722809567e-9*((-(x/(-1 + et)))**4.5*(-234710784000 - 1893333657600*et + 1288394553600*np.sqrt(1 - et)*et - 7839340185600*et**2 + 3030749568000*np.sqrt(1 - et)*et**2 - 19864356019200*et**3 - 68898861662400*np.sqrt(1 - et)*et**3 - 32205253449600*et**4 -            177838754385600*np.sqrt(1 - et)*et**4 - 34153352956800*et**5 + 24520431196800*np.sqrt(1 - et)*et**5 - 23851016481600*et**6 + 401032189404000*np.sqrt(1 - et)*et**6 - 10808920584000*et**7 + 265015159516800*np.sqrt(1 -et)*et**7 - 3021901344000*et**8 -            196292482293600*np.sqrt(1 - et)*et**8 - 455730105600*et**9 - 291323025100800*np.sqrt(1 - et)*et**9 - 22004136000*et**10 - 104868778291200*np.sqrt(1 - et)*et**10 + 1466942400*et**11 + 20185127424000*np.sqrt(1 - et)*et**11 + 56048935219200*np.sqrt(1 - et)*et**12 +            44970586214400*np.sqrt(1 - et)*et**13 + 17086945075200*np.sqrt(1 - et)*et**14 + 2440992153600*np.sqrt(1 - et)*et**15 - 13491713739968*et*np.sqrt(1 + et) - 16340521113344*et**2*np.sqrt(1 + et) + 29886146464112*et**3*np.sqrt(1 + et) + 54235148361552*et**4*np.sqrt(1 + et) +            58536180883864*et**5*np.sqrt(1 + et) + 50347280257232*et**6*np.sqrt(1 + et) - 97543895437168*et**7*np.sqrt(1 + et) - 176533312748400*et**8*np.sqrt(1 + et) + 15020578423800*et**9*np.sqrt(1 + et) + 142704156672000*et**10*np.sqrt(1 + et) + 48819843072000*et**11*np.sqrt(1 + et) -            31920666624000*et**12*np.sqrt(1 + et) - 22532235264000*et**13*np.sqrt(1 + et) - 3755372544000*et**14*np.sqrt(1 + et) + 234710784000*np.sqrt(1 - et**2) + 2894766336000*et*np.sqrt(1 - et**2) + 12244079232000*et**2*np.sqrt(1 - et**2) + 27382924800000*et**3*np.sqrt(1 - et**2) +            37998697968000*et**4*np.sqrt(1 - et**2) + 35279964720000*et**5*np.sqrt(1 - et**2) + 22620251808000*et**6*np.sqrt(1 - et**2) + 9984987936000*et**7*np.sqrt(1 - et**2) + 2909435760000*et**8*np.sqrt(1 - et**2) + 503650224000*et**9*np.sqrt(1 - et**2) + 39118464000*et**10*np.sqrt(1 - et**2) +            886685184000*eta + 5042537660160*et*eta + 114979411008000*np.sqrt(1 - et)*et*eta + 14398798740480*et**2*eta + 287848560384000*np.sqrt(1 - et)*et**2*eta + 27504741559680*et**3*eta + 55837369526400*np.sqrt(1 - et)*et**3*eta + 37121587493760*et**4*eta -            407815482412800*np.sqrt(1 - et)*et**4*eta + 34656671372400*et**5*eta - 505344542472000*np.sqrt(1 - et)*et**5*eta + 21588388224480*et**6*eta - 333109461484800*np.sqrt(1 - et)*et**6*eta + 8596970529840*et**7*eta + 98243996558400*np.sqrt(1 - et)*et**7*eta +            2075404494240*et**8*eta + 604096706505600*np.sqrt(1 - et)*et**8*eta + 310487672880*et**9*eta + 486445923532800*np.sqrt(1 - et)*et**9*eta + 51668971200*et**10*eta - 15522206515200*np.sqrt(1 - et)*et**10*eta + 12591255600*et**11*eta - 202226811494400*np.sqrt(1 - et)*et**11*eta +            1466942400*et**12*eta - 120516163891200*np.sqrt(1 - et)*et**12*eta - 43280668569600*np.sqrt(1 - et)*et**13*eta - 12048486912000*np.sqrt(1 - et)*et**14*eta - 1721212416000*np.sqrt(1 - et)*et**15*eta + 5634155384960*et*np.sqrt(1 + et)*eta + 8815272943360*et**2*np.sqrt(1 + et)*eta -           6339501896800*et**3*np.sqrt(1 + et)*eta - 16114344688800*et**4*np.sqrt(1 + et)*eta - 20586171144080*et**5*np.sqrt(1 + et)*eta - 19402788298600*et**6*np.sqrt(1 + et)*eta + 39038288817160*et**7*np.sqrt(1 + et)*eta + 70601935765800*et**8*np.sqrt(1 + et)*eta -            6005470099800*et**9*np.sqrt(1 + et)*eta - 57081662668800*et**10*np.sqrt(1 + et)*eta - 19527937228800*et**11*np.sqrt(1 + et)*eta + 12768266649600*et**12*np.sqrt(1 + et)*eta + 9012894105600*et**13*np.sqrt(1 + et)*eta + 1502149017600*et**14*np.sqrt(1 + et)*eta -            886685184000*np.sqrt(1 - et**2)*eta - 7020460339200*et*np.sqrt(1 - et**2)*eta - 23082501657600*et**2*np.sqrt(1 - et**2)*eta - 42545241446400*et**3*np.sqrt(1 - et**2)*eta - 49760642131200*et**4*np.sqrt(1 - et**2)*eta - 39562458566400*et**5*np.sqrt(1 - et**2)*eta -            22270956523200*et**6*np.sqrt(1 - et**2)*eta - 8980947360000*et**7*np.sqrt(1 - et**2)*eta - 2507819529600*et**8*np.sqrt(1 - et**2)*eta - 434214950400*et**9*np.sqrt(1 - et**2)*eta - 34717636800*et**10*np.sqrt(1 - et**2)*eta - 78236928000*eta**2 - 665665862400*et*eta**2 -            7082677324800*np.sqrt(1 - et)*et*eta**2 - 2526400800000*et**2*eta**2 - 17776547712000*np.sqrt(1 - et)*et**2*eta**2 - 5701190140800*et**3*eta**2 - 35118601056000*np.sqrt(1 - et)*et**3*eta**2 - 8557489987200*et**4*eta**2 - 53937935251200*np.sqrt(1 - et)*et**4*eta**2 -            8964525754800*et**5*eta**2 + 47166110006400*np.sqrt(1 - et)*et**5*eta**2 - 6584819194800*et**6*eta**2 + 210808284825600*np.sqrt(1 - et)*et**6*eta**2 - 3238968070800*et**7*eta**2 + 113636832724800*np.sqrt(1 - et)*et**7*eta**2 - 904044002400*et**8*eta**2 -            142244444865600*np.sqrt(1 - et)*et**8*eta**2 - 28686873600*et**9*eta**2 - 172668900096000*np.sqrt(1 - et)*et**9*eta**2 + 72695145600*et**10*eta**2 - 38539510732800*np.sqrt(1 - et)*et**10*eta**2 + 23145091200*et**11*eta**2 + 32249261721600*np.sqrt(1 - et)*et**11*eta**2 +            2404155600*et**12*eta**2 + 34033063680000*np.sqrt(1 - et)*et**12*eta**2 + 20263364352000*np.sqrt(1 - et)*et**13*eta**2 + 7119560448000*np.sqrt(1 - et)*et**14*eta**2 + 1017080064000*np.sqrt(1 - et)*et**15*eta**2 + 148305926400*et*np.sqrt(1 + et)*eta**2 +            647105192000*et**2*np.sqrt(1 + et)*eta**2 + 1416338797600*et**3*np.sqrt(1 + et)*eta**2 + 2088682227600*et**4*np.sqrt(1 + et)*eta**2 + 1753824077000*et**5*np.sqrt(1 + et)*eta**2 + 894053855800*et**6*np.sqrt(1 + et)*eta**2 + 311942392200*et**7*np.sqrt(1 + et)*eta**2 +            57569314200*et**8*np.sqrt(1 + et)*eta**2 + 3700489200*et**9*np.sqrt(1 + et)*eta**2 + 78236928000*np.sqrt(1 - et**2)*eta**2 + 672837580800*et*np.sqrt(1 - et**2)*eta**2 + 2409697382400*et**2*np.sqrt(1 - et**2)*eta**2 + 4983692313600*et**3*np.sqrt(1 - et**2)*eta**2 +            6838885468800*et**4*np.sqrt(1 - et**2)*eta**2 + 6651116841600*et**5*np.sqrt(1 - et**2)*eta**2 + 4655586196800*et**6*np.sqrt(1 - et**2)*eta**2 + 2285985240000*et**7*np.sqrt(1 - et**2)*eta**2 + 739827950400*et**8*np.sqrt(1 - et**2)*eta**2 +            140337489600*et**9*np.sqrt(1 - et**2)*eta**2 + 11735539200*et**10*np.sqrt(1 - et**2)*eta**2 + 651974400*et*eta**3 + 7171718400*et**2*eta**3 + 27708912000*et**3*eta**3 + 59329670400*et**4*eta**3 + 83493471600*et**5*eta**3 + 80559586800*et**6*eta**3 +            49835293200*et**7*eta**3 + 14954662800*et**8*eta**3 - 2159665200*et**9*eta**3 - 3463614000*et**10*eta**3 - 1100206800*et**11*eta**3 - 122245200*et**12*eta**3 - 2580486400*et*np.sqrt(1 + et)*eta**3 - 9524640000*et**2*np.sqrt(1 + et)*eta**3 -            17627788800*et**3*np.sqrt(1 + et)*eta**3 - 16340260800*et**4*np.sqrt(1 + et)*eta**3 - 16784833200*et**5*np.sqrt(1 + et)*eta**3 - 18943864800*et**6*np.sqrt(1 + et)*eta**3 - 8702150000*et**7*np.sqrt(1 + et)*eta**3 - 556368000*et**8*np.sqrt(1 + et)*eta**3 -            158856000*et**9*np.sqrt(1 + et)*eta**3 - 10024106400*eta*np.pi**2 - 50120532000*et*eta*np.pi**2 - 3854268910800*np.sqrt(1 - et)*et*eta*np.pi**2 - 130313383200*et**2*eta*np.pi**2 - 9638178303600*np.sqrt(1 - et)*et**2*eta*np.pi**2 - 245590606800*et**3*eta*np.pi**2 +            1910845282500*np.sqrt(1 - et)*et**3*eta*np.pi**2 - 343952150850*et**4*eta*np.pi**2 + 23093035119000*np.sqrt(1 - et)*et**4*eta*np.pi**2 - 332675031150*et**5*eta*np.pi**2 + 14677171289550*np.sqrt(1 - et)*et**5*eta*np.pi**2 - 208939967775*et**6*eta*np.pi**2 -            12389169003750*np.sqrt(1 - et)*et**6*eta*np.pi**2 - 80506104525*et**7*eta*np.pi**2 - 17201366582400*np.sqrt(1 - et)*et**7*eta*np.pi**2 - 17228932875*et**8*eta*np.pi**2 - 5172438902400*np.sqrt(1 - et)*et**8*eta*np.pi**2 - 1566266625*et**9*eta*np.pi**2 +            2044917705600*np.sqrt(1 - et)*et**9*eta*np.pi**2 + 3247810473600*np.sqrt(1 - et)*et**10*eta*np.pi**2 + 2285496259200*np.sqrt(1 - et)*et**11*eta*np.pi**2 + 842024937600*np.sqrt(1 - et)*et**12*eta*np.pi**2 + 120289276800*np.sqrt(1 - et)*et**13*eta*np.pi**2 -            32578345800*et**2*np.sqrt(1 + et)*eta*np.pi**2 - 122795303400*et**3*np.sqrt(1 + et)*eta*np.pi**2 - 166024262250*et**4*np.sqrt(1 + et)*eta*np.pi**2 - 110265170400*et**5*np.sqrt(1 + et)*eta*np.pi**2 - 36963892350*et**6*np.sqrt(1 + et)*eta*np.pi**2 - 5012053200*et**7*np.sqrt(1 + et)*eta*np.pi**2 +            10024106400*np.sqrt(1 - et**2)*eta*np.pi**2 + 70168744800*et*np.sqrt(1 - et**2)*eta*np.pi**2 + 205494181200*et**2*np.sqrt(1 - et**2)*eta*np.pi**2 + 330795511200*et**3*np.sqrt(1 - et**2)*eta*np.pi**2 + 321397911450*et**4*np.sqrt(1 - et**2)*eta*np.pi**2 +            193590554850*et**5*np.sqrt(1 - et**2)*eta*np.pi**2 + 70795251450*et**6*np.sqrt(1 - et**2)*eta*np.pi**2 + 14409652950*et**7*np.sqrt(1 - et**2)*eta*np.pi**2 + 1253013300*et**8*np.sqrt(1 - et**2)*eta*np.pi**2 - 11959073280*et*(1 + et)**3.5*(2 + et)**2*(3 + 2*et)*np.log(1 - et) +            11959073280*et*(1 + et)**3.5*(2 + et)**2*(3 + 2*et)*np.log(x)))/((1 + et)**3*(2 + et)**4)


def ew223pn(eta,x,et,pnOrder):

    omega22Peri_0pn = omega22P_0pn(et,x)
    omega22Peri_1pn = omega22P_1pn(eta,et,x)
    omega22Peri_2pn = omega22P_2pn(eta,et,x)
    omega22Peri_3pn = omega22P_3pn(eta,et,x)


    omega22Apo_0pn = omega22Ap_0pn(et,x)
    omega22Apo_1pn = omega22Ap_1pn(eta,et,x)
    omega22Apo_2pn = omega22Ap_2pn(eta,et,x)
    omega22Apo_3pn = omega22Ap_3pn(eta,et,x)

    if pnOrder == 0:
        pn_order_list = [1,0,0,0]
    elif pnOrder == 1 :
        pn_order_list = [1,1,0,0]
    elif pnOrder == 2 :
        pn_order_list = [1,1,1,0]
    elif pnOrder == 3 :
        pn_order_list = [1,1,1,1]
    else:
        pn_order_list = [1,1,1,1]

    pn_order_list = np.array(pn_order_list)

    omega22Peri = np.array([omega22Peri_0pn,omega22Peri_1pn,omega22Peri_2pn,omega22Peri_3pn])
    omega22periPN = np.dot(pn_order_list,omega22Peri)

    omega22Apo = np.array([omega22Apo_0pn,omega22Apo_1pn,omega22Apo_2pn,omega22Apo_3pn])
    omega22ApoPN = np.dot(pn_order_list,omega22Apo)


    ew22 = (np.sqrt(omega22periPN)-np.sqrt(omega22ApoPN))/(np.sqrt(omega22periPN) + np.sqrt(omega22ApoPN))

    return ew22
