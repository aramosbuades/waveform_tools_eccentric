#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp



def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]

if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument("--Nseeds",type=int,help="Number of seeds to consider",default=3)


    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")


    p.add_argument("--Npoints",type=int,help="Total number of points in random exploration",default=1000000)
    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)
    p.add_argument("--Nout",type=int,help="Number of iterations to produce output",default=500)

    p.add_argument("--eccmin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccmax", type=float,  help="Maximum eccentricity", default=0.3, )

    p.add_argument("--qmin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qmax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chimin", type=float,  help="Minimum chi (chi1 != chi2)", default=-0.99, )
    p.add_argument("--chimax", type=float,  help="Maximum chi (chi1 != chi2)", default=0.99, )

    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)


    if run_dir[-1] != "/":
        run_dir+="/"

    os.makedirs(run_dir, exist_ok=True)
    os.chdir(run_dir)

    ## Set some Parameters

    qmin=args.qmin
    qmax=args.qmax
    chimin=args.chimin
    chimax=args.chimax
    emin=args.eccmin
    emax=args.eccmax
    Nseeds=args.Nseeds
    NN=args.Npoints
    Nout=args.Nout
    Nchunks =args.Nchunks

    idx_list = np.arange(0,NN)

    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))
    # Set boundary for making the test


    submit_time = args.submit_time
    queue = args.queue

    script_dir =  args.script_dir

    header = """#!/bin/bash -
#SBATCH -J chunk_{}                # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue


#source /home/aramosbuades/load_LALenv.sh

cd {}
    """
    seed= 777

    for i in range(n_slurm_jobs):

        case = cases_per_slurm_job[i]

        fp = open("chunk_{}.sh".format(i), "w")
        fp.write(header.format(i,i,i,queue, submit_time, run_dir))

        cmd = """python {}/run_randomExplorer.py --outdir {} --Npoints {}  --eccmin {} --eccmax {} --qmin {} --qmax {} --chimin {} --chimax {} --seed {} --Nout {} --Nchunks {} --idx_chunk {} \n""".format(
            script_dir,
            run_dir,
            NN,
            emin,
            emax,
            qmin,
            qmax,
            chimax,
            chimin,
            seed,
            Nout,
            Nchunks,
            i
            )


        fp.write(cmd)


        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)

        print(submit_cmd)
        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
