#!/usr/bin/env/python

# Script to run the mismatches over eccentric waveforms
import sys, h5py, glob, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *
from eccentric_waveforms import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSE module
try:
    import EOBRun_module
except:
    pass

import numpy as np, pandas as pd, pycbc, json

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *




def get_EOBduration(f_min: float, EccIC: int, params_template: waveform_params) -> float:

    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    EccAvNQCWaveform,EcctAppend,HypPphi0, HypR0, HypE0 = [1, 1, 4.22, 10000, 1.01]
    eccentric_anomaly=0.0 # UNUSED
    #EccIC = 0

    if params_template.approx == "SEOBNRv4E_opt" or params_template.approx == "SEOBNRv4E_opt1" or params_template.approx == "SEOBNRv4":
        SpinAlignedVersion = 4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    else:
        SpinAlignedVersion = 41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary

    if params_template.approx == "SEOBNRv4E_opt" or params_template.approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt(params_template.delta_t,
                                                                  params_template.m1 * lal.MSUN_SI,
                                                                  params_template.m2 * lal.MSUN_SI,
                                                                  f_min,
                                                                  params_template.distance,
                                                                  params_template.s1z,
                                                                  params_template.s2z,
                                                                  params_template.ecc,
                                                                  eccentric_anomaly,
                                                                  SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                                  params_template.EccFphiPNorder,
                                                                  params_template.EccFrPNorder,
                                                                  params_template.EccWaveformPNorder,
                                                                  params_template.EccPNFactorizedForm,
                                                                  params_template.EccBeta,
                                                                  params_template.Ecct0,
                                                                  params_template.EccNQCWaveform,
                                                                  params_template.EccPNRRForm,
                                                                  params_template.EccPNWfForm,
                                                                  EccAvNQCWaveform, EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    elif params_template.approx == "SEOBNRv4E_opt1" or params_template.approx == "SEOBNRv4EHM_opt1":

        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt1(params_template.delta_t,
                                                                  params_template.m1 * lal.MSUN_SI,
                                                                  params_template.m2 * lal.MSUN_SI,
                                                                  f_min,
                                                                  params_template.distance,
                                                                  params_template.s1z,
                                                                  params_template.s2z,
                                                                  params_template.ecc,
                                                                  eccentric_anomaly,
                                                                  SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                                  params_template.EccFphiPNorder,
                                                                  params_template.EccFrPNorder,
                                                                  params_template.EccWaveformPNorder,
                                                                  params_template.EccPNFactorizedForm,
                                                                  params_template.EccBeta,
                                                                  params_template.Ecct0,
                                                                  params_template.EccNQCWaveform,
                                                                  params_template.EccPNRRForm,
                                                                  params_template.EccPNWfForm,
                                                                  EccAvNQCWaveform, EcctAppend,EccIC,HypPphi0, HypR0, HypE0)
    else:
        sphtseries, dyn, dynHi = lalsimul.SimIMRSpinAlignedEOBModes(params_template.delta_t,
                                                                  params_template.m1 * lal.MSUN_SI,
                                                                  params_template.m2 * lal.MSUN_SI,
                                                                  f_min,
                                                                  params_template.distance,
                                                                  params_template.s1z,
                                                                  params_template.s2z,
                                                                 SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1., nqcCoeffsInput, 0)

    if SpinAlignedVersion==4:

        h22 = sphtseries.mode.data.data #This is h_22
        t =  np.arange(0,len(h22)*params_template.delta_t, params_template.delta_t)
        amp22 = np.abs(h22)
        EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
        EOB_duration = EOB_duration / ((params_template.m1 + params_template.m2) * lal.MTSUN_SI)
    else:
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        t =  np.arange(0,len(h22)*params_template.delta_t, params_template.delta_t)
        amp22 = np.abs(h22)
        EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
        EOB_duration = EOB_duration / ((params_template.m1 + params_template.m2) * lal.MTSUN_SI)

    return EOB_duration


def get_NR_duration(params_signal: waveform_params,NR_file: str, NR_ma: lal.Value) -> float:

    # merger time difference manually

    _, hlm_NR = lalsim.SimInspiralNRWaveformGetHlms(
        params_signal.delta_t,
        params_signal.m1 * lal.MSUN_SI,
        params_signal.m2 * lal.MSUN_SI,
        params_signal.distance,
        params_signal.f_min,
        0.0,
        params_signal.s1x,
        params_signal.s1y,
        params_signal.s1z,
        params_signal.s2x,
        params_signal.s2y,
        params_signal.s2z,
        NR_file,
        NR_ma,
    )

    # non LAL waveform (only 22 waveform)
    modes_NR = {}
    modes_NR[2, 2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2).data.data
    modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
    time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
    NR_duration = get_time_to_merger(time_NR, modes_NR)
    NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    return NR_duration

def tdiff_v4E(
    f_min: float, NR_duration: float, params: waveform_params, ell_max: int = 4
) -> float:
    """Compute the difference to the merger time between NR and EOB. Merger time
    is defined as the peak of the frame invariant amplitude.

    Args:
        f_min (float): Starting frequency [Hz]
        NR_duration (float): Duration of the waveform [M]
        params (waveform_params): The parameters of the EOB waveform
        ell_max (int, optional): Max ell to use. Defaults to 4.

    Returns:
        float: The difference in the time to merger
    """
    EccIC = 0
    EOB_duration = get_EOBduration(f_min, EccIC, params)

    #print(f_min, NR_duration, EOB_duration, NR_duration - EOB_duration)
    return NR_duration - EOB_duration



def get_fminEOB_22NRlength_fast(f_min_signal: float, params_template: waveform_params,
                         NR_duration: float,  f_max: float = 2048.0) -> float:

    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(params_template.m1 + params_template.m2)*lal.MTSUN_SI)
    EccIC =0
    # Compute length of the shortest duration possible v4E waveform
    EOB_duration_fmin_Maxv4E = get_EOBduration(fmin_Maxv4E-1e-9, EccIC, params_template)
    EOB_duration_fmin_NR = get_EOBduration(f_min_signal, EccIC, params_template)

    EOB_duration_fmin0 = EOB_duration_fmin_NR

    #print(EOB_duration_fmin0, EOB_duration_fmin_Maxv4E, NR_duration)

    f_min0 = f_min_signal
    while NR_duration > EOB_duration_fmin0:
        f_min0 -= 10
        EOB_duration_fmin0 = get_EOBduration(f_min0, EccIC, params_template)

    #print(EOB_duration, NR_duration)
    if EOB_duration_fmin_Maxv4E > NR_duration:
        f_min_EOB = fmin_Maxv4E
    else:
        #res = root_scalar(tdiff, bracket=(f_min0, fmin_Maxv4E),args=(NR_duration, params_template, 2),)
        res = root_scalar(tdiff_v4E, bracket=(f_min0, fmin_Maxv4E),args=(NR_duration, params_template, 2),)
        f_min_EOB = res.root

    return f_min_EOB



#def generate_eccfminList(params_signal, params_template, e0, NR_file,ma, ell_max,fhigh, Nf, deltaf):
def generate_eccfminList(params_template: waveform_params,
                        f_min_signal:float,
                         e0: float,
                         NR_duration: float, Nf: int, deltaf: float,
                         EccFphiPNorder: int = 18,
                         EccFrPNorder: int= 18,
                         EccWaveformPNorder: int= 16,
                         EccPNFactorizedForm: int= 1,
                         EccBeta: float= 0.09,
                         Ecct0: float= 300,
                         EccNQCWaveform: int= 1,
                         EccPNRRForm: int = 1 ,
                         EccPNWfForm: int = 1 ,
                         EcctAppend: float = 100.0 ,
                         EccAvNQCWaveform: int = 1,
                         EccIC: int =0
                         ):

    params_template.ecc = e0
    LAL_params_template = lal.CreateDict()


    lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

    lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
    lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
    lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)
    lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
    lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)
    lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
    lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)
    lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)
    params_template.wf_param=LAL_params_template

    # Add -1 to account for inaccuracies in the determination of the starting frequency in which NR and EOB have the same length
    fmin_NRlength =get_fminEOB_22NRlength_fast(f_min_signal, params_template, NR_duration) - 1.

    fmin0=fmin_NRlength-deltaf
    fmin1=fmin_NRlength

    fref_list= np.linspace(fmin0,fmin1,Nf)
    ecc_array= np.full( Nf,(e0))
    eccfmin_array=np.stack((ecc_array,fref_list),axis=1)

    return eccfmin_array



def mm_eccfmin(s:pt.TimeSeries, params_template: dict, f_min:float, e0:float, flow: float,
                fhigh: float,modes_dc: bool=False, debug: bool=False,
                         EccFphiPNorder: int = 18,
                         EccFrPNorder: int= 18,
                         EccWaveformPNorder: int= 16,
                         EccPNFactorizedForm: int= 1,
                         EccBeta: float= 0.09,
                         Ecct0: float= 300,
                         EccNQCWaveform: int= 1,
                         EccPNRRForm: int = 1 ,
                         EccPNWfForm: int = 1 ,
                         EcctAppend: float = 100.0 ,
                         EccAvNQCWaveform: int = 1,
                         EccIC: int =0):

    params_template.ecc = e0
    params_template.f_min=f_min
    params_template.f_ref=f_min

    if params_template.approx=="SEOBNRv4E_opt" or params_template.approx=="SEOBNRv4EHM_opt" or params_template.approx=="SEOBNRv4E_opt1" or params_template.approx=="SEOBNRv4EHM_opt1":

        LAL_params_template = lal.CreateDict()


        lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

        lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
        lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
        lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
        lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)
        lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
        lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)
        params_template.wf_param=LAL_params_template


    final = unfaithfulness_RC(s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

    return e0, f_min, final



def get_TEOBduration(f_min: float, params_template: waveform_params, f_max: float,  modes: list) -> float:



    params_template.f_min = f_min

    t, hp, hc, hlm = generate_TEOBResumSE_simple_waveform(params_template,f_max,modes)

    amp22 = hlm['1'][0]

    #t, hp, hc, amp22 = generate_SEOBNREv4_waveform(params, max_freq)

    EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
    EOB_duration = EOB_duration / ((params_template.m1 + params_template.m2) * lal.MTSUN_SI)

    return EOB_duration



def tdiff_TEOB(
    f_min: float, NR_duration: float, params: waveform_params, modes:list
) -> float:
    """Compute the difference to the merger time between NR and EOB. Merger time
    is defined as the peak of the frame invariant amplitude.

    Args:
        f_min (float): Starting frequency [Hz]
        NR_duration (float): Duration of the waveform [M]
        params (waveform_params): The parameters of the EOB waveform
        ell_max (int, optional): Max ell to use. Defaults to 4.

    Returns:
        float: The difference in the time to merger
    """
    fhigh=2048.0
    #for ell in range(2, ell_max + 1):
    #    for m in range(-ell, ell + 1):
    #        modes.append([ell,m])

    EOB_duration =get_TEOBduration(f_min, params, fhigh,  modes)
    #get_TEOBduration(f_min, EccIC, params)

    #print(f_min, NR_duration, EOB_duration, NR_duration - EOB_duration)
    return NR_duration - EOB_duration

def get_fminTEOB_22NRlength_fast(f_min_signal: float, params_template: waveform_params,
                         NR_duration: float,  f_max: float = 2048.0, modes: list=[[2,2]]) -> float:

    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(params_template.m1 + params_template.m2)*lal.MTSUN_SI)

    # Compute length of the shortest duration possible v4E waveform
    #EOB_duration_fmin_Maxv4E = get_EOBduration(fmin_Maxv4E, EccIC, params_template)
    EOB_duration_fmin_Maxv4E = get_TEOBduration(fmin_Maxv4E, params_template, f_max,  modes)

    #EOB_duration_fmin_NR = get_EOBduration(f_min_signal, EccIC, params_template)
    EOB_duration_fmin_NR = get_TEOBduration(f_min_signal,  params_template, f_max,  modes)

    EOB_duration_fmin0 = EOB_duration_fmin_NR

    #print(EOB_duration_fmin0, EOB_duration_fmin_Maxv4E, NR_duration)
    #print("EOB_duration_fmin_NR = ", EOB_duration_fmin_NR)
    f_min0 = f_min_signal
    while NR_duration > EOB_duration_fmin0:
        f_min0 -= 10
        EOB_duration_fmin0 = get_TEOBduration(f_min0, params_template, f_max,  modes)
        #print("f_min0 = ", f_min0,"  EOB_duration_fmin0  = ", EOB_duration_fmin0)
    #print("EOB_shortest_duration", EOB_duration_fmin_Maxv4E, ", NR duration = ",NR_duration)
    #print("f_min_0  =  ", f_min0, ", EOB_duration_fmin0 = ", EOB_duration_fmin0)

    if EOB_duration_fmin_Maxv4E > NR_duration:
        f_min_EOB = fmin_Maxv4E-1e-6
    else:
        #res = root_scalar(tdiff, bracket=(f_min0, fmin_Maxv4E),args=(NR_duration, params_template, 2),)
        res = root_scalar(tdiff_TEOB, bracket=(f_min0, fmin_Maxv4E-1e-8),args=(NR_duration, params_template, modes),)
        f_min_EOB = res.root

    return f_min_EOB

def mm_fixedtotalMass(params_signal:waveform_params, params_template:waveform_params, M_fed: float,
                     m1_dim: float, m2_dim: float, freq_1M:float, freq_1M_new: float, omega_min_opt: float,
                     emin_opt:float, fhigh: float,modes_dc: bool=False, debug: bool=False,
                     EccFphiPNorder: int = 18,
                     EccFrPNorder: int= 18,
                     EccWaveformPNorder: int= 16,
                     EccPNFactorizedForm: int= 1,
                     EccBeta: float= 0.09,
                     Ecct0: float= 300,
                     EccNQCWaveform: int= 1,
                     EccPNRRForm: int = 1 ,
                     EccPNWfForm: int = 1 ,
                     EcctAppend: float = 100.0 ,
                     EccAvNQCWaveform: int = 1,
                     EccIC: int =0,
                     template_approx: str ="SEOBNRv4E_opt",
                     NR_file: str="None",
                     ellMax: int=2,
                     kappa_s: float = 0.0
                     ):

    params_template.ecc = emin_opt

    # Add dictionaries here due to multiprocessing not pickling lal.dict() objects
    if template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4EHM_opt" or template_approx == "SEOBNRv4E_opt1" or template_approx == "SEOBNRv4EHM_opt1":

        LAL_params_template = lal.CreateDict()


        lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

        lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
        lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
        lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
        lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)
        lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
        lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)
        params_template.wf_param=LAL_params_template


    LAL_params_signal = lal.CreateDict()
    ma = lalsim.SimInspiralCreateModeArray()

    # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
    if (
        template_approx == "SEOBNRv4"
        or template_approx == "TEOBResumSE"
        or template_approx == "SEOBNREv4"
        or template_approx == "SEOBNRv4E"
        or template_approx == "SEOBNRv4E_opt"
        or template_approx == "SEOBNRv4E_opt1"
        or template_approx == "IMRPhenomXEv1"
    ):
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
    else:
        for ell in range(2, ellMax + 1):
            lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
    lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
    lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)

    params_signal.wf_param=LAL_params_signal

    #print(" M_fed = ", M_fed)


    ################### Actual calculation
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    f_ref = freq_1M / (m1 + m2)

    if f_ref<10.0:
        f_ref=10.0
    f_min = 1 * f_ref

    fmin_start = freq_1M_new/ (m1 + m2)
    if fmin_start< 10.0:
        fmin_start = 10.0

    # Update signal parameters
    params_signal.m1 = m1
    params_signal.m2 = m2
    params_signal.f_min = f_min
    params_signal.f_ref = f_ref


    # Update template parameters
    f_ref_t= omega_min_opt/(m1+m2)
    f_min_t =  f_ref_t

    params_template.m1 = m1
    params_template.m2 = m2
    params_template.f_min = f_min_t
    params_template.f_ref = f_ref_t
    params_template.ecc = emin_opt

    sp, sc = generate_waveform(params_signal)
    s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)


    if isinstance(s, pt.TimeSeries):
        N = len(s)
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        s.resize(pad)
    s_tilde = make_frequency_series(s)

    """
    amp_signal_FD=np.abs(s_tilde.data)
    imax= np.argmax(amp_signal_FD)
    freq_peak=s_tilde.get_sample_frequencies()[imax]
    flow=1.2*freq_peak
    """

    flow=1.*fmin_start

    if flow < 10:
        flow = 10

    psd = generate_psd(len(s_tilde), s_tilde.delta_f, flow)
    SNR = _filter.sigma(
        s_tilde, psd=psd, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh
    )

    final = unfaithfulness_RC(s, params_template,  flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)
    #print(final)
    #mmtools.append(final)

    return final



#def generate_eccfminList(params_signal, params_template, e0, NR_file,ma, ell_max,fhigh, Nf, deltaf):
def generate_eccfminList_TEOB(params_template: waveform_params,
                        f_min_signal:float,
                         e0: float,
                         NR_duration: float, Nf: int, deltaf: float,fhigh:float, modes:list
                         ):

    params_template.ecc = e0

    #fmin_NRlength =get_fminEOB_22NRlength(params_signal, params_template, NR_file, ma, ell_max, fhigh)

    #print("e = ",e0, ",  fmin_signal = ",f_min_signal)
    fmin_NRlength =get_fminTEOB_22NRlength_fast(f_min_signal, params_template, NR_duration,fhigh,modes)-1

    fmin0=fmin_NRlength-deltaf
    fmin1=fmin_NRlength

    fref_list= np.linspace(fmin0,fmin1,Nf)
    ecc_array= np.full( Nf,(e0))
    eccfmin_array=np.stack((ecc_array,fref_list),axis=1)

    return eccfmin_array

#############################################################

# Main program

#############################################################

#!/usr/bin/env/python3
import argparse


def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=0.0
    )

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=99,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=99,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )


    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=0.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )


    p.add_argument(
        "--TEOB_IC",
        type=int,
        help="TEOB initial conditions: #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation",
        default=0,
    )

    p.add_argument("--Ne",type=int,help="Number of points for 1D eccentricity maximization.",default=50)
    p.add_argument("--Nf",type=int,help="Number of points for 1D starting frequency maximization.",default=200)
    p.add_argument("--deltaEcc",type=float,help="Interval of increasing eccentricity range.",default=0.1)
    p.add_argument("--deltaF",type=float,help="Interval of increasing starting frequency range.",default=10.)

    #p.add_argument("--verbose",type=str,
    #        help="If True it produces a bunch of plots",default="False")

    p.add_argument("--run_dir", type=str, help="Running directory")

    ##################################################


    args = p.parse_args()

    phi_s = 0.0
    kappa_s = 0.0

    parameters = {}

    EccIC=0
    TEOB_IC=args.TEOB_IC

    #if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt" or args.approximant == "SEOBNRv4EHM_opt":
    parameters["EccFphiPNorder"] = args.EccFphiPNorder_template
    parameters["EccFrPNorder"] = args.EccFrPNorder_template
    parameters["EccWaveformPNorder"] = args.EccWaveformPNorder_template
    parameters["EccPNFactorizedForm"] = args.EccPNFactorizedForm_template
    parameters["EccBeta"] = args.EccBeta_template
    parameters["Ecct0"] = args.Ecct0_template
    parameters["EccNQCWaveform"] = args.EccNQCWaveform_template

    parameters["EccPNRRForm"] = args.EccPNRRForm
    parameters["EccPNWfForm"] = args.EccPNWfForm

    parameters["EccAvNQCWaveform"] = args.EccAvNQCWaveform
    parameters["EcctAppend"] = args.EcctAppend
    parameters["TEOB_IC"] =TEOB_IC

    if args.approximant == "IMRPhenomXEv1":

        parameters["Nharmonic_template"] = args.Nharmonic_template
        parameters["kAdvance_template"] = args.kAdvance_template

    elif args.approximant == "TEOBResumSEHM" or args.approximant == "SEOBNRv4EHM_opt" or args.approximant == "SEOBNRv4EHM_opt1":
        modes=[[2,2],[2,1],[3,3],[4,4],[5,5]]
    else:
        modes=[[2,2]]

    ############################################################################

    iota_s = args.iota_s
    NR_file = args.NR_file
    Ne = args.Ne
    Nf = args.Nf

    signal_approx=args.signal
    template_approx=args.approximant

    fhigh=2048.0
    ellMax=args.ell_max
    #verbose = args.verbose

    rundir = args.run_dir
    if rundir == None:
        rundir = "."
    else:
        if rundir[-1]!= '/':
            rundir = rundir+'/'

    #if plotdir == None:
    #    plotdir= rundir+'/NR_plots/'
    #    os.makedirs(plotdir, exist_ok=True)


    outdir = rundir+"/mm_files/"
    os.makedirs(outdir, exist_ok=True)

    # Generic bounds on the initial eccentricity  of each model, below we use also the Newtonian estimate at the starting
    # frequency to set a maximum allowed eccentricity
    if template_approx == "TEOBResumSE":
        e0max_template = 0.3
    elif template_approx == "TEOBResumSEHM":
        e0max_template = 0.3
    elif template_approx == "SEOBNREv4":
        e0max_template = 0.7
    elif template_approx == "SEOBNRv4E":
        e0max_template = 0.3
    elif template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4E_opt1":
        e0max_template = 0.3
    elif template_approx == "SEOBNRv4EHM_opt" or template_approx == "SEOBNRv4EHM_opt1":
        e0max_template = 0.3
    elif template_approx == "IMRPhenomXEv1":
        e0max_template = 0.2
    else:
        e0max_template = 0.0


    # Figure out the domains of the signal and the template
    td_approxs = td_approximants()
    fd_approxs = fd_approximants()
    if signal_approx not in fd_approxs  or signal_approx == "SEOBNRv4":
        signal_domain = "TD"
        delta_t_signal = 1.0 / 8192
        delta_f_signal = None
    else:
        signal_domain = "FD"
        delta_f_signal = 0.125
        delta_t_signal = None

    if template_approx not in fd_approxs or template_approx == "SEOBNRv4":
        template_domain = "TD"
        delta_t_template = 1.0 / 8192
        delta_f_template = None

    else:
        template_domain = "FD"
        delta_f_template = 0.125
        delta_t_template = None

    approx_type = get_approximant_type(template_approx)
    LAL_params_signal = None
    LAL_params_template = None

    LAL_params_template = lal.CreateDict()

    EccFphiPNorder = parameters["EccFphiPNorder"]
    EccFrPNorder = parameters["EccFrPNorder"]
    EccWaveformPNorder = parameters["EccWaveformPNorder"]
    EccPNFactorizedForm = parameters["EccPNFactorizedForm"]


    EccBeta = parameters["EccBeta"]
    Ecct0 = parameters["Ecct0"]
    EccNQCWaveform = parameters["EccNQCWaveform"]

    EccPNRRForm = parameters["EccPNRRForm"]
    EccPNWfForm = parameters["EccPNWfForm"]

    #if template_approx == "SEOBNRv4E_opt"  or template_approx == "SEOBNRv4EHM_opt":
    EcctAppend = parameters["EcctAppend"]
    EccAvNQCWaveform = parameters["EccAvNQCWaveform"]

    if template_approx == "SEOBNRv4E" or template_approx == "SEOBNRv4E_opt"  or template_approx == "SEOBNRv4EHM_opt" or template_approx == "SEOBNRv4E_opt1"  or template_approx == "SEOBNRv4EHM_opt1" :


        #print(EccFphiPNorder, EccFrPNorder, EccWaveformPNorder, EccPNFactorizedForm)
        #print(EccBeta,Ecct0,EccNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

        lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
        lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
        lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)

        lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
        lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)

        lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
        lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)


        if signal_approx =="SEOBNRv4E" or signal_approx =="SEOBNRv4E_opt"  or signal_approx == "SEOBNRv4EHM_opt"  or signal_approx =="SEOBNRv4E_opt1"  or signal_approx == "SEOBNRv4EHM_opt1":
            LAL_params_signal = lal.CreateDict()

            lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_signal, EccFphiPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_signal, EccFrPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_signal, EccWaveformPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_signal, EccPNFactorizedForm)

            lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_signal, EccBeta)
            lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_signal, Ecct0)
            lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_signal, EccNQCWaveform)

            lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_signal, EccPNRRForm)
            lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_signal, EccPNWfForm)

            lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_signal, EcctAppend)
            lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_signal, EccAvNQCWaveform)

    elif template_approx == "IMRPhenomXEv1":
        LAL_params_template = lal.CreateDict()

        Nharmonic = parameters["Nharmonic"]
        kAdvance = parameters["kAdvance"]
        lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template,Nharmonic)
        lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)

    # Read LVCNR format file
    if NR_file:
        fp = h5py.File(NR_file, "r")
        freq_1M = fp.attrs["f_lower_at_1MSUN"]
        # Add rounding to prevent some errors in the calculation of the symmetric mass ratio (for TEOBResumSE)
        m1_dim = round(fp.attrs["mass1"], 5)
        m2_dim = round(fp.attrs["mass2"], 5)

        if fp.attrs["eccentricity"] > 1:
            eccentricity_signal = 0.2
        else:
            eccentricity_signal = round(fp.attrs["eccentricity"], 4)

        mean_anomaly_signal = round(fp.attrs["mean_anomaly"], 4)
        q=m1_dim/m2_dim

        fp.close()
        prefix = os.path.basename(NR_file).split(".")[0]

        LAL_params_signal = lal.CreateDict()
        ma = lalsim.SimInspiralCreateModeArray()

        # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
        if (
            template_approx == "SEOBNRv4"
            or template_approx == "TEOBResumSE"
            or template_approx == "SEOBNREv4"
            or template_approx == "SEOBNRv4E"
            or template_approx == "SEOBNRv4E_opt"
            or template_approx == "SEOBNRv4E_opt1"
            or template_approx == "IMRPhenomXEv1"
        ):
            lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
            lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
        else:
            for ell in range(2, ellMax + 1):
                lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
        lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
        lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)

        (s1x, s1y, s1z, s2x, s2y, s2z,) = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(0.0, 100, NR_file)

        # Since we are using f_ref=0, total mass does not matter

    # Read orbital eccentricity from NR file
    ModeList=[[2,2]]
    timeNR, tHorizon, hlm, amplm, phaselm, omegalm, omega_orb, phase_orb = compute_h22OmegaOrbfromNRfile(NR_file, ModeList)

    # Estimate eccentricity and averaged orbital frequency from the NR simulation to get the initial guesses
    # for the model

    iphase_orb = InterpolatedUnivariateSpline(tHorizon, phase_orb)

    tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima = compute_MaxMin(tHorizon, omega_orb)

    tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(timeNR, omegalm[2,2], amplm[2,2])

    iomega_orb_maxima = InterpolatedUnivariateSpline(tH_maxima, omega_orb_maxima)
    iomega_orb_minima = InterpolatedUnivariateSpline(tH_minima, omega_orb_minima)
    omegaOrb_maxima = iomega_orb_maxima(tHorizon)
    omegaOrb_minima = iomega_orb_minima(tHorizon)
    ecc_omegaorb = (np.sqrt(omegaOrb_maxima)-np.sqrt(omegaOrb_minima))/(np.sqrt(omegaOrb_maxima)+np.sqrt(omegaOrb_minima))

    # Compute the averaged orbital frequency
    omegaOrb_av =1./(tH_maxima[1:]-tH_maxima[:-1])*(iphase_orb(tH_maxima[1:])-iphase_orb(tH_maxima[:-1]))

    if ecc_omegaorb[0]>1:
        ecc_omegaorb[0]=0.2
    elif ecc_omegaorb[0]<0:
        ecc_omegaorb[0]=0.01
    elif np.iscomplex(ecc_omegaorb[0]):
        ecc_omegaorb[0]=np.abs(ecc_omegaorb[0])
    else:
        ecc_omegaorb[0]=ecc_omegaorb[0]


    # Just in case the waveform is noisy
    i=0
    omega22Max = omega_maxima[i]
    while omega22Max < omegaOrb_av[0]*2 :
        omega22Max = omega_maxima[i]
        i+=1

    omega_metadata = freq_1M*2*np.pi*lal.MTSUN_SI

    # Compute the minimum frequency for overlap calculations
    freq_1M_new = 1.2* omega22Max/(2*np.pi)/lal.MTSUN_SI # 1.2 factor to avoid conditioning of the FT for short waveforms

    # Choose some arbitrary parameters
    Mtotals=np.linspace(20, 200, 20)

    dMpc= 500.0
    dist = 1e6 * lal.PC_SI * dMpc  # Doesn't matter what this is

    modes_dc=None

    # Put the name of the NR file in a nicer format
    basename=os.path.basename(NR_file)
    simName = basename.split('.h5')[0]

    # Look for optimal eccentricity and starting frequency for the lowest total mass
    M_fed = Mtotals[5]
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    f_ref = freq_1M / (m1 + m2)
    if f_ref<10.0:
        f_ref=10.0

    f_min = 1 * f_ref

    # The signal does not care about the values of eccentricity and mean anomaly
    params_signal = waveform_params(
        m1, m2,
        s1x, s1y, s1z,
        s2x, s2y, s2z,
        iota_s, phi_s,
        f_ref, f_min,
        dist, delta_t_signal,  delta_f_signal,
        LAL_params_signal,
        signal_approx, signal_domain,
        0.,
        0.,
    )

    NR_duration = get_NR_duration(params_signal,NR_file,ma)
    # Compute the maximum frequency for v4E waveform generation
    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(m1+m2)*lal.MTSUN_SI)

    fmin_start = freq_1M_new/ (m1 + m2)

    # generate signal for  fixed total mass
    sp, sc = generate_waveform(params_signal)
    s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)

    # initialize also the template parameters
    params_template = waveform_params(
        m1, m2,
        0, 0, s1z,
        0, 0, s2z,
        iota_s, phi_s,
        f_ref, f_min,
        dist, delta_t_template, delta_f_template,
        LAL_params_template,
        template_approx, template_domain,
        ecc = ecc_omegaorb[0]
    )
    params_template.TEOB_IC = TEOB_IC
    # Once waveform and NR data loaded. Generate the grid

    params_template.f_min = f_min
    params_template.ecc = ecc_omegaorb[0] # NR eccentricity

    # How much we are stepping back in the frequency optimization
    deltaf=args.deltaF

    # How much we are widening the eccentricity range
    delta_e = args.deltaEcc

    if ecc_omegaorb[0]-delta_e<0:
        e0min=0
    else:
        e0min = ecc_omegaorb[0]-delta_e

    e0max=ecc_omegaorb[0]+delta_e

    # Eccentricity list
    ecc_list = np.linspace(e0min, e0max, Ne)

    # Parallelize the grid generation
    LAL_params_template_grid=None
    params_template.wf_param = LAL_params_template_grid

    if template_approx == "SEOBNRv4E" or template_approx == "SEOBNRv4E_opt" or template_approx == "SEOBNRv4E_opt1" or template_approx == "SEOBNRv4EHM_opt"  or template_approx == "SEOBNRv4EHM_opt1" :

        grid_eccfmin = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
            delayed(generate_eccfminList)(
                #params_signal,
                params_template,
                params_signal.f_min,
                ecc_list[i],
                NR_duration,
                Nf,
                deltaf,
                EccFphiPNorder,
                EccFrPNorder,
                EccWaveformPNorder,
                EccPNFactorizedForm,
                EccBeta,
                Ecct0,
                EccNQCWaveform,
                EccPNRRForm,
                EccPNWfForm,
                EcctAppend,
                EccAvNQCWaveform,
                EccIC
            )
            for i in range(len(ecc_list))
            )
    elif template_approx == "TEOBResumSE"  or template_approx == "TEOBResumSEHM" :

        grid_eccfmin = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(generate_eccfminList_TEOB)(
            #params_signal,
            params_template,
            params_signal.f_min,
            ecc_list[i],
            NR_duration,
            Nf,
            deltaf,
            fhigh,
            modes
        )
        for i in range(len(ecc_list))
        )
    else:
        print(" Template approximant %s  not implemented! Exiting".format(template_approx))
        exit(-1)



    x_ecc = [val[0] for sublist in grid_eccfmin for val in sublist]
    y_fmin = [val[1] for sublist in grid_eccfmin for val in sublist]


    if isinstance(s, pt.TimeSeries):
        N = len(s)
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        s.resize(pad)
    s_tilde = make_frequency_series(s)

    amp_signal_FD=np.abs(s_tilde.data)
    imax= np.argmax(amp_signal_FD)
    freq_peak=s_tilde.get_sample_frequencies()[imax]

    # Set flow for mismatch integral calculation as twice the peak of the strain to avoid FT artifacts
    flow=1.2*freq_peak
    if flow < 10:
        flow = 10

    freq_1M_low=flow*(m1+m2)
    flow=1.*fmin_start

    psd = generate_psd(len(s_tilde), s_tilde.delta_f, flow)
    SNR = _filter.sigma(
        s_tilde, psd=psd, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh
    )
    if flow < 10:
        flow = 10



    # Parallelize the mismatch calculation over the previous grid
    params_template.wf_param=LAL_params_template_grid
    debug=False
    modes_dc =False
    res = Parallel(n_jobs=16, verbose=50, backend="multiprocessing"
                  )(
            delayed(mm_eccfmin)(
                s,
                params_template,
                y_fmin[i],
                x_ecc[i],
                flow,
                fhigh,
                modes_dc,
                debug,
                EccFphiPNorder,
                EccFrPNorder,
                EccWaveformPNorder,
                EccPNFactorizedForm,
                EccBeta,
                Ecct0,
                EccNQCWaveform,
                EccPNRRForm,
                EccPNWfForm,
                EcctAppend,
                EccAvNQCWaveform,
                EccIC)
            for i in range(len(x_ecc))
            )


    # Get optimal values
    mm_list = [ii[2] for ii in res]
    imin = np.argmin(mm_list)
    emin_opt = x_ecc[imin]
    fmin_opt = y_fmin[imin]
    omega_min_opt = fmin_opt*(m1+m2)
    Momega_min_opt = omega_min_opt*(2.*np.pi)*lal.MTSUN_SI
    mm_opt = mm_list[imin]

    print("Optimal Values : e = ",emin_opt,",  fmin_opt = ",fmin_opt, ",  mismatch  = ", mm_opt)


    # Loop over all total mass, also in parallel

    params_template.wf_param=None
    params_signal.wf_param=None
    debug =True # Output optimal time shift and phase offset
    mm_totalMass = Parallel(n_jobs=16, verbose=50,  backend="multiprocessing"
              )(
        delayed(mm_fixedtotalMass)(
            params_signal,
            params_template,
            Mtotals[i],
            m1_dim,
            m2_dim,
            freq_1M,
            freq_1M_new,
            omega_min_opt,
            emin_opt,
            fhigh,
            modes_dc,
            debug,
            EccFphiPNorder,
            EccFrPNorder,
            EccWaveformPNorder,
            EccPNFactorizedForm,
            EccBeta,
            Ecct0,
            EccNQCWaveform,
            EccPNRRForm,
            EccPNWfForm,
            EcctAppend,
            EccAvNQCWaveform,
            EccIC,
            template_approx,
            NR_file,
            ellMax,
            kappa_s
            )
        for i in range(len(Mtotals))
        )


    mm_total_mass= [ii[0] for ii in mm_totalMass]
    time_shift_total_mass= [ii[1] for ii in mm_totalMass]
    phase_offset_total_mass= [ii[2] for ii in mm_totalMass]

    #### Now save all the rellevant information to a .json file


    res={}
    res['NR_file'] = simName
    res['q'] = q
    res['s1z'] = s1z
    res['s2z'] = s2z
    res['eorb_NR'] = ecc_omegaorb[0]
    res['freq_1M_NR'] = freq_1M
    res['freq_1M_match'] = freq_1M_new
    res['freq_1M_low'] = freq_1M_low

    # Store values of 1D optimizations for debugging purposes
    res['ecc_list'] = x_ecc
    res['fmin_list'] = y_fmin
    res['mismatch_list'] = mm_list
    res['Ne'] = Ne
    res['Nf'] = Nf


    # Store optimal values
    res['Momegav4E_min_opt'] = Momega_min_opt
    res['ev4E_opt'] = emin_opt
    res['Mtotal_list'] = Mtotals.tolist()
    res['mismatch_over_Mtotal'] = mm_total_mass
    res['phase_offset'] = phase_offset_total_mass
    res['time_shift'] = time_shift_total_mass


    outfile=outdir+"{}_Ne{}_Nf{}_emismatches.json".format(simName,Ne,Nf)
    with open(outfile, 'w') as f:
        json.dump(res, f)


if __name__ == "__main__":
    main()
