#!/usr/bin/env/python3
import argparse
import numpy as np
import sys

import lal

sys.path.append("/home/sossokine/Sources/waveform_tools")
#from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from waveform_tools.mismatch.unfaithfulness_ecc import run_Eccunfaithfulness
from joblib import delayed, Parallel


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--signal", type=str, help="Approximant used for signal")
    p.add_argument("--template", type=str, help="Approximant to use for template")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="For Phenom waveforms whether to minimize over reference frequency or rigid rotation of the spins",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )
    p.add_argument("--q", type=float, help="Mass ratio, q>=1")
    p.add_argument(
        "--chi1", type=str, help="Dimensionless spin of primary, comma separated"
    )
    p.add_argument(
        "--chi2", type=str, help="Dimensionless spin of secondary, comma separated"
    )
    p.add_argument(
        "--omega_min",
        type=float,
        help="Starting **orbital** frequency of waveform generation, in geometric units",
    )

    p.add_argument(
        "--eccentricity",
        type=float,
        help="Initial eccentricity of the waveform",
        default=0.0,
    )

    p.add_argument(
        "--mean_anomaly",
        type=float,
        help="Initial mean_anomaly the waveform",
        default=0.0,
    )

    p.add_argument(
        "--raw_grid",
        type=bool,
        help="Make a coarse grid unfaithfulness calculations before numerical optimization.",
        default=False,
    )

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )

    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )

    args = p.parse_args()


    chi1 = np.array([float(x) for x in args.chi1.split(",")])
    chi2 = np.array([float(x) for x in args.chi2.split(",")])
    name = f"{args.signal}_{args.template}"

    # We need to convert from omega to freq_1M
    freq_1M = args.omega_min / np.pi / lal.MTSUN_SI
    if args.eccentricity != 0.0 or args.mean_anomaly != 0.0:
        if args.mean_anomaly != 0.0:
            parameters = dict(
                q=args.q,
                chi1=chi1,
                chi2=chi2,
                freq_1M=freq_1M,
                case=name,
                eccentricity=args.eccentricity,
                mean_anomaly=args.mean_anomaly,
            )
        else:
            parameters = dict(
                q=args.q,
                chi1=chi1,
                chi2=chi2,
                freq_1M=freq_1M,
                case=name,
                eccentricity=args.eccentricity,
            )
    else:
        parameters = dict(q=args.q, chi1=chi1, chi2=chi2, freq_1M=freq_1M, case=name )

    if args.template == "SEOBNRv4E" or args.template == "SEOBNRv4E_opt"  or args.template == "SEOBNRv4EHM_opt"  :
        parameters["EccFphiPNorder"] = args.EccFphiPNorder_template
        parameters["EccFrPNorder"] = args.EccFrPNorder_template
        parameters["EccWaveformPNorder"] = args.EccWaveformPNorder_template
        parameters["EccPNFactorizedForm"] = args.EccPNFactorizedForm_template
        parameters["EccBeta"] = args.EccBeta_template
        parameters["Ecct0"] = args.Ecct0_template
        parameters["EccNQCWaveform"] = args.EccNQCWaveform_template
        parameters["EccPNRRForm"] = args.EccPNRRForm
        parameters["EccPNWfForm"] = args.EccPNWfForm
        parameters["EcctAppend"] = args.EcctAppend
        parameters["EccAvNQCWaveform"] = args.EccAvNQCWaveform

    elif args.template == "IMRPhenomXEv1":

        parameters["Nharmonic_template"] = args.Nharmonic_template
        parameters["kAdvance_template"] = args.kAdvance_template

    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()

    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(run_Eccunfaithfulness)(
            y[i],
            x[i],
            args.iota_s,
            i,
            parameters=parameters,
            ellMax=args.ell_max,
            signal_approx=args.signal,
            template_approx=args.template,
            fhigh=2048.0,
            minimization_type=args.min_type,
            unfaithfulness_type=args.unfaithfulness_type,
            raw_grid = args.raw_grid,
        )
        for i in range(len(x))
    )
