#!/usr/bin/env python
import argparse
import glob, os
import numpy as np
import subprocess as sp


import sys, h5py, glob, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *
from eccentric_waveforms import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSE module
try:
    import EOBRun_module
except:
    pass

import numpy as np, pandas as pd, pycbc, json

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *
from run_one_eccMatch22_nestedOpts import *
#%matplotlib inline


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


def SEOBNRv4EHM_modes(q:float, M_fed:float, f_min: float, chi1:float, chi2:float, ecc:float,delta_t:float,
                      dMpc: float, approx:str):



    distance =dMpc *1e6 *lal.PC_SI
    m1_dim  = q/(1.+q)
    m2_dim  = 1./(1.+q)
    m1 = m1_dim*M_fed
    m2 = m2_dim*M_fed

    EccFphiPNorder = EccFrPNorder=99
    EccWaveformPNorder=16
    EccBeta=0.09
    Ecct0=100
    EccPNFactorizedForm = EccNQCWaveform = EccPNRRForm = EccPNWfForm = EccAvNQCWaveform =1
    EcctAppend = 1
    HypPphi0, HypR0, HypE0 =[0.,0,0]
    EccIC=0
    eccentric_anomaly = 0.0

    if approx=="SEOBNRv4E_opt" or  approx=="SEOBNRv4":
        SpinAlignedVersion=4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    else:
        SpinAlignedVersion=41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary

    if approx == "SEOBNRv4E_opt" or approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              ecc,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:



        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)





    hlm={}


    if SpinAlignedVersion==4:
        hlm['2,2'] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm['2,-2'] = np.conjugate(hlm['2,2'])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h55 )

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h44 )

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h21 )

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[f"{modeL},{modeM}"] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h33 )

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[f"{modeL},{modeM}"] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[f"{modeL},{-modeM}"] =  ((-1)**modeL) * np.conjugate(h22 )


    time_array = np.arange(0,len(hlm['2,2'])*delta_t, delta_t)
    timeNR = SectotimeM(time_array,M_fed)
    #return time_array, hlm
    return timeNR, hlm, nqcCoeffsInput.data


def get_nqc_Coeffsv4v4E(q:float, M_fed:float, f_min: float, chi1:float, chi2:float, ecc:float,delta_t:float,
                      dMpc: float):


    approx='SEOBNRv4HM'

    timev4HM, hlmv4HM, nqcCoeffsInputv4HM =SEOBNRv4EHM_modes(q,M_fed, f_min, chi1,chi2, ecc,delta_t,
                                                             dMpc, approx)


    approx='SEOBNRv4EHM'

    timev4EHM, hlmv4EHM, nqcCoeffsInputv4EHM =SEOBNRv4EHM_modes(q,M_fed, f_min, chi1, chi2, ecc,delta_t,
                                                             dMpc, approx)



    nqcCoeffs_v4_list = [item for item in nqcCoeffsInputv4HM]
    nqcCoeffs_v4E_list = [item for item in nqcCoeffsInputv4EHM]
    out0 = [q, chi1, chi2, ecc]
    out0.append(nqcCoeffs_v4_list)
    out0.append(nqcCoeffs_v4E_list)

    # Flatten output list
    out = []
    for item in out0:
        #print(item)
        if isinstance(item,list):
            for item1 in item:
                out.append(item1)
        else:
            out.append(item)


    return out

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    # Arguments of the test
    p.add_argument( "--run_dir", type=str, help="Directory where the results will be output",  default=os.getcwd(), )
    #p.add_argument( "--script_dir", type=str, help="Directory where the script lives",  default=os.environ['WAVEFORM_TOOLS_PATH'], )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=50., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-0.99, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=0.99, )

    #p.add_argument("--approximant", type=str,  help="Waveform approximant (default SEOBNRv4E_opt)", default="SEOBNRv4E_opt", )
    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)

    p.add_argument("--idx_chunk",type=int,help="Subset of cases to run",default=1)

    args = p.parse_args()


    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)

    if run_dir[-1]!="/":
        run_dir +='/'



    # We want to compute the unfaithfulness between SEOBNRv4E and SEOBNRv4E varying the spins between [0.8-0.99]
    # and check the smoothness of the unfaithfulness function

    dMpc= 500
    dist = 1e6 * lal.PC_SI * dMpc  # Doesn't matter what this is
    fhigh=2048.0

    # Choose approximants

    #template_approx="TEOBResumSE"
    #template_approx=args.approximant
    #signal_approx=template_approx

    delta_t = 1./(4*2048.)

    # Total Mass =100 and f_min =20Hz, e0 = 0.3
    M_fed = 100.0
    f_min = 20.0
    e0 = 0.
    f_ref = 1. * f_min
    flow = f_min

    # Unfaithfulness parameters
    debug=False
    modes_dc=None

    q_min= args.qMin
    q_max= args.qMax
    Nq=args.Nq

    q_grid = np.linspace(q_min, q_max, Nq)

    Nchi = args.Nchi
    chi_min = args.chiMin
    chi_max = args.chiMax

    chi1_grid = np.linspace(chi_min, chi_max, Nchi)
    chi2_grid = np.linspace(chi_min, chi_max, Nchi)
    #spins = np.stack((chi1_grid,chi2_grid),stack=1)

    x, y,z = np.meshgrid(q_grid, chi1_grid,chi2_grid)

    x = x.flatten()
    y = y.flatten()
    z = z.flatten()

    NN = len(x)

    idx_list = np.arange(0,NN)
    Nchunks =args.Nchunks
    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    idx_chunk=args.idx_chunk

    case0=cases_per_slurm_job[idx_chunk]
    cases_per_output = list(chunks(case0,Nchunks))[0]



    result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(delayed(get_nqc_Coeffsv4v4E)(
                    x[i],
                    M_fed,
                    f_min,
                    y[i],
                    z[i],
                    e0,
                    delta_t,
                    dMpc)
                for i in cases_per_output
                )


    result=np.array(result)

    np.savetxt(run_dir+"nqcCoeffs_q{}_{}_chi_{}_{}_Nq{}_Nchi{}_idx{}.dat".format(q_min,q_max, chi_min, chi_max,Nq,Nchi,idx_chunk), result)
