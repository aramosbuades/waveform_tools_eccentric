#!/usr/bin/env python
import argparse
import glob
import os, numpy as np
import subprocess as sp
import uuid
import math

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )
    p.add_argument("--signal", type=str, help="Approximant used for signal")
    p.add_argument("--template", type=str, help="Approximant to use for template")
    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")

    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")

    p.add_argument("--ell_max", type=int, help="Maximum ell to use", default=4)
    p.add_argument(
        "--min_type",
        type=str,
        help="Whether to minimize over reference  frequency or spin rotation. Applies only to Phenom waveforms",
        default="QC22mode_v4E",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )

    p.add_argument(
        "--omega_min",
        type=float,
        help="Starting **orbital** frequency of waveform generation, in geometric units",
    )

    p.add_argument(
        "--qmin",
        type=float,
        help="Mass ratio lower bound (q>1).",
        default=1.0,
    )

    p.add_argument(
        "--qmax",
        type=float,
        help="Mass ratio upper bound (q>1).",
        default=20.0,
    )

    p.add_argument(
        "--eccmin",
        type=float,
        help="Eccentricity lower bound.",
        default=0.0,
    )

    p.add_argument(
        "--eccmax",
        type=float,
        help="Eccentricity upper bound",
        default=0.2,
    )

    p.add_argument(
        "--chi_min",
        type=float,
        help="Dimensionless spin z component lower bound (0<= chi_min <= 1)",
        default=0.0,
    )

    p.add_argument(
        "--chi_max",
        type=float,
        help="Dimensionless spin z component upper bound (0<= chi_max <= 1)",
        default=1.0,
    )

    p.add_argument(
        "--Npoints",
        type=int,
        help="Number of iterations to perform for the mass ratio grid.",
        default=10,
    )

    p.add_argument(
        "--raw_grid",
        type=bool,
        help="Make a coarse grid unfaithfulness calculations before numerical optimization.",
        default=False,
    )

    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )

    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=5)
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")


    args = p.parse_args()
    signal = args.signal
    template = args.template
    omega_min = args.omega_min

    run_dir = os.path.abspath(args.run_dir)
    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)
    script_dir =  os.environ['WAVEFORM_TOOLS_PATH']

    NN=args.Npoints #Number of points over which t loop
    qmin=args.qmin
    qmax=args.qmax
    chimin=args.chi_min
    chimax=args.chi_max
    emin=args.eccmin
    emax=args.eccmax
    Nchunks = args.Nchunks

    seed=989809890

    np.random.seed(seed)
    q_list = np.random.uniform(qmin,qmax,NN)
    np.random.seed(seed+999998)
    chi1_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+999997)
    chi2_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+99999)
    ecc_list = np.random.uniform(emin,emax,NN)
    np.random.seed(seed+999)
    mean_ano_min = 0.0
    mean_ano_max = 2*np.pi
    mean_ano_list = np.random.uniform(mean_ano_min, mean_ano_max,NN)

    x, y1, y2, z, ww  = q_list, chi1_list, chi2_list, ecc_list, mean_ano_list

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()
    ww = ww.flatten()

    ########################################################3
    idx_list = np.arange(0,NN)

    n_slurm_jobs = math.ceil(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))


    header = """#!/bin/bash -
#SBATCH -J chunk_{}                 # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                 # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p nr                 # Queue name
#SBATCH -t {}           # Run time
#SBATCH --no-requeue


source /home/aramosbuades/load_LALOrbAvenv_v2.sh
source /scratch/aramosbuades/opt/lalSEOBNRv4E_devel/etc/lalsuite-user-env.sh

cd {}
"""

    for idx_chunk in range(n_slurm_jobs):

        case0=cases_per_slurm_job[idx_chunk]
        cases_per_output = list(chunks(case0,Nchunks))
        fp = open("chunk_{}.sh".format(idx_chunk), "w")

        fp.write(header.format(idx_chunk, idx_chunk, idx_chunk, args.submit_time, run_dir))
        for case in cases_per_output[0]:

            cmd = """python $WAVEFORM_TOOLS_PATH/compare_wfs_v4Eopt.py --signal {} --template {} --ell_max {} --q {} --chi1 {}  \
                 --chi2 {} --eccentricity {} --mean_anomaly {} --omega_min {} --min_type {} --unfaithfulness_type {}  \n""".format(
                    signal,
                    template,
                    args.ell_max,
                    x[case],
                    y1[case],
                    y2[case],
                    z[case],
                    ww[case],
                    omega_min,
                    args.min_type,
                    args.unfaithfulness_type,
                )
            fp.write(cmd)

        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(idx_chunk)
        print(submit_cmd)

        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
