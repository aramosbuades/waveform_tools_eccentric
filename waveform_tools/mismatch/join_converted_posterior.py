#!/usr/bin/env python
import argparse

import numpy as np
import json, os, glob



if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument(
        "--outdir",
        type=str,
        help="Directory where the converted posteriors are. Important in each directory it can only be 1 run!",
        default=os.getcwd(),
    )

    args = p.parse_args()

    # Join files and save it to a json file
    ew22_list = np.array([])
    meanAno22_list = np.array([])

    outdir = args.outdir

    if outdir[-1]!='/':
        outdir+='/'
        
    files = glob.glob(outdir+'*dat')

    for file0 in files:

        data = np.genfromtxt(file0,
                             dtype=None,)

        dataT = data.T
        ew22_i = dataT[0]
        meanAno_i = dataT[1]

        #ew22_list.append(list(ew22_i))
        meanAno22_list = np.append(meanAno22_list,meanAno_i)
        ew22_list = np.append(ew22_list,ew22_i)

    file0 = files[0]
    # Store optimal values
    res ={}
    res['ew22_posterior'] = list(ew22_list)
    res['meanAno22_posterior'] = list(meanAno22_list)

    outfile = file0.split('_result')[0]
    outfile +='_result_ew22meanAno22ref.json'

    with open(outfile, 'w') as f:
        json.dump(res, f)
