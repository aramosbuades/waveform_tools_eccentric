#!/usr/bin/env python
from __future__ import division, print_function
import lal
import lalsimulation as LS
import numpy as np
import scipy.interpolate as ip
import h5py
from scipy.integrate import cumtrapz
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import bisect, brentq


Msuninsec = lal.MSUN_SI * lal.G_SI / lal.C_SI ** 3.0  # Solar mass (secs)


def time_of_frequency(omega, t, omega_desired):
    """Find a time t_* such that omega(t_*)=omega_desired.
    Assumes that such a time exists and that omega is monotonic, so
    a unique solution can be found"""
    intrp = InterpolatedUnivariateSpline(t, omega)
    fc = lambda x: intrp(x) - omega_desired
    res = brentq(fc, t[0], t[-1], args=())
    return res


def vector_to_timeseries(vec):
    return vec.data.data[:] * 1


def orbital_frequency(t, x, y, z):
    """Direct copy from Generate_SEOBNRv3_FD_I_modes_mpi.py"""
    X = np.array([x, y, z]).T

    xdotI = ip.InterpolatedUnivariateSpline(t, x, k=3).derivative(1)
    ydotI = ip.InterpolatedUnivariateSpline(t, y, k=3).derivative(1)
    zdotI = ip.InterpolatedUnivariateSpline(t, z, k=3).derivative(1)
    Xdot = np.array([xdotI(t), ydotI(t), zdotI(t)]).T

    Omega = np.linalg.norm(np.cross(X, Xdot), axis=1) / np.linalg.norm(X, axis=1) ** 2

    return Omega


def get_seob_training_data(*args, **kwargs):
    """from _LS_timeout_wrapper. should be migrated back into
    Generate_SEOBNRv3_FD_I_modes_mpi.py"""

    if kwargs["EOB_version"] == "v3":
        ell_max = 2
        res = LS.SimIMRSpinEOBWaveformAll(*args)
    else:
        tmp = list(args)
        tmp_new = tmp[:-1]
        ar = LS.SimInspiralCreateModeArray()
        LS.SimInspiralModeArrayActivateMode(ar, 2, 2)
        LS.SimInspiralModeArrayActivateMode(ar, 2, 1)
        higher_modes = True
        if higher_modes:
            ell_max = 5
            LS.SimInspiralModeArrayActivateMode(ar, 3, 3)
            LS.SimInspiralModeArrayActivateMode(ar, 4, 4)
            LS.SimInspiralModeArrayActivateMode(ar, 5, 5)

        seobflags = lal.CreateDict()

        lal.DictInsertINT4Value(seobflags, "SEOBNRv4P_SpinAlignedEOBversion", 4)

        lal.DictInsertINT4Value(seobflags, "SEOBNRv4P_SymmetrizehPlminusm", 1)

        lal.DictInsertINT4Value(
            seobflags,
            "SEOBNRv4P_HamiltonianDerivative",
            LS.FLAG_SEOBNRv4P_HAMILTONIAN_DERIVATIVE_NUMERICAL,
        )

        lal.DictInsertINT4Value(
            seobflags,
            "SEOBNRv4P_euler_extension",
            LS.FLAG_SEOBNRv4P_EULEREXT_QNM_SIMPLE_PRECESSION,
        )

        lal.DictInsertINT4Value(
            seobflags, "SEOBNRv4P_Zframe", LS.FLAG_SEOBNRv4P_ZFRAME_L
        )

        lal.DictInsertINT4Value(seobflags, "SEOBNRv4P_debug", 0)

        print(tmp_new)
        tmp_new.append(ar)
        tmp_new.append(seobflags)

        args = tuple(tmp_new)
        res = LS.SimIMRSpinPrecEOBWaveformAll(*args)
        (
            hp,
            hc,
            hI,
            hJ,
            _,
            _,
            dyn,
            t_vec_modes,
            hP22_amp,
            hP22_phase,
            hP21_amp,
            hP21_phase,
            hP33_amp,
            hP33_phase,
            hP44_amp,
            hP44_phase,
            hP55_amp,
            hP55_phase,
            alphaJ2P,
            betaJ2P,
            gammaJ2P,
            merger_params,
        ) = res
    if kwargs["EOB_version"] == "v3":
        (
            phiC,
            deltaT,
            m1SI,
            m2SI,
            f_min,
            r,
            iota,
            S1x,
            S1y,
            S1z,
            S2x,
            S2y,
            S2z,
            PrecEOBversion,
        ) = args
        hp, hc, dyn, hP, hPHi, hJHi, hI, AttachPars = res
        (
            t_path,
            x,
            y,
            z,
            px,
            py,
            pz,
            s1x,
            s1y,
            s1z,
            s2x,
            s2y,
            s2z,
            phiDMod,
            phiMod,
        ) = np.split(dyn.data, 15)
    else:
        (
            phiC,
            deltaT,
            m1SI,
            m2SI,
            f_min,
            r,
            iota,
            S1x,
            S1y,
            S1z,
            S2x,
            S2y,
            S2z,
            _,
            _,
        ) = args
        (
            t_path,
            x,
            y,
            z,
            px,
            py,
            pz,
            s1x,
            s1y,
            s1z,
            s2x,
            s2y,
            s2z,
            phiDMod,
            phiMod,
            velx,
            vely,
            velz,
            polarr,
            polarphi,
            polarpr,
            polarphi,
            omega,
            s1proj,
            s2porj,
            ham,
        ) = np.split(dyn.data, 26)

    # return of inspiral dynamic quantities requires patch of SEOBNRv3 code

    Omega_prec = orbital_frequency(t_path, x, y, z)
    mtot = m1SI + m2SI
    mA = m1SI / mtot
    mB = 1 - mA
    # These are not the same as chi, we need to normalize
    chiA = np.vstack([s1x, s1y, s1z]).T
    chiB = np.vstack([s2x, s2y, s2z]).T
    chiA = chiA / mA ** 2
    chiB = chiB / mB ** 2
    # Inertial waveform modes
    # TODO: are these rh/M scaled?
    h_dict = {}
    for ell in range(2, ell_max + 1):
        for m in range(-ell_max, ell_max + 1):
            try:
                mode = LS.SphHarmTimeSeriesGetMode(hI, ell, m)
                mode = vector_to_timeseries(mode)
            except:
                continue
            h_dict["{},{}".format(ell, m)] = -1 * mode

    # generate inertial waveform DIMENSIONLESS time grid
    Mt = (m1SI + m2SI) / lal.MSUN_SI
    t_waveform = deltaT * np.arange(0, h_dict["2,2"].shape[0], 1) / (Msuninsec * Mt)

    # path
    eob_path = {"x": x, "y": y, "z": z}
    return [h_dict, t_waveform, eob_path, t_path, chiA, chiB]


def get_seob_track(eob_path, m1, m2):
    """We need a very crude 2-body path data to build the surrogate.

    Assume the eob_path is a newtonian 2-body position vector r = r1 - r2
    and use Newtonian formulas to get r1, r2
    (e.g. Theoretical Mechanics of Particles and Continua 4.3)"""

    M = m1 + m2
    r1 = np.array(
        [m2 / M * eob_path["x"], m2 / M * eob_path["y"], m2 / M * eob_path["z"]]
    )
    r2 = [-m1 / M * eob_path["x"], -m1 / M * eob_path["y"], -m1 / M * eob_path["z"]]

    return np.array(r1).T, np.array(r2).T


def write_horizons_h5_SEOBNR(chiA, chiB, xA, xB, t, case_id):
    """Write SEOB track data to a horizons.h5 file (spec format)

    t is a timeseries. ChiA, chiB are the spin timeseries.
    xA and xB are the trajectories"""

    t_array = t.reshape((t.shape[0], 1))
    # print t_array.shape
    # print xA.shape
    # print chiA.shape

    chiA_h5 = np.hstack([t_array, chiA])
    chiB_h5 = np.hstack([t_array, chiB])
    xA_h5 = np.hstack([t_array, xA])
    xB_h5 = np.hstack([t_array, xB])

    h5file = h5py.File("%s/Horizons.h5" % case_id, "w")

    # save hole A's data
    AhA = h5file.create_group("AhA.dir")
    AhA.create_dataset("chiInertial.dat", data=chiA_h5)
    AhA.create_dataset("CoordCenterInertial.dat", data=xA_h5)

    # save hole B's data
    AhB = h5file.create_group("AhB.dir")
    AhB.create_dataset("chiInertial.dat", data=chiB_h5)
    AhB.create_dataset("CoordCenterInertial.dat", data=xB_h5)

    h5file.close()


def write_rhOverM_h5_SEOBNR(t, h_modes, case_id, ell_max):
    """Write SEOB waveform data to an *.h5 file (spec format)

    a case_id is needed to distinguish training datasets."""
    t_array = t.reshape((t.shape[0], 1))

    h5file = h5py.File("%s/rhOverM_Asymptotic_GeometricUnits_CoM.h5" % case_id, "w")
    h_grp = h5file.create_group("Extrapolated_N2.dir")

    h22 = h_modes["2,2"]
    hre = np.real(h22)
    him = np.imag(h22)
    h_amp = np.sqrt(hre ** 2 + him ** 2)

    idx = np.where(h_amp >= 1e-12)[0]  # Cutoff everything which is very small
    # idx = range(len(h22))
    for L in range(2, ell_max + 1):
        for m in range(-L, L + 1):

            h_dataset_name = "Y_l%s_m%s.dat" % (L, m)
            # print(h_dataset_name)

            # if m >= 0:
            #    h_key = "hI%i%i" % (L, m)
            # else:
            #    h_key = "hI%im%i" % (L, -m)
            # print("hkey = %s"%h_key)
            h_key = "{},{}".format(L, m)
            try:
                h = h_modes[h_key]
            except KeyError:
                continue
            hre = np.real(h).reshape((h.shape[0], 1))
            him = np.imag(h).reshape((h.shape[0], 1))

            # print t_array.shape
            # print np.real(h).shape
            # print np.imag(h).shape
            h_data = np.hstack([t_array[idx], hre[idx], him[idx]])
            h_grp.create_dataset(h_dataset_name, data=h_data)

    h5file.close()


def write_metadata(
    m1, m2, Mtotal, s1x, s1y, s1z, s2x, s2y, s2z, omega, t_horizon, n_orbits, case_id
):
    """Write a fake metadata file"""
    # create metadata.text file with masses
    omega_v = omega * np.array([0.0, 0.0, 1.0])
    with open("%s/metadata.txt" % case_id, "w") as f:
        f.write("relaxed-mass1 = %1.15f\n" % (m1 / Mtotal))
        f.write("relaxed-mass2 = %1.15f\n" % (m2 / Mtotal))
        f.write("relaxed-dimensionless-spin1 = {}, {}, {}\n".format(s1x, s1y, s1z))
        f.write("relaxed-dimensionless-spin2 = {}, {}, {}\n".format(s2x, s2y, s2z))
        f.write("relaxed-eccentricity = {}\n".format(0.0))
        f.write(
            "relaxed-orbital-frequency = {}, {}, {}\n".format(
                omega_v[0], omega_v[1], omega_v[2]
            )
        )
        f.write("initial-ADM-energy = 1.0\n")
        f.write("initial-ADM-angular-momentum = 0.0, 0.0, 1.0\n")
        f.write("initial-ADM-linear-momentum = 0.0, 0.0, 0.0\n")
        f.write("common-horizon-time = {}\n".format(t_horizon))
        f.write("number-of-orbits = {}\n".format(n_orbits))
        f.write("alternative-names = {}\n".format(case_id))
        f.write("relaxed-measurement-time = {}\n".format(0.0))


def package_seob_training_data(
    phiC,
    deltaT,
    m1,
    m2,
    f_min,
    r,
    iota,
    s1x,
    s1y,
    s1z,
    s2x,
    s2y,
    s2z,
    PrecEOBversion,
    case_id,
    EOB_version="v3",
):
    """case_id: 4 digit id string (eg 0001)"""

    import os

    os.mkdir(case_id)

    # TODO: make sure SEOB waveforms are scaled by Mtotal
    Mtotal = float(m1 + m2)

    h_dict, t_waveform, eob_path, t_path, chiA, chiB = get_seob_training_data(
        phiC,
        deltaT,
        m1 * lal.MSUN_SI,
        m2 * lal.MSUN_SI,
        f_min,
        r,
        iota,
        s1x,
        s1y,
        s1z,
        s2x,
        s2y,
        s2z,
        PrecEOBversion,
        EOB_version=EOB_version,
    )

    omega = orbital_frequency(t_path, eob_path["x"], eob_path["y"], eob_path["z"])
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(omega)
    phase = cumtrapz(omega, t_path, initial=0)
    n_orbits = phase[-1] / (2 * np.pi)
    h22 = h_dict["2,2"]
    amp = abs(h22)
    idx_max = np.argmax(amp)
    t_max = t_waveform[idx_max]
    write_metadata(
        m1, m2, Mtotal, s1x, s1y, s1z, s2x, s2y, s2z, omega[0], t_max, n_orbits, case_id
    )

    r1, r2 = get_seob_track(eob_path, m1, m2)
    ell_max = 5
    write_horizons_h5_SEOBNR(chiA, chiB, r1, r2, t_path, case_id)
    write_rhOverM_h5_SEOBNR(t_waveform, h_dict, case_id, ell_max)
