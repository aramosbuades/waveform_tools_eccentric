import lal, lalsimulation, os, sys
import matplotlib.pyplot as plt, numpy as np, matplotlib
import pandas as pd
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from typing import Dict, Union

import matplotlib.pyplot as plt
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC

#%matplotlib notebook
import matplotlib as mpl
from matplotlib import cm
import random, numpy as np
import matplotlib.patches as mpatches
import h5py

import lalsimulation as lalsim
import pycbc
from pycbc.filter.matchedfilter import match
from pycbc.filter import make_frequency_series

import pycbc.filter as _filter


from scipy.signal import argrelextrema
LALMTSUNSI=4.925491025543575903411922162094833998e-6
LAL_MRSUN_SI=1.476625061404649406193430731479084713e3



sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from waveform_parameters import waveform_params
from auxillary_funcs import *

def compute_Amp22_phase22_omega22(h22, delta_t, M,dMpc):

    time_array = np.arange(0,len(h22)*delta_t, delta_t)

    amp22 = np.abs(h22)
    phi22 = - np.unwrap(np.angle(h22))

    ius = InterpolatedUnivariateSpline(time_array, phi22)

    dphidt=[]
    for tt in time_array:
        dphidt.append(ius.derivatives(tt)[1])

    omega22 = np.array(dphidt)

    timeM = SectotimeM(time_array, M)
    amp22NR = AmpPhysicaltoNRTD(amp22, M, dMpc)
    omega22NR = OmegaPhysicaltoNR(omega22,M)

    return timeM, amp22NR,  phi22,  omega22NR

def eNewtEstimate(f,fRef,eRef):
    return eRef*(f/fRef)**(-19.0/18.0)

def read_dynamicsEcc(datFile):

    datav4 =pd.read_table(datFile, sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timesDynv4=datav4[0]
    rDynv4=datav4[1]
    phiDynv4=datav4[2]
    prDynv4=datav4[3]
    pphiDynv4=datav4[4]
    omegaDynv4=datav4[5]
    FphiEcc=datav4[6]
    FrEcc=datav4[7]

    timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    return timesPhysv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc


def read_dynamicsv4(datFile):

    datav4 =pd.read_table(datFile, sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timesDynv4=datav4[0]
    rDynv4=datav4[1]
    phiDynv4=datav4[2]
    prDynv4=datav4[3]
    pphiDynv4=datav4[4]
    omegaDynv4=datav4[5]

    timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    return timesPhysv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4


LALMTSUNSI= 4.925491025543575903411922162094833998e-6
#(* Subscript[M, \[CircleDot]][s] = G/c^3Subscript[M, \[CircleDot]][kg] ~ 4.93 10^-6s is the geometrized solar mass in seconds. [LAL_MTSUN_SI] defined in  lal/src/std/LALConstants.h *)
LALCSI=299792458.0 # (* LAL_C_SI 299792458e0 /**< Speed of light in vacuo, m s^-1 *)
LALPCSI = 3.085677581491367e16 # (*  LAL_PC_SI 3.085677581491367278913937957796471611e16 /**< Parsec, m * *)

def MftoHz(Mf,M):
    return Mf/(M*LALMTSUNSI)

def HztoMf(Hz,M):
    return Hz*(M*LALMTSUNSI)

def AmpPhysicaltoNR(ampphysical, M, dMpc):
    return ampphysical*dMpc*1000000*LALPCSI/(LALCSI*( M*LALMTSUNSI)**2)

def AmpNRtoPhysical(ampNR, M, dMpc):
    return ampNR*(LALCSI*(M*LALMTSUNSI)**2)/(1000000*LALPCSI*dMpc)

def AmpPhysicaltoNRTD(ampphysical, M, dMpc):
    return ampphysical*dMpc*1000000*LALPCSI/(LALCSI*(M*LALMTSUNSI))

def AmpNRtoPhysicalTD(ampNR, M, dMpc):
    return ampNR*(LALCSI*( M*LALMTSUNSI))/(1000000*LALPCSI*dMpc)

def OmegaPhysicaltoNR(omegaPhys,M):
    return omegaPhys*M*LALMTSUNSI


def OmegaNRtoPhysical(omegaNR,M):
    return omegaNR/(M*LALMTSUNSI)

def SectotimeM(seconds, M):
    return seconds/(M*LALMTSUNSI)

def timeMtoSec(timeM, M):
    return timeM*M*LALMTSUNSI

def compute_freq(hlm,deltaT,M):
    freq = np.diff(np.unwrap(np.angle(hlm)))/(deltaT/(lal.MTSUN_SI*M))
    #if np.abs(hlm).all() == 0:
    #    freq = np.zeros(len(freq))
    return freq

def compute_freq_Dyn(phidyn,deltaT):
    freq = np.diff(phidyn)/(deltaT)
    #if np.abs(hlm).all() == 0:
    #    freq = np.zeros(len(freq))
    return freq

def read_RRForces(EccFphiPNorder, outdir):

    if EccFphiPNorder < 10:
        datav4 =pd.read_table(outdir+'saDynamicsEcc_Fphi0'+str(EccFphiPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')
    else:
        datav4 =pd.read_table(outdir+'saDynamicsEcc_Fphi'+str(EccFphiPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]

    timesDynv4 = np.array(pd.to_numeric(datav4[0],errors='coerce'))
    rDynv4 = np.array(pd.to_numeric(datav4[1],errors='coerce'))
    phiDynv4 = np.array(pd.to_numeric(datav4[2],errors='coerce'))
    prDynv4 = np.array(pd.to_numeric(datav4[3],errors='coerce'))
    pphiDynv4 = np.array(pd.to_numeric(datav4[4],errors='coerce'))
    omegaDynv4 = np.array(pd.to_numeric(datav4[5],errors='coerce'))

    FphiEcc = np.array(pd.to_numeric(datav4[6],errors='coerce'))
    FrEcc =np.array(pd.to_numeric(datav4[7],errors='coerce'))

    #timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    return np.array(timesDynv4), np.array(rDynv4), np.array(phiDynv4), np.array(prDynv4), np.array(pphiDynv4), np.array(omegaDynv4), np.array(FphiEcc), np.array(FrEcc)


def read_HiRRForces(EccFphiPNorder, outdir):
    if EccFphiPNorder < 10:
        datav4 =pd.read_table(outdir+'saDynamicsHiEcc_Fphi0'+str(EccFphiPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')
    else:
        datav4 =pd.read_table(outdir+'saDynamicsHiEcc_Fphi'+str(EccFphiPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timesDynv4 = np.array(pd.to_numeric(datav4[0],errors='coerce'))
    rDynv4 = np.array(pd.to_numeric(datav4[1],errors='coerce'))
    phiDynv4 = np.array(pd.to_numeric(datav4[2],errors='coerce'))
    prDynv4 = np.array(pd.to_numeric(datav4[3],errors='coerce'))
    pphiDynv4 = np.array(pd.to_numeric(datav4[4],errors='coerce'))
    omegaDynv4 = np.array(pd.to_numeric(datav4[5],errors='coerce'))

    FphiEcc = np.array(pd.to_numeric(datav4[6],errors='coerce'))
    FrEcc =np.array(pd.to_numeric(datav4[7],errors='coerce'))

    #timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    return np.array(timesDynv4), np.array(rDynv4), np.array(phiDynv4), np.array(prDynv4), np.array(pphiDynv4), np.array(omegaDynv4), np.array(FphiEcc), np.array(FrEcc)


def read_v4Dyn(outdir):

    datav4 =pd.read_table(outdir+'saDynamics.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timesDynv4 = np.array(pd.to_numeric(datav4[0],errors='coerce'))
    rDynv4 = np.array(pd.to_numeric(datav4[1],errors='coerce'))
    phiDynv4 = np.array(pd.to_numeric(datav4[2],errors='coerce'))
    prDynv4 = np.array(pd.to_numeric(datav4[3],errors='coerce'))
    pphiDynv4 = np.array(pd.to_numeric(datav4[4],errors='coerce'))
    omegaDynv4 = np.array(pd.to_numeric(datav4[5],errors='coerce'))

    #timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    return np.array(timesDynv4), np.array(rDynv4), np.array(phiDynv4), np.array(prDynv4), np.array(pphiDynv4), np.array(omegaDynv4)




def read_v4HiDyn(outdir):

    datav4 =pd.read_table(outdir+'saDynamicsHi.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timesDynv4 = np.array(pd.to_numeric(datav4[0],errors='coerce'))
    rDynv4 = np.array(pd.to_numeric(datav4[1],errors='coerce'))
    phiDynv4 = np.array(pd.to_numeric(datav4[2],errors='coerce'))
    prDynv4 = np.array(pd.to_numeric(datav4[3],errors='coerce'))
    pphiDynv4 = np.array(pd.to_numeric(datav4[4],errors='coerce'))
    omegaDynv4 = np.array(pd.to_numeric(datav4[5],errors='coerce'))

    #timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    return np.array(timesDynv4), np.array(rDynv4), np.array(phiDynv4), np.array(prDynv4), np.array(pphiDynv4), np.array(omegaDynv4)





def compute_freqv4(hlm,deltaT):
    freq = np.diff(np.unwrap(np.angle(hlm)))/(deltaT/(lal.MTSUN_SI*60))
    if np.abs(hlm).all() == 0:
        freq = np.zeros(len(freq))
    return freq



def generate_modes(q,Mtot,chi1,chi2):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    phi_c = 1.8607037379364015
    f_start22 = 28. #Frequency of the 22 mode at which the signal starts
    distance =20.8176905091852*lal.PC_SI
    deltaT = 1./16384.
    sphtseries, dyn, dynhi = lalsimulation.SimIMRSpinAlignedEOBModes(deltaT, m1, m2, f_start22, distance, chi1, chi2,41, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0)
    hlm = {}

    ##55 mode
    modeL = sphtseries.l
    modeM = sphtseries.m
    h55 = sphtseries.mode.data.data #This is h_55
    hlm[(modeL, modeM)] = h55

    ##44 mode
    modeL = sphtseries.next.l
    modeM = sphtseries.next.m
    h44 = sphtseries.next.mode.data.data #This is h_44
    hlm[(modeL, modeM)] = h44

    ##21 mode
    modeL = sphtseries.next.next.l
    modeM = sphtseries.next.next.m
    h21 = sphtseries.next.next.mode.data.data #This is h_21
    hlm[(modeL, modeM)] = h21

    ##33 mode
    modeL = sphtseries.next.next.next.l
    modeM = sphtseries.next.next.next.m
    h33 = sphtseries.next.next.next.mode.data.data #This is h_33
    hlm[(modeL, modeM)] = h33

    ##22 mode
    modeL = sphtseries.next.next.next.next.l
    modeM = sphtseries.next.next.next.next.m
    h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
    hlm[(modeL, modeM)] = h22

    ##time array (s)
    time_array = np.arange(0,len(h22)*deltaT,deltaT)

    return time_array, hlm


def generate_modesEcc_opt(q, Mtot, chi1, chi2, eccentricity, eccentric_anomaly, delta_t, f_min, dMpc, EccWaveformPNorder,
                      EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder,
                      EccBeta, Ecct0, EccNQCWaveform,EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    phi_c = 1.8607037379364015
    f_start22 = 28. #Frequency of the 22 mode at which the signal starts
    distance = dMpc*1e6*lal.PC_SI
    deltaT = 1./16384.
    HypPphi0, HypR0, HypE0 =[0.,0,0]

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    hlm = {}

    ##22 mode
    modeL = sphtseries.l
    modeM = sphtseries.m
    h22 = sphtseries.mode.data.data #This is h_22
    hlm[(modeL, modeM)] = h22

    ##time array (s)
    time_array = np.arange(0,len(h22)*deltaT,deltaT)

    return time_array, hlm



def generate_modesEcc(q, Mtot, chi1, chi2, eccentricity, delta_t, f_min, dMpc, EccWaveformPNorder,
                      EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder,
                      EccBeta, Ecct0, EccNQCWaveform):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    phi_c = 1.8607037379364015
    f_start22 = 28. #Frequency of the 22 mode at which the signal starts
    distance = dMpc*1e6*lal.PC_SI
    deltaT = 1./16384.

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform)

    hlm = {}

    ##22 mode
    modeL = sphtseries.l
    modeM = sphtseries.m
    h22 = sphtseries.mode.data.data #This is h_22
    hlm[(modeL, modeM)] = h22

    ##time array (s)
    time_array = np.arange(0,len(h22)*deltaT,deltaT)

    return time_array, hlm


def generate_RRForces(q, Mtot, chi1, chi2, eccentricity, eccentric_anomaly, delta_t, f_min, dMpc, EccWaveformPNorder,
                      EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder, EccBeta, Ecct0, EccNQCWaveform,EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC ,HypPphi0, HypR0, HypE0, outdir):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    phi_c = 1.8607037379364015
    f_start22 = 28. #Frequency of the 22 mode at which the signal starts
    distance = dMpc*1e6*lal.PC_SI
    deltaT = 1./16384.

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                          m1,
                                                          m2,
                                                          f_min,
                                                          distance,
                                                          chi1,
                                                          chi2,
                                                          eccentricity,
                                                          eccentric_anomaly,
                                                          4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                          EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                          EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0 )


    timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc =read_RRForces(EccFphiPNorder,
                                                                                                outdir)

    return  timesDynv4, FphiEcc, FrEcc

def generate_modesDynEcc_opt(q, Mtot, chi1, chi2, eccentricity, eccentric_anomaly,delta_t, f_min, dMpc, EccWaveformPNorder,
                        EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm,EccAvNQCWaveform,EcctAppend,EccIC, outdir):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    eta = m1*m2/(m1+m2)**2
    HypPphi0, HypR0, HypE0 = [0.,0.,0.]
    distance = dMpc*1e6*lal.PC_SI

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt1(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    ##22 mode
    h22 = sphtseries.mode.data.data #This is h_22
    amp22 = np.abs(h22)

    ##time array (s)
    time_array = np.arange(0,len(h22)*delta_t, delta_t)

    iAmax, Amax = np.argmax(amp22), np.max(amp22)
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units



    timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc =read_RRForces(EccFphiPNorder,
                                                                                                outdir)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    omegaOrbital = compute_deriv_f(tdyn, phidyn)
    omega22 = compute_freqInterp(timeNR, h22)


    amp22 = np.abs(h22)
    amp22NR = AmpPhysicaltoNRTD(amp22, Mtot, dMpc)
    phase22NR = np.unwrap(np.angle(AmpPhysicaltoNRTD(h22, Mtot, dMpc)))

    timesDynv4 += -timesDynv4[-1]
    h22NR = AmpPhysicaltoNRTD(h22, Mtot, dMpc)

    return  timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc, timeNR, h22NR, amp22NR, omega22, phase22NR


def generate_modesHiDynEcc_opt(q, Mtot, chi1, chi2, eccentricity, eccentric_anomaly, delta_t, f_min, dMpc, EccWaveformPNorder,
                           EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder, EccBeta, Ecct0, EccNQCWaveform,EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC, outdir):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    eta = m1*m2/(m1+m2)**2

    distance = dMpc*1e6*lal.PC_SI
    HypPphi0, HypR0, HypE0 =[0.,0,0]



    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt1(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    ##22 mode
    h22 = sphtseries.mode.data.data #This is h_22
    amp22 = np.abs(h22)


    ##time array (s)
    time_array = np.arange(0,len(h22)*delta_t, delta_t)

    iAmax, Amax = np.argmax(amp22), np.max(amp22)
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units



    timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc =read_HiRRForces(EccFphiPNorder,
                                                                                                  outdir)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    omegaOrbital = compute_freq_Dyn(phidyn,dtDyn)
    #omega22 = compute_freq(h22,delta_t,Mtot)
    omega22 = compute_freqInterp(timeNR, h22)

    amp22NR = AmpPhysicaltoNRTD(amp22, Mtot, dMpc)
    phase22NR = np.unwrap(np.angle(AmpPhysicaltoNRTD(h22, Mtot, dMpc)))


    #amp0 = Mtot * lal.MTSUN_SI / (dMpc*lal.PC_SI*1e6)
    #amp22NR= amp22/(amp0*eta)
    #amp22NR /= eta
    #timesDynv4 += -tAmax

    #timesDynv4 += -SectotimeM(tAmax, Mtot)
    timesDynv4 += -timesDynv4[-1]
    return  timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc, timeNR, h22, amp22NR, omega22, phase22NR


def generate_modesDynEcc(q, Mtot, chi1, chi2, eccentricity, delta_t, f_min, dMpc, EccWaveformPNorder,
                        EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder, EccBeta, Ecct0, EccNQCWaveform, outdir):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    eta = m1*m2/(m1+m2)**2

    distance = dMpc*1e6*lal.PC_SI

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform)

    ##22 mode
    h22 = sphtseries.mode.data.data #This is h_22
    amp22 = np.abs(h22)

    ##time array (s)
    time_array = np.arange(0,len(h22)*delta_t, delta_t)

    iAmax, Amax = np.argmax(amp22), np.max(amp22)
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units



    timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc =read_RRForces(EccFphiPNorder,
                                                                                                outdir)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    omegaOrbital = compute_deriv_f(tdyn, phidyn)
    omega22 = compute_freqInterp(timeNR, h22)


    amp22 = np.abs(h22)
    amp22NR = AmpPhysicaltoNRTD(amp22, Mtot, dMpc)
    phase22NR = np.unwrap(np.angle(AmpPhysicaltoNRTD(h22, Mtot, dMpc)))

    timesDynv4 += -timesDynv4[-1]

    return  timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc, timeNR, h22, amp22NR, omega22, phase22NR


def generate_modesHiDynEcc(q, Mtot, chi1, chi2, eccentricity, delta_t, f_min, dMpc, EccWaveformPNorder,
                           EccPNFactorizedForm, EccFphiPNorder, EccFrPNorder, EccBeta, Ecct0, EccNQCWaveform, outdir):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    eta = m1*m2/(m1+m2)**2

    distance = dMpc*1e6*lal.PC_SI
    EccIC = -1
    HypPphi0, HypR0, HypE0 = [-1,-1,-1]

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform)

    ##22 mode
    h22 = sphtseries.mode.data.data #This is h_22
    amp22 = np.abs(h22)






    ##time array (s)
    time_array = np.arange(0,len(h22)*delta_t, delta_t)

    iAmax, Amax = np.argmax(amp22), np.max(amp22)
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units



    timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc =read_HiRRForces(EccFphiPNorder,
                                                                                                  outdir)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    omegaOrbital = compute_freq_Dyn(phidyn,dtDyn)
    #omega22 = compute_freq(h22,delta_t,Mtot)
    omega22 = compute_freqInterp(timeNR, h22)

    amp22NR = AmpPhysicaltoNRTD(amp22, Mtot, dMpc)
    phase22NR = np.unwrap(np.angle(AmpPhysicaltoNRTD(h22, Mtot, dMpc)))


    #amp0 = Mtot * lal.MTSUN_SI / (dMpc*lal.PC_SI*1e6)
    #amp22NR= amp22/(amp0*eta)
    #amp22NR /= eta
    #timesDynv4 += -tAmax

    #timesDynv4 += -SectotimeM(tAmax, Mtot)
    timesDynv4 += -timesDynv4[-1]
    return  timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, FphiEcc, FrEcc, timeNR, h22, amp22NR, omega22, phase22NR


def generate_modesHiDyn(q, Mtot, chi1, chi2, delta_t, f_min, dMpc, outdir):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    distance = dMpc*1e6*lal.PC_SI
    eta = m1*m2/(m1+m2)**2

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0)

    ##22 mode
    h22 = sphtseries.mode.data.data #This is h_22
    amp22 = np.abs(h22)


    ##time array (s)
    time_array = np.arange(0,len(h22)*delta_t, delta_t)
    iAmax, Amax = np.argmax(amp22), np.max(amp22)
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units



    timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4 = read_v4HiDyn(outdir)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    omegaOrbital = compute_freq_Dyn(phidyn,dtDyn)
    #omega22 = compute_freq(h22, delta_t,Mtot)
    omega22 = compute_freqInterp(timeNR, h22)
    amp22 = np.abs(h22)
    amp22NR = AmpPhysicaltoNRTD(amp22, Mtot, dMpc)

    #amp0 = Mtot * lal.MTSUN_SI / (dMpc*lal.PC_SI*1e6)
    #amp22NR= amp22/(amp0*eta)
    #amp22NR /= eta

    #timesDynv4 += -SectotimeM(tAmax, Mtot)
    timesDynv4 += -timesDynv4[-1]
    #print(tAmax)
    return  timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, timeNR, h22, amp22NR, omega22




def compute_freqhLM(hlm,deltaT):
    freq = np.diff(np.unwrap(np.angle(hlm)))/(deltaT)
    #if np.abs(hlm).all() == 0:
    #    freq = np.zeros(len(freq))
    return freq


def read_HiEccModes(EccWaveformPNorder, outdir):

    if EccWaveformPNorder < 10:

        datav4 =pd.read_table(outdir+'saModes22HiNQC_EccPN0'+str(EccWaveformPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    else:

        datav4 =pd.read_table(outdir+'saModes22HiNQC_EccPN'+str(EccWaveformPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timeHi = np.array(datav4[0] )
    rehLM = np.array(datav4[1] )
    imhLM = np.array(datav4[2] )

    rehNQC = np.array(datav4[3])
    imhNQC = np.array(datav4[4] )

    rehEcc = np.array(datav4[5])
    imhEcc = np.array(datav4[6])

    #timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    hLM = rehLM - 1.j * imhLM
    amphLM = np.abs( hLM )

    hNQC = rehNQC  - 1.j * imhNQC
    amphNQC = np.abs(hNQC)

    hEcc = rehEcc  - 1.j * imhEcc
    amphEcc = np.abs(hEcc)

    dt= timeHi[2]- timeHi[1]
    omegahLM  = compute_freqhLM(hLM ,dt)
    omegahEcc  = compute_freqhLM(hEcc ,dt)
    omegahNQC  = compute_freqhLM(hNQC ,dt)

    phasehLM  = np.unwrap(np.angle(hLM))
    phasehEcc  = np.unwrap(np.angle(hEcc))
    phasehNQC  = np.unwrap(np.angle(hNQC))

    imax=np.argmax(omegahLM)
    tmax=timeHi[imax]
    #print(tmax)
    #timeHi -= timeHi[-1]
    timeHi -= tmax

    return timeHi, rehLM, imhLM, rehNQC, imhNQC, rehEcc, imhEcc, amphLM, amphNQC, amphEcc, omegahLM, omegahEcc, omegahNQC, phasehLM, phasehEcc, phasehNQC




def read_EccModes(EccWaveformPNorder, outdir):

    if EccWaveformPNorder < 10:

        datav4 =pd.read_table(outdir+'saModesEcc_wfPN0'+str(EccWaveformPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    else:
        datav4 =pd.read_table(outdir+'saModesEcc_wfPN'+str(EccWaveformPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')

    # timeHi.data[i],rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i], omegaHi->data[i]
    timeHi = np.array(datav4[0])
    rehLM = np.array(datav4[1])
    imhLM = np.array(datav4[2])

    rehNQC = np.array(datav4[3])
    imhNQC = np.array(datav4[4])

    rehEcc = np.array(datav4[5])
    imhEcc = np.array(datav4[6])

    #timesPhysv4=timesDynv4*mtotal*LALMTSUNSI

    hLM = rehLM - 1.j * imhLM
    amphLM = np.abs( hLM )

    hNQC = rehNQC  - 1.j * imhNQC
    amphNQC = np.abs(hNQC)

    hEcc = rehEcc  - 1.j * imhEcc
    amphEcc = np.abs(hEcc)

    dt= timeHi[2]- timeHi[1]
    omegahLM  = compute_freqInterp(timeHi, hLM)
    omegahEcc  = compute_freqInterp(timeHi, hEcc)
    omegahNQC  = compute_freqInterp(timeHi, hNQC)

    phasehLM  = np.unwrap(np.angle(hLM))
    phasehEcc  = np.unwrap(np.angle(hEcc))
    phasehNQC  = np.unwrap(np.angle(hNQC))

    imax=np.argmax(omegahLM)
    tmax=timeHi[imax]
    #print(tmax)
    #timeHi -= timeHi[-1]
    timeHi -= tmax

    return timeHi, rehLM, imhLM, rehNQC, imhNQC, rehEcc, imhEcc, amphLM, amphNQC, amphEcc, omegahLM, omegahEcc, omegahNQC, phasehLM, phasehEcc, phasehNQC


def generate_modesDyn(q, Mtot, chi1, chi2, delta_t, f_min, dMpc):
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1+q)
    m2 = M/(1+q)
    distance = dMpc*1e6*lal.PC_SI
    eta = m1*m2/(m1+m2)**2

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0)

    ##22 mode
    h22 = sphtseries.mode.data.data #This is h_22
    amp22 = np.abs(h22)


    ##time array (s)
    time_array = np.arange(0,len(h22)*delta_t, delta_t)
    iAmax, Amax = np.argmax(amp22), np.max(amp22)
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units


    #timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4 = read_v4Dyn(outdir)

    lenDyn = int(dyn.length/5)

    tdyn = dyn.data[ 0 : lenDyn]
    dtDyn = tdyn[1] - tdyn[0]

    rdyn = dyn.data[ lenDyn: 2* lenDyn]
    phidyn = dyn.data[ 2* lenDyn: 3* lenDyn]
    prdyn = dyn.data[ 3* lenDyn: 4* lenDyn]
    pphidyn = dyn.data[ 4* lenDyn: 5* lenDyn]

    #omegaOrbital = compute_freq_Dyn(phidyn,dtDyn)
    omegaOrbital = compute_deriv_f(tdyn, phidyn)

    omega22 = compute_freqInterp(timeNR, h22)
    amp22 = np.abs(h22)
    amp22NR = AmpPhysicaltoNRTD(amp22, Mtot, dMpc)

    #print(-SectotimeM(tAmax, Mtot))
    #timesDynv4 += -SectotimeM(tAmax, Mtot)
    #timesDynv4 += - timesDynv4[-1]
    tdyn += - tdyn[-1]

    #print(tAmax)
    #return  timesDynv4, rDynv4, phiDynv4, prDynv4, pphiDynv4, omegaDynv4, timeNR, h22, amp22NR, omega22
    return  tdyn, rdyn, phidyn, prdyn, pphidyn, omegaOrbital, timeNR, h22, amp22NR, omega22


from scipy.interpolate import Rbf, InterpolatedUnivariateSpline


def compute_freqInterp(time, hlm):

    philm=np.unwrap(np.angle(hlm))

    intrp = InterpolatedUnivariateSpline(time,philm)
    omegalm = intrp.derivative()(time)

    return omegalm



def compute_deriv_f(time, f):

    intrp = InterpolatedUnivariateSpline(time,f)
    deriv = intrp.derivative()(time)

    return deriv

def compute_phase_diff(timeNR, phaseNR, timev4E,phasev4E, align_start=True):


    dtv4E = timev4E[-1]- timev4E[-2]
    dtNR = timeNR[-1]- timeNR[-2]
    dt = min(dtv4E,dtNR)


    t0NR = timeNR[0]
    t0v4E = timev4E[0]

    tmin = max(t0NR,t0v4E)
    tmax = min(timev4E[-1],timeNR[-1])

    timeCommon = np.arange(tmin,tmax,dt)

    if t0NR >t0v4E: # NR longer than v4E

        indt0 = np.argwhere(timeNR > tmin).flatten()
        tMod = timeNR[timeNR > tmin]
        phaseMod = phaseNR[indt0]
        iphiNR = InterpolatedUnivariateSpline(tMod, phaseMod)
        iphiv4E = InterpolatedUnivariateSpline(timev4E, phasev4E)

    else:

        indt0 = np.argwhere(timev4E > tmin).flatten()
        phaseMod = phasev4E[indt0]
        tMod = timev4E[timev4E > tmin]

        iphiv4E = InterpolatedUnivariateSpline(tMod, phaseMod)
        iphiNR = InterpolatedUnivariateSpline(timeNR, phaseNR)


    if align_start == True:
        delta_phi0 = iphiNR(tmin) + iphiv4E(tmin)

    else:
        tAlign = 0
        delta_phi0 = iphiNR(tAlign) + iphiv4E(tAlign)


    phidiff = iphiNR(timeCommon)+iphiv4E(timeCommon)-delta_phi0

    return  timeCommon, phidiff

# Compute local maxima and minima from a certain quantity

def compute_MaxMin(tHorizon, omega_orb):

    # for local maxima
    maxima = argrelextrema(omega_orb, np.greater) # Use A22 mode as it is cleaner than the frequency
    omega_orb_maxima = omega_orb[maxima]
    tH_maxima = tHorizon[maxima]

    # for local minima
    minima = argrelextrema(omega_orb, np.less)  # Use A22 mode as it is cleaner than the frequency

    omega_orb_minima =omega_orb[minima]
    tH_minima = tHorizon[minima]


    return tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima


    # Compute local extrema from the amplitude and get the values of the frequency at these extrema


def compute_MaxMin22(timeNR, omega22, amp22):

    # for local maxima
    amp22 = amp22[timeNR<-50] # Avoid picking the peak of the amplitude

    maxima = argrelextrema(amp22, np.greater) # Use A22 mode as it is cleaner than the frequency
    omega_maxima = omega22[maxima]
    tNR_maxima = timeNR[maxima]

    # for local minima
    minima = argrelextrema(amp22, np.less)  # Use A22 mode as it is cleaner than the frequency

    omega_minima =omega22[minima]
    tNR_minima = timeNR[minima]

    return tNR_maxima, omega_maxima, tNR_minima, omega_minima



def compute_h22OmegaOrbfromNRfile(NR_file, ModeList):

    hlm = {}
    amplm = {}
    phaselm = {}
    omegalm = {}
    tNRomegalm = {}

    fp = h5py.File(NR_file, "r")

    t_omega_orb = fp['Omega-vs-time/X'][:]
    tHorizon = fp['HorizonBTimes'][:]
    omega_orb = fp['Omega-vs-time/Y'][:]
    iOmega_orb = InterpolatedUnivariateSpline(t_omega_orb, omega_orb)


    for l,m in ModeList:

        Alm = fp['amp_l'+str(l)+'_m'+str(m)+'/Y'][:]
        tAlm =fp['amp_l'+str(l)+'_m'+str(m)+'/X'][:]

        philm = fp['phase_l'+str(l)+'_m'+str(m)+'/Y'][:]
        tphilm =fp['phase_l'+str(l)+'_m'+str(m)+'/X'][:]

        tNR = fp['NRtimes'][:]
        iAlm = InterpolatedUnivariateSpline(tAlm,Alm)
        iphilm = InterpolatedUnivariateSpline(tphilm,-philm)

        amplm[l,m] = iAlm(tNR)
        phaselm[l,m] = iphilm(tNR)
        omegalm[l,m] = iphilm.derivative()(tNR)
        hlm[l,m] = amplm[l,m] * np.exp(1.j*phaselm[l,m])

    fp.close()

    om_orb = iOmega_orb(tHorizon)


    phi =  []

    for time in tHorizon:
        phi.append(iOmega_orb.integral(tHorizon[0],time))

    phase_orb = np.array(phi)

    return tNR, tHorizon, hlm, amplm, phaselm, omegalm, om_orb, phase_orb





def compute_OmegaOrbfromNRfile(NR_file, ModeList):



    fp = h5py.File(NR_file, "r")

    t_omega_orb = fp['Omega-vs-time/X'][:]
    tHorizon = fp['HorizonBTimes'][:]
    omega_orb = fp['Omega-vs-time/Y'][:]
    iOmega_orb = InterpolatedUnivariateSpline(t_omega_orb, omega_orb)

    fp.close()

    om_orb = iOmega_orb(tHorizon)


    phi =  []

    for time in tHorizon:
        phi.append(iOmega_orb.integral(tHorizon[0],time))

    phase_orb = np.array(phi)

    return tHorizon, om_orb, phase_orb



# Cross check with pycbc
# Make a loop for a quick check

def Params(filepath, mtotal):
    f = h5py.File(filepath, 'r')
    mass1 = f.attrs['mass1']
    mass2 = f.attrs['mass2']

    #print('freq_1M = ', f.attrs['f_lower_at_1MSUN'])

    f_lower = f.attrs['f_lower_at_1MSUN']/mtotal  # this generates the whole NR waveforms
    fRef=f_lower

    # The NR spins need to be transformed into the lal frame:
    spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(0, mtotal, filepath)

    spin1x = spins[0]
    spin1y = spins[1]
    spin1z = spins[2]
    spin2x = spins[3]
    spin2y = spins[4]
    spin2z = spins[5]

    f.close()

    return mass1, mass2, spin1z, spin2z, f_lower


def MatchFD(filepath, mtotal, e0,  distance, inclination, phiRef, Omega, f_ref_template, f_max, deltaF, maxoverphiref, debug, approximant, LAL_params_template):

    # Metadata parameters:
    mass1, mass2, spin1z, spin2z, f_lower = Params(filepath, mtotal)

    params = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertNumRelData(params, filepath)

    if approximant==lalsim.IMRPhenomXHM:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[3,2],[3,-2],[4,4],[4,-4]]
    elif approximant==lalsim.SEOBNRv4HM:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[4,4],[4,-4],[5,5],[5,-5]]
    elif approximant==lalsim.NRHybSur3dq8:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[3,2],[3,-2],[4,4],[4,-4],[4,3],[4,-3],[5,5],[5,-5]]
    elif approximant==lalsim.SEOBNRv4P:
        ma=[[2,2]]
    elif approximant==lalsim.IMRPhenomTHM:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[4,4],[4,-4],[5,5],[5,-5]]
    else:
        ma=[[2,2],[2,-2]]

    spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(0, mtotal,filepath)
    s1x = spins[0]
    s1y = spins[1]
    s1z = spins[2]
    s2x = spins[3]
    s2y = spins[4]
    s2z = spins[5]

    m1SI = mass1*mtotal*lal.MSUN_SI
    m2SI = mass2*mtotal*lal.MSUN_SI
    distance = 100.*1.0e6*lal.PC_SI
    f_min=1.01*f_lower
    f_ref=1.01*f_lower

    ModeArray = lalsim.SimInspiralCreateModeArray()
    for mode in ma:
        lalsim.SimInspiralModeArrayActivateMode(ModeArray, mode[0], mode[1])
    lalsim.SimInspiralWaveformParamsInsertModeArray(params, ModeArray)


    # NR in Fourier Domain
    buffer_factor = 1.  # required due to waveform conditioning
    inspiralFDparams = {
        'm1': m1SI, 'm2': m2SI,
        'S1x': s1x, 'S1y': s1y, 'S1z': s1z,
        'S2x': s2x, 'S2y': s2y, 'S2z': s2z,
        'distance': distance, 'inclination': inclination,
        'phiRef': phiRef, 'longAscNodes': Omega,
        'meanPerAno': 0, 'eccentricity': 0,
        'deltaF': deltaF, 'f_min': f_min, 'f_max': f_max, 'f_ref': f_ref,
        'LALparams': params, 'approximant': lalsim.NR_hdf5
    };  # This functions returns a conditioned frequency domain version of a time domain waveform
    # NOTE: due to the conditioning, the starting frequency needs to be increased for the conversion to work
    sp, sc = lalsim.SimInspiralFD(**inspiralFDparams)  # Frequency array

    hp, hc = lalsim.SimInspiralFD(m1=m1SI,
                                                    m2=m2SI,
                                                    S1x=0, S1y=0, S1z=s1z,
                                                    S2x=0, S2y=0, S2z=s2z,
                                                    distance=distance,
                                                    inclination=inclination,
                                                    LALparams= LAL_params_template,
                                                    phiRef=phiRef,
                                                    f_ref=f_ref_template,
                                                    deltaF=sp.deltaF,
                                                    f_min=f_ref_template,
                                                    f_max = f_max,
                                                    longAscNodes=Omega,
                                                    eccentricity=e0,
                                                    meanPerAno=0.0,
                                                    approximant=approximant)

    spy  = pycbc.types.frequencyseries.FrequencySeries(sp.data.data, sp.deltaF,  epoch='', dtype=complex, copy=True)
    scy  = pycbc.types.frequencyseries.FrequencySeries(sc.data.data, sc.deltaF,  epoch='', dtype=complex, copy=True)
    hpy  = pycbc.types.frequencyseries.FrequencySeries(hp.data.data, hp.deltaF,  epoch='', dtype=complex, copy=True)
    hcy  = pycbc.types.frequencyseries.FrequencySeries(hc.data.data, hc.deltaF,  epoch='', dtype=complex, copy=True)

    flen = len(sp.data.data)

    # Generate the aLIGO ZDHP PSD
    psd = pycbc.psd.aLIGOZeroDetHighPower(flen, sp.deltaF, 1.2*f_min)

    HPmatch, pos = match(spy, hpy, psd=psd, low_frequency_cutoff=1.2*f_min)
    HCmatch, pos = match(scy, hcy, psd=psd, low_frequency_cutoff=1.2*f_min)


    # Note: This takes a while the first time as an FFT plan is generated
    # subsequent calls are much faster.
    mpmax, mcmax = [0, 0]
    if maxoverphiref !=0:
        for phiRef in np.arange(0,2*np.pi,2*np.pi/maxoverphiref):
            hp, hc = lalsim.SimInspiralFD(m1=m1SI,
                                                    m2=m2SI,
                                                    S1x=0, S1y=0, S1z=s1z,
                                                    S2x=0, S2y=0, S2z=s2z,
                                                    distance=distance,
                                                    inclination=inclination,
                                                    LALparams=params,
                                                    phiRef=phiRef,
                                                    f_ref=f_ref_template,
                                                    deltaF=sp.deltaF,
                                                    f_min=f_ref_template,
                                                    f_max = f_max,
                                                    longAscNodes=Omega,
                                                    eccentricity=e0,
                                                    meanPerAno=0.0,
                                                    approximant=approximant)
            hpy  = pycbc.types.frequencyseries.FrequencySeries(hp.data.data, hp.deltaF,  epoch='', dtype=complex, copy=True)
            hcy  = pycbc.types.frequencyseries.FrequencySeries(hc.data.data, hc.deltaF,  epoch='', dtype=complex, copy=True)
            hpy.resize(flen)
            hcy.resize(flen)


            mp, i = match(spy, hpy, psd=psd, low_frequency_cutoff=1.2*f_min, high_frequency_cutoff=2048)
            mc, i = match(scy, hcy, psd=psd, low_frequency_cutoff=1.2*f_min, high_frequency_cutoff=2048)

            if mp > mpmax:
                mpmax = mp
            if mc > mcmax:
                mcmax = mc

            if debug:
                freqs  = sp.get_sample_frequencies()
                freqs2 = hp.get_sample_frequencies()
                plt.plot(freqs, sp)
                plt.plot(freqs2, hp)
                plt.show()
                print(1-mp, 1-mc)
    else:
        mpmax, i = match(spy, hpy, psd=psd, low_frequency_cutoff=1.2*f_min)
        mcmax, i = match(scy, hcy, psd=psd, low_frequency_cutoff=1.2*f_min)

    return 1-mpmax, 1-mcmax


def MatchQCFD(filepath, mtotal, e0,  dMpc, inclination, phiRef, Omega, f_max, deltaF, maxoverphiref, debug, approximant):

    # Metadata parameters:
    mass1, mass2, spin1z, spin2z, f_lower = Params(filepath, mtotal)

    params = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertNumRelData(params, filepath)

    if approximant==lalsim.IMRPhenomXHM:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[3,2],[3,-2],[4,4],[4,-4]]
    elif approximant==lalsim.SEOBNRv4HM:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[4,4],[4,-4],[5,5],[5,-5]]
    elif approximant==lalsim.NRHybSur3dq8:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[3,2],[3,-2],[4,4],[4,-4],[4,3],[4,-3],[5,5],[5,-5]]
    elif approximant==lalsim.SEOBNRv4P:
        ma=[[2,2],[2,-2]]
    elif approximant==lalsim.IMRPhenomTHM:
        ma=[[2,2],[2,-2],[2,1],[2,-1],[3,3],[3,-3],[4,4],[4,-4],[5,5],[5,-5]]
    else:
        ma=[[2,2],[2,-2]]

    spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(0, mtotal,filepath)
    s1x = spins[0]
    s1y = spins[1]
    s1z = spins[2]
    s2x = spins[3]
    s2y = spins[4]
    s2z = spins[5]

    m1SI = np.round(mass1,3)*mtotal*lal.MSUN_SI
    m2SI = np.round(mass2,3)*mtotal*lal.MSUN_SI
    distance = dMpc*1.0e6*lal.PC_SI
    f_min=f_lower
    f_ref=f_lower

    ModeArray = lalsim.SimInspiralCreateModeArray()
    for mode in ma:
        lalsim.SimInspiralModeArrayActivateMode(ModeArray, mode[0], mode[1])
    lalsim.SimInspiralWaveformParamsInsertModeArray(params, ModeArray)

    #print("f_min = ", f_min)
    # NR in Fourier Domain
    buffer_factor = 1.  # required due to waveform conditioning
    inspiralFDparams = {
        'm1': m1SI, 'm2': m2SI,
        'S1x': s1x, 'S1y': s1y, 'S1z': s1z,
        'S2x': s2x, 'S2y': s2y, 'S2z': s2z,
        'distance': distance, 'inclination': inclination,
        'phiRef': phiRef, 'longAscNodes': Omega,
        'meanPerAno': 0, 'eccentricity': 0,
        'deltaF': deltaF, 'f_min': f_min, 'f_max': f_max, 'f_ref': f_ref,
        'LALparams': params, 'approximant': lalsim.NR_hdf5
    };  # This functions returns a conditioned frequency domain version of a time domain waveform
    # NOTE: due to the conditioning, the starting frequency needs to be increased for the conversion to work
    sp, sc = lalsim.SimInspiralFD(**inspiralFDparams)  # Frequency array
    #print(inspiralFDparams)
    hp, hc = lalsim.SimInspiralFD(m1=m1SI,
                                                    m2=m2SI,
                                                    S1x=0, S1y=0, S1z=s1z,
                                                    S2x=0, S2y=0, S2z=s2z,
                                                    distance=distance,
                                                    inclination=inclination,
                                                    LALparams=params,
                                                    phiRef=phiRef,
                                                    f_ref=f_ref,
                                                    deltaF=sp.deltaF,
                                                    f_min=f_min,
                                                    f_max = f_max,
                                                    longAscNodes=Omega,
                                                    eccentricity=e0,
                                                    meanPerAno=0.0,
                                                    approximant=approximant)

    spy  = pycbc.types.frequencyseries.FrequencySeries(sp.data.data, sp.deltaF,  epoch='', dtype=complex, copy=True)
    scy  = pycbc.types.frequencyseries.FrequencySeries(sc.data.data, sc.deltaF,  epoch='', dtype=complex, copy=True)
    hpy  = pycbc.types.frequencyseries.FrequencySeries(hp.data.data, hp.deltaF,  epoch='', dtype=complex, copy=True)
    hcy  = pycbc.types.frequencyseries.FrequencySeries(hc.data.data, hc.deltaF,  epoch='', dtype=complex, copy=True)

    flen = len(sp.data.data)

    # Generate the aLIGO ZDHP PSD
    psd = pycbc.psd.aLIGOZeroDetHighPower(flen, sp.deltaF, 2.*f_min)

    HPmatch, pos = match(spy, hpy, psd=psd, low_frequency_cutoff=2.*f_min)
    HCmatch, pos = match(scy, hcy, psd=psd, low_frequency_cutoff=2.*f_min)


    # Note: This takes a while the first time as an FFT plan is generated
    # subsequent calls are much faster.
    mpmax, mcmax = [0, 0]
    if maxoverphiref !=0:
        for phiRef in np.arange(0,2*np.pi,2*np.pi/maxoverphiref):
            hp, hc = lalsim.SimInspiralFD(m1=m1SI,
                                                    m2=m2SI,
                                                    S1x=0, S1y=0, S1z=s1z,
                                                    S2x=0, S2y=0, S2z=s2z,
                                                    distance=distance,
                                                    inclination=inclination,
                                                    LALparams=params,
                                                    phiRef=phiRef,
                                                    f_ref=f_ref,
                                                    deltaF=sp.deltaF,
                                                    f_min= f_min,
                                                    f_max = f_max,
                                                    longAscNodes=Omega,
                                                    eccentricity=e0,
                                                    meanPerAno=0.0,
                                                    approximant=approximant)
            hpy  = pycbc.types.frequencyseries.FrequencySeries(hp.data.data, hp.deltaF,  epoch='', dtype=complex, copy=True)
            hcy  = pycbc.types.frequencyseries.FrequencySeries(hc.data.data, hc.deltaF,  epoch='', dtype=complex, copy=True)
            hpy.resize(flen)
            hcy.resize(flen)


            mp, i = match(spy, hpy, psd=psd, low_frequency_cutoff=2.*f_min, high_frequency_cutoff=2048)
            mc, i = match(scy, hcy, psd=psd, low_frequency_cutoff=2.*f_min, high_frequency_cutoff=2048)

            if mp > mpmax:
                mpmax = mp
            if mc > mcmax:
                mcmax = mc

            if debug:
                freqs  = sp.get_sample_frequencies()
                freqs2 = hp.get_sample_frequencies()
                plt.plot(freqs, sp)
                plt.plot(freqs2, hp)
                plt.show()
                print(1-mp, 1-mc)
    else:
        mpmax, i = match(spy, hpy, psd=psd, low_frequency_cutoff=2.*f_min)
        mcmax, i = match(scy, hcy, psd=psd, low_frequency_cutoff=2.*f_min)

    return 1-mpmax, 1-mcmax





def unfaithfulness_RC_FD(
    s_arg: pycbc.types.frequencyseries.FrequencySeries,
    inspiralFDparams: dict,
    flow: float = 10.0,
    fhigh: float = None,
    debug: bool = False,
) -> Union[float, tuple]:
    """This function returns the overlap maximized over the effective polarization
    for given value of the phi.
    See https://arxiv.org/pdf/1803.10701.pdf and https://arxiv.org/pdf/1709.09181.pdf

    Args:
        s_arg (pt.TimeSeries): The signal in the detector
        params_template (waveform_params): The template parameters
        flow (float, optional): Low frequency cutoff. Defaults to 10.0.
        fhigh (float, optional): High frequency cutoff. Defaults to None.
        modes_dict (Dict, optional): Dictionary of modes. To be used for EOB. Defaults to None.
        debug (bool, optional): Outout debug information. Defaults to False.

    Returns:
        float: The unfaithfulness
    """
    s = s_arg.copy()

    hp_simFD, hc_simFD = lalsim.SimInspiralFD(**inspiralFDparams)  # Frequency array
    print(inspiralFDparams)
    #hp_tilde = FrequencySeries(hp_simFD.data.data, delta_f=inspiralFDparams['deltaF'])
    #hc_tilde = FrequencySeries(hc_simFD.data.data, delta_f=inspiralFDparams['deltaF'])



    hp_tilde  = pycbc.types.frequencyseries.FrequencySeries(hp_simFD.data.data, hp_simFD.deltaF,  epoch='', dtype=complex, copy=True)
    hc_tilde  = pycbc.types.frequencyseries.FrequencySeries(hc_simFD.data.data, hc_simFD.deltaF,  epoch='', dtype=complex, copy=True)

    deltaF = hp_simFD.deltaF

#s = sp_pycbc * np.cos(kappa_s) + sc_pycbc * np.sin(kappa_s)

    #s_tilde = make_frequency_series(s)
    s_tilde = s
   # Sanity check
    assert len(s_tilde) == len(
        hp_tilde
    ), "The length of template and signal don't match!, {} and {} ".format(
        len(s_tilde),
        len(hp_tilde)
        # , pad, len(hp_td), len(s)
    )



    psd_a = aLIGOZeroDetHighPowerGWINC(len(s_tilde), deltaF, flow)
    #generate_psd(len(s_tilde), deltaF, flow)
    # For debugging only
    # print("pycbc mismatch = {}".format(1-match(s_tilde,hp_tilde,psd=psd_a,low_frequency_cutoff=flow,high_frequency_cutoff=fhigh)[0]))
    # Norm of h_{+}
    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of h_{x}
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of s, |s|
    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    # Normalized templates
    hp_hat = hp_tilde / A_p
    hc_hat = hc_tilde / A_c

    # Cross term, i.e. (h_{+}|h_{x})
    # Notice that normalized=False, because we do our own normalization
    Ipc = _filter.overlap(
        hp_hat,
        hc_hat,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    # Compute the complex SNRs
    # Again, sigmasq=1, don't normalize!
    rho_p_hat = _filter.matched_filter(
        hp_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    rho_c_hat = _filter.matched_filter(
        hc_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )
    rho_p_hat = rho_p_hat.real()
    rho_c_hat = rho_c_hat.real()
    rho_p_hat = rho_p_hat.numpy()
    rho_c_hat = rho_c_hat.numpy()

    num = rho_p_hat ** 2 + rho_c_hat ** 2 - 2 * rho_p_hat * rho_c_hat * Ipc
    denum = 1 - Ipc ** 2
    overlap = np.sqrt(num / denum) * 1 / N_1
    overlap = np.max(overlap)
    #print("mm = {}".format(1 - overlap))


    return 1 - overlap



# Make a python wrapper function to generate the 22-mode and orbital phase

def SEOBNRv4E_modes(q: float, chi1: float,chi2: float, eccentricity: float,  omega_min: float, delta_t: float,
                    EccFphiPNorder: int, EccFrPNorder: int,  EccWaveformPNorder: int, EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int
                    ,EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int,EcctAppend: float,EccIC: int, align_peak: bool= True):

    # omega_min refers to the initial orbital frequency
    freq_1M = omega_min / (lal.MTSUN_SI) / np.pi

    dMpc=100. # It does not matter as in the end we are removing the scaling by the luminosity distance
    distance = dMpc*1e6*lal.PC_SI
    Mtot=70. # It does not matter as in the end we are removing the scaling by total mass
    m1 = Mtot*q/(1.+q)
    m2 = Mtot/(1.+q)
    f_min = freq_1M / (m1+m2)

    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    HypPphi0, HypR0, HypE0 =[0.,0,0]
    eccentric_anomaly = 0.0
    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    hlm[2,-2] = np.conjugate(hlm[2,2])

    amplm[2,2] = np.abs(hlm[2,2])
    amplm[2, -2] = amplm[2, 2]

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))
    phaselm[2,-2] = - np.unwrap(np.angle(hlm[2,-2]))


    ##time array waveform (s)
    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    if align_peak ==True:
        time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])
    omegalm[2,-2] = - compute_freqInterp(timeNR, hlm[2, -2])

    # dynamics

    lenDyn0 = int(dyn.length/5)

    tdyn0 = dyn.data[ 0 : lenDyn0]
    tdyn0 -= tdyn0[-1]

    dtDyn0 = tdyn0[1] - tdyn0[0]

    rdyn0 = dyn.data[ lenDyn0: 2* lenDyn0]
    phidyn0 = dyn.data[ 2* lenDyn0: 3* lenDyn0]
    prdyn0 = dyn.data[ 3* lenDyn0: 4* lenDyn0]
    pphidyn0 = dyn.data[ 4* lenDyn0: 5* lenDyn0]

    omegaOrbital0 = compute_deriv_f(tdyn0, phidyn0)

    return timeNR, tdyn0, hlm, amplm, phaselm, omegalm, omegaOrbital0, phidyn0


def SEOBNRv4E_modes_Ano(q: float, chi1: float,chi2: float, eccentricity: float,eccentric_anomaly: float,  omega_min: float, delta_t: float,
                    EccFphiPNorder: int, EccFrPNorder: int,  EccWaveformPNorder: int, EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int
                    ,EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int,EcctAppend: float,EccIC: int, align_peak: bool= True):

    # omega_min refers to the initial orbital frequency
    freq_1M = omega_min / (lal.MTSUN_SI) / np.pi

    dMpc=100. # It does not matter as in the end we are removing the scaling by the luminosity distance
    distance = dMpc*1e6*lal.PC_SI
    Mtot=20. # It does not matter as in the end we are removing the scaling by total mass
    m1 = Mtot*q/(1.+q)
    m2 = Mtot/(1.+q)
    f_min = freq_1M / (m1+m2)

    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    HypPphi0, HypR0, HypE0 =[0.,0,0]
    #eccentric_anomaly = 0.0
    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    hlm[2,-2] = np.conjugate(hlm[2,2])

    amplm[2,2] = np.abs(hlm[2,2])
    amplm[2, -2] = amplm[2, 2]

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))
    phaselm[2,-2] = - np.unwrap(np.angle(hlm[2,-2]))


    ##time array waveform (s)
    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    if align_peak ==True:
        time_array = time_array-tAmax


    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])
    omegalm[2,-2] = - compute_freqInterp(timeNR, hlm[2, -2])

    # dynamics

    lenDyn0 = int(dyn.length/5)

    tdyn0 = dyn.data[ 0 : lenDyn0]
    tdyn0 -= tdyn0[-1]

    dtDyn0 = tdyn0[1] - tdyn0[0]

    rdyn0 = dyn.data[ lenDyn0: 2* lenDyn0]
    phidyn0 = dyn.data[ 2* lenDyn0: 3* lenDyn0]
    prdyn0 = dyn.data[ 3* lenDyn0: 4* lenDyn0]
    pphidyn0 = dyn.data[ 4* lenDyn0: 5* lenDyn0]

    omegaOrbital0 = compute_deriv_f(tdyn0, phidyn0)

    return timeNR, tdyn0, hlm, amplm, phaselm, omegalm, omegaOrbital0, phidyn0



########################################################################################

# Functions used in the new test to get the amplitude and the frequency from SEOBNRv4E


########   Functions required by the test
def SEOBNRv4E_tAmpfreq(q: float, chi1: float,chi2: float, eccentricity: float, dMpc:float, f_min:float, Mtot:float, delta_t: float,
                    EccFphiPNorder: int, EccFrPNorder: int,  EccWaveformPNorder: int, EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int
                    ,EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int, EcctAppend: float,EccIC:int):

    distance = dMpc*1e6*lal.PC_SI

    M = Mtot*lal.MSUN_SI # Mtot in units of solar masses
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    HypPphi0, HypR0, HypE0 =[0.,0,0]
    eccentric_anomaly = 0.0
    print("q = ",q, " chi1 = ",chi1, " chi2 = ",chi2, "ecc = ", eccentricity)
    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform, EcctAppend,EccIC,HypPphi0, HypR0, HypE0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    amplm[2,2] = np.abs(hlm[2,2])

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))

    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax

    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])


    return timeNR, amplm[2,2], omegalm[2,2]





def SEOBNRv4_modes(q: float, chi1: float,chi2: float, omega_min: float, delta_t: float):

    # omega_min refers to the initial orbital frequency
    freq_1M = omega_min / (lal.MTSUN_SI) / np.pi

    dMpc=100. # It does not matter as in the end we are removing the scaling by the luminosity distance
    distance = dMpc*1e6*lal.PC_SI
    Mtot=70. # It does not matter as in the end we are removing the scaling by total mass
    m1 = Mtot*q/(1.+q)
    m2 = Mtot/(1.+q)
    f_min = freq_1M / (m1+m2)

    M = Mtot*lal.MSUN_SI
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    hlm[2,-2] = np.conjugate(hlm[2,2])

    amplm[2,2] = np.abs(hlm[2,2])
    amplm[2, -2] = amplm[2, 2]

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))
    phaselm[2,-2] = - np.unwrap(np.angle(hlm[2,-2]))


    ##time array waveform (s)
    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax

    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])
    omegalm[2,-2] = - compute_freqInterp(timeNR, hlm[2, -2])

    # dynamics

    lenDyn0 = int(dyn.length/5)

    tdyn0 = dyn.data[ 0 : lenDyn0]
    tdyn0 -= tdyn0[-1]

    dtDyn0 = tdyn0[1] - tdyn0[0]

    rdyn0 = dyn.data[ lenDyn0: 2* lenDyn0]
    phidyn0 = dyn.data[ 2* lenDyn0: 3* lenDyn0]
    prdyn0 = dyn.data[ 3* lenDyn0: 4* lenDyn0]
    pphidyn0 = dyn.data[ 4* lenDyn0: 5* lenDyn0]

    omegaOrbital0 = compute_deriv_f(tdyn0, phidyn0)

    return timeNR, tdyn0, hlm, amplm, phaselm, omegalm, omegaOrbital0, phidyn0



def SEOBNRv4_tAmpfreq(q: float, chi1: float,chi2: float,  dMpc:float, f_min:float, Mtot:float, delta_t: float):

    distance = dMpc*1e6*lal.PC_SI

    M = Mtot*lal.MSUN_SI # Mtot in units of solar masses
    m1 = M*q/(1.+q)
    m2 = M/(1.+q)
    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1,
                                                              m2,
                                                              f_min,
                                                              distance,
                                                              chi1,
                                                              chi2,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0)

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    # (2,2),(2,-2) modes
    hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data , Mtot, dMpc) #This is h_22
    amplm[2,2] = np.abs(hlm[2,2])

    phaselm[2,2] = - np.unwrap(np.angle(hlm[2,2]))

    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)

    iAmax, Amax = np.argmax(amplm[2,2]), np.max(amplm[2,2])
    tAmax = time_array[iAmax]
    time_array = time_array-tAmax

    timeNR = SectotimeM(time_array,Mtot) # In geometric units

    omegalm[2,2] = - compute_freqInterp(timeNR, hlm[2,2])


    return timeNR, amplm[2,2], omegalm[2,2]


########################################################################################


def easy_phase_diff(timeNR, phaseNR, timev4E,phasev4E, align_start=True):

    iphiNR = InterpolatedUnivariateSpline(timeNR,phaseNR)
    iphiv4E = InterpolatedUnivariateSpline(timev4E, phasev4E)

    dtv4E = timev4E[-1]- timev4E[-2]
    dtNR = timeNR[-1]- timeNR[-2]
    dt = min(dtv4E,dtNR)

    t0NR = timeNR[0]
    t0v4E = timev4E[0]

    tmin = max(t0NR,t0v4E)
    tmax = min(timev4E[-1],timeNR[-1])

    timeCommon = np.arange(tmin,tmax,dt)


    if align_start == True:

        delta_phi0 = iphiNR(tmin) - iphiv4E(tmin)

    else:
        delta_phi0 = iphiNR(0) - iphiv4E(0)

    print(delta_phi0)
    phidiff0 = iphiNR(timeCommon)-iphiv4E(timeCommon) - delta_phi0

    return timeCommon, phidiff0



def sigmoid(t, t0, beta):
    return 1./(1. + np.exp(-beta*(t-t0)))

# The simplest function to find neighborhoods around maxima and minima
def compute_Maxima(tt, ff, DeltaT):

    dynLen = len(ff)

    idxs_max =[]
    idxs_min =[]

    for idx in range(dynLen-1):

        if idx != 0:

            ffm1 = ff[idx-1]
            ff0 = ff[idx]
            tt0 = np.abs(tt[idx])
            ff1 = ff[idx + 1]

            if tt0 > 2:
                if(ff0 > ff1) and (ff0 > ffm1):
                    idxs_max.append(idx)
                if(ff0 < ff1) and (ff0 < ffm1):
                    idxs_min.append(idx)


            if tt0 <  DeltaT:
                idxs_max.append(idx)
                idxs_min.append(idx)


    tt_max = tt[idxs_max]
    tt_min = tt[idxs_min]

    ff_max = ff[idxs_max]
    ff_min = ff[idxs_min]

    return tt_max, tt_min, ff_max, ff_min


def compute_freq_from_phase(timeHiE, phiHiE):
    iphi = InterpolatedUnivariateSpline(timeHiE,phiHiE)

    omegaHi = iphi.derivative()(timeHiE)

    return omegaHi

def compute_phase_omega_Amp_before_NQC(EccWaveformPNorder, outdir):

    datav4E =pd.read_table(outdir+'saModes22Hi_EccPN'+str(EccWaveformPNorder)+'.dat', sep="\s+",header=None,skipfooter=1,engine='python')
    timeHiE = np.array(datav4E[0] )
    rehLME = np.array(datav4E[1])
    imhLME = np.array(datav4E[2])
    hLME =  rehLME +1.j*imhLME
    amphLME = np.abs(hLME)
    phasehLME = np.unwrap(np.angle(hLME))
    omegahLME = compute_freq_from_phase(timeHiE,phasehLME)

    rehEccE = np.array(datav4E[3])
    imhEccE = np.array(datav4E[4])
    hEccE =  rehEccE +1.j*imhEccE
    ampNQCE = np.abs(hEccE)
    phaseNQCE = np.unwrap(np.angle(hEccE))
    omegaNQCE = compute_freq_from_phase(timeHiE,phaseNQCE)

    return timeHiE, rehLME, imhLME, rehEccE, imhEccE, ampNQCE, phaseNQCE, omegaNQCE, amphLME, phasehLME, omegahLME




def get_EOBduration(f_min: float, EccIC: int, params_template: waveform_params) -> float:

    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
    EccAvNQCWaveform,EcctAppend,HypPphi0, HypR0, HypE0 = [1, 1, 4.22, 10000, 1.01]
    eccentric_anomaly=0.0
    sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt(params_template.delta_t,
                                                              params_template.m1 * lal.MSUN_SI,
                                                              params_template.m2 * lal.MSUN_SI,
                                                              f_min,
                                                              params_template.distance,
                                                              params_template.s1z,
                                                              params_template.s2z,
                                                              params_template.ecc,
                                                              eccentric_anomaly,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              params_template.EccFphiPNorder,
                                                              params_template.EccFrPNorder,
                                                              params_template.EccWaveformPNorder,
                                                              params_template.EccPNFactorizedForm,
                                                              params_template.EccBeta,
                                                              params_template.Ecct0,
                                                              params_template.EccNQCWaveform,
                                                              params_template.EccPNRRForm,
                                                              params_template.EccPNWfForm,
                                                              EccAvNQCWaveform,EcctAppend,EccIC,HypPphi0, HypR0, HypE0
                                                              )

    h22 = sphtseries.mode.data.data #This is h_22
    t =  np.arange(0,len(h22)*params_template.delta_t, params_template.delta_t)
    amp22 = np.abs(h22)
    #t, hp, hc, amp22 = generate_SEOBNREv4_waveform(params, max_freq)
    EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
    EOB_duration = EOB_duration / ((params_template.m1 + params_template.m2) * lal.MTSUN_SI)

    return EOB_duration


def get_NR_duration(params_signal: waveform_params,params_template: waveform_params,NR_file: str, NR_ma: lal.Value,
                    ell_max: int = 4) -> float:

    # merger time difference manually

    _, hlm_NR = lalsim.SimInspiralNRWaveformGetHlms(
        params_signal.delta_t,
        params_signal.m1 * lal.MSUN_SI,
        params_signal.m2 * lal.MSUN_SI,
        params_signal.distance,
        params_signal.f_min,
        0.0,
        params_signal.s1x,
        params_signal.s1y,
        params_signal.s1z,
        params_signal.s2x,
        params_signal.s2y,
        params_signal.s2z,
        NR_file,
        NR_ma,
    )

    # non LAL waveform (only 22 waveform)
    modes_NR = {}

    if params_template.approx=="SEOBNRv4E_opt":
        modes_NR[2, 2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2).data.data
        modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    else:

        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell+1 ):
                #print(ell,m)
                if m!=0:
                    modes_NR[ell, m] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, ell,m).data.data

                #modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
    time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
    NR_duration = get_time_to_merger(time_NR, modes_NR)
    NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    return NR_duration

def get_fminEOB_22NRlength(params_signal: waveform_params, params_template: waveform_params,
                         NR_file: str, NR_ma: lal.Value,  ell_max: int = 2,  f_max: float = 2048.0) -> float:

    # merger time difference manually

    _, hlm_NR = lalsim.SimInspiralNRWaveformGetHlms(
        params_signal.delta_t,
        params_signal.m1 * lal.MSUN_SI,
        params_signal.m2 * lal.MSUN_SI,
        params_signal.distance,
        params_signal.f_min,
        0.0,
        params_signal.s1x,
        params_signal.s1y,
        params_signal.s1z,
        params_signal.s2x,
        params_signal.s2y,
        params_signal.s2z,
        NR_file,
        NR_ma,
    )

    # non LAL waveform (only 22 waveform)
    modes_NR = {}
    modes_NR[2, 2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2).data.data
    modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
    time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
    NR_duration = get_time_to_merger(time_NR, modes_NR)
    NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(params_template.m1 + params_template.m2)*lal.MTSUN_SI)

    EccIC=0
    # Compute length of the shortest duration possible v4E waveform
    EOB_duration_fmin_Maxv4E = get_EOBduration(fmin_Maxv4E, EccIC, params_template)
    EOB_duration_fmin_NR = get_EOBduration(params_signal.f_min, EccIC, params_template)

    EOB_duration_fmin0 = EOB_duration_fmin_NR

    #print(EOB_duration_fmin0, EOB_duration_fmin_Maxv4E, NR_duration)

    f_min0 = params_signal.f_min
    while NR_duration > EOB_duration_fmin0:
        f_min0 -= 10
        EOB_duration_fmin0 = get_EOBduration(f_min0, EccIC, params_template)

    #print(EOB_duration, NR_duration)
    if EOB_duration_fmin_Maxv4E > NR_duration:
        f_min_EOB = fmin_Maxv4E-0.1
    else:
        #res = root_scalar(tdiff, bracket=(f_min0, fmin_Maxv4E),args=(NR_duration, params_template, 2),)
        res = root_scalar(tdiff_v4E, bracket=(f_min0, fmin_Maxv4E-1),args=(NR_duration, params_template, 2),)
        f_min_EOB = res.root

    return f_min_EOB


def tdiff_v4E(
    f_min: float, NR_duration: float, params: waveform_params, ell_max: int = 4
) -> float:
    """Compute the difference to the merger time between NR and EOB. Merger time
    is defined as the peak of the frame invariant amplitude.

    Args:
        f_min (float): Starting frequency [Hz]
        NR_duration (float): Duration of the waveform [M]
        params (waveform_params): The parameters of the EOB waveform
        ell_max (int, optional): Max ell to use. Defaults to 4.

    Returns:
        float: The difference in the time to merger
    """
    EccIC = 0
    EOB_duration = get_EOBduration(f_min, EccIC, params)

    #print(f_min, NR_duration, EOB_duration, NR_duration - EOB_duration)
    return NR_duration - EOB_duration

#def generate_eccfminList(params_signal, params_template, e0, NR_file,ma, ell_max,fhigh, Nf, deltaf):
def generate_eccfminList(params_template: waveform_params,
                        f_min_signal:float,
                         e0: float,
                         NR_duration: float, Nf: int, deltaf: float,
                         EccFphiPNorder: int = 18,
                         EccFrPNorder: int= 18,
                         EccWaveformPNorder: int= 16,
                         EccPNFactorizedForm: int= 1,
                         EccBeta: float= 0.09,
                         Ecct0: float= 300,
                         EccNQCWaveform: int= 1,
                         EccPNRRForm: int = 1 ,
                         EccPNWfForm: int = 1 ,
                         EcctAppend: float = 100.0 ,
                         EccAvNQCWaveform: int = 1,
                         EccIC: int =0
                         ):

    params_template.ecc = e0
    LAL_params_template = lal.CreateDict()


    lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

    lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
    lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
    lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)
    lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
    lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)
    lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
    lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)
    lalsim.SimInspiralWaveformParamsInsertEccIC(LAL_params_template, EccIC)
    params_template.wf_param=LAL_params_template
    #fmin_NRlength =get_fminEOB_22NRlength(params_signal, params_template, NR_file, ma, ell_max, fhigh)
    fmin_NRlength =get_fminEOB_22NRlength_fast(f_min_signal, params_template, NR_duration)

    fmin0=fmin_NRlength-deltaf
    fmin1=fmin_NRlength

    fref_list= np.linspace(fmin0,fmin1,Nf)
    ecc_array= np.full( Nf,(e0))
    eccfmin_array=np.stack((ecc_array,fref_list),axis=1)

    return eccfmin_array


# Useful functions

def maxima_minima_fit(ff, tt, tt0, deltaT):

    idx_data=(np.where((tt <= tt0+deltaT) & (tt >= tt0 - deltaT)))[0]
    tt_idx=tt[idx_data]
    ff_idx=ff[idx_data]


    fit = np.polyfit(tt_idx, ff_idx, 2)
    polynomial = np.poly1d(fit)
    ff_fit=polynomial(tt_idx)
    p2 = np.polyder(polynomial)
    tt_parabola_max=np.roots(p2)

    return tt_parabola_max[0], polynomial(tt_parabola_max)[0]

def loop_maxima_minima_fit(ff, tt, ttmaxmin_list, deltaT):

    tt_maxs=[]
    ff_maxs=[]
    for tt0 in ttmaxmin_list:

        tt_max_fit, ff_max_fit = maxima_minima_fit(ff, tt, tt0, deltaT)

        tt_maxs.append(tt_max_fit)
        ff_maxs.append(ff_max_fit)

    ttmaxs=np.array(tt_maxs)
    ffmaxs=np.array(ff_maxs)

    return ttmaxs,ffmaxs

def duplicate_position(tH_minima,ii):
    tti=tH_minima[ii]
    ttdiff= np.array([np.abs(ttw-tti) for ttw in tH_minima])
    ttdiff=np.delete(ttdiff, [ii]) # Remove position of the element
    pos=np.where(ttdiff<1e-10)[0]
    #tH_minima=np.delete(tH_minima, [pos])
    #return tH_minima
    return pos

def duplicate_position_list(tH_minima):

    pos_list=[]
    for ii in range(len(tH_minima)):
        pos= duplicate_position(tH_minima,ii)

        if pos.size!=0:
            pos_list.append(pos)

    if pos_list:
        out = np.concatenate(pos_list).ravel().tolist()
        pos_to_remove = list(set(out))
    else:
        pos_to_remove=[]


    #tH_minima=np.delete(tH_minima, [pos])
    #return tH_minima
    return pos_to_remove

def remove_duplicate_elements(tH_minima,omega_orb_minima):

    pos_repeat_min=duplicate_position_list(omega_orb_minima)

    if pos_repeat_min:
        omega_orb_minima = np.delete(omega_orb_minima, pos_repeat_min)
        tH_minima = np.delete(tH_minima, pos_repeat_min)

    return tH_minima,omega_orb_minima



def SEOBNRv4EHM_modes(q: float, chi1: float,chi2: float, eccentricity: float, eccentric_anomaly:float, f_min:float, M_fed:float,
                      delta_t: float,
                      EccFphiPNorder: int, EccFrPNorder: int, EccWaveformPNorder: int,
                      EccPNFactorizedForm: int, EccBeta: float, Ecct0: float, EccNQCWaveform: int,
                      EccPNRRForm: int, EccPNWfForm: int, EccAvNQCWaveform: int, EcctAppend: float, approx: str):


    HypPphi0, HypR0, HypE0 =[0.,0,0]
    EccIC=-1

    m1= q/(1+q)*M_fed
    m2= 1/(1+q)*M_fed
    dMpc=500
    dist = dMpc*(1e6*lal.PC_SI)

    if approx=="SEOBNRv4E_opt" or  "SEOBNRv4E_opt1" or  approx=="SEOBNRv4":
        SpinAlignedVersion=4
        nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary
        mode_list=[[2,2]]


    else:
        SpinAlignedVersion=41
        nqcCoeffsInput=lal.CreateREAL8Vector(50) ##This will be unused, but it is necessary
        mode_list=[[2,2],[2,1],[3,3],[4,4],[5,5]]


    if approx == "SEOBNRv4E_opt" or approx == "SEOBNRv4E_opt1" or approx == "SEOBNRv4EHM_opt":

        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModesEcc_opt(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              eccentricity,
                                                              eccentric_anomaly,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0,
                                                              EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                              EccPNFactorizedForm, EccBeta, Ecct0, EccNQCWaveform,
                                                              EccPNRRForm, EccPNWfForm, EccAvNQCWaveform,
                                                              EcctAppend,EccIC,HypPphi0, HypR0, HypE0)


    else:


        #print("SEOBNRv4HM modes")
        sphtseries, dyn, dynHi = lalsimulation.SimIMRSpinAlignedEOBModes(delta_t,
                                                              m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,
                                                              f_min,
                                                              dist,
                                                              chi1,
                                                              chi2,
                                                              SpinAlignedVersion, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,
                                                              nqcCoeffsInput, 0)





    hlm={}


    if SpinAlignedVersion==4:
        hlm[2,2] = AmpPhysicaltoNRTD(sphtseries.mode.data.data,M_fed,dMpc)
        hlm[2,-2] = np.conjugate(hlm[2,2])

    else:
        ##55 mode
        modeL = sphtseries.l
        modeM = sphtseries.m
        h55 = sphtseries.mode.data.data #This is h_55
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h55 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(h55 )

        ##44 mode
        modeL = sphtseries.next.l
        modeM = sphtseries.next.m
        h44 = sphtseries.next.mode.data.data #This is h_44
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h44 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(h44 )

        ##21 mode
        modeL = sphtseries.next.next.l
        modeM = sphtseries.next.next.m
        h21 = sphtseries.next.next.mode.data.data #This is h_21
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h21,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(h21 )

        ##33 mode
        modeL = sphtseries.next.next.next.l
        modeM = sphtseries.next.next.next.m
        h33 = sphtseries.next.next.next.mode.data.data #This is h_33
        hlm[modeL,modeM] =  AmpPhysicaltoNRTD(h33 ,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(h33 )

        ##22 mode
        modeL = sphtseries.next.next.next.next.l
        modeM = sphtseries.next.next.next.next.m
        h22 = sphtseries.next.next.next.next.mode.data.data #This is h_22
        hlm[modeL,modeM] = AmpPhysicaltoNRTD(h22,M_fed,dMpc)
        hlm[modeL,-modeM] =  ((-1)**modeL) * np.conjugate(h22 )


    time_array = np.arange(0,len(hlm[2,2])*delta_t, delta_t)
    timeNR = SectotimeM(time_array,M_fed)
    #return time_array, hlm
    omegalm={}
    amplm={}

    for l,m in mode_list:
        omegalm[l,m]=-compute_freqInterp(timeNR,hlm[l,m])
        amplm[l,m]=np.abs(hlm[l,m])


    imax = np.argmax(amplm[2,2])
    timeNR -= timeNR[imax]


    lenDyn0 = int(dyn.length/5)

    tdyn0 = dyn.data[ 0 : lenDyn0]
    tdyn0 -= tdyn0[-1]

    dtDyn0 = tdyn0[1] - tdyn0[0]

    rdyn0 = dyn.data[ lenDyn0: 2* lenDyn0]
    phidyn0 = dyn.data[ 2* lenDyn0: 3* lenDyn0]
    prdyn0 = dyn.data[ 3* lenDyn0: 4* lenDyn0]
    pphidyn0 = dyn.data[ 4* lenDyn0: 5* lenDyn0]

    omegaOrbital0 = compute_deriv_f(tdyn0, phidyn0)

    #return timeNR, tdyn0, hlm, amplm, phaselm, omegalm, omegaOrbital0, phidyn0


    return timeNR, hlm, amplm, omegalm




# Function to parse the modes of TEOBResumSE to the linear index they use
def modes_to_k(modes):
    """
    Map multipolar (l,m) -> linear index k
    """
    return [int(x[0] * (x[0] - 1) / 2 + x[1] - 2) for x in modes]


def TEOBResumSEHM_modes(q: float, chi1: float,chi2: float, eccentricity: float, f_min:float, M_fed:float,
                      delta_t: float, modes:list):

    k = modes_to_k(modes)
    srate=1./delta_t
    eta = q/(1.+q)**2

    m1= q/(1+q)*M_fed
    m2= 1/(1+q)*M_fed
    dMpc=500
    dist = dMpc*(1e6*lal.PC_SI)
    iota_s=0
    phi=0
    TEOB_IC=0

    pars = {
        "M":M_fed,
        "q": q,
        "chi1": chi1,
        "chi2": chi2,
        "Lambda1": 0.0,
        "Lambda2": 0.0,
        "domain": 0,  # Set 1 for FD. Default = 0
        "arg_out": 1,  # Output hlm/hflm. Default = 0
        "use_mode_lm": k,  # List of modes to use/output through EOBRunPy
        "output_lm": k,  # List of modes to print on file
        #'srate'       : np.round(1.0/p.delta_t)*0.5, #srate at which to interpolate. Default = 4096.
        "srate_interp": srate,
        #'srate_interp'       : 1650.0, #srate at which to interpolate. Default = 4096.
        "use_geometric_units": 0,  # output quantities in geometric units. Default = 1
        "df": 0.01,  # df for FD interpolation
        "initial_frequency": f_min,  # in Hz if use_geometric_units = 0, else in geometric units
        "interp_uniform_grid": 1,  # interpolate mode by mode on a uniform grid. Default = 0 (no interpolation)
        "distance": dMpc,  # Distance in Mpc
        "inclination": iota_s,
        # "dt"                 : p.delta_t,
        # "r0"                 : pow(p.f_min*np.pi , -2./3.),
        #'dt_interp'          : 0.5,
        "coalescence_angle": phi,
        "ecc": eccentricity,  # Eccentricity. Default = 0.
        'ecc_freq' : TEOB_IC,      #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation. Default = 1
    }


    t, hp, hcm, hlm1, dyn = EOBRun_module.EOBRunPy(pars)
    timeNR = SectotimeM(t,M_fed) # In geometric units

    hlm={}
    amplm = {}
    phaselm = {}
    omegalm = {}

    for l,m in modes:

        k=modes_to_k([[l,m]])
        k=str(k[0])
        hlm[l,m] = hlm1[k][0]*eta*np.exp(1.j*hlm1[k][1])

        amplm[l,m]=np.abs(hlm[l,m])

        phaselm[l,m] = - np.unwrap(np.angle(hlm[l,m]))
        omegalm[l,m] = compute_freqInterp(timeNR, hlm[l,m])

    imax=np.argmax(amplm[2,2])
    tPeak=timeNR[imax]
    timeNR -=tPeak

    return timeNR, hlm, amplm, omegalm
