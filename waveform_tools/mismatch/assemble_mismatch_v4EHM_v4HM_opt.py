#!/usr/bin/env python
import argparse
import glob
import numpy as np
import os

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--result_dir", type=str, help="Directory with files")
    p.add_argument("--output_dir", type=str, help="Directory where to output result file")
    args = p.parse_args()

    mm_dir = args.result_dir
    out_dir = args.output_dir

    if mm_dir[-1]!= "/":
        mm_dir += '/'

    if out_dir[-1]!= "/":
        out_dir += '/'

    if not os.path.isdir(out_dir):
         os.makedirs(out_dir,exist_ok=True)


    files=glob.glob(mm_dir+'mismatch*dat')
    print(len(files))

    name = mm_dir.split('/')[-2]


    result =[]

    for file in files:

        d = np.genfromtxt(file)

        weighted_mismatches = 0.0
        SNRs = 0.0
        mismatches = 0.0

        if len(d.shape) > 1:
            for i in range(len(d)):
                weighted_mismatches += (1 - d[i,-1]) ** 3 * d[i,-2] ** 3
                mismatches += d[i,-1]
                SNRs += d[i, -2] ** 3

        mass = d[0, 0]
        q = d[0, 1]
        chi1 = d[0, 2]
        chi2 = d[0, 3]
        ecc = d[0, 4]

        mismatches /= len(d)
        averaged_mismatches = 1 - (weighted_mismatches / SNRs) ** (1.0 / 3)

        result.append([mass, q,chi1,chi2,ecc,mismatches,averaged_mismatches])


    res = np.c_[result]
    np.savetxt(out_dir+"Allmismatches_{}_N{}.dat".format(name,len(files)), res)
