#!/usr/bin/env/python

# Script to run the mismatches over eccentric waveforms
import sys, h5py, glob, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSe module
#import EOBRun_module

import numpy as np, pandas as pd, pycbc, json

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *

def get_EOBduration(f_min: float, params_template: waveform_params) -> float:

    nqcCoeffsInput=lal.CreateREAL8Vector(10) ##This will be unused, but it is necessary

    sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModesEcc_opt(params_template.delta_t,
                                                              params_template.m1 * lal.MSUN_SI,
                                                              params_template.m2 * lal.MSUN_SI,
                                                              f_min,
                                                              params_template.distance,
                                                              params_template.s1z,
                                                              params_template.s2z,
                                                              params_template.ecc,
                                                              4, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0,
                                                              params_template.EccFphiPNorder,
                                                              params_template.EccFrPNorder,
                                                              params_template.EccWaveformPNorder,
                                                              params_template.EccPNFactorizedForm,
                                                              params_template.EccBeta,
                                                              params_template.Ecct0,
                                                              params_template.EccNQCWaveform,
                                                              params_template.EccPNRRForm,
                                                              params_template.EccPNWfForm
                                                             )

    h22 = sphtseries.mode.data.data #This is h_22
    t =  np.arange(0,len(h22)*params_template.delta_t, params_template.delta_t)
    amp22 = np.abs(h22)
    #t, hp, hc, amp22 = generate_SEOBNREv4_waveform(params, max_freq)
    EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
    EOB_duration = EOB_duration / ((params_template.m1 + params_template.m2) * lal.MTSUN_SI)

    return EOB_duration


def get_fminEOB_22NRlength(params_signal: waveform_params, params_template: waveform_params,
                         NR_file: str, NR_ma: lal.Value,  ell_max: int = 2,  f_max: float = 2048.0) -> float:

    # merger time difference manually

    _, hlm_NR = lalsim.SimInspiralNRWaveformGetHlms(
        params_signal.delta_t,
        params_signal.m1 * lal.MSUN_SI,
        params_signal.m2 * lal.MSUN_SI,
        params_signal.distance,
        params_signal.f_min,
        0.0,
        params_signal.s1x,
        params_signal.s1y,
        params_signal.s1z,
        params_signal.s2x,
        params_signal.s2y,
        params_signal.s2z,
        NR_file,
        NR_ma,
    )

    # non LAL waveform (only 22 waveform)
    modes_NR = {}
    modes_NR[2, 2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2).data.data
    modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

    tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
    time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
    NR_duration = get_time_to_merger(time_NR, modes_NR)
    NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(params_template.m1 + params_template.m2)*lal.MTSUN_SI)

    # Compute length of the shortest duration possible v4E waveform
    EOB_duration_fmin_Maxv4E = get_EOBduration(fmin_Maxv4E, params_template)
    EOB_duration_fmin_NR = get_EOBduration(params_signal.f_min, params_template)

    EOB_duration_fmin0 = EOB_duration_fmin_NR
    f_min0 = params_signal.f_min

    while EOB_duration_fmin0 < NR_duration:
        f_min0 = params_signal.f_min-5
        EOB_duration_fmin0 = get_EOBduration(f_min0, params_template)

    #print(EOB_duration, NR_duration)
    if EOB_duration_fmin_Maxv4E > NR_duration:
        f_min_EOB = fmin_Maxv4E-0.1
    else:
        res = root_scalar(tdiff, bracket=(f_min0, fmin_Maxv4E),args=(NR_duration, params_template, 2),)
        f_min_EOB = res.root

    return f_min_EOB


def run_EccNRmismatch(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    NR_file: str ,
    Ne: int = 50 ,
    Nf: int =200,
    template_approx: str = "SEOBNRv4E_opt",
    signal_approx: str = "NR_hdf5",
    parameters: Dict = None,
    fhigh: float = 2048.0,
    ellMax: int = 2,
    debug: bool = False,
    verbose: str = "False",
    rundir: str=None,
    plotdir: str=None,
    outdir: str=None,

):

    if rundir ==None:
        rundir = "."

    if plotdir == None:
        plotdir= rundir+'/NR_plots/'
        os.makedirs(plotdir, exist_ok=True)

    if outdir == None:
        outdir = rundir+"/mm_files/"
        os.makedirs(outdir, exist_ok=True)

    # Generic bounds on the initial eccentricity  of each model, below we use also the Newtonian estimate at the starting
    # frequency to set a maximum allowed eccentricity
    if template_approx == "TEOBResumSe":
        e0max_template = 0.3
    elif template_approx == "TEOBResumSeHM":
        e0max_template = 0.3
    elif template_approx == "SEOBNREv4":
        e0max_template = 0.7
    elif template_approx == "SEOBNRv4E":
        e0max_template = 0.3
    elif template_approx == "SEOBNRv4E_opt":
        e0max_template = 0.3
    elif template_approx == "IMRPhenomXEv1":
        e0max_template = 0.2
    else:
        e0max_template = 0.0


    # Figure out the domains of the signal and the template
    td_approxs = td_approximants()
    fd_approxs = fd_approximants()
    if signal_approx not in fd_approxs  or signal_approx == "SEOBNRv4":
        signal_domain = "TD"
        delta_t_signal = 1.0 / 8192
        delta_f_signal = None
    else:
        signal_domain = "FD"
        delta_f_signal = 0.125
        delta_t_signal = None

    if template_approx not in fd_approxs or template_approx == "SEOBNRv4":
        template_domain = "TD"
        delta_t_template = 1.0 / 8192
        delta_f_template = None

    else:
        template_domain = "FD"
        delta_f_template = 0.125
        delta_t_template = None

    approx_type = get_approximant_type(template_approx)
    LAL_params_signal = None
    LAL_params_template = None

    if template_approx == "SEOBNRv4E" or template_approx == "SEOBNRv4E_opt" :
        LAL_params_template = lal.CreateDict()

        EccFphiPNorder = parameters["EccFphiPNorder"]
        EccFrPNorder = parameters["EccFrPNorder"]
        EccWaveformPNorder = parameters["EccWaveformPNorder"]
        EccPNFactorizedForm = parameters["EccPNFactorizedForm"]


        EccBeta = parameters["EccBeta"]
        Ecct0 = parameters["Ecct0"]
        EccNQCWaveform = parameters["EccNQCWaveform"]

        EccPNRRForm = parameters["EccPNRRForm"]
        EccPNWfForm = parameters["EccPNWfForm"]

        if template_approx == "SEOBNRv4E_opt" :
            EcctAppend = parameters["EcctAppend"]
            EccAvNQCWaveform = parameters["EccAvNQCWaveform"]

        #print(EccFphiPNorder, EccFrPNorder, EccWaveformPNorder, EccPNFactorizedForm)
        #print(EccBeta,Ecct0,EccNQCWaveform)
        lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_template, EccFphiPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_template, EccFrPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_template, EccWaveformPNorder)
        lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_template, EccPNFactorizedForm)

        lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_template, EccBeta)
        lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_template, Ecct0)
        lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_template, EccNQCWaveform)

        lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_template, EccPNRRForm)
        lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_template, EccPNWfForm)


        lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
        lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)

        if signal_approx =="SEOBNRv4E" or signal_approx =="SEOBNRv4E_opt":
            LAL_params_signal = lal.CreateDict()

            lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params_signal, EccFphiPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params_signal, EccFrPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params_signal, EccWaveformPNorder)
            lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params_signal, EccPNFactorizedForm)

            lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params_signal, EccBeta)
            lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params_signal, Ecct0)
            lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params_signal, EccNQCWaveform)

            lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params_signal, EccPNRRForm)
            lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params_signal, EccPNWfForm)


            lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params_template, EcctAppend)
            lalsim.SimInspiralWaveformParamsInsertEccEccAvNQCWaveform(LAL_params_template, EccAvNQCWaveform)

    elif template_approx == "IMRPhenomXEv1":
        LAL_params_template = lal.CreateDict()

        Nharmonic = parameters["Nharmonic"]
        kAdvance = parameters["kAdvance"]
        lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template,Nharmonic)
        lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)

    # Read LVCNR format file
    if NR_file:
        fp = h5py.File(NR_file, "r")
        freq_1M = fp.attrs["f_lower_at_1MSUN"]
        # Add rounding to prevent some errors in the calculation of the symmetric mass ratio (for TEOBResumSe)
        m1_dim = round(fp.attrs["mass1"], 5)
        m2_dim = round(fp.attrs["mass2"], 5)

        if fp.attrs["eccentricity"] > 1:
            eccentricity_signal = 0.2
        else:
            eccentricity_signal = round(fp.attrs["eccentricity"], 4)

        mean_anomaly_signal = round(fp.attrs["mean_anomaly"], 4)
        q=m1_dim/m2_dim

        fp.close()
        prefix = os.path.basename(NR_file).split(".")[0]

        LAL_params_signal = lal.CreateDict()
        ma = lalsim.SimInspiralCreateModeArray()

        # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
        if (
            template_approx == "SEOBNRv4"
            or template_approx == "TEOBResumSe"
            or template_approx == "SEOBNREv4"
            or template_approx == "SEOBNRv4E"
            or template_approx == "SEOBNRv4E_opt"
            or template_approx == "IMRPhenomXEv1"
        ):
            lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
            lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
        else:
            for ell in range(2, ellMax + 1):
                lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
        lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
        lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)

        (s1x, s1y, s1z, s2x, s2y, s2z,) = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(0.0, 100, NR_file)
        # Since we are using f_ref=0, total mass does not matter

    # Read orbital eccentricity from NR file
    ModeList=[[2,2]]
    timeNR, tHorizon, hlm, amplm, phaselm, omegalm, omega_orb, phase_orb = compute_h22OmegaOrbfromNRfile(NR_file, ModeList)

    # Estimate eccentricity and averaged orbital frequency from the NR simulation to get the initial guesses
    # for the model

    iphase_orb = InterpolatedUnivariateSpline(tHorizon, phase_orb)

    tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima = compute_MaxMin(tHorizon, omega_orb)

    tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(timeNR, omegalm[2,2], amplm[2,2])

    iomega_orb_maxima = InterpolatedUnivariateSpline(tH_maxima, omega_orb_maxima)
    iomega_orb_minima = InterpolatedUnivariateSpline(tH_minima, omega_orb_minima)
    omegaOrb_maxima = iomega_orb_maxima(tHorizon)
    omegaOrb_minima = iomega_orb_minima(tHorizon)
    ecc_omegaorb = (np.sqrt(omegaOrb_maxima)-np.sqrt(omegaOrb_minima))/(np.sqrt(omegaOrb_maxima)+np.sqrt(omegaOrb_minima))

    # Compute the averaged orbital frequency
    omegaOrb_av =1./(tH_maxima[1:]-tH_maxima[:-1])*(iphase_orb(tH_maxima[1:])-iphase_orb(tH_maxima[:-1]))

    # Just in case the waveform is noisy
    i=0
    omega22Max = omega_maxima[i]
    while omega22Max < omegaOrb_av[0]*2 :
        omega22Max = omega_maxima[i]
        i+=1

    omega_metadata = freq_1M*2*np.pi*lal.MTSUN_SI

    # Compute the minimum frequency for overlap calculations
    freq_1M_new = 1.2* omega22Max/(2*np.pi)/lal.MTSUN_SI # 1.2 factor to avoid conditioning of the FT for short waveforms

    # Choose some arbitrary parameters
    Mtotals=np.linspace(20, 200, 20)

    dMpc= 500.0
    dist = 1e6 * lal.PC_SI * dMpc  # Doesn't matter what this is

    modes_dc=None

    # Put the name of the NR file in a nicer format
    basename=os.path.basename(NR_file)
    simName = basename.split('.h5')[0]

    # Look for optimal eccentricity and starting frequency for the lowest total mass
    M_fed = Mtotals[0]
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    f_ref = freq_1M / (m1 + m2)
    if f_ref<10.0:
        f_ref=10.0

    f_min = 1 * f_ref

    # The signal does not care about the values of eccentricity and mean anomaly
    params_signal = waveform_params(
        m1, m2,
        s1x, s1y, s1z,
        s2x, s2y, s2z,
        iota_s, phi_s,
        f_ref, f_min,
        dist, delta_t_signal,  delta_f_signal,
        LAL_params_signal,
        signal_approx, signal_domain,
        0.,
        0.,
    )

    # Compute the maximum frequency for v4E waveform generation
    fmin_Maxv4E = 10.**(-3./2.)/(np.pi*(m1+m2)*lal.MTSUN_SI)

    fmin_start = freq_1M_new/ (m1 + m2)

    # initialize also the template parameters
    params_template = waveform_params(
        m1, m2,
        0, 0, s1z,
        0, 0, s2z,
        iota_s, phi_s,
        f_ref, f_min,
        dist, delta_t_template, delta_f_template,
        LAL_params_template,
        template_approx, template_domain,
        ecc = ecc_omegaorb[0]
    )

    params_template.EccFphiPNorder = EccFphiPNorder
    params_template.EccFrPNorder = EccFrPNorder
    params_template.EccNQCWaveform = EccNQCWaveform
    params_template.EccWaveformPNorder = EccWaveformPNorder

    params_template.EccPNFactorizedForm = EccPNFactorizedForm
    params_template.EccBeta = EccBeta
    params_template.Ecct0 = Ecct0

    params_template.EccPNRRForm = EccPNRRForm
    params_template.EccPNWfForm = EccPNWfForm

    params_template.EcctAppend = EcctAppend
    params_template.EccAvNQCWaveform = EccAvNQCWaveform

    # 1D optimization over eccentricity at fixed starting frequency
    ecc_list = np.linspace(0, e0max_template, Ne)

    f_ref_t=freq_1M_new/ (m1 + m2)
    if f_ref_t > fmin_Maxv4E:
        f_ref_t = fmin_Maxv4E -1. # Add a buffer of 1Hz to avoid problems, but it should never reach this if condition!

    f_min_t = 1 * f_ref_t
    params_template.f_ref = f_ref_t
    params_template.f_min = f_min_t

    mmtools_e = []
    mmf_elist = []
    for e0  in ecc_list:

        print("===============================================================")
        #print(e0)
        params_template.ecc = e0
        print(e0)
        # Get minimum frequency compatible with the length of the NR simulation
        fmin_NRlength = get_fminEOB_22NRlength(params_signal, params_template, NR_file, ma, ellMax, fhigh)

        fmin_bound = f_min
        fmax_bound = min(fmin_NRlength, fmin_Maxv4E)

        fref_list= np.linspace(fmin_bound,fmax_bound,Nf)

        fmin_start = freq_1M_new/ (m1 + m2)
        #print("(f0min,f1min) = ", fref_list[0], fref_list[-1])

        mmtools_f = []


        for f_ref_t in fref_list:

            f_min_t = 1 * f_ref_t
            params_template.f_ref = f_ref_t
            params_template.f_min = f_min_t

            sp, sc = generate_waveform(params_signal)
            s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)

            final = unfaithfulness_RC(s, params_template, fmin_start, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

            mmtools_f.append(final)

        if verbose == "True":
            plt.rcParams["figure.figsize"] = (14,10)

            plt.plot(fref_list,mmtools_f)
            plt.ylabel(r"Mismatch",fontsize=14)
            plt.xlabel(r"$f_{start} (Hz) $",fontsize=14)
            plt.xticks(fontsize=14)
            plt.yticks(fontsize=14)

            eTag= np.round(e0,3)
            plt.title(r'v4E $e='+str(eTag)+'$ vs  '+simName+' at $'+str(M_fed)+' M_\odot$',fontsize=20)

            plt.grid(b=None)
            #plt.legend(loc='best', prop={'size':14})
            plt.savefig(plotdir+'mismatch_'+simName+'_fixed_e'+str(eTag)+'_freqArray.png')
            plt.show()

        mmf_elist.append(mmtools_f)

        mmf_min = min(mmtools_f)
        ifmin = np.argmin(mmtools_f)
        foptmin=fref_list[ifmin]
        omega_min_opt = foptmin*(m1+m2)
        Momega_min_opt = omega_min_opt*(2.*np.pi)*lal.MTSUN_SI

        mmtools_e.append([e0,foptmin, mmf_min])

    mmtools_e = np.array(mmtools_e)

    imin = np.argmin(mmtools_e[:,2])
    emin_opt, foptmin, mm_minOpt = mmtools_e[imin]

    #Read optimal values
    omega_min_opt = foptmin*(m1+m2)
    Momega_min_opt = omega_min_opt*(2.*np.pi)*lal.MTSUN_SI # dimensionless units
    if verbose == "True":
        print("emin_opt = ",emin_opt,"  Momega_min_opt = ", Momega_min_opt,"   mismatch_opt = ", mm_minOpt)


    # With the optimal values, now loop over all total masses

    # Then loop over all all total masses using optimal values for e and fstart
    mmtools  = []

    for M_fed in Mtotals:

        m1 = m1_dim * M_fed
        m2 = m2_dim * M_fed
        f_ref = freq_1M / (m1 + m2)

        if f_ref<10.0:
            f_ref=10.0
        f_min = 1 * f_ref

        fmin_start = freq_1M_new/ (m1 + m2)
        if fmin_start< 10.0:
            fmin_start = 10.0
        # Update signal parameters
        params_signal.m1 = m1
        params_signal.m2 = m2
        params_signal.f_min = f_min
        params_signal.f_ref = f_ref


        # Update template parameters
        f_ref_t= omega_min_opt/(m1+m2)
        f_min_t =  f_ref_t

        params_template.m1 = m1
        params_template.m2 = m2
        params_template.f_min = f_min_t
        params_template.f_ref = f_ref_t
        params_template.ecc = emin_opt


        sp, sc = generate_waveform(params_signal)
        s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)

        final = unfaithfulness_RC(s, params_template,  fmin_start, fhigh=fhigh, modes_dict=modes_dc, debug=True)
        mmtools.append(final)

    eTag = np.round(emin_opt,2)
    omegaTag  = np.round(Momega_min_opt,2)

    mismatch = [val[0] for val in mmtools]
    phase_offset = [val[1] for val in mmtools]
    time_shift = [val[2] for val in mmtools]

    if verbose == "True":

        plt.rcParams["figure.figsize"] = (14,10)
        plt.rc('font', size=17)

        plt.plot(mismatch,mmtools)

        plt.ylabel(r"Mismatch",fontsize=20)
        plt.xlabel(r"$M_{total} (M_\odot ) $",fontsize=20)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)

        plt.title(r'v4E $\quad e^{opt}='+str(eTag)+'$, $\quad  M\omega^{v4E,opt}_{22}= '+str(omegaTag )+'$ vs  '+simName,fontsize=20)

        plt.grid(b=None)
        plt.savefig(plotdir+'mismatch_'+simName+'_over_totalMass.png')
        plt.close()
        #plt.show()

    # save to a json file as much information as possible

    res={}
    res['NR_file'] = simName
    res['q'] = q
    res['s1z'] = s1z
    res['s2z'] = s2z
    res['eorb_NR'] = ecc_omegaorb[0]
    res['freq_1M_NR'] = freq_1M
    res['freq_1M_match'] = freq_1M_new

    # Store values of 1D optimizations for debugging purposes
    res['mismatch_fixed_fmin_vary_ecc'] = mmtools_e.tolist()
    res['mismatch_fixed_ecc_vary_fmin'] = mmf_elist
    res['mismatch_fixed_eminopt_vary_fmin'] = mmtools_f

    res['eccentricity_list'] = ecc_list.tolist()
    res['fmin_list'] = fref_list.tolist()


    # Store optimal values
    res['Momegav4E_min_opt'] = Momega_min_opt
    res['ev4E_opt'] = emin_opt
    res['Mtotal_list'] = Mtotals.tolist()
    res['mismatch_over_Mtotal'] = mismatch
    res['phase_offset'] = phase_offset
    res['time_shift'] = time_shift

    # Store v4E parameters
    res['EccFphiPNorder'] = EccFphiPNorder
    res['EccFrPNorder'] = EccFrPNorder
    res['EccWaveformPNorder'] = EccWaveformPNorder
    res['EccPNFactorizedForm'] = EccPNFactorizedForm
    res['EccBeta'] = EccBeta
    res['Ecct0'] = Ecct0
    res['EccNQCWaveform'] = EccNQCWaveform
    res['EccPNRRForm'] = EccPNRRForm
    res['EccPNWfForm'] = EccPNWfForm


    outfile=outdir+"{}_emismatches.json".format(simName)
    with open(outfile, 'w') as f:
        json.dump(res, f)

#############################################################

# Main program

#############################################################

#!/usr/bin/env/python3
import argparse


def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=0.0
    )

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=20,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )


    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-0.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )

    p.add_argument("--Ne",type=int,help="Number of points for 1D eccentricity maximization.",default=50)
    p.add_argument("--Nf",type=int,help="Number of points for 1D starting frequency maximization.",default=200)
    p.add_argument("--verbose",type=str,
            help="If True it produces a bunch of plots",default="False")

    p.add_argument("--run_dir", type=str, help="Running directory")

##################################################


    args = p.parse_args()

    phi_s = 0.0
    kappa_s = 0.0

    parameters = {}

    if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt":
        parameters["EccFphiPNorder"] = args.EccFphiPNorder_template
        parameters["EccFrPNorder"] = args.EccFrPNorder_template
        parameters["EccWaveformPNorder"] = args.EccWaveformPNorder_template
        parameters["EccPNFactorizedForm"] = args.EccPNFactorizedForm_template
        parameters["EccBeta"] = args.EccBeta_template
        parameters["Ecct0"] = args.Ecct0_template
        parameters["EccNQCWaveform"] = args.EccNQCWaveform_template

        parameters["EccPNRRForm"] = args.EccPNRRForm
        parameters["EccPNWfForm"] = args.EccPNWfForm

        parameters["EccAvNQCWaveform"] = args.EccAvNQCWaveform
        parameters["EcctAppend"] = args.EcctAppend


    elif args.approximant == "IMRPhenomXEv1":

        parameters["Nharmonic_template"] = args.Nharmonic_template
        parameters["kAdvance_template"] = args.kAdvance_template


    run_EccNRmismatch(
        phi_s,
        kappa_s,
        args.iota_s,
        args.NR_file,
        Ne = args.Ne,
        Nf = args.Nf,
        signal_approx=args.signal,
        template_approx=args.approximant,
        parameters=parameters,
        fhigh=2048.0,
        ellMax=args.ell_max,
        verbose = args.verbose,
        rundir = args.run_dir
    )



if __name__ == "__main__":
    main()
