#!/usr/bin/env python
import argparse
import glob
import os, numpy as np
import subprocess as sp
import uuid

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )
    p.add_argument("--signal", type=str, help="Approximant used for signal")
    p.add_argument("--template", type=str, help="Approximant to use for template")
    p.add_argument("--submit_time", type=str, help="Requested time for each chunk.", default="24:0:00")
    p.add_argument("--queue", type=str, help="Queue where to submit the jobsl.", default="nr")

    p.add_argument("--submit",action="store_true",help="Submit the jobs that have been created")

    p.add_argument("--ell_max", type=int, help="Maximum ell to use", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="Whether to minimize over reference  frequency or spin rotation. Applies only to Phenom waveforms",
        default="phi",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )

    p.add_argument(
        "--omega_min",
        type=float,
        help="Starting **orbital** frequency of waveform generation, in geometric units",
    )

    p.add_argument(
        "--qmin",
        type=float,
        help="Mass ratio lower bound (q>1).",
        default=1.0,
    )

    p.add_argument(
        "--qmax",
        type=float,
        help="Mass ratio upper bound (q>1).",
        default=20.0,
    )

    p.add_argument(
        "--eccentricity",
        type=float,
        help="Eccentricity",
        default=0.0,
    )

    p.add_argument(
        "--chi_min",
        type=float,
        help="Dimensionless spin z component lower bound (0<= chi_min <= 1)",
        default=0.0,
    )

    p.add_argument(
        "--chi_max",
        type=float,
        help="Dimensionless spin z component upper bound (0<= chi_max <= 1)",
        default=1.0,
    )

    p.add_argument(
        "--Nq",
        type=int,
        help="Number of iterations to perform for the mass ratio grid)",
        default=10,
    )

    p.add_argument(
        "--NN",
        type=int,
        help="Number of cases in the same SLURM script.",
        default=8,
    )

    p.add_argument(
        "--Nchi",
        type=int,
        help="Number of iterations to perform for the spin grid)",
        default=10,
    )

    p.add_argument("--equalSpins",action="store_true",help="Run the comparison with equal z-component of the dimensionless spins. Faster, less dimensions.")

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )

    args = p.parse_args()

    EccFphiPNorder = args.EccFphiPNorder_template
    EccFrPNorder = args.EccFrPNorder_template
    EccWaveformPNorder = args.EccWaveformPNorder_template
    EccPNFactorizedForm = args.EccPNFactorizedForm_template

    EccBeta_template = args.EccBeta_template
    Ecct0_template = args.Ecct0_template
    EccNQCWaveform_template = args.EccNQCWaveform_template
    EccPNRRForm = args.EccPNRRForm
    EccPNWfForm = args.EccPNWfForm

    EcctAppend = args.EcctAppend
    EccAvNQCWaveform = args.EccAvNQCWaveform

    signal = args.signal
    template = args.template
    omega_min = args.omega_min
    eccentricity = args.eccentricity

    script_dir =  os.environ['WAVEFORM_TOOLS_PATH']

    run_dir = os.path.abspath(args.run_dir)
    try:
        os.chdir(run_dir)
    except OSError:
        print("Could not go into the run directory {}, quitting!".format(run_dir))
        exit(-1)

    #script_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    script_dir =  os.environ['WAVEFORM_TOOLS_PATH']
    #with open(args.file_list, "r") as fp:
    #    lst = fp.readlines()
    if args.equalSpins:
        Ntotal = args.Nq * args.Nchi
    else:
        Ntotal = args.Nq * args.Nchi * args.Nchi

    qlist = np.linspace(args.qmin,args.qmax,args.Nq)

    spins = np.linspace(args.chi_min,args.chi_max,args.Nchi)
    chilist = np.column_stack((spins,spins))


    qchilist=[]

    for q in qlist:
        if args.equalSpins:
            for chi1,chi2 in chilist:
                qchilist.append([q,chi1,chi2])
        else:
            for chi1 in spins:
                for chi2 in spins:
                    qchilist.append([q,chi1,chi2])


    qchilist=np.array(qchilist)

    chk = list(chunks(qchilist, args.NN))

    submit_time = args.submit_time
    queue = args.queue

    header = """#!/bin/bash -
#SBATCH -J chunk_{}                 # Job Name
#SBATCH -o chunk_{}.stdout          # Output file name
#SBATCH -e chunk_{}.stderr          # Error file name
#SBATCH -n 16                 # Number of cores
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node
#SBATCH -p {}                 # Queue name
#SBATCH -t {}                # Run time
#SBATCH --no-requeue
#SBATCH --exclusive

#source /home/aramosbuades/load_LALenv.sh

cd {}
"""

    unq_name = uuid.uuid4().hex
    #root = "/work/sossokine/LVCFormatWaveforms"
    for i in range(len(chk)):
        nm = unq_name + "_{}".format(i)


        fp = open("chunk_{}.sh".format(i), "w")
        fp.write(header.format(i, i, i,  queue, submit_time, run_dir))
        for it, qchi_array  in enumerate(chk[i]):
            q,chi1,chi2 = qchi_array
    #            NR_file = "{}/{}".format(root, it.strip())
            cmd = """python {}/compare_wfs_v4E.py --signal {} --template {} --ell_max {} --q {} --eccentricity {} --chi1 0.,0.,{}  --chi2 0.,0.,{} --omega_min {} --min_type {} --unfaithfulness_type {} --EccFphiPNorder_template  {}  --EccFrPNorder_template  {} --EccWaveformPNorder_template  {} --EccPNFactorizedForm_template {} --EccBeta_template {} --Ecct0_template {} --EccNQCWaveform_template {} --EccPNRRForm {} --EccPNWfForm {} --EcctAppend {} --EccAvNQCWaveform {} --iota_s {} \n""".format(
                    script_dir,
                    signal,
                    template,
                    args.ell_max,
                    q,
                    eccentricity,
                    chi1,
                    chi2,
                    omega_min,
                    args.min_type,
                    args.unfaithfulness_type,
                    EccFphiPNorder,
                    EccFrPNorder,
                    EccWaveformPNorder ,
                    EccPNFactorizedForm ,
                    EccBeta_template,
                    Ecct0_template,
                    EccNQCWaveform_template,
                    EccPNRRForm,
                    EccPNWfForm,
                    EcctAppend,
                    EccAvNQCWaveform,
                    args.iota_s
                )
            fp.write(cmd)
        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(i)
        print(submit_cmd)

        if args.submit:
            sp.call(submit_cmd, shell=True)
        else:
            print(".sh files created. To also automatically submit, rerun with --submit")
