# Path to the SEOBNREv4 binary file
import os


# Populate the SEOBNREv4_PATH bash variable with the path to the SEOBNREv4 executable
binary_path = os.environ['SEOBNREv4_PATH']
output_dir = binary_path+"/output_data"

#binary_path = "/home/antoniramosbuades/git/seobnrv4e/seobnrv4e_code/"
#output_dir = "/home/antoniramosbuades/git/seobnrv4e/seobnrv4e_code/output_data"


# TEOBResumSE  python module
try:
    import EOBRun_module
except:
    pass
from waveform_tools.mismatch.waveform_parameters import waveform_params
import numpy as np
import lal, os, pandas as pd
import types

# Newtonina estimate of the eccentricity as a function of frequency
# used to estimate the eccentricity at the start of the frequency  band
def e0Newt(f: float, e0: float, f_ref: float) -> float:
    return e0 * pow(f / f_ref, -19.0 / 18.0)


# Function to parse the modes of TEOBResumSE to the linear index they use
def modes_to_k(modes):
    """
    Map multipolar (l,m) -> linear index k
    """
    return [int(x[0] * (x[0] - 1) / 2 + x[1] - 2) for x in modes]


# To make this function work visit:  https://bitbucket.org/eob_ihes/teobresums
def generate_TEOBResumSE_simple_waveform(p: waveform_params, f_max: float,  modes):

    mass_ratio = p.m1 / p.m2
    srate = np.round(1.0 / p.delta_t)
    chi1z = p.s1z# / p.m1 / p.m1
    chi2z = p.s2z# / p.m2 / p.m2
    k = modes_to_k(modes)
    pars = {
        "M": p.m1 + p.m2,
        "q": mass_ratio,
        "chi1": chi1z,
        "chi2": chi2z,
        "Lambda1": 0.0,
        "Lambda2": 0.0,
        "domain": 0,  # Set 1 for FD. Default = 0
        "arg_out": 1,  # Output hlm/hflm. Default = 0
        "use_mode_lm": k,  # List of modes to use/output through EOBRunPy
        "output_lm": k,  # List of modes to print on file
        #'srate'       : np.round(1.0/p.delta_t)*0.5, #srate at which to interpolate. Default = 4096.
        "srate_interp": srate,
        #'srate_interp'       : 1650.0, #srate at which to interpolate. Default = 4096.
        "use_geometric_units": 0,  # output quantities in geometric units. Default = 1
        "df": 0.01,  # df for FD interpolation
        "initial_frequency": p.f_min,  # in Hz if use_geometric_units = 0, else in geometric units
        "interp_uniform_grid": 1,  # interpolate mode by mode on a uniform grid. Default = 0 (no interpolation)
        "distance": p.distance / 1.0e6 / lal.PC_SI,  # Distance in Mpc
        "inclination": p.iota,
        # "dt"                 : p.delta_t,
        # "r0"                 : pow(p.f_min*np.pi , -2./3.),
        #'dt_interp'          : 0.5,
        "coalescence_angle": p.phi,
        "ecc": p.ecc,  # Eccentricity. Default = 0.
        'ecc_freq' : p.TEOB_IC,      #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation. Default = 1
    }
    t, hp, hc, hlm, dynamics = EOBRun_module.EOBRunPy(pars)

    # amp22 =  hlm['1'][0]

    return t, hp, hc, hlm


def generate_SEOBNREv4_waveform(p: waveform_params, f_max: float):
    iidd = os.getpid()
    outfile = output_dir + "/simulation_"+str(iidd)+".dat"

    srate = np.round(1.0 / p.delta_t)
    chi1z = p.s1z / p.m1 / p.m1
    chi2z = p.s2z / p.m2 / p.m2

    argsCommand = (
        "SEOBNRE -R "
        + str(srate)
        + " -f "
        + str(p.f_min)
        + " -F "
        + str(f_max)
        + " -r "
        + str(p.f_ref)
        + " \
                -M "
        + str(p.m1)
        + " -m "
        + str(p.m2)
        + " -Z "
        + str(chi1z)
        + " -z "
        + str(chi2z)
        + " -d "
        + str(p.distance / 1.0e6 / lal.PC_SI)
        + " -u "
        + str(p.phi)
        + " -i "
        + str(p.iota)
        + " -e "
        + str(p.ecc)
        + " > "
        + outfile
    )
    # We have removed f_max flag: +" -F "+str(p.fmax)

    cmd = binary_path + argsCommand
    so = os.popen(cmd).read()

    # Read generated output file for SEOBNREv4

    data_SEOBNREv4 = pd.read_csv(outfile, sep="\t", header=None)
    cols = [data_SEOBNREv4[0][0], data_SEOBNREv4[1][0], data_SEOBNREv4[2][0]]
    data_SEOBNREv4 = pd.read_csv(outfile, sep="\t", header=0)
    t = data_SEOBNREv4[cols[0]]
    h22NREv4 = data_SEOBNREv4[cols[1]] - 1.0j * data_SEOBNREv4[cols[2]]
    amp22 = np.abs(h22NREv4)
    h2m2NREv4 = np.conjugate(h22NREv4)

    #print("type p.phi = ", type(p.phi))
    if type(p.phi) is np.ndarray:
        phi=p.phi[0].astype(double)
        phi=p.phi[0].astype(np.float64)

    #    print("p.phi is a list ",p.phi ,", phi = ", phi)
    else:
        phi=p.phi
    #    print("p.phi =",p.phi)

    #print("iota = ", p.iota, ",   phi = ",phi)
    Y22 = lal.SpinWeightedSphericalHarmonic(0.0 + p.iota, 0.0 + 0.0 * np.pi / 2 - phi, -2, 2, 2)
    Y2m2 = lal.SpinWeightedSphericalHarmonic(0.0 + p.iota, 0.0 + 0.0 * np.pi / 2 - phi, -2, 2, -2)
    hstrain = Y22 * h22NREv4 + Y2m2 * h2m2NREv4
    hp = np.real(hstrain)
    hc = np.imag(hstrain)

    #print("outfile = ", outfile)
    os.remove(outfile)

    return t, hp, hc, amp22


def generate_TEOBResumSE_waveform(p: waveform_params):

    pars = {
        "M": p.m1 + p.m2,
        "q": p.m1 / p.m2,
        "chi1": p.s1z / p.m1 / p.m1,
        "chi2": p.s2z / p.m2 / p.m2,
        "Lambda1": 0.0,
        "Lambda2": 0.0,
        "domain": 0,  # Set 1 for FD. Default = 0
        "arg_out": 1,  # Output hlm/hflm. Default = 0
        "use_mode_lm": [1],  # List of modes to use/output through EOBRunPy
        "output_lm": [1],  # List of modes to print on file
        "srate_interp": np.round(
            1.0 / p.delta_t
        ),  # srate at which to interpolate. Default = 4096.
        #'srate_interp'       : 8192.0,#4096.0,
        #'srate_interp'       : 1650.0, #srate at which to interpolate. Default = 4096.
        "use_geometric_units": 0,  # output quantities in geometric units. Default = 1
        "df": 0.01,  # df for FD interpolation
        "initial_frequency": p.f_min,  # in Hz if use_geometric_units = 0, else in geometric units
        "interp_uniform_grid": 2,  # interpolate mode by mode on a uniform grid. Default = 0 (no interpolation)
        "distance": p.distance / 1.0e6 / lal.PC_SI,  # Distance in Mpc
        "inclination": p.iota,
        # "dt"                 : p.delta_t,
        # "r0"                 : pow(p.f_min*np.pi , -2./3.),
        #'dt_interp'          : 0.5,
        "coalescence_angle": -p.phi
        + np.pi / 2.0,  # This is because TEOBResumS defines phi=pi/2-coalescence_angle
        "ecc": p.ecc,  # Eccentricity. Default = 0.
        'ecc_freq' : 1,      #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation. Default = 1

    }

    # Sanity check
    assert (
        pars["arg_out"] == 1
    ), "Output the modes of TEOBResumSE to compute the peak time!"

    t, hp, hc, hlm, dynamics = EOBRun_module.EOBRunPy(pars)

    # Compute tPeak of TEOBResumSE waveform
    A22 = hlm["1"][0]
    A = 2.0 * (A22 ** 2)
    idx_max = np.argmax(A)
    t_max = t[idx_max]
    t_min = t[0]

    # Compute SEOBNRv4 waveform with the same parameters (e=0)

    hpv4, hcv4 = lalsim.SimInspiralChooseTDWaveform(
        p.m1 * lal.MSUN_SI,
        p.m2 * lal.MSUN_SI,
        p.s1x,
        p.s1y,
        p.s1z,
        p.s2x,
        p.s2y,
        p.s2z,
        p.distance,
        p.iota,
        p.phi,
        0.0,
        0.0,
        0.0,
        p.delta_t,
        p.f_min,
        p.f_ref,
        p.wf_param,
        lalsim.GetApproximantFromString("SEOBNRv4"),
    )
    times = np.arange(len(hpv4.data.data)) * hpv4.deltaT

    # Compute tPeak from a SEOBNRv4 (e=0) waveform
    Y22 = lal.SpinWeightedSphericalHarmonic(p.iota, p.phi, -2, 2, 2)
    Y2m2 = lal.SpinWeightedSphericalHarmonic(p.iota, p.phi, -2, 2, -2)
    h22LAL = (hpv4.data.data - 1.0j * hcv4.data.data) / (Y22 + Y2m2)

    amp22LAL = 2.0 * np.abs(h22LAL)
    idx_maxLAL = np.argmax(amp22LAL)
    tPeakEOB = times[idx_maxLAL]

    print("t_max = ", t_max)
    print("tPeakEOB = ", tPeakEOB)

    # Amount to shift the TEOBResumSE waveform
    Dt = np.abs(t_max - tPeakEOB)

    print("Dt = ", Dt)
    ## Shift TEOBResumSE waveform

    tshifted = t - Dt
    idx_shift = np.where(tshifted >= times[0])[0]
    tResumSe = tshifted[idx_shift]
    hpResumSe = hp[idx_shift]
    hcResumSe = hc[idx_shift]

    return tResumSe, hpResumSe, hcResumSe
