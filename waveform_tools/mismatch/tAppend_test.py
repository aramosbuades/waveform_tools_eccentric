#!/usr/python

import argparse
import glob, os ,sys
#import subprocess as sp

import lal, lalsimulation
import pandas as pd
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from joblib import delayed, Parallel


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from waveform_analysis  import *

plt.rcParams["figure.figsize"] = (14,10)
plt.rcParams['font.size'] = '18'


def mergerTest_ISCO(q:float, chi1 : float, chi2:float, eccentricity:float, M_fed:float, dMpc:float, f_min:float, delta_t:float,
                EccFphiPNorder : int, EccFrPNorder : int, EccWaveformPNorder : int, EccPNFactorizedForm : int,
                  EccBeta : float, Ecct0 : float, EccNQCWaveform : int, EccPNRRForm : int, EccPNWfForm : int, EccAvNQCWaveform: int, EcctAppend: float, run_dir: str,
                  output_waveform: str #,EccIC:int, HypPphi0: float, HypR0:float, HypE0:float
                ):

    #print(q, chi1, chi2, eccentricity, M_fed, dMpc, f_min, delta_t, EccFphiPNorder, EccFrPNorder, EccWaveformPNorder, EccPNFactorizedForm ,  EccBeta , Ecct0 , EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform, EcctAppend, run_dir)

    m1_dim = q / (1.0 + q)
    m2_dim = 1 - m1_dim
    eta= q/(1+q)**2

    EccIC = 0
    #HypPphi0, HypR0, HypE0 =[0.,0,0]

    timeNRv4E, amp22v4E, omega22v4E = SEOBNRv4E_tAmpfreq(q, chi1, chi2, eccentricity, dMpc, f_min, M_fed, delta_t,
                                       EccFphiPNorder, EccFrPNorder, EccWaveformPNorder, EccPNFactorizedForm,
                                                         EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm, EccAvNQCWaveform, EcctAppend,
                                                         EccIC)#,  HypPphi0, HypR0, HypE0)

    iamp22v4E = InterpolatedUnivariateSpline(timeNRv4E, amp22v4E)
    iomega22v4E = InterpolatedUnivariateSpline(timeNRv4E, omega22v4E)

    ISCO_file = glob.glob(run_dir+'tISCO*dat')
    #sys.stdout.write(run_dir+'tISCO*dat '+ISCO_file[0]+' \n')
    #print(ISCO_file)
    if ISCO_file != []:
        try:
            data=pd.read_table(ISCO_file[0], sep="\s+",header=None,engine='python')
            tISCO = data[0]
            ecc_Last2Osc =  data[1]
            t_ecc =  data[2]
        except:
            tISCO=-1
            t_ecc = -1
            ecc_Last2Osc =-1
            pass
    else:
        tISCO=-1.
        t_ecc = -1
        ecc_Last2Osc =-1
    #tISCO=-1

    timeNRv4, amp22v4, omega22v4 = SEOBNRv4_tAmpfreq(q, chi1, chi2, dMpc, f_min, M_fed, delta_t)
    iamp22v4 = InterpolatedUnivariateSpline(timeNRv4, amp22v4)
    iomega22v4 = InterpolatedUnivariateSpline(timeNRv4, omega22v4)



    tt = timeNRv4E[(timeNRv4E > tt0) & (ttf >= timeNRv4E )]
    ff_amp22v4E = iamp22v4E(tt)
    ff_omega22v4E = iomega22v4E(tt)

    ff_amp22v4 = iamp22v4(tt)
    ff_omega22v4 = iomega22v4(tt)


    # Compute the absolute difference
    absdiffOmega = np.abs(ff_omega22v4E - ff_omega22v4)
    absdiffAmp = np.abs(ff_amp22v4E - ff_amp22v4)

    maxabsdiffAmp = max(absdiffAmp)
    maxabsdiffOmega = max(absdiffOmega)

    absreldiffOmega = np.abs(ff_omega22v4E/ff_omega22v4-1)
    absreldiffAmp = np.abs(ff_amp22v4E/ff_amp22v4-1)

    maxabsreldiffOmega = max(absreldiffOmega)
    maxabsreldiffAmp = max(absreldiffAmp)

    l2normdiffOmega = np.sqrt(np.abs(ff_omega22v4E*ff_omega22v4E-ff_omega22v4*ff_omega22v4))
    l2normdiffAmp = np.sqrt(np.abs(ff_amp22v4E*ff_amp22v4E-ff_amp22v4*ff_amp22v4))

    maxl2normdiffOmega = max(l2normdiffOmega)
    maxl2normdiffAmp = max(l2normdiffAmp)

    imaxAmp =np.argmax(absdiffAmp)
    imaxOmega =np.argmax(absdiffOmega)

    tmaxAmp = tt[imaxAmp]
    tmaxOmega = tt[imaxOmega]

    #diffOmega_list.append(maxdiffOmega)
    #diffAmp_list.append(maxdiffAmp)
    #if maxabsreldiffOmega > diffthreshold:

    fminTag=np.round(f_ref,3)
    qTag = np.round(q,3)
    eTag = np.round(eccentricity,3)
    chi1Tag =  np.round(chi1,3)
    chi2Tag =  np.round(chi2,3)
    tAppTag = EcctAppend

    title=r'SEOBNRv4E $ \quad q='+str(q)+'$, $\quad f_{min}=$'+str(fminTag)+'Hz, $\quad M_{tot}=$'+str(M_fed)+\
                    '$M_\odot, \quad \chi1 =$'+str(chi1Tag)+'$, \quad \chi2 =$'+str(chi2Tag)+'$  \quad  e =$'+str(eTag)


    #l2Int = (ff_omega22v4-ff_omega22v4E)*(ff_omega22v4-ff_omega22v4E)
    #il2Int = InterpolatedUnivariateSpline(tt,l2Int)

    #integral=[]
    #for tt1 in tt:
    #    integral.append(il2Int.integral(tt[0],tt1))

    #Sqrt[1/(t_f-t_i) Integrate[|omega22_v4E/omega22_v4-1|^2,{t,0,tf}]]
    #Or even simpler Sqrt[Sum[relative_error^2]/N]?

    term=(ff_omega22v4E/ff_omega22v4-1)**2.0;
    iterm=InterpolatedUnivariateSpline(tt,term)

    integral=[]
    for tt1 in tt:
        if tt1 != tt[-1]:
            integrand=(1./(tt[-1]-tt1) )* iterm.integral(tt1,tt[-1])
            integral.append( np.sqrt(np.abs(integrand)))

    integral=np.array(integral)


    l2norm=np.sqrt((1./(tt[-1]-tt[0]) )* iterm.integral(tt[0],tt[-1]))



    if output_waveform =="True":

        dataFile=plotdir+'wf_q'+str(qTag)+'_chi1'+str(chi1Tag)+'_chi2'+str(chi2Tag)+'_e'+str(eTag)+'_tApp_'+str(EcctAppend)+'.h5'

        hf = h5py.File(dataFile, 'w')
        hf.create_dataset('q', data=q)
        hf.create_dataset('chi1', data=chi1)
        hf.create_dataset('chi2', data=chi2)
        hf.create_dataset('eccentricity', data=eccentricity)
        hf.create_dataset('EcctAppend', data=EcctAppend)

        hf.create_dataset('timeNRv4', data=timeNRv4)
        hf.create_dataset('amp22v4', data=amp22v4)
        hf.create_dataset('omega22v4', data=omega22v4)

        hf.create_dataset('timeNRv4E', data=timeNRv4E)
        hf.create_dataset('amp22v4E', data=amp22v4E)
        hf.create_dataset('omega22v4E', data=omega22v4E)

        hf.create_dataset('absreldiffOmega', data=absreldiffOmega)
        hf.create_dataset('absreldiffAmp', data=absreldiffAmp)
        hf.create_dataset('l2norm', data=l2norm)
        hf.create_dataset('tt', data=tt)


        hf.close()

    ###################################################################
    # Plot the amplitude22 and omega22

    # Plot omega22
    plt.figure()

    plt.plot(timeNRv4, omega22v4, label=r'v4 $\omega_{22}$',linestyle='solid')
    plt.plot(timeNRv4E, omega22v4E, label=r'v4E $\omega_{22}$',linestyle='dashed')#, color= 'k')

    # Plot amplitude22
    plt.plot(timeNRv4, amp22v4,label=r'v4 $|h_{22}| $',linestyle='solid')
    plt.plot(timeNRv4E, amp22v4E,label=r'v4E $|h_{22}| $',linestyle='dashed')#, color= 'k')


    plt.legend(loc='best', prop={'size': 20}, ncol=2)
    plt.xlabel('t/M',fontsize=20)
    plt.ylabel(r'',fontsize=20)
    plt.title(title,fontsize=20)
    plt.grid(b=None)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)

    plt.xlim(-700, ttf+10)
    plt.axvline(x=tt0,linestyle='dashed',color='black')
    plt.axvline(x=ttf,linestyle='dashed',color='black')
    plt.ylim(0.001,1.0)
    plt.savefig(plotdir+'OmegaAmpWaveform_q'+str(qTag)+'_'+str(chi1Tag)+'_'+str(chi2Tag)+'_e'+str(eTag)+'_tApp'+str(tAppTag)+'_zoom.png')

    plt.close()


    # Plot the amplitude and omega difference
    plt.figure()

    plt.plot(tt, absreldiffOmega, label=r"$X=\omega_{22}$",linestyle='solid',color='blue')
    plt.plot(tt, absreldiffOmega, label=r"$X=|h_{22}|$",linestyle='dashed',color='red')

    plt.legend(loc='best', prop={'size': 20}, ncol=1)
    plt.xlabel(r't/M',fontsize=20)
    plt.ylabel(r'$X^{v4E}/X^{v4}-1 $',fontsize=20)
    plt.axvline(x=tt0,linestyle='dashed',color='black')
    plt.axvline(x=ttf,linestyle='dashed',color='black')

    plt.title(title,fontsize=18)
    plt.grid(b=None)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)

    plt.xlim(tt0, ttf)
    #plt.savefig(plotdir+'diffOmegaAmp_q'+str(q)+'_chi'+str(chi1)+'_e'+str(eccentricity)+'_zoom.png')
    #plt.show()
    plt.savefig(plotdir+'diffOmegaAmp_q'+str(qTag)+'_chi1_'+str(chi1Tag)+'_chi2_'+str(chi2Tag)+'_e'+str(eTag)+'_tApp'+str(tAppTag)+'_zoom.png')

    #plt.show()
    plt.close()

    return  q, chi1, chi2, eccentricity, M_fed, f_min, tmaxOmega, tmaxAmp, maxabsdiffOmega, maxabsdiffAmp, maxabsreldiffOmega, maxabsreldiffAmp, l2norm, EcctAppend, tISCO, t_ecc ,  ecc_Last2Osc



if __name__ == "__main__":
    p = argparse.ArgumentParser()

    # Arguments of the test
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Ne",type=int,help="Number of eccentricity points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)
    p.add_argument("--NtApp",type=int,help="Number of chi points",default=10)



## SEOBNRv4E arguments

    p.add_argument(
        "--EccFphiPNorder",
        type=int,
        help="EccFphiPNorder only for SEOBNRv4E",
        default=27,
    )

    p.add_argument(
        "--EccFrPNorder",
        type=int,
        help="EccFrPNorder only for SEOBNRv4E",
        default=27,
    )

    p.add_argument(
        "--EccWaveformPNorder",
        type=int,
        help="EccWaveformPNorder for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm",
        type=int,
        help="EccPNFactorizedForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta",
        type=float,
        help="EccBeta only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0",
        type=float,
        help="This currently has no effect",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform",
        type=int,
        help="EccNQCWaveform only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )


    p.add_argument("--eccMin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccMax", type=float,  help="Maximum eccentricity", default=0.7, )

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )


    p.add_argument("--tAppMin", type=float,  help="Minimum tAppend", default=-70.0, )
    p.add_argument("--tAppMax", type=float,  help="Maximum tAppend", default=5.0, )
    p.add_argument("--output_waveform", type=str,  help="Whether to output the waveformd", default="False", )


    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)


    # Plot dir
    plotdir = run_dir+"/plots"
    os.makedirs(plotdir, exist_ok=True)

    if run_dir[-1] != "/":
        run_dir=run_dir+"/"

    #print(run_dir)
    if plotdir[-1] != "/":
        plotdir=plotdir+"/"

    data_dir = run_dir+"/data"
    os.makedirs(data_dir, exist_ok=True)

    if data_dir[-1] != "/":
        data_dir=data_dir+"/"


    ## Set some Parameters
    output_waveform= args.output_waveform
    # PN orders of the eccentric corrections
    EccWaveformPNorder = args.EccWaveformPNorder  # Full 2PN order in the WF
    EccPNFactorizedForm = args.EccPNFactorizedForm

    EccFphiPNorder = args.EccFphiPNorder # Full 2PN order
    EccFrPNorder = args.EccFrPNorder  # 2PN order without the tail term

    # Use older expressions
    EccPNRRForm = args.EccPNRRForm
    EccPNWfForm = args.EccPNWfForm

    EccAvNQCWaveform = args.EccAvNQCWaveform

    EccIC=0
    HypPphi0 = 0.0
    HypR0 = 0.0
    HypE0 = 0.0

    # Set some extrinsic parameters
    phi_s = 0.
    kappa_s =0.
    iota_s = 0.

    # Choose a total mass
    M_fed = 60.0

    # Choose a reference frequency at which to define eccentricity
    f_ref = 20.0
    f_min = 1 * f_ref
    #print(" fmin  = ", f_min, ",   Momega_min = ",f_ref *M_fed *np.pi*lal.MTSUN_SI)

    dMpc=100.0
    distance = dMpc*1e6*lal.PC_SI
    delta_t=1/(4096.0)
    delta_t=1/(2048.0) # to reduce computational cost

    # Parameters of the sigmoid
    EccBeta = args.EccBeta
    Ecct0 = args.Ecct0
    EccNQCWaveform = args.EccNQCWaveform

    # Set boundary for making the test
    Ne = args.Ne
    ecc_max = args.eccMax
    ecc_min = args.eccMin
    ecc_list = np.linspace(ecc_min,ecc_max,Ne)

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    q_list = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    chi_list = np.linspace(chi_min, chi_max, Nchi)


    NtApp =  args.NtApp
    tApp_max = args.tAppMax
    tApp_min = args.tAppMin
    tAppend_grid = np.linspace(tApp_min, tApp_max, NtApp)


    tAppend_grid = [100.,90.,80.,70.,60.,50.,40.,30.,20.,15.,10.,5.]

    # Thresholds for the test
    tt0 = - 100
    ttf = 50.0
    diffthreshold = 0.2

    result_q0=[]

    x, y1, y2, z, t = np.meshgrid(q_list, chi_list,chi_list, ecc_list,tAppend_grid)

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()
    t = t.flatten()


    result= Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
            delayed(mergerTest_ISCO)(
                x[i],
                y1[i],
                y2[i],
                z[i],
                M_fed,
                dMpc,
                f_min,
                delta_t,
                EccFphiPNorder,
                EccFrPNorder,
                EccWaveformPNorder,
                EccPNFactorizedForm,
                EccBeta,
                Ecct0,
                EccNQCWaveform,
                EccPNRRForm,
                EccPNWfForm,
                EccAvNQCWaveform,
                t[i],
                run_dir,
                output_waveform
            )
            for i in range(len(x))
            )


    result = np.array(result)

    np.savetxt("tAppendTest_Nq{}_Ne{}_Nchi{}_result.dat".format(Nq,Ne,Nchi), result)
