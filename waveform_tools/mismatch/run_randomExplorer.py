#!/usr/python
# Load module
import sys, h5py, glob,os

#binary_path="/home/antoniramosbuades/git/seobnrv4e/seobnrv4e_code/"
#output_dir="."
#outfile=output_dir+"/simulation.dat"

#sys.path.append("/home/antoniramosbuades/git/waveform_tools_eccentric/waveform_tools/mismatch/")
sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])
#sys.path.append("/home/antoniramosbuades/git/teobresumse/Python/")

import matplotlib
from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSe module
#import EOBRun_module
import matplotlib.pyplot as plt, numpy as np
import pandas as pd
import pycbc, json

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *


def random_explorer(q0:float,chi1:float,chi2:float, ecc:float, omega_min:float,
                          delta_t:float):


    EccPNFactorizedForm = 1
    EccFphiPNorder = 99
    EccFrPNorder = 99
    EccWaveformPNorder = 16
    EccAvNQCWaveform=1
    EcctAppend=20
    EccPNRRForm =  EccPNWfForm = 1
    EccBeta = 0.09
    Ecct0 = 100
    EccNQCWaveform = 1
    EccIC=0

    test=1
    try:
        timeNRv4E, tHorizonv4E, hlmv4E, amplmv4E, phaselmv4E, omegalmv4E, \
        omega_orbv4E, phase_orbv4E =  SEOBNRv4E_modes(q0,chi1,chi2, ecc, omega_min, delta_t,
                                                      EccFphiPNorder,EccFrPNorder, EccWaveformPNorder,
                                                      EccPNFactorizedForm,
                                                      EccBeta, Ecct0, EccNQCWaveform, EccPNRRForm, EccPNWfForm,
                                                      EccAvNQCWaveform,EcctAppend, EccIC)
    except:
        test=0
        pass


    #print(q0, chi1, chi2, ecc)
    return q0, chi1, chi2, ecc, test


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument(
        "--outdir",
        type=str,
        help="Directory where to store the output",
        default=os.getcwd(),
    )

    p.add_argument("--Npoints",type=int,help="Number of points in random exploration",default=10)

    p.add_argument("--eccmin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccmax", type=float,  help="Maximum eccentricity", default=0.3, )

    p.add_argument("--qmin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qmax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chimin", type=float,  help="Minimum chi (chi1 != chi2)", default=-0.99, )
    p.add_argument("--chimax", type=float,  help="Maximum chi (chi1 != chi2)", default=0.99, )
    p.add_argument("--seed", type=int,  help="Random seed to use for the random distributions", default=111, )


    p.add_argument("--Nchunks",type=int,help="Total number of cases in each submission script",default=50000)
    p.add_argument("--Nout",type=int,help="Number of iterations to produce output",default=500)
    p.add_argument("--debug",action="store_true",help="Debug mode")

    p.add_argument("--idx_chunk",type=int,help="Subset of cases to run",default=0)
    args = p.parse_args()


    NN=args.Npoints #Number of points over which t loop
    qmin=args.qmin
    qmax=args.qmax
    chimin=args.chimin
    chimax=args.chimax
    emin=args.eccmin
    emax=args.eccmax
    Nchunks = args.Nchunks
    Nout = args.Nout
    seed=args.seed
    debug=args.debug

    outdir=args.outdir
    if outdir[-1]!="/":
        outdir +='/'

    os.makedirs(outdir,exist_ok=True)

    np.random.seed(seed)
    q_list = np.random.uniform(qmin,qmax,NN)
    np.random.seed(seed+999998)
    chi1_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+999997)
    chi2_list = np.random.uniform(chimin,chimax,NN)
    np.random.seed(seed+99999)
    ecc_list = np.random.uniform(emin,emax,NN)


    x, y1, y2, z = q_list, chi1_list,chi2_list, ecc_list

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()


    idx_list = np.arange(0,NN)

    n_slurm_jobs = int(NN/Nchunks)
    cases_per_slurm_job = list(chunks(idx_list, Nchunks))

    idx_chunk=args.idx_chunk

    case0=cases_per_slurm_job[idx_chunk]
    cases_per_output = list(chunks(case0,Nout))



    # For the test we set
    M_fed=80.0
    f_min=20.0

    freq_1M= M_fed*f_min
    omega_min= freq_1M*(np.pi*lal.MTSUN_SI)
    #print(omega_min)
    delta_t=1./(2*2048.)

    k=0
    for case in cases_per_output:

        result = Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(random_explorer)(
            x[i],
            y1[i],
            y2[i],
            z[i],
            omega_min,
            delta_t)
        for i in case
        )

        result = np.array(result)

        np.savetxt(outdir+"randExplorer_seed{}_Npoints{}_k{}_idx{}_result.dat".format(seed,NN,k,idx_chunk), result)
        k+=1
