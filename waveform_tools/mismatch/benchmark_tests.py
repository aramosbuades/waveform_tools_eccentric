#!/usr/python

import argparse
import glob, os ,sys, math, numpy as np
#import subprocess as sp

import lal, lalsimulation as lalsim
import pandas as pd
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from joblib import delayed, Parallel
import time


import random

# Simply run ChooseTDWaveforn



if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )

    p.add_argument("--Npoints",type=int,help="Number of points in random exploration",default=10)

    p.add_argument("--Nq",type=int,help="Number of mass ratio points",default=10)
    p.add_argument("--Ne",type=int,help="Number of eccentricity points",default=10)
    p.add_argument("--Nchi",type=int,help="Number of chi points",default=10)


    p.add_argument("--eccMin", type=float,  help="Minimum eccentricity", default=0., )
    p.add_argument("--eccMax", type=float,  help="Maximum eccentricity", default=0.7, )

    p.add_argument("--qMin", type=float,  help="Minimum q", default=1.0, )
    p.add_argument("--qMax", type=float,  help="Maximum q", default=20., )

    p.add_argument("--chiMin", type=float,  help="Minimum chi (chi1=chi2)", default=-1.0, )
    p.add_argument("--chiMax", type=float,  help="Maximum chi (chi1=chi2)", default=1.0, )

    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )

    p.add_argument(
        "--EccFphiPNorder",
        type=int,
        help="EccFphiPNorder only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder",
        type=int,
        help="EccFrPNorder only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder",
        type=int,
        help="EccWaveformPNorder only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm",
        type=int,
        help="EccPNFactorizedForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccBeta",
        type=float,
        help="EccBeta only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0",
        type=float,
        help="Ecct0 only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform",
        type=int,
        help="EccNQCWaveform only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNRRForm",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )



    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)

    os.makedirs(run_dir,exist_ok=True)
    os.chdir(run_dir)


    EccFphiPNorder = args.EccFphiPNorder
    EccFrPNorder = args.EccFrPNorder
    EccWaveformPNorder = args.EccWaveformPNorder
    EccPNFactorizedForm = args.EccPNFactorizedForm
    EccBeta = args.EccBeta
    Ecct0 = args.Ecct0
    EccNQCWaveform = args.EccNQCWaveform
    EccPNWfForm = args.EccPNWfForm
    EccPNRRForm = args.EccPNRRForm
    EcctAppend = args.EcctAppend
    EccAvNQCWaveform = args.EccAvNQCWaveform


    # Set boundary for making the test
    Ne = args.Ne
    ecc_max = args.eccMax
    ecc_min = args.eccMin
    ecc_list = np.linspace(ecc_min,ecc_max,Ne)

    Nq = args.Nq
    q_max = args.qMax
    q_min = args.qMin
    q_list = np.linspace(q_min, q_max, Nq)

    Nchi =  args.Nchi
    chi_max = args.chiMax
    chi_min = args.chiMin
    chi_list = np.linspace(chi_min, chi_max, Nchi)


    LAL_params = lal.CreateDict()

    lalsim.SimInspiralWaveformParamsInsertEccFphiPNorder(LAL_params, EccFphiPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccFrPNorder(LAL_params, EccFrPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccWaveformPNorder(LAL_params, EccWaveformPNorder)
    lalsim.SimInspiralWaveformParamsInsertEccPNFactorizedForm(LAL_params, EccPNFactorizedForm)

    lalsim.SimInspiralWaveformParamsInsertEccBeta(LAL_params, EccBeta)
    lalsim.SimInspiralWaveformParamsInsertEcct0(LAL_params, Ecct0)
    lalsim.SimInspiralWaveformParamsInsertEccNQCWaveform(LAL_params, EccNQCWaveform)

    lalsim.SimInspiralWaveformParamsInsertEccPNRRForm(LAL_params, EccPNRRForm)
    lalsim.SimInspiralWaveformParamsInsertEccPNWfForm(LAL_params, EccPNWfForm)
    lalsim.SimInspiralWaveformParamsInsertEcctAppend(LAL_params, EcctAppend)
    lalsim.SimInspiralWaveformParamsInsertEccAvNQCWaveform(LAL_params, EccAvNQCWaveform)

    # fixed parameters
    distance = lal.PC_SI*100*1e6
    inclination = 1.5
    phiRef   = 0.
    fRef_In  = 20.

    f_min = 20.
    f_max = 2048.

    f_low = 20.
    f_startTD = f_low
    f_ref = f_startTD
    sample_rate = 2*4096
    deltaT=1./sample_rate
    distance=1000000*lal.PC_SI*1 #distance to the source

    longAscNodes  = lal.PI*0.5
    meanPerAno   = 0.

    Npoints = args.Npoints
    chi1r=[-0.9,0.9]
    chi2r=[-0.9,0.9]
    etar=[0.25,0.08]
    eccr = [0,0.3]
    solarMasses=[20.,200.]
    inclinationr=[0,lal.PI]

    Mtot = 60.


    x, y1, y2, z = np.meshgrid(q_list, chi_list,chi_list, ecc_list)

    x = x.flatten()
    y1 = y1.flatten()
    y2 = y2.flatten()
    z = z.flatten()
    #print(len(x))


    model="SEOBNRv4E_opt"

    start = time.time()


    for i in range(len(x)):

        q = x[i]
        chi1    = y1[i]
        chi2    = y2[i]

        #logMass = random.uniform(math.log(solarMasses[0]), math.log(solarMasses[1]))
        inclination = 0.
        eccentricity    = z[i]

        #Mtot = math.exp(logMass)
        m1   = q/(1.+q)*Mtot
        m2   = 1./(1.+q)*Mtot

        sys.stdout.write('%i   %.10f   %.10f  %.10f  %.10f \n' % (i, q, chi1, chi2, eccentricity))
        sys.stdout.flush()



        m1_SI = m1 * lal.MSUN_SI
        m2_SI = m2 * lal.MSUN_SI


        """print("Parameters : ",[m1_SI,
            m2_SI,
            chi1,
            chi2,
            distance,
            inclination,
            phiRef,
            longAscNodes,
            eccentricity,
            meanPerAno,
            deltaT,
            f_startTD,
            f_ref],"\n")
        """

        hp, hc = lalsim.SimInspiralChooseTDWaveform(
        m1_SI,
        m2_SI,
        0.,
        0.,
        chi1,
        0.,
        0.,
        chi2,
        distance,
        inclination,
        phiRef,
        longAscNodes,
        eccentricity,
        meanPerAno,
        deltaT,
        f_startTD,
        f_ref,
        LAL_params,
        lalsim.GetApproximantFromString(model)
        )

    end = time.time()
    duration = end-start
    units = 's'
    if duration>60:
        duration = duration/60
        units = 'min'
        if duration>60:
            duration = duration/60
            units = 'h'

    sys.stdout.write('%.4f %s for %i evaluations' %(duration, units, len(x)))
