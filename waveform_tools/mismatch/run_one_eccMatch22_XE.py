#!/usr/bin/env/python

# Script to run the mismatches over eccentric waveforms
import sys, h5py, glob, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])

from pycbc.types import TimeSeries
import pycbc.types as pt
import pycbc.waveform as pw
from pycbc.waveform import td_approximants, fd_approximants
from pycbc.waveform.utils import taper_timeseries
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
import pycbc.filter as _filter
from pycbc.filter import make_frequency_series

from scipy.optimize import root_scalar, brute, dual_annealing, minimize, minimize_scalar

from auxillary_funcs import *
from waveform_parameters import waveform_params
from unfaithfulness_ecc import *
from eccentric_waveforms import *

# import LALsuite
import lal
import lalsimulation as lalsim

# TEOBResumSE module
try:
    import EOBRun_module
except:
    pass

import numpy as np, pandas as pd, pycbc, json

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.signal import argrelextrema
from waveform_analysis import *


#############################################################

# Main program

#############################################################

#!/usr/bin/env/python3
import argparse


def mm_fixedtotalMass_varykN(params_signal:waveform_params,
                        params_template:waveform_params, M_fed: float,
                     m1_dim: float, m2_dim: float, freq_1M:float, freq_1M_new: float, fhigh: float,
                      modes_dc: bool=False, debug: bool=False,
                     template_approx: str ="IMRPhenomXEv1",
                     NR_file: str="None",
                     ellMax: int=2,
                     kappa_s: float = 0.0
                     ):

    min_func = minimize_Eccunfaithfulness

    e0max_template = 0.2
    bounds = ((0, e0max_template), (0, 2 * np.pi))
    unfaithfulness_type = "unfaithfulness_RC"
    minimization_type = "eccentricity_meanAnomaly_22mode"

    LAL_params_signal = lal.CreateDict()
    ma = lalsim.SimInspiralCreateModeArray()

    # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
    if (
        template_approx == "SEOBNRv4"
        or template_approx == "TEOBResumSE"
        or template_approx == "SEOBNREv4"
        or template_approx == "SEOBNRv4E"
        or template_approx == "SEOBNRv4E_opt"
        or template_approx == "IMRPhenomXEv1"
    ):
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
    else:
        for ell in range(2, ellMax + 1):
            lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
    lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
    lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)

    params_signal.wf_param=LAL_params_signal

    #print(" M_fed = ", M_fed)


    ################### Actual calculation
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    f_ref = freq_1M / (m1 + m2)

    if f_ref<10.0:
        f_ref=10.0
    f_min = 1 * f_ref

    fmin_start = freq_1M_new/ (m1 + m2)
    if fmin_start< 10.0:
        fmin_start = 10.0

    # Update signal parameters
    params_signal.m1 = m1
    params_signal.m2 = m2
    params_signal.f_min = f_min
    params_signal.f_ref = f_ref


    # Update template parameters
    #f_ref_t= omega_min_opt/(m1+m2)
    #f_min_t =  f_ref_t

    params_template.m1 = m1
    params_template.m2 = m2

    params_template.f_min = fmin_start
    params_template.f_ref = fmin_start

    sp, sc = generate_waveform(params_signal)
    s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)


    if isinstance(s, pt.TimeSeries):
        N = len(s)
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        s.resize(pad)
    s_tilde = make_frequency_series(s)

    """
    amp_signal_FD=np.abs(s_tilde.data)
    imax= np.argmax(amp_signal_FD)
    freq_peak=s_tilde.get_sample_frequencies()[imax]
    flow=1.2*freq_peak
    """

    flow=1.*fmin_start

    if flow < 10:
        flow = 10

    psd = generate_psd(len(s_tilde), s_tilde.delta_f, flow)
    SNR = _filter.sigma(
        s_tilde, psd=psd, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh
    )

    #final = unfaithfulness_RC(s, params_template,  flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)


    params_template.ecc= 0.
    mm_e0 = unfaithfulness_RC(s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)

    params_template.ecc= 0.
    mm_e0 = unfaithfulness_RC(s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug)



    # k = 1, N = 7
    Nharmonic=7
    kAdvance= 1
    LAL_params_template = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template, Nharmonic)
    lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)



    final = dual_annealing(
                        min_func,
                        bounds,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            minimization_type,
                        ),
                        local_search_options={"method": "Nelder-Mead"},
                        maxfun=200,
                    )

    emin_opt_k1_N7, lmin_opt_k1_N7 = final.x
    mm_ecc_k1_N7 = final.fun




    # k = 1, N = 2
    Nharmonic=2
    kAdvance= 1
    LAL_params_template = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template, Nharmonic)
    lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)

    params_template.wf_param = LAL_params_template

    final = dual_annealing(
                        min_func,
                        bounds,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            minimization_type,
                        ),
                        local_search_options={"method": "Nelder-Mead"},
                        maxfun=200,
                    )


    emin_opt_k1_N2, lmin_opt_k1_N2 = final.x
    mm_ecc_k1_N2 = final.fun




    # k = 0, N = 2
    Nharmonic = 2
    kAdvance= 0
    LAL_params_template = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template, Nharmonic)
    lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)


    params_template.wf_param = LAL_params_template
    final = dual_annealing(
                        min_func,
                        bounds,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            minimization_type,
                        ),
                        local_search_options={"method": "Nelder-Mead"},
                        maxfun=200,
                    )

    emin_opt_k0_N2, lmin_opt_k0_N2 = final.x
    mm_ecc_k0_N2 = final.fun



    # k = 0, N = 7
    Nharmonic = 7
    kAdvance= 0
    LAL_params_template = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template, Nharmonic)
    lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)

    params_template.wf_param = LAL_params_template
    final = dual_annealing(
                        min_func,
                        bounds,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            minimization_type,
                        ),
                        local_search_options={"method": "Nelder-Mead"},
                        maxfun=200,
                    )


    emin_opt_k0_N7, lmin_opt_k0_N7 = final.x
    mm_ecc_k0_N7 = final.fun


    return [mm_e0, mm_ecc_k1_N7, mm_ecc_k0_N7, mm_ecc_k1_N2, mm_ecc_k0_N2, emin_opt_k1_N7, emin_opt_k0_N7, emin_opt_k1_N2, emin_opt_k0_N2,  lmin_opt_k1_N7, lmin_opt_k0_N7, lmin_opt_k1_N2, lmin_opt_k0_N2, SNR]


def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=0.0
    )
    p.add_argument(
        "--delta_f_template", type=float, help="Delta_f for the template", default=0.00125
    )

    """p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )
    """

    #p.add_argument("--Ne",type=int,help="Number of points for 1D eccentricity maximization.",default=50)
    #p.add_argument("--Nf",type=int,help="Number of points for 1D starting frequency maximization.",default=200)
    #p.add_argument("--deltaEcc",type=float,help="Interval of increasing eccentricity range.",default=0.1)
    #p.add_argument("--deltaF",type=float,help="Interval of increasing starting frequency range.",default=10.)

    #p.add_argument("--verbose",type=str,
    #        help="If True it produces a bunch of plots",default="False")

    p.add_argument("--run_dir", type=str, help="Running directory")

    ##################################################


    args = p.parse_args()

    phi_s = 0.0
    kappa_s = 0.0

    parameters = {}

    """
    if args.approximant == "IMRPhenomXEv1":

        parameters["Nharmonic_template"] = args.Nharmonic_template
        parameters["kAdvance_template"] = args.kAdvance_template
    """

    iota_s = args.iota_s
    NR_file = args.NR_file

    signal_approx=args.signal
    template_approx=args.approximant

    fhigh=2048.0
    ellMax=args.ell_max
    #verbose = args.verbose

    rundir = args.run_dir
    if rundir == None:
        rundir = "."
    else:
        if rundir[-1]!= '/':
            rundir = rundir+'/'


    outdir = rundir+"/mm_files/"
    os.makedirs(outdir, exist_ok=True)

    # Generic bounds on the initial eccentricity  of each model, below we use also the Newtonian estimate at the starting
    # frequency to set a maximum allowed eccentricity
    if template_approx == "TEOBResumSE":
        e0max_template = 0.3
    elif template_approx == "TEOBResumSEHM":
        e0max_template = 0.3
    elif template_approx == "SEOBNREv4":
        e0max_template = 0.7
    elif template_approx == "SEOBNRv4E":
        e0max_template = 0.3
    elif template_approx == "SEOBNRv4E_opt":
        e0max_template = 0.3
    elif template_approx == "SEOBNRv4EHM_opt":
        e0max_template = 0.3
    elif template_approx == "IMRPhenomXEv1":
        e0max_template = 0.2
    else:
        e0max_template = 0.0


    # Figure out the domains of the signal and the template
    td_approxs = td_approximants()
    fd_approxs = fd_approximants()
    if signal_approx not in fd_approxs  or signal_approx == "SEOBNRv4":
        signal_domain = "TD"
        delta_t_signal = 1.0 / 8192
        delta_f_signal = None
    else:
        signal_domain = "FD"
        delta_f_signal = 0.125
        delta_t_signal = None

    if template_approx not in fd_approxs or template_approx == "SEOBNRv4":
        template_domain = "TD"
        delta_t_template = 1.0 / 8192
        delta_f_template = None

    else:
        template_domain = "FD"
        delta_f_template = 0.125
        delta_t_template = None

    
    delta_f_template = args.delta_f_template

    approx_type = get_approximant_type(template_approx)
    LAL_params_signal = None
    LAL_params_template = None

    """
    if template_approx == "IMRPhenomXEv1":
        LAL_params_template = lal.CreateDict()

        Nharmonic = parameters["Nharmonic"]
        kAdvance = parameters["kAdvance"]
        lalsim.SimInspiralWaveformParamsInsertPhenomXENHarmonics(LAL_params_template,Nharmonic)
        lalsim.SimInspiralWaveformParamsInsertPhenomXEPeriastronAdvance(LAL_params_template, kAdvance)
    """

    # Read LVCNR format file
    if NR_file:
        fp = h5py.File(NR_file, "r")
        freq_1M = fp.attrs["f_lower_at_1MSUN"]
        # Add rounding to prevent some errors in the calculation of the symmetric mass ratio (for TEOBResumSE)
        m1_dim = round(fp.attrs["mass1"], 5)
        m2_dim = round(fp.attrs["mass2"], 5)
        e0_signal = round(fp.attrs["eccentricity"], 4)
        if e0_signal>1. :
            e0_signal =0.1
        e0_signal=0.
        #mean_anomaly_signal = round(fp.attrs["mean_anomaly"], 4)
        mean_anomaly_signal =0.0
        q=m1_dim/m2_dim

        fp.close()
        prefix = os.path.basename(NR_file).split(".")[0]

        LAL_params_signal = lal.CreateDict()
        ma = lalsim.SimInspiralCreateModeArray()

        # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
        if (
            template_approx == "SEOBNRv4"
            or template_approx == "TEOBResumSE"
            or template_approx == "SEOBNREv4"
            or template_approx == "SEOBNRv4E"
            or template_approx == "SEOBNRv4E_opt"
            or template_approx == "IMRPhenomXEv1"
        ):
            lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
            lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
        else:
            for ell in range(2, ellMax + 1):
                lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
        lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
        lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)

        (s1x, s1y, s1z, s2x, s2y, s2z,) = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(0.0, 100, NR_file)
        # Since we are using f_ref=0, total mass does not matter

    # Read orbital eccentricity from NR file
    ModeList=[[2,2]]
    timeNR, tHorizon, hlm, amplm, phaselm, omegalm, omega_orb, phase_orb = compute_h22OmegaOrbfromNRfile(NR_file, ModeList)

    # Estimate eccentricity and averaged orbital frequency from the NR simulation to get the initial guesses
    # for the model

    iphase_orb = InterpolatedUnivariateSpline(tHorizon, phase_orb)

    tH_maxima, omega_orb_maxima, tH_minima, omega_orb_minima = compute_MaxMin(tHorizon, omega_orb)

    tNR_maxima, omega_maxima, tNR_minima, omega_minima =  compute_MaxMin22(timeNR, omegalm[2,2], amplm[2,2])

    iomega_orb_maxima = InterpolatedUnivariateSpline(tH_maxima, omega_orb_maxima)
    iomega_orb_minima = InterpolatedUnivariateSpline(tH_minima, omega_orb_minima)
    omegaOrb_maxima = iomega_orb_maxima(tHorizon)
    omegaOrb_minima = iomega_orb_minima(tHorizon)
    ecc_omegaorb = (np.sqrt(omegaOrb_maxima)-np.sqrt(omegaOrb_minima))/(np.sqrt(omegaOrb_maxima)+np.sqrt(omegaOrb_minima))

    # Compute the averaged orbital frequency
    omegaOrb_av =1./(tH_maxima[1:]-tH_maxima[:-1])*(iphase_orb(tH_maxima[1:])-iphase_orb(tH_maxima[:-1]))

    if ecc_omegaorb[0]>1:
        ecc_omegaorb[0]=0.2
    elif ecc_omegaorb[0]<0:
        ecc_omegaorb[0]=0.01
    elif np.iscomplex(ecc_omegaorb[0]):
        ecc_omegaorb[0]=np.abs(ecc_omegaorb[0])
    else:
        ecc_omegaorb[0]=ecc_omegaorb[0]


    # Just in case the waveform is noisy
    i=0
    omega22Max = omega_maxima[i]
    while omega22Max < omegaOrb_av[0]*2 :
        omega22Max = omega_maxima[i]
        i+=1

    omega_metadata = freq_1M*2*np.pi*lal.MTSUN_SI


    # Compute the minimum frequency for overlap calculations
    freq_1M_new = 1.2* omega22Max/(2*np.pi)/lal.MTSUN_SI # 1.2 factor to avoid condition
    debug=False

    Mtotals = np.arange(20, 220, 20)
    # Mtotals = [64.0]
    dist = 1e6 * lal.PC_SI * 500  # Doesn't matter what this is
    res = []
    # First set the parameters using a feducial total mass
    M_fed = 20.0
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    fmin_start = freq_1M_new/ (m1 + m2)
    f_ref = freq_1M / (m1 + m2)
    f_min = 1 * f_ref

    LAL_params_signal = None
    LAL_params_template = None

    params_signal = waveform_params(
        m1,
        m2,
        s1x,
        s1y,
        s1z,
        s2x,
        s2y,
        s2z,
        iota_s,
        phi_s,
        f_ref,
        f_min,
        dist,
        delta_t_signal,
        delta_f_signal,
        LAL_params_signal,
        signal_approx,
        signal_domain,
        e0_signal,
        mean_anomaly_signal,
    )

    if approx_type == "aligned":
        if np.abs(s1x) < 1e-3:
            s1x = 0
        if np.abs(s1y) < 1e-3:
            s1y = 0
        if np.abs(s2x) < 1e-3:
            s2x = 0
        if np.abs(s2y) < 1e-3:
            s2y = 0

    if f_ref < 10.0:
        f_ref = 10.0 # Define eccentricity at 10Hz

    params_template = waveform_params(
                        m1,
                        m2,
                        0.,
                        0.,
                        s1z,
                        0.,
                        0.,
                        s2z,
                        iota_s,
                        phi_s,
                        f_ref,
                        f_min,
                        dist,
                        delta_t_template,
                        delta_f_template,
                        LAL_params_template,
                        template_approx,
                        template_domain,
                        e0_signal,
                        mean_anomaly_signal,
                    )

    modes_dc=None

    # Put the name of the NR file in a nicer format
    basename=os.path.basename(NR_file)
    tag = 'varykN'
    simName = basename.split('.h5')[0]+'_'+tag


    mm_totalMass = Parallel(n_jobs=16, verbose=50,  backend="multiprocessing"
              )(
        delayed(mm_fixedtotalMass_varykN)(
            params_signal,
            params_template,
            Mtotals[i],
            m1_dim,
            m2_dim,
            freq_1M,
            freq_1M_new,
            fhigh,
            modes_dc,
            debug,
            template_approx,
            NR_file,
            ellMax,
            kappa_s
            )
        for i in range(len(Mtotals))
        )



    mm_e0 = [ii[0] for ii in mm_totalMass]
    mm_ecc_k1_N7 = [ii[1] for ii in mm_totalMass]
    mm_ecc_k0_N7 = [ii[2] for ii in mm_totalMass]
    mm_ecc_k1_N2 = [ii[3] for ii in mm_totalMass]
    mm_ecc_k0_N2 = [ii[4] for ii in mm_totalMass]
    emin_opt_k1_N7 = [ii[5] for ii in mm_totalMass]
    emin_opt_k0_N7 = [ii[6] for ii in mm_totalMass]
    emin_opt_k1_N2 = [ii[7] for ii in mm_totalMass]
    emin_opt_k0_N2 = [ii[8] for ii in mm_totalMass]
    lmin_opt_k1_N7 = [ii[9] for ii in mm_totalMass]
    lmin_opt_k0_N7 = [ii[10] for ii in mm_totalMass]
    lmin_opt_k1_N2 = [ii[11] for ii in mm_totalMass]
    lmin_opt_k0_N2 = [ii[12] for ii in mm_totalMass]
    SNR_total_mass = [ii[13] for ii in mm_totalMass]


    res={}
    res['NR_file'] = simName
    res['q'] = q
    res['s1z'] = s1z
    res['s2z'] = s2z
    res['eorb_NR'] = ecc_omegaorb[0]
    res['freq_1M_NR'] = freq_1M
    res['freq_1M_match'] = freq_1M_new

    # Store optimal values
    res['mm_e0'] = mm_e0
    res['mm_ecc_k1_N7'] = mm_ecc_k1_N7
    res['mm_ecc_k0_N7'] = mm_ecc_k0_N7
    res['mm_ecc_k1_N2'] = mm_ecc_k1_N2
    res['mm_ecc_k0_N2'] = mm_ecc_k0_N2

    res['Mtotal_list'] = Mtotals.tolist()
    res['emin_opt_k1_N7'] = emin_opt_k1_N7
    res['emin_opt_k0_N7'] = emin_opt_k0_N7
    res['emin_opt_k1_N2'] = emin_opt_k1_N2
    res['emin_opt_k0_N2'] = emin_opt_k0_N2

    res['lmin_opt_k1_N7'] = lmin_opt_k1_N7
    res['lmin_opt_k0_N7'] = lmin_opt_k0_N7
    res['lmin_opt_k1_N2'] = lmin_opt_k1_N2
    res['lmin_opt_k0_N2'] = lmin_opt_k0_N2

    #res['phi_min_opt'] = phi_min_total_mass

    res['SNR_total_mass'] = SNR_total_mass
    res['kappa_s'] = kappa_s
    res['phi_s'] = phi_s
    res['iota_s'] = iota_s

    #res['phase_offset'] = phase_offset_total_mass
    #res['time_shift'] = time_shift_total_mass


    outfile=outdir+"{}_emismatches__iota{:.3f}_result.json".format(simName,iota_s)

    with open(outfile, 'w') as f:
        json.dump(res, f)



if __name__ == "__main__":
    main()
