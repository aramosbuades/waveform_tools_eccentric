#!/usr/bin/env/python3
import argparse
import numpy as np
import sys, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])
from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from waveform_tools.mismatch.unfaithfulness_ecc import run_Eccunfaithfulness
from joblib import delayed, Parallel


def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=0
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="For Phenom waveforms whether to minimize over reference frequency or rigid rotation of the spins",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )


    p.add_argument(
        "--raw_grid",
        type=str,
        help="Make a coarse grid unfaithfulness calculations before numerical optimization.",
        default="False",
    )

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=18,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )


    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=0,
    )


    p.add_argument(
        "--EccPNRRForm_template",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=0,
    )

    p.add_argument(
        "--EccPNWfForm_template",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=0,
    )


    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )


    p.add_argument(
        "--Nharmonic_template",
        type=int,
        help="Nharmonic_template only for IMRPhenomXEv1",
        default=7,
    )

    p.add_argument(
        "--kAdvance_template",
        type=int,
        help="kAdvance_template only for IMRPhenomXEv1",
        default=1,
    )


##################################################

    args = p.parse_args()

    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()


    parameters = {}

    if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt" or args.approximant == "SEOBNRv4EHM_opt":
        parameters["EccFphiPNorder"] = args.EccFphiPNorder_template
        parameters["EccFrPNorder"] = args.EccFrPNorder_template
        parameters["EccWaveformPNorder"] = args.EccWaveformPNorder_template
        parameters["EccPNFactorizedForm"] = args.EccPNFactorizedForm_template
        parameters["EccBeta"] = args.EccBeta_template
        parameters["Ecct0"] = args.Ecct0_template
        parameters["EccNQCWaveform"] = args.EccNQCWaveform_template

        parameters["EccPNRRForm"] = args.EccPNRRForm_template
        parameters["EccPNWfForm"] = args.EccPNWfForm_template

        parameters["EcctAppend"] = args.EcctAppend
        parameters["EccAvNQCWaveform"] = args.EccAvNQCWaveform

    elif args.approximant == "IMRPhenomXEv1":

        parameters["Nharmonic_template"] = args.Nharmonic_template
        parameters["kAdvance_template"] = args.kAdvance_template


    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(run_Eccunfaithfulness)(
            y[i],
            x[i],
            args.iota_s,
            i,
            NR_file=args.NR_file,
            parameters=parameters,
            ellMax=args.ell_max,
            signal_approx=args.signal,
            template_approx=args.approximant,
            fhigh=2048.0,
            minimization_type=args.min_type,
            unfaithfulness_type=args.unfaithfulness_type,
            raw_grid = args.raw_grid,
        )
        for i in range(len(x))
    )


if __name__ == "__main__":
    main()
