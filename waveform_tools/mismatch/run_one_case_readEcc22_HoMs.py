#!/usr/bin/env/python3
import argparse
import numpy as np
import sys, os

sys.path.append(os.environ['WAVEFORM_TOOLS_PATH'])
from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from waveform_tools.mismatch.unfaithfulness_ecc import run_Eccunfaithfulness
from joblib import delayed, Parallel


def run_EccHoMs_script(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    run_dir: str ,
    index: int,
    NR_file: str = None,
    signal_approx: str = "NR_hdf5",
    template_approx: str = "IMRPhenomPv3HM",
    parameters: dict = None,
    ellMax: int = 4,
    Ne: int = 100,
    Nf: int = 200,
    deltaEcc: float=0.1,
    deltaF: float=10.,
    mismatch_22mode_dir: str = "None"
    ):


    cmd=f'python $WAVEFORM_TOOLS_PATH/run_one_eccMatchHoMs_nestedOpts.py --run_dir {run_dir} --NR_file {NR_file}  --signal {signal_approx} --approximant {template_approx} \
    --ell_max {ellMax} --EccFphiPNorder_template  {parameters["EccFphiPNorder"]} --EccFrPNorder_template  {parameters["EccFrPNorder"]} --EccWaveformPNorder_template  {parameters["EccWaveformPNorder"]} \
    --EccPNFactorizedForm_template {parameters["EccPNFactorizedForm"]} --EccBeta_template {parameters["EccBeta"]} --Ecct0_template {parameters["Ecct0"]}  --EccNQCWaveform_template {parameters["EccNQCWaveform"]} \
    --EccPNRRForm {parameters["EccPNRRForm"]} --EccPNWfForm {parameters["EccPNWfForm"]}  --EcctAppend  {parameters["EcctAppend"]} --EccAvNQCWaveform {parameters["EccAvNQCWaveform"]} \
    --Ne {Ne} --Nf {Nf} --deltaEcc 0.1 --deltaF 10. --iota_s {iota_s} --kappa_s {kappa_s} --phi_s {phi_s} --mismatch_22mode_dir {mismatch_22mode_dir} --index {index} --TEOB_IC {parameters["TEOB_IC"]}'
    #print(cmd)
    os.system(cmd+' 1>out 2>err ')

def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--signal", type=str, help="Type of NR file to use for the signal. It can be 'NR_hdf5', 'NR_custom' or 'NR_v4'.", default="NR_hdf5")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=5)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )


    p.add_argument(
        "--run_dir",
        type=str,
        help="Run directory.",
        default="False",
    )
    p.add_argument("--Ne",type=int,help="Number of points for 1D eccentricity maximization.",default=50)
    p.add_argument("--Nf",type=int,help="Number of points for 1D starting frequency maximization.",default=200)
    p.add_argument("--deltaEcc",type=float,help="Interval of increasing eccentricity range.",default=0.1)
    p.add_argument("--deltaF",type=float,help="Interval of increasing starting frequency range.",default=10.)

    p.add_argument(
        "--mismatch_22mode_dir",
        type=str,
        help="Directory where the json file of the mismatches for the (2,2)-mode live.",
        default="False",
    )

### Flags only useful when SEOBNRv4E is the template
    p.add_argument(
        "--EccFphiPNorder_template",
        type=int,
        help="EccFphiPNorder_template only for SEOBNRv4E",
        default=99,
    )

    p.add_argument(
        "--EccFrPNorder_template",
        type=int,
        help="EccFrPNorder_template only for SEOBNRv4E",
        default=99,
    )

    p.add_argument(
        "--EccWaveformPNorder_template",
        type=int,
        help="EccWaveformPNorder_template only for SEOBNRv4E",
        default=16,
    )

    p.add_argument(
        "--EccPNFactorizedForm_template",
        type=int,
        help="EccPNFactorizedForm_template only for SEOBNRv4E",
        default=1,
    )


    p.add_argument(
        "--EccBeta_template",
        type=float,
        help="EccBeta_template only for SEOBNRv4E",
        default=0.06,
    )

    p.add_argument(
        "--Ecct0_template",
        type=float,
        help="Ecct0_template only for SEOBNRv4E",
        default=-100.0,
    )

    p.add_argument(
        "--EccNQCWaveform_template",
        type=int,
        help="EccNQCWaveform_template only for SEOBNRv4E",
        default=1,
    )


    p.add_argument(
        "--EccPNRRForm_template",
        type=int,
        help="EccPNRRForm only for SEOBNRv4E",
        default=1,
    )

    p.add_argument(
        "--EccPNWfForm_template",
        type=int,
        help="EccPNWfForm only for SEOBNRv4E",
        default=1,
    )


    p.add_argument(
        "--EcctAppend",
        type=float,
        help="EcctAppend only for SEOBNRv4E_opt",
        default=40.0,
    )

    p.add_argument(
        "--EccAvNQCWaveform",
        type=int,
        help="EccAvNQCWaveform only for SEOBNRv4E_opt",
        default=1,
    )


    p.add_argument(
        "--TEOB_IC",
        type=int,
        help="TEOB initial conditions: #Use periastron (0), average (1) or apastron (2) frequency for initial condition computation",
        default=0,
    )



##################################################

    args = p.parse_args()

    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()


    parameters = {}

#    if args.approximant == "SEOBNRv4E" or args.approximant == "SEOBNRv4E_opt" or args.approximant == "SEOBNRv4EHM_opt":
    parameters["EccFphiPNorder"] = args.EccFphiPNorder_template
    parameters["EccFrPNorder"] = args.EccFrPNorder_template
    parameters["EccWaveformPNorder"] = args.EccWaveformPNorder_template
    parameters["EccPNFactorizedForm"] = args.EccPNFactorizedForm_template
    parameters["EccBeta"] = args.EccBeta_template
    parameters["Ecct0"] = args.Ecct0_template
    parameters["EccNQCWaveform"] = args.EccNQCWaveform_template

    parameters["EccPNRRForm"] = args.EccPNRRForm_template
    parameters["EccPNWfForm"] = args.EccPNWfForm_template

    parameters["EcctAppend"] = args.EcctAppend
    parameters["EccAvNQCWaveform"] = args.EccAvNQCWaveform
    parameters["TEOB_IC"] = args.TEOB_IC

    """Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(run_EccHoMs_script)(
            y[i],
            x[i],
            args.iota_s,
            args.run_dir,
            NR_file=args.NR_file,
            signal_approx=args.signal,
            template_approx=args.approximant,
            parameters=parameters,
            ellMax=args.ell_max,
            Ne=args.Ne,
            Nf=args.Nf,
            deltaEcc=args.deltaEcc,
            deltaF=args.deltaF,
            mismatch_22mode_dir = args.mismatch_22mode_dir,
        )
        for i in range(len(x))
    )
    """
    # As the inner code is already parallelized with joblib, here we do a normal loop
    for i in range(len(x)):
        run_EccHoMs_script(
            y[i],
            x[i],
            args.iota_s,
            args.run_dir,
            i,
            NR_file=args.NR_file,
            signal_approx=args.signal,
            template_approx=args.approximant,
            parameters=parameters,
            ellMax=args.ell_max,
            Ne=args.Ne,
            Nf=args.Nf,
            deltaEcc=args.deltaEcc,
            deltaF=args.deltaF,
            mismatch_22mode_dir = args.mismatch_22mode_dir,
        )


if __name__ == "__main__":
    main()
