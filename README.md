# Introduction
This repo contains various tools useful when working with waveforms, e.g. computation of generic matches.

This package requires `Python` version `3.7` or greater.

## Examples
To compute the mismatch between an NR waveform (in LVCNR format) and an approximant use:

```bash
python ${waveform_tools_home}/mismatch/run_one_case.py --NR_file /path/to/LVCannex/SXS_BBH_1225_Res5.h5 --approximant IMRPhenomXPHM --ell_max 4
```
Run `run_one_case.py` for more details



To compute mismatch between approximants use

```bash
python ${waveform_tools_home}/mismatch/compare_wfs.py --signal IMRPhenomPv3HM --template IMRPhenomXPHM --ell_max 4 --q 2.0 --chi1=0.4,0.3,0.2 --chi2=0.5,0.1,-0.1 --omega_min 0.0170
```

Run `compare_wfs.py -h` for more details


See also the tests directory for a more low-level example of how unfaithfulness can be called.